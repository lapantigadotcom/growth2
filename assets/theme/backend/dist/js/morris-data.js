/*Morris Init*/
$(function() {
	"use strict";
	
    if($('#morris_area_chart').length > 0)
		// Area Chart
		Morris.Area({
			element: 'morris_area_chart',
			data: [{
				period: '2010 Q1',
				iphone: 2666,
				ipad: null,
				itouch: 2647
			}, {
				period: '2010 Q2',
				iphone: 2778,
				ipad: 2294,
				itouch: 2441
			}, {
				period: '2010 Q3',	
				iphone: 4912,
				ipad: 1969,
				itouch: 2501
			}, {
				period: '2010 Q4',
				iphone: 3767,
				ipad: 3597,
				itouch: 5689
			}, {
				period: '2011 Q1',
				iphone: 6810,
				ipad: 1914,
				itouch: 2293
			}, {
				period: '2011 Q2',
				iphone: 5670,
				ipad: 4293,
				itouch: 1881
			}, {
				period: '2011 Q3',
				iphone: 4820,
				ipad: 3795,
				itouch: 1588
			}, {
				period: '2011 Q4',
				iphone: 15073,
				ipad: 5967,
				itouch: 5175
			}, {
				period: '2012 Q1',
				iphone: 10687,
				ipad: 4460,
				itouch: 2028
			}, {
				period: '2012 Q2',
				iphone: 8432,
				ipad: 5713,
				itouch: 1791
			}],
			xkey: 'period',
			ykeys: ['iphone', 'ipad', 'itouch'],
			labels: ['iPhone', 'iPad', 'iPod Touch'],
			pointSize: 0,
			pointStrokeColors:['#177ec1', '#dc4666', '#e69a2a'],
			behaveLikeLine: true,
			gridLineColor: '#878787',
			lineWidth: 0,
			smooth: true,
			hideHover: 'auto',
			lineColors: ['#177ec1', '#dc4666', '#e69a2a'],
			resize: true,
			gridTextColor:'#878787',
			gridTextFamily:"Roboto",
		});

    if($('#morris_donut_chart').length > 0) {
		// Donut Chart
		Morris.Donut({
			element: 'morris_donut_chart',
			data: [{
				label: "Download Sales",
				value: 12
			}, {
				label: "In-Store Sales",
				value: 30
			}, {
				label: "Mail-Order Sales",
				value: 20
			}],
			colors: ['#177ec1', '#dc4666', '#e69a2a'],
			resize: true,
			labelColor: '#878787',
		});
		$("div svg text").attr("style","font-family: Roboto").attr("font-weight","400");
	}	

    if($('#morris_line_chart').length > 0)
		// Line Chart
		Morris.Line({
			// ID of the element in which to draw the chart.
			element: 'morris_line_chart',
			// Chart data records -- each entry in this array corresponds to a point on
			// the chart.
			data: [{
				d: '2018-10-01',
				Sales: 1022
			}, {
				d: '2018-10-02',
				Sales: 783
			}, {
				d: '2018-10-03',
				Sales: 820
			}, {
				d: '2018-10-04',
				Sales: 839
			}, {
				d: '2018-10-05',
				Sales: 792
			}, {
				d: '2018-10-06',
				Sales: 859
			}, {
				d: '2018-10-07',
				Sales: 2790
			}, {
				d: '2018-10-08',
				Sales: 1680
			}, {
				d: '2018-10-09',
				Sales: 1592
			}, {
				d: '2018-10-10',
				Sales: 1420
			}, {
				d: '2018-10-11',
				Sales: 882
			}, {
				d: '2018-10-12',
				Sales: 3004
			}, ],
			// The name of the data record attribute that contains x-Saless.
			xkey: 'd',
			// A list of names of data record attributes that contain y-visitss.
			ykeys: ['Sales'],
			// Labels for the ykeys -- will be displayed when you hover over the
			// chart.
			labels: ['Order'],
			// Disables line smoothing
			pointSize: 1,
			pointStrokeColors:['#177ec1'],
			behaveLikeLine: true,
			gridLineColor: '#878787',
			gridTextColor:'#878787',
			lineWidth: 2,
			smooth: true,
			hideHover: 'false',
			lineColors: ['#177ec1'],
			resize: true,
			gridTextFamily:"Roboto"
		});

	if($('#morris_bar_chart').length > 0)
	   // Bar Chart
		Morris.Bar({
			element: 'morris_bar_chart',
			data: [{
				device: 'iPhone',
				geekbench: 136
			}, {
				device: 'iPhone 3G',
				geekbench: 137
			}, {
				device: 'iPhone 3GS',
				geekbench: 275
			}, {
				device: 'iPhone 4',
				geekbench: 380
			}, {
				device: 'iPhone 4S',
				geekbench: 655
			}, {
				device: 'iPhone 5',
				geekbench: 1571
			}],
			xkey: 'device',
			ykeys: ['geekbench'],
			labels: ['Geekbench'],
			barRatio: 0.4,
			xLabelAngle: 35,
			pointSize: 1,
			pointStrokeColors:['#e69a2a'],
			behaveLikeLine: true,
			gridLineColor: '#878787',
			gridTextColor:'#878787',
			hideHover: 'auto',
			barColors: ['#e69a2a'],
			resize: true,
			gridTextFamily:"Roboto"
		});
	
	if($('#morris_extra_line_chart').length > 0)
		Morris.Line({
        element: 'morris_extra_line_chart',
        data: [{
            period: '2010',
            iphone: 50,
            ipad: 80,
            itouch: 20
        }, {
            period: '2011',
            iphone: 130,
            ipad: 100,
            itouch: 80
        }, {
            period: '2012',
            iphone: 80,
            ipad: 60,
            itouch: 70
        }, {
            period: '2013',
            iphone: 70,
            ipad: 200,
            itouch: 140
        }, {
            period: '2014',
            iphone: 180,
            ipad: 150,
            itouch: 140
        }, {
            period: '2015',
            iphone: 105,
            ipad: 100,
            itouch: 80
        },
         {
            period: '2016',
            iphone: 250,
            ipad: 150,
            itouch: 200
        }],
        xkey: 'period',
        ykeys: ['iphone', 'ipad', 'itouch'],
        labels: ['iPhone', 'iPad', 'iPod Touch'],
        pointSize: 2,
        fillOpacity: 0,
		lineWidth:2,
		pointStrokeColors:['#177ec1', '#dc4666', '#e69a2a'],
		behaveLikeLine: true,
		gridLineColor: '#878787',
		hideHover: 'auto',
		lineColors: ['#177ec1', '#dc4666', '#e69a2a'],
		resize: true,
		gridTextColor:'#878787',
		gridTextFamily:"Roboto"
        
    });
	
	if($('#morris_extra_bar_chart').length > 0)
		// Morris bar chart
		Morris.Bar({
			element: 'morris_extra_bar_chart',
			data: [{
				y: '2006',
				a: 100,
				b: 90,
				c: 60
			}, {
				y: '2007',
				a: 75,
				b: 65,
				c: 40
			}, {
				y: '2008',
				a: 50,
				b: 40,
				c: 30
			}, {
				y: '2009',
				a: 75,
				b: 65,
				c: 40
			}, {
				y: '2010',
				a: 50,
				b: 40,
				c: 30
			}, {
				y: '2011',
				a: 75,
				b: 65,
				c: 40
			}, {
				y: '2012',
				a: 100,
				b: 90,
				c: 40
			}],
			xkey: 'y',
			ykeys: ['a', 'b', 'c'],
			labels: ['A', 'B', 'C'],
			barColors:['#177ec1', '#dc4666', '#e69a2a'],
			hideHover: 'auto',
			gridLineColor: '#878787',
			resize: true,
			gridTextColor:'#878787',
			gridTextFamily:"Roboto"
		});

});
