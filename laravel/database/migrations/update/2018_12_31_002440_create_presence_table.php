<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresenceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presence', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('kd_user');
            $table->foreign('kd_user')->references('id')->on('user');
            $table->integer('kd_office');
            $table->foreign('kd_office')->references('id')->on('office');
            $table->timestamp('date');
            $table->timestamp('date_checkin_presence');
            $table->timestamp('date_checkout_presence');
            $table->timestamp('date_checkin_visit');
            $table->timestamp('date_checkout_visit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presence');
    }
}
