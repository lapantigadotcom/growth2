<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('office', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kd_office',10);
            $table->integer('kd_area');
            $table->foreign('kd_area')->references('id')->on('area');
            $table->integer('kd_user');
            $table->foreign('kd_user')->references('id')->on('user');
            $table->string('nm_office',100);
            $table->string('almt_office',255);
            $table->string('latitude',255);
            $table->string('longitude',255);
            $table->smallInteger('status_office');
            $table->string('foto_office',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('office');
    }
}
