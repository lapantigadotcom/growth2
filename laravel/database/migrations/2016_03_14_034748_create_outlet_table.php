<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutletTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outlet', function (Blueprint $table) {
            $table->increments('kd_outlet');
            $table->primary('kd_outlet');
            $table->string('kd_dist',50);
            $table->foreign('kd_dist')->references('kd_dist')->on('distributor');
            $table->string('kd_area',10);
            $table->foreign('kd_area')->references('kd_area')->on('area');
            $table->integer('kd_user');
            $table->foreign('kd_user')->references('kd_user')->on('user');
            $table->string('nm_outlet',100);
            $table->string('almt_outlet',255);
            $table->string('kt_outlet',50);
            $table->string('tipe_outlet',50);
            $table->string('rank_outlet',1);
            $table->string('telp_outlet',50);
            $table->string('reg_status',5);
            $table->string('latitude',255);
            $table->string('longitude',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('outlet');
    }
}
