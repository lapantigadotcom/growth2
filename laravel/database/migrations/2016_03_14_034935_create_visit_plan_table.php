<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visit_plan', function (Blueprint $table) {
            $table->increments('kd_visitplan');
            $table->primary('kd_order');
            $table->integer('kd_outlet');
            $table->foreign('kd_outlet')->references('kd_outlet')->on('outlet');
            $table->timestamp('date_visit');
            $table->timestamp('date_create_visit');
            $table->smallInteger('approve_visit');
            $table->smallInteger('status_visit');
            $table->timestamp('date_visiting');
            $table->string('skip_order_reason',100)->nullable();
            $table->string('skip_reason',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visit_plan');
    }
}
