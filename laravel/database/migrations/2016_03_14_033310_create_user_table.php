<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('kd_user');
            $table->smallInteger('kd_role');
            $table->foreign('kd_role')->references('kd_role')->on('role');
            $table->string('NIK',20);
            $table->string('nama',255);
            $table->string('alamat',255);
            $table->string('kota',20);
            $table->string('telepon',20);
            $table->string('foto',255)->nullable();
            $table->string('username',50);
            $table->string('password',255);
            $table->string('email_user',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user');
    }
}
