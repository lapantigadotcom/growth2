<?php

Route::get('/', function () {
return Redirect::to('/admin');
});

//Login Admin
Route::get('/admin',array('as' => 'admin.login','uses' => 'AdminController@getLogin'));
Route::post('/admin', array('as' => 'admin.auth.login','uses' => 'AdminController@postLogin'));

//Login Manager
//Route::get('/manager',array('as' => 'manager.login','uses' => 'ManagerController@getLogin'));
//Route::post('/manager', array('as' => 'manager.auth.login','uses' => 'ManagerController@postLogin'));

//Reset Password
Route::get('/resetpassword',array('as' => 'admin.autentikasi','uses' => 'AdminController@autentikasi'));
Route::post('/resetpassword/auth',array('as' => 'admin.reset.password','uses' => 'AdminController@sendEmailReminder'));

//REST API
Route::group(array('prefix'=>'api'),function(){
  Route::resource('webService','WebServiceController',array('except'=>array('create','edit')));
});

//Register
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

//AJAX
Route::get('/ajax-call', function(){
  $id = Input::get('kd_outlet');
  $user = \App\Outlet::where('kd_outlet', '=', $id)
  ->join('user','outlet.kd_user','=','user.id')
  ->get();
  return Response::json($user);
});

Route::get('/ajax-city', function(){
  $id = Input::get('id');
  $city = \App\Kota::where('kd_area','=',$id)
  ->get();
  return Response::json($city);
});

Route::get('/ajax-area', function(){
  $id = Input::get('id');
  $area = \App\Area::where('id','=',$id)
  ->get();
  return Response::json($area);
});

Route::get('/ajax-sales', function(){
  $id = Input::get('id');
  $area = \App\User::where('kd_area','=',$id)
  ->where('kd_role','=','3')
  ->get();
  return Response::json($area);
});

// Route::group(['middlewareGroups' => ['web']], function () {
  Route::group(['middleware' => ['auth','admin','acl']], function()
  {
    // for debugging
    Route::get('outlet', array('as'=> 'outlet','uses' => 'MasterController@checking'));

    //Dashboard
    Route::get('/admin/dashboard', array('as' => 'admin.dashboard','uses' =>'AdminController@getDashboard'));
    Route::post('/admin/dashboard', array('as' => 'admin.dashboard','uses' =>'AdminController@getDashboard'));

    //ChangesPassword
    Route::get('/admin/changepassword',array('as' => 'admin.getChangePassword', 'uses' => 'AdminController@getChangePassword'));
    Route::post('/admin/changepassword',array('as' => 'admin.postChangePassword', 'uses' => 'AdminController@postChangePassword'));

    //Logout
    Route::get('admin/logout',array('as' => 'admin.logout','uses' =>'AdminController@getLogout'));

    //Sales Force
    Route::get('/admin/sales/{id}/delete',array('as'=> 'admin.sales.delete','uses' => 'SalesController@destroy'));
    Route::resource('/admin/sales', 'SalesController', ['as' => 'admin'] );

    //News
    Route::get('/admin/news/{id}/delete',array('as'=> 'admin.news.delete','uses' => 'NewsController@destroy'));
    Route::resource('/admin/news', 'NewsController', ['as' => "admin"] );

    //Banner
    Route::resource('/admin/banner', 'BannerController', ['as' => 'admin']);
    Route::get('/admin/banner/{id}/delete',array('as'=> 'admin.banner.delete','uses' => 'BannerController@destroy'));
    Route::get('/admin/banner/{id}/up',array('as'=> 'admin.banner.up','uses' => 'BannerController@up'));

    //User
    Route::get('/admin/user/{id}/delete',array('as'=> 'admin.user.delete','uses' => 'UserController@destroy'));
    Route::resource('/admin/user', 'UserController', ['as' => 'admin']);

    Route::get('/admin/master', array('as' => 'admin.master', 'uses' => 'MasterController@getMaster'));

    //Area
    Route::get('/admin/divisi/{id}/delete',array('as'=> 'admin.divisi.delete','uses' => 'AreaController@destroy'));
    Route::resource('/admin/divisi', 'AreaController', ['as' => 'admin']);

    //Kota
    Route::get('/admin/kota/{id}/delete',array('as'=> 'admin.kota.delete','uses' => 'KotaController@destroy'));
    Route::resource('/admin/kota', 'KotaController', ['as' => 'admin']);

    //Produk
    Route::get('/admin/produk/history/all', array('as'=> 'admin.produk.historyIndex','uses' => 'ProdukController@historyIndex'));
    Route::get('/admin/produk/history/{id}', array('as'=> 'admin.produk.history','uses' => 'ProdukController@ahistory'));

    Route::get('/admin/produk/{id}/delete',array('as'=> 'admin.produk.delete','uses' => 'ProdukController@destroy'));
    Route::resource('/admin/produk', 'ProdukController', ['as' => 'admin']);

    //Distributor
    Route::get('/admin/distributor/{id}/delete',array('as'=> 'admin.distributor.delete','uses' => 'DistributorController@destroy'));
    Route::resource('/admin/distributor', 'DistributorController', ['as' => 'admin']);

    Route::post('/ajax/outlet', array('as'=> 'admin.outlet.ajax','uses' => 'OutletController@ajax'));

    //Map
    Route::get('/admin/maps/view', array('as' => 'admin.outlet.mapform','uses' => 'OutletController@formMap'));

    //Map API
    Route::get('/admin/maps', array('as' => 'admin.outlet.maps','uses' => 'OutletController@maps'));


    //Toleransi Koordinat
    Route::get('admin/outletManage', array('as' => 'admin.outletManage.index','uses' => 'OutletController@formToleransi'));
    Route::post('admin/outletManage', array('as' => 'admin.outletManage.updateToleransi','uses' => 'OutletController@updateToleransi'));
    Route::post('admin/updateTvisit', array('as' => 'admin.outletManage.updateTvisit','uses' => 'OutletController@updateTvisit'));
    Route::post('admin/updateTec', array('as' => 'admin.outletManage.updateTec','uses' => 'OutletController@updateTec'));

    //Tipe Outlet
    Route::get('/admin/tipe/{id}/delete',array('as'=> 'admin.tipe.delete','uses' => 'TipeOutletController@destroy'));
    Route::resource('/admin/tipe', 'TipeOutletController', ['as' => 'admin']);

    //Visit Plan Get data
    Route::get('admin/visitPlan', array('as' => 'admin.visitPlan.form', 'uses' => 'VisitPlanController@formVisit'));
    Route::post('admin/visitPlan', array('as' => 'admin.visitPlan.result','uses' => 'VisitPlanController@index'));

    //Visit Bukti
     Route::get('/admin/visitbukti', array('as' => 'admin.visitbukti.form','uses' =>'VisitBuktiController@formbukti'));
     Route::post('admin/visitbukti', array('as' => 'admin.visitbukti.result','uses' => 'VisitBuktiController@index'));
      Route::get('admin/download/visitbukti',array('as' => 'admin.download.visitbukti','uses' =>'DownloadController@exportVisitBukti'));
     Route::get('/admin/visitbukti/{id}/delete',array('as'=> 'admin.visitbukti.delete','uses' => 'VisitBuktiController@destroy'));

     //Billing
    //Visit Monitor Get data
    Route::get('admin/billing', array('as' => 'admin.billing.form', 'uses' => 'BillingController@formBilling'));
    Route::post('admin/billing', array('as' => 'admin.billing.result','uses' => 'BillingController@index'));
     Route::get('/admin/billing/{id}/delete',array('as'=> 'admin.billing.delete','uses' => 'BillingController@destroy'));

      //Satuan
    Route::get('/admin/satuan/{id}/delete',array('as'=> 'admin.satuan.delete','uses' => 'SatuanController@destroy'));
    Route::resource('/admin/satuan', 'SatuanController', ['as' => "admin"] );

      //brand
    Route::get('/admin/brand/{id}/delete',array('as'=> 'admin.brand.delete','uses' => 'BrandController@destroy'));
    Route::resource('/admin/brand', 'BrandController', ['as' => "admin"] );

    //Approval Checklist
    Route::get('/admin/visit/approved',array('as'=> 'admin.visit.editApprove','uses' => 'VisitPlanController@editApprove'));
    Route::post('/admin/visit/approved',array('as'=> 'admin.visit.approved','uses' => 'VisitPlanController@approvedVisit'));
    Route::get('/admin/visit/approvedHO',array('as'=> 'admin.visit.approvedHO','uses' => 'VisitPlanController@editApproveHO'));

    //Visit Monitor Get data
    Route::get('admin/visitingMonitor', array('as' => 'admin.visitingMonitor.form', 'uses' => 'VisitMonitorController@formVisit'));
    Route::post('admin/visitingMonitor', array('as' => 'admin.visitingMonitor.result','uses' => 'VisitMonitorController@index'));

    //Visit Plan
    Route::get('/admin/visit/{id}/delete',array('as'=> 'admin.visit.delete','uses' => 'VisitPlanController@destroy'));
    Route::resource('/admin/visit', 'VisitPlanController', ['as' => 'admin']);

    //Visiting Monitor
    Route::get('/admin/visitMonitor/{id}/delete',array('as'=> 'admin.visitMonitor.delete','uses' => 'VisitMonitorController@destroy'));
    Route::resource('/admin/visitMonitor', 'VisitMonitorController', ['as' => 'admin']);

    //Visit Problem
    Route::resource('/admin/visitProblem', 'VisitProblemController');
    Route::get('/admin/visitProblem/{id}/delete',array('as'=> 'admin.visitProblem.delete','uses' => 'VisitProblemController@destroy'));
    Route::post('admin/visitProblem',  array('as' => 'admin.visitProblem.result', 'uses'=>'VisitProblemController@index' ));
    Route::get('admin/visitProblem', array('as'=> 'admin.visitProblem.form', 'uses'=> 'VisitProblemController@form'));

    //Approval VisitProblem
    Route::get('/admin/visitProblem/approving/yet',array('as'=> 'admin.visitProblem.editApprove','uses' => 'VisitProblemController@formapproval'));
    Route::post('/admin/visitProblem/approving',array('as'=> 'admin.visitProblem.approved','uses' => 'VisitProblemController@approvedProblem'));

    //Outlet
    Route::resource('/admin/outlet', 'OutletController', [ 'as' => 'admin']);
    Route::get('/admin/outlet/{kd_outlet}/delete',array('as'=> 'admin.outlet.delete','uses' => 'OutletController@destroy'));
    // Route::get('admin/outlet', array('as' => 'admin.outlet.index', 'uses'=> 'OutletController@formOutlet'));

   

    //Sampling
    Route::resource('/admin/sampling', 'SamplingController', ['as' => 'admin']);
    Route::get('/admin/sampling/{id}/delete',array('as'=> 'admin.sampling.delete','uses' => 'SamplingController@destroy'));
    Route::get('/admin/sampling', array('as' => 'admin.sampling.form', 'uses'=> 'SamplingController@formSampling'));
    Route::post('/admin/sampling',  array('as' => 'admin.sampling.result', 'uses'=>'SamplingController@index' ));

    //Stok
    Route::resource('/admin/stok', 'StokController', ['as' => 'admin']);
    Route::get('/admin/stok/{id}/delete',array('as'=> 'admin.stok.delete','uses' => 'StokController@destroy'));
    Route::get('admin/stok', array('as'=> 'admin.stok.form', 'uses'=> 'StokController@formStok'));
    Route::post('admin/stok',  array('as' => 'admin.stok.result', 'uses'=>'StokController@index' ));

    //POP
    Route::resource('/admin/pop', 'PopController', ['as' => 'admin']);
    Route::get('/admin/pop/{id}/delete',array('as'=> 'admin.pop.delete','uses' => 'PopController@destroy'));
    Route::get('admin/pop', array('as'=> 'admin.pop.form', 'uses'=> 'PopController@formPop'));
    Route::post('admin/pop',  array('as' => 'admin.pop.result', 'uses'=>'PopController@index' ));

    //Take Order Get data
    Route::get('admin/takeOrder', array('as' => 'admin.takeOrder.form', 'uses' => 'OrderController@formOrder'));
    Route::post('admin/takeOrder', array('as' => 'admin.takeOrder.result','uses' => 'OrderController@index'));

    //TipeActivity
    Route::resource('admin/tipeActivity', 'TipeActivityController', ['as' => 'admin']);
    Route::get('/admin/tipeActivity/{id}/delete',array('as'=> 'admin.tipeActivity.delete','uses' => 'TipeActivityController@destroy'));

    //TipePOP
    Route::resource('/admin/tipePop', 'TipePopController', ['as' => 'admin']);
    Route::get('/admin/tipePop/{id}/delete',array('as'=> 'admin.tipePop.delete','uses' => 'TipePopController@destroy'));

    //Sample
    Route::resource('/admin/sample', 'SampleController', ['as' => 'admin']);
    Route::get('/admin/sample/{id}/delete',array('as'=> 'admin.sample.delete','uses' => 'SampleController@destroy'));

    //Take Order
    Route::get('/admin/order/{id}/delete',array('as'=> 'admin.order.delete','uses' => 'OrderController@destroy'));
    Route::resource('/admin/order', 'OrderController', ['as' => 'admin']);

    //Photo Activity Get data
    Route::get('admin/photoAct', array('as' => 'admin.photoAct.form', 'uses' => 'PhotoActivityController@formPhoto'));
    Route::post('admin/photoAct', array('as' => 'admin.photoAct.result','uses' =>'PhotoActivityController@index'));

    //Photo Display
    Route::get('/admin/photo/{id}/delete',array('as'=> 'admin.photo.delete','uses' => 'PhotoActivityController@destroy'));
    Route::resource('/admin/photo', 'PhotoActivityController', ['as' => 'admin']);

    //Tipe Photo
    Route::get('/admin/tipephoto/{id}/delete',array('as'=> 'admin.tipephoto.delete','uses' => 'TipePhotoController@destroy'));
    Route::resource('/admin/tipephoto', 'TipePhotoController', ['as' => 'admin']);

    //Competitor Activity
    Route::get('/admin/competitor/{id}/delete',array('as'=> 'admin.competitor.delete','uses' => 'CompetitorActivityController@destroy'));
    Route::resource('/admin/competitor', 'CompetitorActivityController', ['as' => 'admin']);
    Route::post('admin/competitor', array('as' => 'admin.competitor.result', 'uses' => 'CompetitorActivityController@index'));
    Route::get('admin/competitor', array('as' => 'admin.competitor.form', 'uses' => 'CompetitorActivityController@form'));

    //Competitor
    Route::get('/admin/comp/{id}/delete',array('as'=> 'admin.comp.delete','uses' => 'CompetitorController@destroy'));
    Route::resource('/admin/comp', 'CompetitorController', ['as' => 'admin']);

    //Report
    Route::get('admin/report/', array('as' => 'admin.report', 'uses' => 'ReportController@getReport'));

    //Report Outlet Visit per sales
    Route::get('admin/report/outletVisit', array('as' => 'admin.report.outletVisit', 'uses' => 'ReportController@outletVisit'));
    Route::post('admin/report/outletVisit', array('as' => 'admin.report.resultReport','uses' => 'ReportController@resultReport'));

    //Report Working Time per sales
    Route::get('admin/report/workingTime', array('as' => 'admin.report.workingTime', 'uses' => 'ReportController@workingTime'));
    Route::post('admin/report/workingTime', array('as' => 'admin.report.resultWorkTime','uses' => 'ReportController@resultWorkTime'));

    //Report Sampling
    Route::get('admin/report/sampling', array('as' => 'admin.report.sampling', 'uses' => 'ReportController@sampling'));
    Route::post('admin/report/sampling', array('as' => 'admin.report.resultSampling','uses' => 'ReportController@resultSampling'));

    //Report POP
    Route::get('admin/report/pop', array('as' => 'admin.report.pop', 'uses' => 'ReportController@pop'));
    Route::post('admin/report/pop', array('as' => 'admin.report.resultPop','uses' => 'ReportController@resultPop'));

    //Report Effective Call per sales
    Route::get('admin/report/effectiveCall', array('as' => 'admin.report.effectiveCall', 'uses' => 'ReportController@effectiveCall'));
    Route::post('admin/report/effectiveCall', array('as' => 'admin.report.resultEffectiveCall', 'uses' => 'ReportController@resultEffectiveCall'));

    //Report Photo Activity per sales
    Route::get('admin/report/photoActivity', array('as' => 'admin.report.photoActivity', 'uses' => 'ReportController@photoActivity'));
    Route::post('admin/report/photoActivity', array('as' => 'admin.report.resultPhotoActivity', 'uses' => 'ReportController@resultPhotoActivity'));
    Route::get('admin/download/reportPhoto',array('as' => 'admin.reportPhoto.download','uses' =>'ReportController@exportAllPhoto'));
    //Report Competitor Photo Activity per sales
    Route::get('admin/report/competitorActivity', array('as' => 'admin.report.competitorActivity', 'uses' => 'ReportController@competitorActivity'));
    Route::post('admin/report/competitorActivity', array('as' => 'admin.report.resultCompetitorActivity', 'uses' => 'ReportController@resultCompetitorActivity'));

    //Report Outlet Visit per sales
    Route::get('admin/report/presence', array('as' => 'admin.report.presence', 'uses' => 'ReportController@presence'));
    Route::post('admin/report/presence', array('as' => 'admin.report.resultPresence','uses' => 'ReportController@resultPresence'));
    
    //Report Business Trip per sales
    Route::get('admin/report/businessTrip', array('as' => 'admin.report.businessTrip', 'uses' => 'ReportController@businessTrip'));
    Route::post('admin/report/businessTrip', array('as' => 'admin.report.resultBusinessTrip','uses' => 'ReportController@resultBusinessTrip'));

    //Route::get('admin/download/reportPhoto',array('as' => 'admin.reportPhoto.download','uses' =>'ReportController@exportAllPhoto'));
    //Configuration
    Route::get('/admin/permission/{id}/delete',array('as'=> 'admin.permission.delete','uses' => 'PermissionController@destroy'));
    Route::resource('/admin/permission', 'PermissionController', ['as' => 'admin']);

    Route::get('/admin/role/{id}/delete',array('as'=> 'admin.role.delete','uses' => 'RoleController@destroy'));
    Route::resource('/admin/role', 'RoleController', ['as' => 'admin']);

    Route::get('/admin/adminmenu/{id}/delete',array('as'=> 'admin.adminmenu.delete','uses' => 'AdminMenuController@destroy'));
    Route::resource('/admin/adminmenu', 'AdminMenuController', ['as' => 'admin']);

    //Download
    Route::get('admin/download',array('as' => 'admin.download','uses' =>'DownloadController@index'));
    Route::get('admin/download/area',array('as' => 'admin.download.area','uses' =>'DownloadController@exportArea'));
    Route::get('admin/download/outlet',array('as' => 'admin.download.outlet','uses' =>'DownloadController@exportOutlet'));
    Route::get('admin/download/distributor',array('as' => 'admin.download.distributor','uses' =>'DownloadController@exportDistributor'));
    Route::get('admin/download/produk',array('as' => 'admin.download.produk','uses' =>'DownloadController@exportProduk'));
    Route::get('admin/download/visitPlan',array('as' => 'admin.download.visitPlan','uses' =>'DownloadController@exportVisitPlan'));
    Route::get('admin/download/visitMonitor',array('as' => 'admin.download.visitMonitor','uses' =>'DownloadController@exportVisitMonitor'));
    Route::get('admin/download/visitProblem',array('as' => 'admin.download.visitProblem','uses' =>'VisitProblemController@downloadVisitProblem'));
    Route::get('admin/download/sampling',array('as' => 'admin.download.sampling','uses' =>'SamplingController@downloadSampling'));

    Route::get('admin/download/stok',array('as' => 'admin.download.stok','uses' =>'StokController@downloadStok'));
    Route::get('admin/download/takeOrder',array('as' => 'admin.download.takeOrder','uses' =>'DownloadController@exportTakeOrder'));
    Route::get('admin/download/salesForce',array('as' => 'admin.download.salesForce','uses' =>'DownloadController@exportSalesForce'));
    Route::get('admin/download/nonSF',array('as' => 'admin.download.nonSF','uses' =>'DownloadController@exportNonSF'));
    Route::get('admin/download/salesDetail/{id}',array('as' => 'admin.download.salesDetail','uses' =>'DownloadController@exportSalesDetail'));
 
    //Upload
    Route::post('admin/upload/outlet',array('as' => 'admin.upload.outlet','uses' =>'DownloadController@uploadOutlet'));
    Route::post('admin/upload/produk',array('as' => 'admin.upload.produk','uses' =>'DownloadController@uploadProduk'));
    Route::post('admin/upload/distributor',array('as' => 'admin.upload.distributor','uses' =>'DownloadController@uploadDistributor'));
    Route::post('admin/upload/area',array('as' => 'admin.upload.area','uses' =>'DownloadController@uploadArea'));
    Route::post('admin/upload/user',array('as' => 'admin.upload.user','uses' =>'DownloadController@uploadUser'));

    //My Profile
    Route::get('admin/profile/{name}',array('as'=> 'admin.profile','uses' => 'UserController@myProfile'));

    //Edit Profile
    Route::get('admin/editprofile/{name}',array('as'=> 'admin.edit.profile','uses' => 'UserController@editProfile'));
    Route::put('admin/editprofile/{name}',array('as'=> 'admin.edited.profile','uses' => 'UserController@updateProfile'));
  });

// });

//Web Service
Route::post('/setOutlet', 'WebServiceController@setOutlet');

// Route::get('/login/{username}/{password}/{fcmID}', 'WebServiceController@getUser');
Route::get('/getFoto/{kd_outlet}', 'WebServiceController@getFoto');
 