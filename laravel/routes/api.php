<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:api');


Route::post('auth/login', 'AuthApiController@login');
Route::get('profile', 'AuthApiController@profile')->middleware('auth:api');
// user terauth
Route::post('getFoto', 'WebServiceController@getFoto')->middleware('auth:api');
Route::post('outlet', 'WebServiceController@getOutlet')->middleware('auth:api');
Route::post('kota', 'WebServiceController@getKota')->middleware('auth:api');
Route::post('distributor', 'WebServiceController@getDistributor')->middleware('auth:api');
Route::post('tpop', 'WebServiceController@getTPop')->middleware('auth:api');
Route::post('visit', 'WebServiceController@getVisit')->middleware('auth:api'); //ga perlu jare
Route::post('visitedToday', 'WebServiceController@getVisitToday')->middleware('auth:api');
Route::post('visitPlanToday', 'WebServiceController@getVisitPlanToday')->middleware('auth:api');
Route::get('tipe', 'WebServiceController@getTipe')->middleware('auth:api');
Route::get('sample', 'WebServiceController@getSample')->middleware('auth:api');
Route::get('produk', 'WebServiceController@getProduk')->middleware('auth:api');
Route::get('tipePhoto', 'WebServiceController@getTipePhoto')->middleware('auth:api');
Route::get('office', 'WebServiceController@getOffice')->middleware('auth:api');
Route::get('presence', 'WebServiceController@getPresence')->middleware('auth:api');
Route::get('businessTrip', 'WebServiceController@getBusinessTrip')->middleware('auth:api');
Route::post('setOutlet', 'WebServiceController@setOutlet')->middleware('auth:api');
Route::post('setVisitBukti', 'WebServiceController@setVisitBukti')->middleware('auth:api');
Route::post('submitBilling', 'WebServiceController@submitBilling')->middleware('auth:api');



Route::get('getTO/{id}', 'WebServiceController@getTO')->middleware('auth:api');
Route::get('getSampling/{id}', 'WebServiceController@getSampling')->middleware('auth:api');
Route::get('getPop/{id}', 'WebServiceController@getPop')->middleware('auth:api');
Route::get('getStok/{id}', 'WebServiceController@getStok')->middleware('auth:api');
Route::get('news/{id}', 'WebServiceController@getDetailNews')->middleware('auth:api');
Route::get('banner', 'WebServiceController@getBanner')->middleware('auth:api');
Route::get('getCompetitor', 'WebServiceController@getCompetitor')->middleware('auth:api');
Route::get('resetPass/{email}/{nik}/{telp}', 'WebServiceController@sendEmailReminder')->middleware('auth:api');
Route::get('getAll/{kd_user}/{kd_area}', 'WebServiceController@getAllData')->middleware('auth:api');
Route::get('newsphoto/{id}', 'WebServiceController@getNewsImage')->middleware('auth:api');

Route::post('setOutlet', 'WebServiceController@setOutlet')->middleware('auth:api');
Route::post('getSatuan', 'WebServiceController@getSatuan')->middleware('auth:api');
Route::post('getBrand', 'WebServiceController@getBrand')->middleware('auth:api');
Route::post('getLogOrder', 'WebServiceController@getLogOrder')->middleware('auth:api');
Route::post('getLogBilling', 'WebServiceController@getLogBilling')->middleware('auth:api');


Route::post('setVisit', 'WebServiceController@setVisit')->middleware('auth:api');
Route::post('setOffice', 'WebServiceController@setOffice')->middleware('auth:api');
Route::post('setPresence', 'WebServiceController@setPresence')->middleware('auth:api');
Route::post('setBusinessTrip', 'WebServiceController@setBusinessTrip')->middleware('auth:api');
Route::post('deleteVisit', 'WebServiceController@deleteVisit')->middleware('auth:api');
Route::post('submitVisit', 'WebServiceController@submitVisit')->middleware('auth:api');
Route::post('submitcheckOut', 'WebServiceController@submitcheckOut')->middleware('auth:api');

Route::post('submitTO', 'WebServiceController@submitTO')->middleware('auth:api'); 
Route::post('submitPop', 'WebServiceController@submitPop')->middleware('auth:api');
Route::post('submitSampling', 'WebServiceController@submitSampling')->middleware('auth:api');
Route::post('submitStok', 'WebServiceController@submitStok')->middleware('auth:api'); //autoincreament stok ?
Route::post('setPhoto', 'WebServiceController@setPhoto')->middleware('auth:api');
Route::post('setCompAct', 'WebServiceController@setCompetitor')->middleware('auth:api');
Route::post('setIdGCM', 'WebServiceController@setGCM')->middleware('auth:api');
Route::post('setVisitProblem', 'WebServiceController@setVisitProblem')->middleware('auth:api');
Route::get('news', 'WebServiceController@getNews')->middleware('auth:api');
// Route::get('news', 'GuestApiController@getNews')->middleware('auth:apibasic');

