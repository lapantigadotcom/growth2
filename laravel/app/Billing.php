<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    //
    protected $table = 'billing';
    protected $guarded = ['kd_outlet'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function outlets()
    {
    	return $this->belongsTo('App\Outlet', 'kd_outlet', 'kd_outlet');
    }
    public function takeOrders()
    {
    	return $this->hasMany('App\TakeOrder', 'kd_visitplan', 'id');
    }

     public function users()
    {
        return $this->belongsTo('App\User', 'kd_user', 'id');
    }
    
}
