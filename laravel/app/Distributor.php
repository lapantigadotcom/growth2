<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Outlet;

class Distributor extends Model
{
    //
    protected $table = 'distributor';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    protected $fillable =  [
    'kd_dist',
    'target_visit',
    'target_ec',
    'nm_dist', 
    'kd_tipe', 
    'almt_dist', 
    'kd_kota', 
    'telp_dist'];

    public $timestamps = false;
    public function outlets()
    {
    	return $this->hasMany('App\Outlet', 'kd_dist', 'id');
    }
}
