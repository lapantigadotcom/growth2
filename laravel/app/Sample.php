<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sample extends Model
{
	protected $table = 'sample';
	protected $guarded = ['id'];
	protected $primaryKey = 'id';
	public $timestamps = false;

	public function sampling(){
		return $this->belongsTo('App\Sampling', 'id', 'kd_outlet');
	}

}
