<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryHarga extends Model
{
  protected $table = 'history_harga';
  protected $guarded = ['id'];
  protected $primaryKey = 'id';
  public $timestamps = false;
}
