<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract{

	public function transform(User $user){
		$data = [
			'id' 		=> $user->id,
			'name' 		=> $user->nama,
			'username' 	=> $user->username,
			'email'		=> $user->email_user,
		];
		return $data;
	}
}