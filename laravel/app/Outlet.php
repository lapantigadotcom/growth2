<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Area;
use App\Kota;
use App\Distributor;
use App\PhotoActivity;
use App\VisitPlan;

class Outlet extends Model
{
    protected $table = 'outlet';
    
    protected $primaryKey = 'kd_outlet';
    protected $fillable = ['kode',
    'kd_dist',
    'kd_area',
    'kd_kota',
    'kd_user',
    'kd_tipe',
    'nm_outlet',
    'almt_outlet',
    'rank_outlet',
    'kodepos',
    'kode',
    'nm_pic',
    'tlp_pic',
    'reg_status',
    'longitude',
    'latitude',
    'toleransi_long',
    'toleransi_lat',
    'foto_outlet',
    'status_area'
    ];
    public $timestamps = false;

    public function users()
    {
    	return $this->belongsTo('App\User', 'kd_user', 'id');
    }

    public function visitPlans()
    {
    	return $this->hasMany('App\VisitPlan', 'kd_outlet', 'kd_outlet');
    }

    public function distributors()
    {
    	return $this->belongsTo('App\Distributor', 'kd_dist', 'id');
    }

    public function photoActivities()
    {
    	return $this->hasMany('App\PhotoActivity', 'kd_outlet', 'kd_outlet');
    }

    public function areas()
    {
    	return $this->belongsTo('App\Area', 'kd_area', 'id');
    }

    public function kotas()
    {
        return $this->belongsTo('App\kota', 'kd_kota', 'id');
    }

}
