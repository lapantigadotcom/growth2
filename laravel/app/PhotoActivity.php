<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Outlet;
use App\Competitor;
use App\tipePhoto;


class PhotoActivity extends Model
{
    //
    protected $table = 'photo_activity';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;


    public function outlets()
    {
    	return $this->belongsTo('App\Outlet', 'kd_outlet', 'kd_outlet');
    }

    public function competitors()
    {
    	return $this->belongsTo('App\Competitor', 'kd_competitor', 'id');
    }

    public function tipePhoto()
    {
        return $this->belongsTo('App\tipePhoto', 'jenis_photo', 'id');
    }
}
