<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessTrip extends Model
{
    protected $table = 'business_trip';
    
    protected $primaryKey = 'id';
    protected $fillable = [
    'kd_user',
    'kd_kota',
    'date_request',
    'date_start',
    'date_end'
    ];
    public $timestamps = false;
    
}
