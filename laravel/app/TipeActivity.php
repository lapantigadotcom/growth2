<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipeActivity extends Model
{
    protected $table = 'tp_activity';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;

}
