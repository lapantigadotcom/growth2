<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipePop extends Model
{
    protected $table = 'tp_pop';
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    public $timestamps = false;

}
