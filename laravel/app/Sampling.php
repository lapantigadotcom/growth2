<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sampling extends Model
{
    protected $table = 'sampling';
    protected $guarded = ['kd_sampling'];
    protected $primaryKey = 'kd_sampling';
    public $timestamps = false;

    public function samples(){
    	return $this->hasMany('App\Sample', 'kd_sampling', 'id');
    }
}
