<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Area;

class Office extends Model
{
    protected $table = 'office';
    
    protected $primaryKey = 'id';
    protected $fillable = [
    'kd_office',
    'kd_area',
    'kd_user',
    'nm_office',
    'almt_office',
    'koordinat',
    'foto_office',
    'status_office'
    ];
    public $timestamps = false;

    public function users()
    {
    	return $this->belongsTo('App\User', 'kd_user', 'id');
    }

    public function areas()
    {
    	return $this->belongsTo('App\Area', 'kd_area', 'id');
    }

    public function presences()
    {
        return $this->hasMany('App\Presence', 'kd_office', 'id');
    }
}
