<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TakeOrder;

class Produk extends Model
{
    //
    protected $table = 'produk';
    protected $guarded = 'id';
    protected $fillable = ['kd_produk','nm_produk', 'harga_produk','kd_area','id_brand'];
    public $timestamps = false;

    public function takeOrders()
    {
    	return $this->hasMany('App\TakeOrder', 'kd_produk', 'id');
    }

    public function areas()
    {
    	return $this->belongsTo('App\Area', 'kd_area', 'id');
    }

     public function brand()
    {
        return $this->belongsTo('App\Brand', 'id_brand', 'id');
    }
}
