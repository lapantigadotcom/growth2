<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Konfigurasi extends Model
{
    //
    protected $table = 'konfigurasi';
    protected $guarded = ['id'];
    public $timestamps = false;
	protected $fillable = ['toleransi_max', 'target_ec', 'target_visit'];
}
