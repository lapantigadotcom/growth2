<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\visitPlan;



class Pop extends Model
{
    protected $table = 'pop';
    protected $guarded = ['kd_pop'];
    protected $primaryKey = 'kd_pop';
    public $timestamps = false;

    public function visitPlan()
    {
    	return $this->belongsTo('App\visitPlan', 'kd_visit', 'id');
    }
}
