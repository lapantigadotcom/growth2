<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stok extends Model
{
  protected $table = 'stok';
  protected $guarded = ['kd_stok'];
  protected $primaryKey = 'kd_stok';
  public $timestamps = false;

  public function outlets(){
    return $this->belongsTo('App\Outlet', 'kd_outlet', 'kd_outlet');
  }

  public function visits(){
    return $this->belongsTo('App\VisitPlan', 'kd_outlet', 'kd_outlet');
  }



}
