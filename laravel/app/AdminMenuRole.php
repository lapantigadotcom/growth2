<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Role;
use App\PermissionRole;

class AdminMenuRole extends Model 
{
    //
    protected $table = 'admin_menu_role';
    protected $guarded = ['id'];

    public $timestamps = false;

    public function roles()
    {
        return $this->belongsTo('App\Role', 'kd_role', 'kd_role');
    }

    public function adminmenus()
    {
    	return $this->belongsTo('App\AdminMenu', 'kd_admin_menu', 'id');
    }
}
