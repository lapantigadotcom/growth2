<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Presence extends Model
{
    protected $table = 'presence';
    
    protected $primaryKey = 'id';
    protected $fillable = [
    'kd_user',
    'kd_office',
    'date_checkin',
    'date_checkout',
    ];
    public $timestamps = false;

    public function users()
    {
    	return $this->belongsTo('App\User', 'kd_user', 'id');
    }
}
