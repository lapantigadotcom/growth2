<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitProblem extends Model
{
    //
    protected $table = 'visit_problem';
    protected $guarded = ['kd_problem'];
    protected $primaryKey = 'kd_problem';
    public $timestamps = false;

    public function outlets(){
      return $this->belongsTo('App\VisitPlan', 'id', 'kd_visit');
    }




}
