<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisitPlan extends Model
{
    //
    protected $table = 'visit_plan';
    protected $guarded = ['kd_visitplan'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function outlets()
    {
        return $this->belongsTo('App\Outlet', 'kd_outlet', 'kd_outlet');
    }
    public function takeOrders()
    {
        return $this->hasMany('App\TakeOrder', 'kd_visitplan', 'id');
    }
}
