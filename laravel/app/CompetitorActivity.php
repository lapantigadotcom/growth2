<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompetitorActivity extends Model
{
    protected $table = 'comp_activity';
    protected $guarded = ['kd_comp_act'];
    protected $primaryKey = 'kd_comp_act';
    public $timestamps = false;

    
}
