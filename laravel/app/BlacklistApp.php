<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlacklistApp extends Model
{
    public $table = 'blacklist_app';
    protected $guarded = ['id'];
    protected $fillable = [
                    'nm_app',
                    ];

    public $timestamps = false;
}
