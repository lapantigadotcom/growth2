<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } 
            else if($request->wantsJson()){
                if($request->api_token == env('API_KEY') && $request->api_token != null){
                    return $next($request);
                }else{
                    $response = [
                        'status'    => 'error',
                        'message'   => "You don't have a credential to access this",
                        'data'      => ''
                    ];
                    return response()->json($response, 401);
                }
            }
            else {
                Session::flash('error_message', 'Silahkan login terlebih dahulu..');
                return redirect()->route('admin.login');
            }
        }
        return $next($request);
    }
}
