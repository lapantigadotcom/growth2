<?php namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;
use Route;
use Request; 

class CheckPermission {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (Auth::check()) {
			if(!Request::ajax()){
				$route = Route::getCurrentRoute()->getName();
				if(Auth::user()->hasAccess($route))  {
		        	return $next($request);
		        }
		        else{
					return abort(401);
				}
			}else{
				// if user pass auth and ajax
				return $next($request);
			}
		}
		else{
			return $next($request);
		}
		
	}
}
