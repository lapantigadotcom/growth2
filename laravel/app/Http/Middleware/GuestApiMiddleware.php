<?php

namespace App\Http\Middleware;

use Closure;

class GuestApiMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if($request->api_token == env('API_KEY')){
        //     return $next($request);
        // }else{
        //     $response = [
        //         'status'    => 'error',
        //         'message'   => "You don't have a credential to access this data : middleware",
        //         'data'      => ''
        //     ];
        //     return response()->json($response, 401);
        // }
        return $next($request);
    }
}
