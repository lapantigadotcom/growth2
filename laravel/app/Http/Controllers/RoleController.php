<?php
/*
PT. Trikarya Teknologi Indonesia
Tenggilis raya 127
Office Complex Apartment Metropolis MKB 206
Surabaya, Jawa timur, Indonesia
Phone : +6231-8420384 / +6281235537717
*/
namespace App\Http\Controllers;
use DB;
use Input;
use App\Role;
use App\Permission;
use App\AdminMenu;
use App\PermissionRole;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\AdminRoleRequest;
use App\Http\Requests\RoleRequest;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;

class RoleController extends Controller {

	public function index()
	{
		$data = Role::all();
		return view('pages.role.index', compact('data'));
	}

	public function create()
	{
		return view('pages.role.create');
	}

	public function store(RoleRequest $request)
	{
		Role::create($request->all());
                $log = new AdminController;
	        $log->getLogHistory('Make New Role');
		return redirect()->route('admin.role.index');
	}

	public function edit($kd_role)
	{
		$data['adminmenu'] = Adminmenu::orderBy('nm_menu')->pluck('nm_menu','id');
		$data['permission'] = Permission::orderBy('nm_permission')->pluck('nm_permission','id');
		$data['content'] = Role::with('permission')->find($kd_role);
		$data['permission_t'] = $data['content']->permission->pluck('id')->toArray();
		$data['adminmenu_t'] = $data['content']->adminmenu->pluck('id')->toArray();
		return view('pages.role.edit', compact('data'));
	}

	public function update(RoleRequest $request, $kd_role)
	{
		$data = Role::find($kd_role);
		$data->type_role = $request->input('type_role');
		$data->save();
		$data->adminmenu()->detach();
		if(is_array($request->input('adminmenu')))
		{
			foreach ($request->input('adminmenu') as $row) {
				$data->adminmenu()->attach($row);
			}
		}
		$data->permission()->detach();
		if(is_array($request->input('permission')))
		{
			foreach ($request->input('permission') as $row) {
				$data->permission()->attach($row);
			}
		}
                $log = new AdminController;
	        $log->getLogHistory('Update Role with ID '.$kd_role);
		return redirect()->route('admin.role.index');
	}

	public function destroy($kd_role)
	{
            $data = Role::with(
            'permissions',
            'adminmenus'
             )->find($kd_role);
             foreach ($data->permissions as $row) {
        	$row->delete();
             }
             foreach ($data->adminmenus as $row) {
		$row->delete();
             }
            $data->delete();
            $log = new AdminController;
	    $log->getLogHistory('Delete Role with ID '.$kd_role);
	    return redirect()->route('admin.role.index');
	}

}
