<?php



namespace App\Http\Controllers;

use DB;
use Auth;
use Validator;
use Hash;
use Session;

use Excel;
use Redirect;
use Mail;
use App\User;
use App\Area;
use App\Distributor;
use App\VisitPlan;
use App\TakeOrder;
use App\VisitProblem;
use App\PhotoActivity;
use App\Outlet;
use App\Logging;
use App\Http\Controllers\Controller;
use App\Http\Requests\AreaRequest;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;
use Khill\Lavacharts\Lavacharts;
use Lava;

class AdminController extends Controller
{

	public function getLogHistory($action)
	{
		$logging = new Logging;
		$logging->kd_user = Auth::user()->id;
		$logging->description = $action;
		$logging->log_time = DB::raw('CURRENT_TIMESTAMP');
		$logging->detail_akses = 'Backend';
		$logging->save();
	}

	public function getLogin()
	{
		return view('auth.login');
	}

	public function postLogin(Request $request)
	{
		$username = $request->input('username');
		$password = $request->input('password');
		// $remember_me=false;

		$tmp = $request->input('remember_me');
		if(!empty($tmp))
			// $remember_me=true;
		echo $username;
		if (Auth::attempt(['username' => $username, 'password' => $password]))
		{

			$this->getLogHistory('LOGIN');
			return redirect()->intended('admin/dashboard');

		}
		else
		{
			Session::flash('error_message', 'Username / Password tidak sesuai');
			return redirect()->back()->withInput()->with('message', 'Login Failed');;
		}
	}

	
    public function getDashboard(Request $request)
    {
        $area = $request->input('kd_area');
        $dist = $request->input('kd_dist');
        $osales=$oarea=$otipe=$odist='=';

        echo Input::get('download');
        if($area=='' || $area=='100'){
            $area='NULL';
            $oarea='<>';
        }

        if($dist==''){
            $dist='NULL';
            $odist='<>';
        }
        if(!Input::get('download')){
            if(Auth::user()->kd_area==100)
            {
                $data['finishedVisit']= VisitPlan::where('visit_plan.status_visit', '=', 1)
                ->where(DB::raw('MONTH(date_visiting)'), '=', date('m'))
                ->where(DB::raw('YEAR(date_visiting)'), '=', date('Y'))
                ->count();

                // dd(DB::raw('MONTH(date_visiting)'));
                $data['countTO']=DB::table('take_order')
                ->distinct('kd_to')
                ->where(DB::raw('MONTH(date_order)'), '=', date('m'))
                ->where(DB::raw('YEAR(date_order)'), '=', date('Y'))
                // ->join('visit_plan', 'visit_plan.id' ,'=','take_order.kd_visitplan')
                ->count('kd_to');

                $data['totalValue']=TakeOrder::select(DB::raw('SUM(take_order.qty_order*produk.harga_produk) as total'))
                    ->join('produk', 'produk.id', '=','take_order.kd_produk')
                ->join('visit_plan', 'visit_plan.id' ,'=','take_order.kd_visitplan')
                ->where(DB::raw('MONTH(date_order)'), '=', date('m'))
                ->where(DB::raw('YEAR(date_order)'), '=', date('Y'))
                ->get();

                $data['photoOutlet'] = PhotoActivity::where(DB::raw('MONTH(date_upload_photo)'), '=', date('m'))
                ->where(DB::raw('YEAR(date_upload_photo)'), '=', date('Y'))
                ->count();

                $data['visitProblem'] = VisitProblem::where(DB::raw('MONTH(date_upload_photo)'), '=', date('m'))
                ->where(DB::raw('YEAR(date_upload_photo)'), '=', date('Y'))
                ->count();

                $data['regOutlet'] = DB::table('outlet')
                ->count();

                $data['countPOP']=DB::table('pop')
                ->where(DB::raw('MONTH(date_upload)'), '=', date('m'))
                ->where(DB::raw('YEAR(date_upload)'), '=', date('Y'))
                ->count('kd_pop'); 

                $data['countVisitbukti']=DB::table('visit_bukti')
				->distinct('kd_outlet')
				->where(DB::raw('MONTH(created_at)'), '=', date('m'))
				->where(DB::raw('YEAR(created_at)'), '=', date('Y'))
				->count('kd_outlet');

		$data['countBilling']=DB::table('billing')
				->distinct('kd_outlet')
				->where(DB::raw('MONTH(submit_at)'), '=', date('m'))
				->where(DB::raw('YEAR(submit_at)'), '=', date('Y'))
				->count('kd_outlet');

                    if($area=='NULL' || $area=='100'){
                        $oarea='IS NOT';
                    }

                    if($dist=='NULL'){
                        $odist='IS NOT';
                    }

                    $query = "
                        SELECT V.id, V.nama, V.nm_area, V.TotalVisit, E.TotalEc, E.TotalValue 
                            FROM (
                                SELECT user.id, user.nama, area.nm_area, 
                                COUNT(distinct visit_plan.id) AS TotalVisit
                                FROM user, visit_plan, outlet, area
                                WHERE visit_plan.kd_outlet = outlet.kd_outlet
                                AND visit_plan.status_visit = 1
                                AND YEAR(date_visiting) = YEAR(NOW())
                                AND MONTH(date_visiting) = MONTH(NOW()) 
                                AND user.id = outlet.kd_user
                                AND kd_role = 3
                                AND outlet.kd_area = area.id ";
                     $query .= "AND outlet.kd_area " . "$oarea" . " $area ";
                    $query .= "AND outlet.kd_dist " . "$odist" . " $dist ";
                    $query .= "
                            GROUP BY user.nama
                            ) V
                            LEFT JOIN 
                            (
                                SELECT S.nama,
                                 COUNT(S.nama) AS totalEc, 
                                SUM(S.totalValue) AS TotalValue
                                FROM
                                    (
                                        SELECT 
                                        user.nama AS nama, 
                                        take_order.kd_to,
                                        SUM( take_order.qty_order*produk.harga_produk) AS totalValue    
                                    FROM outlet, visit_plan, take_order, user, produk
                                    WHERE kd_role = 3
                                    AND visit_plan.kd_outlet = outlet.kd_outlet
                                    AND visit_plan.status_visit = 1
                                    AND YEAR(date_visiting) = YEAR(NOW())
                                    AND MONTH(date_visiting) = MONTH(NOW())
                                    AND user.id = outlet.kd_user
                                     AND take_order.kd_produk = produk.id ";
                    $query .= "AND outlet.kd_area " . "$oarea" . " $area ";
                    $query .= "AND outlet.kd_dist " . "$odist" . " $dist ";
                    $query .=  "GROUP BY user.nama, take_order.kd_to
                                ) AS S
                                GROUP BY S.nama
                            ) E
                            ON E.nama = V.nama
                            ORDER BY TotalValue DESC";
                $data['topsalesorder'] = DB::select(DB::raw($query));


                foreach ($data['topsalesorder'] as $key => $value) {

                    $photosBlock = PhotoActivity::where('kd_jenis', '=', '14')
                    ->join('outlet', 'outlet.kd_outlet' ,'=', 'photo_activity.kd_outlet')
                    ->where('outlet.kd_user', '=', $value->id)
                    ->whereRaw("MONTH(date_take_photo) = MONTH(NOW())")
                    ->whereRaw("YEAR(date_take_photo) = YEAR(NOW())")
                    ->count();
                    $data['topsalesorder'][$key]->block = $photosBlock;

                    $photosSpecial = PhotoActivity::where('kd_jenis', '=', '17')
                    ->join('outlet', 'outlet.kd_outlet' ,'=', 'photo_activity.kd_outlet')
                    ->where('outlet.kd_user', '=', $value->id)
                    ->whereRaw("MONTH(date_take_photo) = MONTH(NOW())")
                    ->whereRaw("YEAR(date_take_photo) = YEAR(NOW())")
                    ->count();
                    $data['topsalesorder'][$key]->special = $photosSpecial;

                     $jumlahORDER = TakeOrder::where('status_order', '=', '1')
                     ->distinct('kd_to') 
                    ->join('outlet', 'outlet.kd_outlet' ,'=', 'take_order.kd_outlet')
                    ->where('outlet.kd_user', '=', $value->id)
                    ->whereRaw("MONTH(date_order) = MONTH(NOW())")
                    ->whereRaw("YEAR(date_order) = YEAR(NOW())")
                    ->count('kd_to');
                    $data['topsalesorder'][$key]->totalorderku = $jumlahORDER;


                    $photosContest = PhotoActivity::where('kd_jenis', '=', '22')
                    ->join('outlet', 'outlet.kd_outlet' ,'=', 'photo_activity.kd_outlet')
                    ->where('outlet.kd_user', '=', $value->id)
                    ->whereRaw("MONTH(date_take_photo) = MONTH(NOW())")
                    ->whereRaw("YEAR(date_take_photo) = YEAR(NOW())")
                    ->count();
                    $data['topsalesorder'][$key]->contest = $photosContest;

                } 
                    $data['visitMonth'] = DB::select(DB::raw("
                                                                SELECT day(date_visiting) as dates, 
                                                                count(date_visiting) as jumlah_visit 
                                                                FROM visit_plan 
                                                                WHERE month(date_visiting) = month(now()) 
                                                                AND year(date_visiting)=year(now()) 
                                                                AND status_visit=1 
                                                                group by day(date_visiting) 
                                                                order by date_visiting"));

                    $result = Lava::DataTable();
                    $result->addStringColumn('Visit Plan')->addNumberColumn('Jumlah Visit');

                    foreach($data['visitMonth'] as $graphVisit){
                        $result->addRow(array($graphVisit->dates,$graphVisit->jumlah_visit));
                    }
                    Lava::AreaChart('VisitPlan', $result, [
                        'title'     => 'Submited Visit Graph',
                        'legend'    => ['position' => 'in'],
                        'hAxis'     => ['title'=>'Tanggal'],
                        'vAxis'     => ['title'=>'Jumlah Visit'],
                        'colors'    => ['green'],
                        'width'         =>'1000'
                        ]);

                    $data['orderMonth'] = DB::select(DB::raw("
                                                                SELECT newtab.dates as dates, 
                                                                count(newtab.dates) as jumlah_order 
                                                                from(
                                                                        SELECT distinct kd_to, day(date_order) as dates 
                                                                        FROM take_order 
                                                                        WHERE month(date_order)=month(now()) 
                                                                        AND year(date_order)=year(now()))  as newtab 
                                                                        group by dates"));

                    $result = Lava::DataTable();
                    $result->addStringColumn('Effective Call')->addNumberColumn('Jumlah Outlet Transaction');
                    
                    foreach($data['orderMonth'] as $graphVisit){
                        $result->addRow(array($graphVisit->dates,$graphVisit->jumlah_order));
                    }
                    Lava::AreaChart('TakeOrder', $result, [
                        'title'         => 'Take Order Graph',
                        'legend'    => ['position' => 'in'],
                        'hAxis'     => ['title'=>'Tanggal'],
                        'vAxis'     => ['title'=>'Jumlah Order'],
                        'colors'    => ['blue'],
                        'width'         =>'1000'
                    ]);

                $data['valueMonth'] = DB::select(DB::raw("
                                                            SELECT sum(produk.harga_produk*take_order.qty_order) as jumlah_value, 
                                                            day(date_order) as dates 
                                                            FROM take_order 
                                                            join produk on take_order.kd_produk = produk.id 
                                                            WHERE month(date_order)=month(now()) 
                                                            AND year(date_order)=year(now()) 
                                                            group by dates"));

                $result = Lava::DataTable();
                $result->addStringColumn('Total Value')->addNumberColumn('Jumlah Total Value');

                foreach($data['valueMonth'] as $graphVisit){
                    $result->addRow(array($graphVisit->dates,$graphVisit->jumlah_value));
                }
                Lava::AreaChart('TotalValue', $result, [
                    'title'         => 'Total Value Graph',
                    'width'         =>'1000',
                    'legend'    => ['position' => 'in'],
                    'hAxis'     => ['title'=>'Tanggal'],
                    'vAxis'     => ['title'=>'Jumlah Value'],
                    'colors'    => ['red']
                ]);

                $data['area']=[''=>'All Area']+Area::pluck('kd_area', 'id')->toArray();
                $data['carea']=$area;
                $data['distributor']=[''=>'All Distributor']+Distributor::pluck('kd_dist', 'id')->toArray();
                $data['cdist']=$dist;
                $data['carea']=$area;

                return view('dashboard.index',compact('data'));
                
            }
            else if(Auth::user()->kd_role == 3)
            {
                $data = $this->getSalesDashboard();
                return view('dashboard.index',compact('data'));
            }
            else
            {
                $data = $this->getAreaManagerDashboard($request);
                return view('dashboard.index',compact('data'));
            }
        }
        else{
            $this->downloadTop($request, $dist, $odist, $area, $oarea);
        }
    }

	public function getAreaManagerDashboard(Request $request)
	{
		$dist = $request->input('kd_dist');
		$odist='=';
		if($dist==''){
			$dist='NULL';
			$odist='IS NOT';
		}

		$kode = Auth::user()->kd_area;
		// print_r("dist : " . $dist );
		// print_r($odist);
		// print_r($kode);
		// die();

		$data['finishedVisit']= VisitPlan::where('visit_plan.status_visit', '=', 1)
		->leftJoin('outlet', 'outlet.kd_outlet', '=', 'visit_plan.kd_outlet')
		->where(DB::raw('MONTH(date_visiting)'), '=', date('m'))
		->where('outlet.kd_area', '=', $kode)
		->count();

		$data['countTO']=DB::table('take_order')
		->distinct('kd_to')

		->where(DB::raw('MONTH(date_order)'), '=', date('m'))
  		->where('outlet.kd_area', '=', $kode)
		->count('kd_to');

		$data['countVisitbukti']=DB::table('visit_bukti')
				->distinct('kd_outlet')
				->where(DB::raw('MONTH(created_at)'), '=', date('m'))
				->where(DB::raw('YEAR(created_at)'), '=', date('Y'))
				->count('kd_outlet');

		$data['countBilling']=DB::table('billing')
				->distinct('kd_outlet')
				->where(DB::raw('MONTH(submit_at)'), '=', date('m'))
				->where(DB::raw('YEAR(submit_at)'), '=', date('Y'))
				->count('kd_outlet');

		$data['totalValue']=TakeOrder::select(DB::raw('SUM(take_order.qty_order) as total'))
		->join('produk', 'produk.id', '=','take_order.kd_produk')
		->join('visit_plan', 'visit_plan.id' ,'=','take_order.kd_visitplan')
		->leftJoin('outlet', 'outlet.kd_outlet', '=', 'visit_plan.kd_outlet')
		->where('outlet.kd_area', '=', $kode)
		->where(DB::raw('MONTH(date_order)'), '=', date('m'))
		->get();

		$data['photoOutlet'] = PhotoActivity::leftJoin('outlet', 'outlet.kd_outlet', '=', 'photo_activity.kd_outlet')
		->where('outlet.kd_area', '=', $kode)
		->where(DB::raw('MONTH(date_upload_photo)'), '=', date('m'))
		->count();

		$data['visitProblem'] = VisitProblem::join('visit_plan', 'visit_plan.id' ,'=','visit_problem.kd_visit')
		->leftJoin('outlet', 'outlet.kd_outlet', '=', 'visit_plan.kd_outlet')
		->where('outlet.kd_area', '=', $kode)
		->where(DB::raw('MONTH(date_upload_photo)'), '=', date('m'))->count();

		 $jumlahORDER = TakeOrder::where('status_order', '=', '1')
                    ->join('outlet', 'outlet.kd_outlet' ,'=', 'take_order.kd_outlet')
                    ->where('outlet.kd_user', '=', $value->id)
                    ->whereRaw("MONTH(date_order) = MONTH(NOW())")
                    ->whereRaw("YEAR(date_order) = YEAR(NOW())")
                    ->count();
                    $data['topsalesorder'][$key]->totalorderku = $jumlahORDER;

		$data['regOutlet'] = DB::table('outlet')
		->where('outlet.kd_area', '=', $kode)
		->where('reg_status', '=', 'YES')
		->count();
 
		$query = "
			SELECT V.nama, V.nm_area, V.TotalVisit, E.TotalEc, E.TotalValue
				FROM (
					SELECT user.nama, area.nm_area, visit_plan.id, COUNT(user.nama) AS TotalVisit
					FROM user, visit_plan, outlet, area
					WHERE visit_plan.kd_outlet = outlet.kd_outlet
					AND visit_plan.status_visit = 1
					AND MONTH(date_visiting) = MONTH(NOW())
					AND user.id = outlet.kd_user
					AND kd_role = 3
					AND outlet.kd_area = area.id ";
		$query .= "AND outlet.kd_area =" . " $kode ";
		$query .= "AND outlet.kd_dist " . "$odist" . " $dist ";
		$query .= "
				GROUP BY user.nama
				) V
				LEFT JOIN 
				(
					SELECT user.nama, COUNT(DISTINCT take_order.kd_to) AS TotalEc, SUM(take_order.qty_order) AS TotalValue
					FROM outlet, visit_plan, take_order, user, produk
					WHERE kd_role = 3
					AND visit_plan.kd_outlet = outlet.kd_outlet
					AND visit_plan.status_visit = 1
					AND MONTH(date_visiting) = MONTH(NOW())
					AND user.id = outlet.kd_user
					AND visit_plan.id = take_order.kd_visitplan
					AND take_order.kd_produk = produk.id ";
		$query .= "AND outlet.kd_area =". " $kode ";
		$query .= "AND outlet.kd_dist " . "$odist" . " $dist ";
		$query .= "
					Group BY nama
				) E
				ON E.nama = V.nama
				ORDER BY TotalValue DESC";
		$data['topsalesorder'] = DB::select(DB::raw($query));

		$data['visitMonth']=DB::select(DB::raw("SELECT day(date_visiting) as dates, count(date_visiting) as jumlah_visit
			FROM visit_plan, outlet
			WHERE visit_plan.kd_outlet=outlet.kd_outlet
			AND outlet.kd_area = :kodeArea
			AND month(date_visiting)=month(now())
			AND status_visit=1
			GROUP BY day(date_visiting)
			ORDER BY date_visiting"),array('kodeArea' => $kode,));

		$result = Lava::DataTable();
		$result->addStringColumn('Visit Plan')
		->addNumberColumn('Jumlah Visit');
		foreach($data['visitMonth'] as $graphVisit){
			$result->addRow(array($graphVisit->dates,$graphVisit->jumlah_visit));
		}

		Lava::AreaChart('VisitPlan', $result, [
			'title'     => 'Submited Visit Graph',
			'legend'    => ['position' => 'in'],
			'hAxis'     => ['title'=>'Tanggal'],
			'vAxis'     => ['title'=>'Jumlah Visit'],
			'colors'    => ['green'],
			'width' =>'1000'
			]);

		$data['orderMonth']=DB::select(DB::raw("SELECT newtab.dates as dates, count(newtab.dates) as jumlah_order
			from(SELECT distinct kd_to, day(date_order) as dates FROM take_order
			join visit_plan on take_order.kd_visitplan = visit_plan.id
			join outlet on visit_plan.kd_outlet = outlet.kd_outlet
			WHERE month(date_order)=month(now()) and outlet.kd_area = :kodeArea) as newtab group by dates"),array('kodeArea' => $kode,));
		$result = \Lava::DataTable();
		$result->addStringColumn('Effective Call')
		->addNumberColumn('Jumlah Effective Call');
		foreach($data['orderMonth'] as $graphVisit){
			$result->addRow(array($graphVisit->dates,$graphVisit->jumlah_order));
		}
		\Lava::AreaChart('TakeOrder', $result, [
			'title' => 'Take Order Graph',
			'legend'    => ['position' => 'in'],
			'hAxis'     => ['title'=>'Tanggal'],
			'vAxis'     => ['title'=>'Jumlah Order'],
			'colors'    => ['blue'],
			'width' =>'1000'
			]);

		$data['valueMonth']=DB::select(DB::raw("SELECT sum(produk.harga_produk*take_order.qty_order) as jumlah_value, day(date_order) as dates FROM take_order join produk on take_order.kd_produk = produk.id
			join visit_plan on take_order.kd_visitplan = visit_plan.id
			join outlet on visit_plan.kd_outlet = outlet.kd_outlet
			WHERE month(date_order)=month(now()) and outlet.kd_area = :kodeArea group by dates"),array('kodeArea' => $kode,));
		$result = Lava::DataTable();
		$result->addStringColumn('Total Value')
		->addNumberColumn('Jumlah Total Value');
		foreach($data['valueMonth'] as $graphVisit){
			$result->addRow(array($graphVisit->dates,$graphVisit->jumlah_value));
		}
		Lava::AreaChart('TotalValue', $result, [
			'title' => 'Total Value Graph',
			'legend'    => ['position' => 'in'],
			'hAxis'     => ['title'=>'Tanggal'],
			'vAxis'     => ['title'=>'Jumlah Value'],
			'colors'    => ['red'],
			'width' =>'1000'

			]);

		$data['area']=Area::where('id', '=', $kode)->pluck('kd_area', 'id')->toArray();
		$data['carea']=$kode;
		$data['distributor']=[''=>'All Distributor']+Distributor::pluck('kd_dist', 'id')->toArray();
		$data['cdist']=$dist;



		return $data;
	}

	public function getSalesDashboard()
	{
		$id = Auth::user()->id;
// Pending Visit Plan,
		$data['pending']= VisitPlan::join('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet' )
		->where('approve_visit', '=', 1)
		->where('status_visit', '=', 0)
		->where(DB::raw('MONTH(date_visit)'), '=', date('m'))
		->where('outlet.kd_user', '=', $id)
		->count();

// My Outlet
		$data['outlet'] = DB::table('outlet')
		->where('reg_status', '=', 'YES')
		->where('outlet.kd_user', '=', $id)
		->count();
// My Take Orders
		$data['order'] = DB::table('take_order')
		->distinct('kd_to')
		->where(DB::raw('MONTH(date_order)'), '=', date('m'))
		->join('visit_plan', 'visit_plan.id','=','take_order.kd_visitplan' )
		->join('outlet', 'outlet.kd_outlet','=','visit_plan.kd_outlet' )
		->where('status_order', '=', 1)
		->where('outlet.kd_user', '=', $id)
		->count('kd_to');
// Visit Plan Today
		$data['visit'] =VisitPlan::join('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet' )
		->where('approve_visit', '=', 1)
		->where('status_visit', '=', 2)
		->where('date_visit','=',date('Y-m-d'))
		->where('outlet.kd_user', '=', $id)
		->count();



		$data['visitMonth']=DB::select(DB::raw("SELECT day(date_visiting) as dates, count(date_visiting) as jumlah_visit
			FROM visit_plan, outlet
			WHERE visit_plan.kd_outlet=outlet.kd_outlet
			AND outlet.kd_user = :kodeSales
			AND month(date_visiting)=month(now())
			AND status_visit=1
			GROUP BY day(date_visiting)
			ORDER BY date_visiting"),array('kodeSales' => $id));
		$result = Lava::DataTable();
		$result->addStringColumn('Visit Plan')
		->addNumberColumn('Jumlah Visit');
		foreach($data['visitMonth'] as $graphVisit){
			$result->addRow(array($graphVisit->dates,$graphVisit->jumlah_visit));
		}
		Lava::AreaChart('VisitPlan', $result, [
			'title'     => 'Submited Visit Graph',
			'legend'    => ['position' => 'in'],
			'hAxis'     => ['title'=>'Tanggal'],
			'vAxis'     => ['title'=>'Jumlah Visit'],
			'colors'    => ['green'],
			'width' =>'1000'
			]);

		$data['orderMonth']=DB::select(DB::raw("SELECT newtab.dates as dates, count(newtab.dates) as jumlah_order
			from(SELECT distinct kd_to, day(date_order) as dates FROM take_order
			join visit_plan on take_order.kd_visitplan = visit_plan.id
			join outlet on visit_plan.kd_outlet = outlet.kd_outlet
			WHERE month(date_order)=month(now()) and outlet.kd_user = :kodeSales) as newtab
			group by dates"),array('kodeSales' => $id));
		$result = Lava::DataTable();
		$result->addStringColumn('Effective Call')
		->addNumberColumn('Jumlah Effective Call');
		foreach($data['orderMonth'] as $graphVisit){
			$result->addRow(array($graphVisit->dates,$graphVisit->jumlah_order));
		}
		Lava::AreaChart('TakeOrder', $result, [
			'title' => 'Take Order Graph',
			'legend'    => ['position' => 'in'],
			'hAxis'     => ['title'=>'Tanggal'],
			'vAxis'     => ['title'=>'Jumlah Order'],
			'colors'    => ['blue'],
			'width' =>'1000'
			]);

		$data['valueMonth']=DB::select(DB::raw("SELECT sum(produk.harga_produk*take_order.qty_order) as jumlah_value, day(date_order) as dates FROM take_order join produk on take_order.kd_produk = produk.id
			join visit_plan on take_order.kd_visitplan = visit_plan.id
			join outlet on visit_plan.kd_outlet = outlet.kd_outlet
			WHERE month(date_order)=month(now()) and outlet.kd_user = :kodeSales group by dates"),array('kodeSales' => $id,));
		$result = Lava::DataTable();
		$result->addStringColumn('Total Value')
		->addNumberColumn('Jumlah Total Value');
		foreach($data['valueMonth'] as $graphVisit){
			$result->addRow(array($graphVisit->dates,$graphVisit->jumlah_value));
		}
		Lava::AreaChart('TotalValue', $result, [
			'title' => 'Total Value Graph',
			'legend'    => ['position' => 'in'],
			'hAxis'     => ['title'=>'Tanggal'],
			'vAxis'     => ['title'=>'Jumlah Value'],
			'colors'    => ['red'],
'width' =>'1000'
			]);

		return $data;
	}

	public function getLogout()
	{
		$this->getLogHistory('LOGOUT');
		Auth::logout();
		return redirect()->route('admin.login');
	}

	public function getChangePassword()
	{
		return view('pages.admin.changepassword');
	}

	public function postChangePassword(Request $request)
	{
		$user = Auth::user();
		$oldPassword = Input::get('old_password');
		$newPassword = Input::get('new_password');
		$retypePassword = Input::get('retype_password');
		if($newPassword != $retypePassword) {
			Session::flash('errorMessage', 'Re-type Password tidak sama');
			return redirect()->route('admin.getChangePassword');
		}
		if(!Hash::check($oldPassword, $user->password)) {
			Session::flash('errorMessage', 'Password salah');
			return redirect()->route('admin.getChangePassword');
		}
		$user->password = $newPassword;
		$user->save();
		Session::flash('successMessage', 'Ganti password berhasil');
		return redirect()->route('admin.dashboard');
	}

	public function autentikasi()
	{
		return view('auth.forgot.autentikasi');
	}

	public function sendEmailReminder(Request $request)
	{
		$email = $request->input('email_user');
		$nik = $request->input('nik');
		$telp = $request->input('telepon');

		$user = User::where('email_user', '=', $email)->first();
		if(is_null($user))
		{
			Session::flash('error_message','User Not Found');
			return view('auth.login');
		}
		else
		{
			if(!($user->nik == $nik && $user->telepon == $telp))
			{
				Session::flash('error_message','User Not Found');
				return view('auth.login');
			}
		}
		$newPass = str_random(8);
		if($user->update(array('password' => $newPass))){
			Mail::send('email.ubahPass', ['newPass' => $newPass], function($message) use ($user)
			{
				$message->from('sales@hisamitsu.co.id', 'Sales App');
				$message->to($user->email_user, $user->nama)->subject('Hisamitsu Reset Password!');
			});
		}
		Session::flash('success_message','Password baru telah dikirim ke email '.$email);
		return view('auth.login');
	}
	public function downloadTop(Request $request, $dist, $odist, $area, $oarea)
	{
		$log = new AdminController;
		$log->getLogHistory('Download Data Top Sales Achievement');


		$role = Auth::user()->kd_role;
		$areas = Auth::user()->kd_area;
		if($areas == 100)
		{
			ob_end_clean();
			ob_start();
			$top= User::selectRaw('user.nama as SALES_FORCE, area.nm_area as AREA, count(distinct visit_plan.id) as TOTAL_VISIT, count(distinct take_order.kd_to) as TOTAL_EC, sum(take_order.qty_order) as TOTAL_VALUE')

			->join('outlet','outlet.kd_user','=','user.id')
			->join('visit_plan','visit_plan.kd_outlet','=','outlet.kd_outlet')
			->leftjoin('take_order','take_order.kd_visitplan','=','visit_plan.id')
			->leftJoin('produk','take_order.kd_produk','=', 'produk.id')

			->leftJoin('area','area.id','=','outlet.kd_area')
			->where('visit_plan.status_visit','=',1)
			->where(DB::raw('MONTH(date_visiting)'), '=', date('m'))
			->where('user.kd_role','=',3)
			->where('outlet.kd_area', $oarea, $area)
			->where('outlet.kd_dist', $odist, $dist)
			->groupBy('user.id')
			->orderBy('TOTAL_VALUE', 'desc')
			->take(10)->get();

		}
		else
		{
			ob_end_clean();
			ob_start();
			$top= User::selectRaw('user.nama as SALES_FORCE, area.nm_area as AREA, count(distinct visit_plan.id) as TOTAL_VISIT, count(distinct take_order.kd_to) as TOTAL_EC, sum(take_order.qty_order) as TOTAL_VALUE')

			->join('outlet','outlet.kd_user','=','user.id')
			->join('visit_plan','visit_plan.kd_outlet','=','outlet.kd_outlet')
			->leftjoin('take_order','take_order.kd_visitplan','=','visit_plan.id')
			->leftJoin('produk','take_order.kd_produk','=', 'produk.id')
			->leftJoin('area','area.id','=','outlet.kd_area')
			->where('visit_plan.status_visit','=',1)
			->where(DB::raw('MONTH(date_visiting)'), '=', date('m'))
			->where('user.kd_role','=',3)
			->where('outlet.kd_area', '=', $areas)
			->where('outlet.kd_dist', $odist, $dist)
			->groupBy('user.id')
			->orderBy('TOTAL_VALUE', 'desc')
			->take(10)->get();

		}
		date_default_timezone_set("Asia/Jakarta");
		$d= strtotime("now");
		$time =date("d-m-Y H:i:s", $d);
		$filename = $time."_TOPSALES";

		Excel::create($filename, function($excel) use($top) {
			$excel->sheet('TOP', function($sheet) use($top) {
				$sheet->fromArray($top);
			});
		})->download('xls');
	}
}