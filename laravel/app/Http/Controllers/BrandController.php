<?php
/*
PT. Trikarya Teknologi Indonesia
Tenggilis raya 127
Office Complex Apartment Metropolis MKB 206
Surabaya, Jawa timur, Indonesia
Phone : +6231-8420384 / +6281235537717
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Input;
use Excel;
use App\Billing;
use App\Satuan;
use App\Brand;
use App\VisitPlan;
use App\VisitBukti;
use App\Outlet;
use App\Distributor;
use App\Area;
use App\Tipe;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\VisitBuktiRequest;
use App\Http\Requests\BillingRequest;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;


class BrandController extends Controller
{
      
   public function index(Request $request)
    {
 

        $data = Brand::orderBy('id', 'asc')
         ->get();
        return view('pages.brand.index', compact('data'));
     }
    
      
     public function store(Request $request)
    {
         
        $brand = new Brand();
        $brand->nama = $request->input('nama');
        $brand->kode = $request->input('kode');
        $brand->created_at = date('Y-m-d H:i:s'); 
        $brand->save();  
        
        $data['tipe']='Brand';
        $data['status']='new';
        $data['message']=json_encode($brand);  
        
        $log = new AdminController;
        $log->getLogHistory('Add Brand');
        
        
        return redirect()->route('admin.brand.index');
    }

    public function create()
    {
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        $id = Auth::user()->id;
        if($area == 100)
        {
            $dist=Distributor::all();
            $outlet = Outlet::where('reg_status','=', 'YES')->pluck('kd_outlet', 'nm_outlet')->toArray();
        }
        else if($role == 3)
        {
            $dist=Distributor::all();
            $outlet= Outlet::where('kd_user','=',$id)->get();
        }
        else
        {
            $dist=Distributor::all();
            $outlet = Outlet::select('kd_outlet', 'nm_outlet')->where('kd_area', '=', $area)->get()->toArray();
        }

        return view('pages.brand.create')->with('dist',$dist)->with('outlet',$outlet);
    } 

    public function edit($id)
    {
        $data['content'] = Brand::find($id);

        
        return view('pages.brand.edit')->with('data',$data);
    }

    public function update(Request $request, $id)
    {
        $brand = Brand::find($id);
        $brand->update($request->all()); 
        $log = new AdminController;
        $log->getLogHistory('Update Brand with ID '.$id);

        return redirect()->route('admin.brand.index');
    }
 
     public function destroy($id)
    {
        $data = Brand::find($id);
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Brand with ID '.$id);
        return redirect()->route('admin.brand.index');
    }

    
}
