<?php 
/*
PT. Trikarya Teknologi Indonesia
Tenggilis raya 127
Office Complex Apartment Metropolis MKB 206
Surabaya, Jawa timur, Indonesia
Phone : +6231-8420384 / +6281235537717
*/
namespace App\Http\Controllers;
use DB;
use Input;
use App\AdminMenu;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;
use App\Http\Requests\AdminMenuRequest;

class AdminMenuController extends Controller {

	public function index()
	{
		$data['content'] = AdminMenu::with('roles','child')->get();
		return view('pages.adminMenu.index', compact('data'));
	}

	public function create()
	{
		$data['adminmenu'] = ['0'=>'No Parent']+AdminMenu::where('parent_menu','0')->pluck('nm_menu','id')->toArray();
		return view('pages.adminMenu.create',compact('data'));
	}

	public function store(AdminMenuRequest $request)
	{
		$data = AdminMenu::create($request->all());
		$urutan_t = AdminMenu::where('parent_menu', $request->input('parent_menu'))->count();
		$data->urutan= $urutan_t+1;
		$data->save();
                $log = new AdminController;
		$log->getLogHistory('Make Admin Menu');
		return redirect()->route('admin.adminmenu.index');
	}

	public function edit($id)
	{
		$data['adminmenu'] = ['0'=>'No Parent']+AdminMenu::where('parent_menu','0')->pluck('nm_menu','id')->toArray();
		$data['content'] = AdminMenu::find($id);
		return view('pages.adminMenu.edit',compact('data'));
	}

	public function update(AdminMenuRequest $request, $id)
	{
		$data = AdminMenu::find($id);
		$data->update($request->all());
	        $log = new AdminController;
		$log->getLogHistory('Update Admin Menu with ID '.$id);
		return redirect()->route('admin.adminmenu.index');
	}

	public function destroy($id)
	{
		$data=AdminMenu::find($id);
		$data->roles()->detach();
		$data->delete();
	        $log = new AdminController;
		$log->getLogHistory('Delete Admin Menu with ID '.$id);
		return redirect()->route('admin.adminmenu.index');;
	}
}
