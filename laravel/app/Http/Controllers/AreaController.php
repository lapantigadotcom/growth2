<?php
/*
PT. Trikarya Teknologi Indonesia
Tenggilis raya 127
Office Complex Apartment Metropolis MKB 206
Surabaya, Jawa timur, Indonesia
Phone : +6231-8420384 / +6281235537717
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use App\Area;
use App\Kota;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;
use App\Http\Requests\AreaRequest;
use Illuminate\Http\Request;

class AreaController extends Controller
{
    public function index()
    {
        $data['all'] = DB::table('kota')
        ->select('kota.id','kota.nm_kota','kota.kd_area','area.kd_area','area.nm_area')
        ->leftJoin('area', 'kota.kd_area','=','area.id')
        ->orderBy('id', 'desc')
        ->get();

        $data['area'] = Area::all();
        return view('pages.divisi.index', compact('data'));
    }

    public function create()
    {
        return view('pages.area.create');
    }

    public function store(AreaRequest $request)
    {
        Area::create($request->all());
        $log = new AdminController;
        $log->getLogHistory('Make New Area');
        return redirect()->route('admin.area.index');
    }

    public function edit($id)
    {
        $data['content'] = Area::find($id);
        return view('pages.area.edit', compact('data'));
    }

    public function update(AreaRequest $request, $id)
    {
        $data = Area::find($id);
        $data->update($request->all());
        $log = new AdminController;
        $log->getLogHistory('Update Area with ID '.$id);
        return redirect()->route('admin.area.index');
    }

    public function destroy($id)
    {
        $data =Area::find($id);
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Area with ID '.$id);
        return redirect()->route('admin.area.index');
        
    }

    
}
