<?php
/*
PT. Trikarya Teknologi Indonesia
Tenggilis raya 127
Office Complex Apartment Metropolis MKB 206
Surabaya, Jawa timur, Indonesia
Phone : +6231-8420384 / +6281235537717
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Input;
use Validator;
use App\PhotoActivity;
use App\Outlet;
use App\Competitor;
use App\User;
use File;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompetitorRequest;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;

class CompetitorController extends Controller
{
    public function index()
    {
        $data = Competitor::all();
        return view('pages.comp.index', compact('data'));
    }

    public function create()
    {
        return view('pages.comp.create');
    }

    public function store(CompetitorRequest $request)
    {
        $rules = array(
            'nm_competitor' => 'required',    
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return view('pages.comp.create')->withErrors($validator);

        } else {
            $comp= new Competitor;
            $comp->nm_competitor = $request->input('nm_competitor');
            $comp->save();        
            $log = new AdminController;
       	    $log->getLogHistory('Make New Competitor');
            return redirect()->route('admin.comp.index');
        } 
    }

    public function show($id)
    {
        $page = 'show';
        $data['content'] = PhotoActivity::find($id);       
        return view('pages.comp.'.$page,compact('data'));
    }

    public function edit($id)
    {
        $data['content'] = Competitor::find($id);
        return view('pages.comp.edit')->with('data',$data);
    }

    public function update(CompetitorRequest $request, $id)
    {
        $comp = Competitor::find($id); 
        $comp->update($request->all());
 	    $log = new AdminController;
        $log->getLogHistory('Update Competitor with ID '.$id);
        return redirect()->route('admin.comp.index');

    }

    public function destroy($id)
    {
        $data = Competitor::find($id);
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Competitor with ID '.$id);
        return redirect()->route('admin.comp.index');  
    }

    
}
