<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Sessions;
use Input;
use Validator;
use Redirect;

use App\BlacklistApp;
use App\Konfigurasi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlacklistAppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apps = BlacklistApp::all();
        $allStatus = array(
            '0' => 'Belum Aktif',
            '1' => 'Aktif'
        );
        return view('pages.blacklistApp.index', [
            'allStatus' => $allStatus,
            'apps' => $apps,

        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.blacklistApp.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'nm_app'  => 'required|unique:blacklist_app',
        );

        $messages = array(
            'nm_app.unique'      => 'Nama Aplikasi has already been taken!',
            'nm_app.required'    => 'Nama Aplikasi field is required'
        );

        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            $data = $this->create();
            return $data->withInput(Input::all())->withErrors($validator);
        }else{
            $data = new blacklistApp;
            $data->nm_app    =$request->input('nm_app');
            $data->save();

            $log = new AdminController;
            $log->getLogHistory('Make New Blacklist App');
            return redirect()->route('admin.blacklistApp.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data= BlacklistApp::find($id);
        return view('pages.blacklistApp.edit', [
            'blacklistApp' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'nm_app'  => 'required',
        );

        $messages = array(
            'nm_app.required'    => 'Nama Aplikasi field is required'
        );

        $validator = Validator::make(Input::all(), $rules, $messages);

        if ($validator->fails()) {
            $data = $this->create();
            
            $blacklistApp= BlacklistApp::find($id);
            return $data->withInput(Input::all())->withErrors($validator)->with('blacklistApp', $blacklistApp);
        }else{
            $data= blacklistApp::find($id);
            $data->nm_app    =$request->input('nm_app');
            $data->save();

            $log = new AdminController;
            $log->getLogHistory('Update BlackList App with ID '.$id);
            return redirect()->route('admin.blacklistApp.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = BlacklistApp::find($id);
        $data->delete();

        $log = new AdminController;
        $log->getLogHistory('Delete Blacklist App with ID '.$id);
        return redirect()->route('admin.blacklistApp.index');
    }
}
