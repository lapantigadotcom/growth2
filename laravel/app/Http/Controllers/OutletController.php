<?php
/*
PT. Trikarya Teknologi Indonesia
Tenggilis raya 127
Office Complex Apartment Metropolis MKB 206
Surabaya, Jawa timur, Indonesia
Phone : +6231-8420384 / +6281235537717
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Sessions;
use Input;
use Validator;
use Redirect;

use App\Outlet;
use App\Distributor;
use App\Area;
use App\Kota;
use App\Tipe;
use App\User;
use App\Konfigurasi;
use App\Http\Controllers\Controller;
use App\Http\Requests\OutletRequest;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Datatables;
use File;

class OutletController extends Controller
{

    public function maps(Request $request){
        $sales = $request->input('sales');
        $area = $request->input('kd_area');
        $tipe = $request->input('nm_tipe');
        $dist = $request->input('kd_dist');
        $osales=$oarea=$otipe=$odist='=';


        if($sales==''){
            $sales='null';
            $osales='!=';
        }
        if($area==''||$area=='100'){
            $area='null';
            $oarea='!=';
        }
        if($tipe==''){
            $tipe='null';
            $otipe='!=';
        }
        if($dist==''){
            $dist='null';
            $odist='!=';
        }


        if(Auth::user()->kd_area == 100){
            $marker = Outlet::select('user.nama',
                'tipe.nm_tipe',
                'outlet.nm_outlet',
                'distributor.kd_dist',
                'outlet.latitude',
                'outlet.longitude')
            ->join('tipe', 'tipe.id','=', 'outlet.kd_tipe')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->join('distributor', 'distributor.id', '=', 'outlet.kd_dist')
            ->join('area', 'area.id', '=', 'outlet.kd_area')
            ->where('user.id', $osales, $sales)
            ->where('outlet.kd_area', $oarea, $area)
            ->where('outlet.kd_tipe', $otipe, $tipe)
            ->where('outlet.kd_dist', $odist, $dist)
            ->get();
        }
        else if(Auth::user()->kd_role == 3){
            $id =Auth::user()->id;
            $marker = Outlet::select('user.nama',
                'tipe.nm_tipe',
                'outlet.nm_outlet',
                'distributor.kd_dist',
                'outlet.latitude',
                'outlet.longitude')
            ->join('tipe', 'tipe.id','=', 'outlet.kd_tipe')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->join('distributor', 'distributor.id', '=', 'outlet.kd_dist')
            ->join('area', 'area.id', '=', 'outlet.kd_area')
            ->where('outlet.kd_user','=',$id)
            ->where('outlet.kd_area', $oarea, $area)
            ->where('outlet.kd_tipe', $otipe, $tipe)
            ->where('outlet.kd_dist', $odist, $dist)
            ->get();
        }
        else
        {
            $area =Auth::user()->kd_area;
            $marker = Outlet::select('user.nama',
                'tipe.nm_tipe',
                'outlet.nm_outlet',
                'distributor.kd_dist',
                'outlet.latitude',
                'outlet.longitude')
            ->join('tipe', 'tipe.id','=', 'outlet.kd_tipe')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->join('distributor', 'distributor.id', '=', 'outlet.kd_dist')
            ->join('area', 'area.id', '=', 'outlet.kd_area')
            ->where('user.id', $osales, $sales)
            ->where('outlet.kd_area','=',$area)
            ->where('outlet.kd_tipe', $otipe, $tipe)
            ->where('outlet.kd_dist', $odist, $dist)
            ->get();
        }
        return view('pages.outlet.xml',['markers'=> $marker]);

    }

    public function formMap(Request $request){
        $area = Auth::user()->kd_area;
        if($area==100)
        {
            $data['areas'] = [''=>'All Area'] + Area::pluck('kd_area', 'id')->toArray();
            $data['sales'] =  [''=>'All Sales'];
        }
        else
        {
            $data['areas'] =  Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
            $data['sales'] =  [''=>'All Sales']+User::where('kd_area','=',$area)->where('kd_role', '=','3')->pluck('nama', 'id')->toArray();
        }

        $data['tipe'] =  [''=>'All Tipe'] + Tipe::pluck( 'nm_tipe','id')->toArray();
        $data['distributor'] =  [''=>'All Distributor'] + Distributor::pluck('kd_dist','id' )->toArray();

        $data['map'] = $this->maps($request);

        $data['csales'] = $request->input('sales');
        $data['carea'] = $request->input('kd_area');
        if($data['carea']!='' && $data['carea']!=100 ){
            $data['sales']=  [''=>'All Sales']+User::where('kd_area','=',$data['carea'])->where('kd_role', '=','3')->pluck('nama', 'id')->toArray();
        }
        $data['ctipe'] = $request->input('nm_tipe');
        $data['cdist'] = $request->input('kd_dist');
        return view('pages.outlet.map',compact('data'));
    }

     public function index()
    {
        $area = Auth::user()->kd_area;
        if($area==100)
        {
          $data['areas'] = [''=>'All Area'] + Area::pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'All Sales'];
        }
        else
        {
          $data['areas'] =  Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'All Sales']+User::where('kd_area','=',$area)->where('kd_role', '=','3')->pluck('nama', 'id')->toArray();
        }

        $data['tipe'] =  [''=>'All Tipe'] + Tipe::pluck( 'nm_tipe','id')->toArray();
        $data['distributor'] =  [''=>'All Distributor'] + Distributor::pluck('kd_dist','id' )->toArray();

        return view('pages.outlet.index')->with('data',$data);
     }

    public function ajax(Request $request)
    {
        
        $area = $request->input('area');
        $sales = $request->input('sales');
        $tipe = $request->input('tipe');
        $dist = $request->input('dist');
        $osales=$oarea=$otipe=$odist='=';

        if($sales==''){
          $sales='null';
          $osales='<>';
        }
        if($area==''||$area=='100'){
          $area='null';
          $oarea='<>';
        }
        if($tipe==''){
          $tipe='null';
          $otipe='<>';
        }
        if($dist==''){
          $dist='null';
          $odist='<>';
        }
        
        if(Auth::user()->kd_role == 1 || Auth::user()->kd_area == 100)
        {
            $data = DB::table('outlet')
            ->select('outlet.kd_outlet',
                'outlet.kode',
                'outlet.kd_dist',
                'outlet.nm_outlet',
                'outlet.almt_outlet',
                'outlet.nm_pic',
                'user.nama',
                'outlet.tlp_pic',
                'tipe.nm_tipe',
                'area.kd_area',
                'distributor.kd_dist',
                'kota.nm_kota')
            ->where('outlet.kd_area',$oarea, $area)
            ->where('user.id', $osales, $sales)
            ->where('outlet.kd_tipe', $otipe, $tipe)
            ->where('outlet.kd_dist', $odist, $dist)
            ->leftJoin('area', 'area.id', '=', 'outlet.kd_area')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->leftJoin('user', 'user.id', '=', 'outlet.kd_user')
            ->leftJoin('tipe', 'tipe.id', '=', 'outlet.kd_tipe')
            ->leftJoin('distributor', 'distributor.id', '=', 'outlet.kd_dist');
        }
        else if(Auth::user()->kd_role == 3)
        {
            $id =Auth::user()->id;
            $data = DB::table('outlet')
            ->select('outlet.kd_outlet',
                'outlet.kode',
                'outlet.kd_dist',
                'outlet.nm_outlet',
                'outlet.almt_outlet',
                'outlet.nm_pic',
                'outlet.tlp_pic',
                'user.nama',
                'tipe.nm_tipe',
                'area.kd_area',
                'distributor.kd_dist',
                'kota.nm_kota')
            ->where('outlet.kd_area',$oarea, $area)
            ->where('user.id', $osales, $sales)
            ->where('outlet.kd_tipe', $otipe, $tipe)
            ->where('outlet.kd_dist', $odist, $dist)
            ->leftJoin('area', 'area.id', '=', 'outlet.kd_area')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->leftJoin('user', 'user.id', '=', 'outlet.kd_user')
            ->leftJoin('tipe', 'tipe.id', '=', 'outlet.kd_tipe')
            ->leftJoin('distributor', 'distributor.id', '=', 'outlet.kd_dist')
            ->where('outlet.kd_user','=',$id);
        }
        else
        {
            $area =Auth::user()->kd_area;
            $data = DB::table('outlet')
            ->select('outlet.kd_outlet',
                'outlet.kode',
                'outlet.kd_dist',
                'outlet.nm_outlet',
                'outlet.almt_outlet',
                'outlet.nm_pic',
                'outlet.tlp_pic',
                'user.nama',
                'tipe.nm_tipe',
                'area.kd_area',
                'distributor.kd_dist',
                'kota.nm_kota')
            ->where('outlet.kd_area','=',$area)
            ->where('user.id', $osales, $sales)
            ->where('outlet.kd_tipe', $otipe, $tipe)
            ->where('outlet.kd_dist', $odist, $dist)
            ->leftJoin('area', 'area.id', '=', 'outlet.kd_area')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->leftJoin('user', 'user.id', '=', 'outlet.kd_user')
            ->leftJoin('tipe', 'tipe.id', '=', 'outlet.kd_tipe')
            ->leftJoin('distributor', 'distributor.id', '=', 'outlet.kd_dist');
        }
       
        return Datatables::of($data)->make(true);
    }


    public function create()
    {
        if(Auth::user()->kd_role == 1 || Auth::user()->kd_area == 100)
        {
            $dist = [''=>''] + Distributor::pluck('nm_dist', 'id')->toArray();
            $areas = [''=>''] + Area::pluck('kd_area', 'id')->toArray();
            $users = [''=>''] + User::where('kd_role','=','3')->pluck('nama', 'id')->toArray();
            $tipe = [''=>''] + Tipe::pluck('nm_tipe', 'id')->toArray();
        }
        else
        {
            $dist = [''=>''] + Distributor::pluck('nm_dist', 'id')->toArray();
            $areas = [''=>''] + Area::where('id','=',Auth::user()->kd_area)->pluck('kd_area', 'id')->toArray();
            $users = [''=>''] + User::where('kd_role','=','3')->where('kd_area','=',Auth::user()->kd_area)->pluck('nama', 'id')->toArray();
            $tipe = [''=>''] + Tipe::pluck('nm_tipe', 'id')->toArray();
        }
        return view('pages.outlet.create')->with('dist',$dist)->with('areas',$areas)->with('users',$users)->with('tipe',$tipe);
    }

    public function store(OutletRequest $request)
    {
        $rules = array(
                'kode'              => 'required|unique:outlet',
                'nm_outlet'         => 'required|unique:outlet',
                'kd_tipe'           => 'required',
                'kd_user'           => 'required',
                'almt_outlet'       => 'required',
                'kd_dist'           => 'required',
        );

        $messages = array(
                'kode.unique'           => 'Kode Outlet Distributor has already been taken!',
                'nm_outlet,unique'      => 'Nama Outlet has already been taken!',
                'kode.required'         => 'Kode Outlet Distributor field is required',
                'nm_outlet.required'    => 'Nama Outlet field is required',
                'kd_tipe.required'      => 'Tipe Outlet field is required',
                'kd_user.required'      => 'Registered Sales field is required',
                'almt_outlet.required'  => 'Alamat Outlet field is required',
                'kd_dist.required'      => 'Outlet Distributor field is required',
        );

        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            $data = $this->create();
            return $data->withInput(Input::all())->withErrors($validator);

        } else {
            $data = new Outlet;
            $konfig = Konfigurasi::first();
            $data->kode         =$request->input('kode');
            $data->nm_outlet    =$request->input('nm_outlet');
            $data->almt_outlet  =$request->input('almt_outlet');
            $data->kd_area      =$request->input('kd_area');
            $data->kd_kota      =$request->input('kd_kota');
            $data->kodepos      =$request->input('kodepos');
            $data->kd_dist      =$request->input('kd_dist');
            $data->kd_user      =$request->input('kd_user');
            $data->nm_pic       =$request->input('nm_pic');
            $data->tlp_pic      =$request->input('tlp_pic');
            $data->kd_tipe      =$request->input('kd_tipe');
            $data->latitude     =$request->input('latitude');
            $data->longitude    =$request->input('longitude');
            $data->rank_outlet  ='A';
            $data->reg_status   ='1';
            $data->toleransi_long=$konfig->toleransi_max;
            $data->save();

            if(Input::hasFile('path_photo')){
                $file = $request->file('path_photo');
                $destinationPath = 'image_upload/outlet';
                $filename = date('YmdHis').'_'.$data->kd_outlet.'_'. $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $data->update(array('foto_outlet' => $filename));
            }

            $log = new AdminController;
            $log->getLogHistory('Make New Outlet');
            return redirect()->route('admin.outlet.index');
        }
    }

    public function show($kd_outlet)
    {
        $page = 'show';
        $data['content'] = DB::table('outlet')
        ->select('outlet.*',
            'tipe.nm_tipe',
            'area.kd_area',
            'area.nm_area',
            'kota.nm_kota',
            'user.nama',
            'distributor.nm_dist')
        ->leftJoin('area', 'area.id', '=', 'outlet.kd_area')
        ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
        ->leftJoin('distributor', 'distributor.id', '=', 'outlet.kd_dist')
        ->leftJoin('tipe', 'tipe.id', '=', 'outlet.kd_tipe')
        ->leftJoin('user', 'user.id', '=', 'outlet.kd_user')
        ->where('outlet.kd_outlet', '=', $kd_outlet)
        ->first();

        return view('pages.outlet.'.$page,compact('data'));
    }

    public function edit($kd_outlet)
    {
        $data['content'] = Outlet::find($kd_outlet);
        $dist = [''=>''] + Distributor::pluck('nm_dist', 'id')->toArray();
        $areas = [''=>''] + Area::pluck('kd_area', 'id')->toArray();
        $kodeArea = Outlet::select('kd_area')->where('kd_outlet','=',$kd_outlet)->first();
        $value = $kodeArea->kd_area;
        $users = [''=>''] + User::where('kd_role','=',3)->where('kd_area','=',$value)->pluck('nama', 'id')->toArray();
        $tipe = [''=>''] + Tipe::pluck('nm_tipe', 'id')->toArray();
        $city = [''=>''] + Kota::where('kd_area', '=', $value)->pluck('nm_kota', 'id')->toArray();

        return view('pages.outlet.edit')->with('data',$data)
        ->with('dist',$dist)
        ->with('areas',$areas)
        ->with('users',$users)
        ->with('tipe',$tipe)
        ->with('city',$city);
    }

    public function update(OutletRequest $request, $kd_outlet)
    {
        $data = Outlet::find($kd_outlet);
        $konfig = Konfigurasi::first();
        $data->kd_dist=$request->input('kd_dist');
        $data->kode=$request->input('kode');
        $data->kd_kota=$request->input('kd_kota');
        $data->kd_area=$request->input('kd_area');
        $data->kd_user=$request->input('kd_user');
        $data->nm_outlet=$request->input('nm_outlet');
        $data->almt_outlet=$request->input('almt_outlet');
        $data->kd_tipe=$request->input('kd_tipe');
        $data->rank_outlet=$request->input('rank_outlet');
        $data->kodepos=$request->input('kodepos');
        $data->reg_status=$request->input('reg_status');
        $data->latitude=$request->input('latitude');
        $data->longitude=$request->input('longitude');
        $data->nm_pic=$request->input('nm_pic');
        $data->tlp_pic=$request->input('tlp_pic');
        $data->toleransi_long=$konfig->toleransi_max;
        $data->save();
        if(!is_null($request->file('path_photo')))
        {
            $file = $request->file('path_photo');
            $destinationPath = 'image_upload/outlet';
            $filename = date('YmdHis').'_'.$data->kd_outlet.'_'. $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $data->update(array('foto_outlet' => $filename));
        }
        $log = new AdminController;
        $log->getLogHistory('Update Outlet with ID '.$kd_outlet);
        return redirect()->route('admin.outlet.index');
    }

    public function destroy($kd_outlet)
    {
        $data = Outlet::with('visitPlans', 'photoActivities')->find($kd_outlet);
        foreach($data->visitPlans as $row)
        {
            $row->delete();
        }
        foreach($data->photoActivities as $row)
        {
            $row->delete();
        }
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Outlet with ID '.$kd_outlet);
        return redirect()->route('admin.outlet.index');

    }

    public function formToleransi()
    {
        $data['tipe'] = Tipe::all();
        $data['konfigurasi'] = Konfigurasi::find(1);
        $data['dist'] = Distributor::all();
        return view('pages.outletManage.edit', compact('data'));
    }

    public function updateToleransi(OutletRequest $request)
    {
        $long = $request->input('toleransi_long');
        //$lat = $request->input('toleransi_lat');
        DB::table('outlet')->update(['toleransi_long' => $long]);
        DB::table('konfigurasi')->update(['toleransi_max' => $long]);
        $log = new AdminController;
        $log->getLogHistory('Update Toleransi Outlet');
        return redirect()->route('admin.outletManage.index');
    }

    public function updateTvisit(OutletRequest $request)
    {
        $visit = $request->all();
        foreach($visit as $key => $row){
            $id=substr($key, 13,1);
            $dist=Distributor::where('id',$id)->update(['target_visit'=>$row]);
        }
        //print_r($visit['target_visit_MUP']);
        //DB::table('konfigurasi')->update(['target_visit' => $visit]);
        $log = new AdminController;
        $log->getLogHistory('Update Target Visit');
        return redirect()->route('admin.outletManage.index');
    }

    public function updateTec(OutletRequest $request)
    {
        $visit = $request->all();
        foreach($visit as $key => $row){
            $id=substr($key, 10,1);
            $dist=Distributor::where('id',$id)->update(['target_ec'=>$row]);
        }
        $log = new AdminController;
        $log->getLogHistory('Update Target EC');
        return redirect()->route('admin.outletManage.index');
    }



}
