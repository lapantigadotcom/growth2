<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use DB;
use Auth;
use Session;
use Input;
use Excel;
use App\VisitPlan;
use App\Http\Requests\VisitPlanRequest;
use App\Http\Requests\SamplingRequest;
use App\Http\Requests\SampleRequest;
use App\Outlet;
use App\Kota;
use App\Area;
use App\Tipe;
use App\Sampling;
use App\Sample;
use App\Distributor;

class SamplingController extends Controller
{
  public function formSampling()
  {

    $area = Auth::user()->kd_area;
        if($area==100)
        {
          $data['areas'] = [''=>'All Area'] + Area::pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'All Sales'];
        }
        else
        {
          $data['areas'] =  Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'All Sales']+User::where('kd_area','=',$area)->where('kd_role', '=','3')->pluck('nama', 'id')->toArray();
        }

        $data['tipe'] =  [''=>'All Tipe'] + Tipe::pluck( 'nm_tipe','id')->toArray();
        $data['distributor'] =  [''=>'All Distributor'] + Distributor::pluck('kd_dist','id' )->toArray();

    if(Auth::user()->kd_area==100){
      $data['data'] = Sampling::select('outlet.nm_outlet', 'outlet.kd_outlet',
      'user.nama',
      'sample.nm_sample',
      'sampling.*')
      ->join('visit_plan', 'sampling.kd_visit','=','visit_plan.id')
      ->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
      ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
      ->leftJoin('sample', 'sample.id', '=', 'sampling.kd_sample')
      ->whereBetween('sampling.date_upload', [date('Y-m-d', strtotime("-7 day")), date('Y-m-d', strtotime("+1 day"))])
      ->orderBy('sampling.date_upload', 'desc')
      ->get();
      return view('pages.sampling.viewFormSampling')->with('data',$data);
    }
    else if(Auth::user()->kd_role == 3){
      $id = Auth::user()->id;
      $data['data'] = Sampling::select('outlet.nm_outlet', 'outlet.kd_outlet',
      'user.nama',
      'sample.nm_sample',
      'sampling.*')
      ->join('visit_plan', 'sampling.kd_visit','=','visit_plan.id')
      ->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
      ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
      ->leftJoin('sample', 'sample.id', '=', 'sampling.kd_sample')
      ->where('outlet.kd_user','=', $id)
      ->whereBetween('sampling.date_upload', [date('Y-m-d', strtotime("-7 day")), date('Y-m-d', strtotime("+1 day"))])
      ->orderBy('sampling.date_upload', 'desc')
      ->get();
      return view('pages.sampling.viewFormSampling')->with('data',$data);

    }
    else{
      $area = Auth::user()->kd_area;
      $data['data'] = Sampling::select('outlet.nm_outlet', 'outlet.kd_outlet',
      'user.nama',
      'sample.nm_sample',
      'sampling.*')
      ->join('visit_plan', 'sampling.kd_visit','=','visit_plan.id')
      ->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
      ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
      ->leftJoin('sample', 'sample.id', '=', 'sampling.kd_sample')
      ->where('outlet.kd_area','=', $area)
      ->whereBetween('sampling.date_upload', [date('Y-m-d', strtotime("-7 day")), date('Y-m-d', strtotime("+1 day"))])
      ->orderBy('sampling.date_upload', 'desc')
      ->get();
      return view('pages.sampling.viewFormSampling')->with('data',$data);
    }
  }


  public function index(SamplingRequest $request){
    $data['dateFrom'] = $request->input('dateFrom');
    $data['dateTo'] = $request->input('dateTo');
    $sales = $request->input('sales');
    $area = $request->input('kd_area');
    $tipe = $request->input('nm_tipe');
    $dist = $request->input('kd_dist');
    $osales=$oarea=$otipe=$odist='=';
    if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
    if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");
    if($sales==''){
      $sales='null';
      $osales='!=';
    }
    if($area==''||$area=='100'){
      $area='null';
      $oarea='!=';
    }
    if($tipe==''){
      $tipe='null';
      $otipe='!=';
    }
    if($dist==''){
      $dist='null';
      $odist='!=';
    }


    $dateFrom = date("Y-m-d",strtotime($data['dateFrom']));
    $dateTo =  date("Y-m-d", strtotime('+1 day', strtotime($data['dateTo'])));

    if(Input::get('show')){
      if(Auth::user()->kd_area==100){
        $data['data'] = Sampling::select('outlet.nm_outlet', 'outlet.kd_outlet',
        'user.nama',
       'sample.nm_sample',
      'sampling.*')
      ->join('visit_plan', 'sampling.kd_visit','=','visit_plan.id')
        ->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
        ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
        ->leftJoin('sample', 'sample.id', '=', 'sampling.kd_sample')
        ->where('user.id', $osales, $sales)
        ->where('outlet.kd_area', $oarea, $area)
        ->where('outlet.kd_tipe', $otipe, $tipe)
        ->where('outlet.kd_dist', $odist, $dist)
        ->whereBetween('sampling.date_upload',  [$dateFrom, $dateTo])
        ->orderBy('sampling.date_upload', 'desc')
        ->get();
        return view('pages.sampling.index')->with('data',$data);
      }
      else if(Auth::user()->kd_role ==3){
        $id = Auth::user()->id;
        $data['data'] = Sampling::select('outlet.nm_outlet', 'outlet.kd_outlet',
        'user.nama',
        'sample.nm_sample',
      'sampling.*')
      ->join('visit_plan', 'sampling.kd_visit','=','visit_plan.id')
        ->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
        ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
        ->leftJoin('sample', 'sample.id', '=', 'sampling.kd_sample')
        ->where('outlet.kd_user','=', $id)

        ->where('outlet.kd_area', $oarea, $area)
        ->where('outlet.kd_tipe', $otipe, $tipe)
        ->where('outlet.kd_dist', $odist, $dist)
        ->whereBetween('sampling.date_upload', [$dateFrom, $dateTo])
        ->orderBy('sampling.date_upload', 'desc')
        ->get();
        return view('pages.sampling.index')->with('data',$data);
      }
      else{
        $area = Auth::user()->kd_area;
        $data['data'] = Sampling::select('outlet.nm_outlet', 'outlet.kd_outlet',
        'user.nama',
        'sample.nm_sample',
      'sampling.*')
      ->join('visit_plan', 'sampling.kd_visit','=','visit_plan.id')
        ->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
        ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
        ->leftJoin('sample', 'sample.id', '=', 'sampling.kd_sample')
        ->where('outlet.kd_area','=', $area)
        ->where('user.id', $osales, $sales)

        ->where('outlet.kd_tipe', $otipe, $tipe)
        ->where('outlet.kd_dist', $odist, $dist)
        ->whereBetween('sampling.date_upload', [$dateFrom, $dateTo])
        ->orderBy('sampling.date_upload', 'desc')
        ->get();
        return view('pages.sampling.index')->with('data',$data);
      }
    }
    else if(Input::get('download')){
      $this->downloadSampling($sales, $osales, $tipe, $otipe, $area, $oarea, $dist, $odist, $dateFrom, $dateTo);
    }
  }

  public function edit($id){
    $data['content']=Sampling::find($id);
    $role = Auth::user()->kd_role;
    $area = Auth::user()->kd_area;
    $id= Auth::user()->id;
    $sample = Sample::pluck('nm_sample','id')->toArray();
    return view('pages.sampling.edit')->with('data',$data)->with('sample',$sample);
  }

  public function destroy($id)
  {
    $data = Sampling::find($id);

    $data->delete();
    $log = new AdminController;
    $log->getLogHistory('Delete Sampling with ID'.$id);
    return redirect()->route('admin.sampling.form');

  }
  public function store(SamplingRequest $request)
  {
    $log = new AdminController;
    Sampling::create($request->all());
    $log->getLogHistory('Make Sampling');
    return redirect()->route('admin.sampling.form');
  }

  public function update(SamplingRequest $request, $id)
  {
    $data = Sampling::find($id);

    $data->update($request->all());

    $log = new AdminController;
    $log->getLogHistory('Update Sampling with ID'.$id);
    return redirect()->route('admin.sampling.form');
  }



  public function downloadSampling($sales, $osales, $tipe, $otipe, $area, $oarea, $dist, $odist, $dateFrom, $dateTo)
  {
    $log = new AdminController;
    $log->getLogHistory('Download Data Sampling');

    $role = Auth::user()->kd_role;
    $areas = Auth::user()->kd_area;
    if($areas == 100)
    {
      ob_end_clean();
      ob_start();
      $sampling = Sampling::select(DB::raw("
      user.nama as SALES,
      sampling.kd_sampling as KODE_SAMPLING,
      sampling.kd_visit as KODE_VISIT,
      visit_plan.kd_outlet as KODE_OUTLET,
      outlet.nm_outlet as NAMA_OUTLET,
      tipe.nm_tipe as TIPE_OUTLET,
      sample.nm_sample as PRODUK_SAMPLE,
      sampling.qty as QTY,
      sampling.qty_l as L,
      sampling.qty_p as P,
      sampling.date_upload as DATE_UPLOAD
      ")
      )
      ->join('visit_plan', 'sampling.kd_visit','=','visit_plan.id')
      ->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
      ->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')
      ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
      ->leftJoin('sample', 'sample.id', '=', 'sampling.kd_sample')
      ->where('user.id', $osales, $sales)
      ->where('outlet.kd_area', $oarea, $area)
      ->where('outlet.kd_tipe', $otipe, $tipe)
      ->where('outlet.kd_dist', $odist, $dist)
      ->whereBetween('sampling.date_upload',  [$dateFrom, $dateTo])
      ->orderBy('sampling.date_upload', 'desc')
      ->get();

    }
    else if(Auth::user()->kd_role == 3){
      ob_end_clean();
      ob_start();
      $id =Auth::user()->id;
      $sampling = Sampling::select(DB::raw("
      user.nama as SALES,
      sampling.kd_sampling as KODE_SAMPLING,
      sampling.kd_visit as KODE_VISIT,
      visit_plan.kd_outlet as KODE_OUTLET,
      outlet.nm_outlet as NAMA_OUTLET,
      tipe.nm_tipe as TIPE_OUTLET,
      sample.nm_sample as PRODUK_SAMPLE,
      sampling.qty as QTY,
      sampling.qty_l as L,
      sampling.qty_p as P,
      sampling.date_upload as DATE_UPLOAD
      ")
      )
      ->join('visit_plan', 'sampling.kd_visit','=','visit_plan.id')
      ->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
      ->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')
      ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
      ->leftJoin('sample', 'sample.id', '=', 'sampling.kd_sample')
      ->where('user.id', '=', $id)
      ->where('outlet.kd_area', $oarea, $area)
      ->where('outlet.kd_tipe', $otipe, $tipe)
      ->where('outlet.kd_dist', $odist, $dist)
      ->whereBetween('sampling.date_upload',  [$dateFrom, $dateTo])
      ->orderBy('sampling.date_upload', 'desc')
      ->get();

    }
    else
    {
      ob_end_clean();
      ob_start();
      $sampling = Sampling::select(DB::raw("
      user.nama as SALES,
      sampling.kd_sampling as KODE_SAMPLING,
      sampling.kd_visit as KODE_VISIT,
      visit_plan.kd_outlet as KODE_OUTLET,
      outlet.nm_outlet as NAMA_OUTLET,
      tipe.nm_tipe as TIPE_OUTLET,
      sample.nm_sample as PRODUK_SAMPLE,
      sampling.qty as QTY,
      sampling.qty_l as L,
      sampling.qty_p as P,
      sampling.date_upload as DATE_UPLOAD
      ")
      )
      ->join('visit_plan', 'sampling.kd_visit','=','visit_plan.id')
      ->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
      ->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')
      ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
      ->leftJoin('sample', 'sample.id', '=', 'sampling.kd_sample')
      ->where('user.id', $osales, $sales)
      ->where('outlet.kd_area', '=', $areas)
      ->where('outlet.kd_tipe', $otipe, $tipe)
      ->where('outlet.kd_dist', $odist, $dist)
      ->whereBetween('sampling.date_upload',  [$dateFrom, $dateTo])
      ->orderBy('sampling.date_upload', 'desc')
      ->get();

    }
     date_default_timezone_set("Asia/Jakarta");
      $d= strtotime("now");
      $time =date("d-m-Y H:i:s", $d);
      $filename = $time."_SAMPLING";
      Excel::create($filename, function($excel) use($sampling) {
        $excel->sheet('Sampling', function($sheet) use($sampling) {
          $sheet->fromArray($sampling);
        });
      })->download('xls');

  }
}
