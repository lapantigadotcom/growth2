<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Excel;
use Input;
use Validator;
use App\PhotoActivity;
use App\Outlet;
use App\Competitor;
use App\User;
use App\TipePhoto;
use App\Area;
use App\Tipe;
use App\Distributor;
use File;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;
use App\Http\Requests\PhotoRequest;
use Illuminate\Http\Request;

class PhotoActivityController extends Controller
{
  public function formPhoto()
  {
    $area = Auth::user()->kd_area;
    if($area==100)
    {
      $data['areas'] = [''=>'All Area'] + Area::pluck('kd_area', 'id')->toArray();
      $data['sales'] =  [''=>'All Sales'];
    }
    else
    {
      $data['areas'] =  Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
      $data['sales'] =  [''=>'All Sales']+User::where('kd_area','=',$area)->where('kd_role', '=','3')->pluck('nama', 'id')->toArray();
    }

    $data['tipe'] =  [''=>'All Tipe'] + Tipe::pluck( 'nm_tipe','id')->toArray();
    $data['distributor'] =  [''=>'All Distributor'] + Distributor::pluck('kd_dist','id' )->toArray();
    if(Auth::user()->kd_area == 100){
      $data['content'] = PhotoActivity::select('photo_activity.*',
        'outlet.nm_outlet',
        'tp_photo.nama_tipe',
        'user.nama'
        )
      ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
      ->join('user','user.id','=','outlet.kd_user')
      ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.kd_jenis')

      ->whereBetween('photo_activity.date_upload_photo',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
      ->groupBy('photo_activity.id')
      ->orderBy('photo_activity.date_upload_photo', 'desc')
      ->get();
    }
    else if(Auth::user()->kd_role == 3){
      $id =Auth::user()->id;
      $data['content'] = PhotoActivity::select('photo_activity.*',
        'outlet.nm_outlet',
        'tp_photo.nama_tipe',
        'user.nama'
        )
      ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
      ->join('user','user.id','=','outlet.kd_user')
      ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.kd_jenis')
      ->where('outlet.kd_user','=',$id)
      ->whereBetween('photo_activity.date_upload_photo',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
      ->groupBy('photo_activity.id')
      ->orderBy('photo_activity.date_upload_photo', 'desc')
      ->get();
    }
    else {
      $area =Auth::user()->kd_area;
      $data['content']= PhotoActivity::select('photo_activity.*',
        'outlet.nm_outlet',
        'tp_photo.nama_tipe',
        'user.nama'
        )
      ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
      ->join('user','user.id','=','outlet.kd_user')
      ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.kd_jenis')
      ->where('outlet.kd_area','=',$area)
      ->whereBetween('photo_activity.date_take_photo',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
      ->groupBy('photo_activity.id')
      ->orderBy('photo_activity.date_take_photo', 'desc')
      ->get();
    }
    return view('pages.photo.photo')->with('data',$data);
    
  }

  public function index(PhotoRequest $request)
  {
    $data['dateFrom'] = $request->input('dateFrom');
    $data['dateTo'] = $request->input('dateTo');
    $sales = $request->input('sales');
    $area = $request->input('kd_area');
    $tipe = $request->input('nm_tipe');
    $dist = $request->input('kd_dist');
    $osales=$oarea=$otipe=$odist='=';

    if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
    if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");
    if($sales==''){
      $sales='null';
      $osales='!=';
    }
    if($area==''||$area=='100'){
      $area='null';
      $oarea='!=';
    }
    if($tipe==''){
      $tipe='null';
      $otipe='!=';
    }
    if($dist==''){
      $dist='null';
      $odist='!=';
    }


    $dateFrom = date("Y-m-d",strtotime($data['dateFrom']));
    $dateTo =  date("Y-m-d", strtotime('+1 day', strtotime($data['dateTo'])));
    if(Input::get('show')){
      if(Auth::user()->kd_area == 100){
        $data['content'] = PhotoActivity::select('photo_activity.*',
          'outlet.nm_outlet',
          'tp_photo.nama_tipe',
          'user.nama'
          )
        ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
        ->join('user','user.id','=','outlet.kd_user')
        ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.kd_jenis')
        ->where('user.id', $osales, $sales)
        ->where('outlet.kd_area', $oarea, $area)
        ->where('outlet.kd_tipe', $otipe, $tipe)
        ->where('outlet.kd_dist', $odist, $dist)
        ->whereBetween('photo_activity.date_take_photo',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
        ->groupBy('photo_activity.id')
        ->orderBy('photo_activity.date_take_photo', 'desc')
        ->get();
      }
      else if(Auth::user()->kd_role == 3){
        $id =Auth::user()->id;
        $data['content'] = PhotoActivity::select('photo_activity.*',
          'outlet.nm_outlet',
          'tp_photo.nama_tipe',
          'user.nama'
          )
        ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
        ->join('user','user.id','=','outlet.kd_user')
        ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.kd_jenis')


        ->where('outlet.kd_area', $oarea, $area)
        ->where('outlet.kd_tipe', $otipe, $tipe)
        ->where('outlet.kd_dist', $odist, $dist)
        ->where('outlet.kd_user','=',$id)
        ->whereBetween('photo_activity.date_upload_photo',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
        ->groupBy('photo_activity.id')
        ->orderBy('photo_activity.date_upload_photo', 'desc')
        ->get();
      }
      else
      {
        $area =Auth::user()->kd_area;
        $data['content'] = PhotoActivity::select('photo_activity.*',
          'outlet.nm_outlet',
          'tp_photo.nama_tipe',

          'user.nama'
          )
        ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
        ->join('user','user.id','=','outlet.kd_user')
        ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.kd_jenis')
        ->where('outlet.kd_area','=',$area)
        ->where('user.id', $osales, $sales)

        ->where('outlet.kd_tipe', $otipe, $tipe)
        ->where('outlet.kd_dist', $odist, $dist)

        ->whereBetween('photo_activity.date_upload_photo',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
        ->groupBy('photo_activity.id')
        ->orderBy('photo_activity.date_upload_photo', 'desc')
        ->get();
      }

      return view('pages.photo.index')->with('data',$data);
    }
    else if(Input::get('download')){
      $this->downloadPhoto($sales, $osales, $tipe, $otipe, $area, $oarea, $dist, $odist, $dateFrom, $dateTo);
    }
  }


  public function create()
  {
    $role = Auth::user()->kd_role;
    $area = Auth::user()->kd_area;
    $id = Auth::user()->id;
    if($area == 100){
      $outlets = [''=>''] + Outlet::pluck('nm_outlet', 'kd_outlet')->toArray();
    }
    else if ($role == 3) {
      $outlets = [''=>''] + Outlet::where('kd_user','=',$id)
      ->pluck('nm_outlet', 'kd_outlet')
      ->toArray();
    }
    else {
      $outlets = [''=>''] + Outlet::where('kd_area','=',$area)
      ->pluck('nm_outlet', 'kd_outlet')
      ->toArray();
    }
    $competitors = [''=>''] + Competitor::pluck('nm_competitor', 'id')->toArray();
    $users = [''=>''] + User::where('kd_role','=','3')->pluck('nama', 'id')->toArray();
    $tipe = [''=>''] + TipePhoto::pluck('nama_tipe', 'id')->toArray();
    return view('pages.photo.create')->with('outlets',$outlets)->with('competitors',$competitors)->with('users',$users)->with('tipe',$tipe);
  }

  public function store(PhotoRequest $request)
  {
    $rules = array(
      'nm_photo'         => 'required',
      'path_photo'	=> 'required',
      );

    $validator = Validator::make(Input::all(), $rules);
    if ($validator->fails()) {

      $messages = $validator->messages();

      $outlets = [''=>''] + Outlet::pluck('nm_dist', 'id')->toArray();
      $competitors = [''=>''] + Competitor::pluck('kd_area', 'id')->toArray();
      $users = [''=>''] + User::where('kd_role','=','3')->pluck('nama', 'id')->toArray();

      return view('pages.photo.create')->with('outlets',$outlets)
      ->with('competitors',$competitors)
      ->with('users',$users)
      ->withErrors($validator);

    } else {
      $photoAct = new PhotoActivity;

      $file = $request->file('path_photo');
      $destinationPath = 'image_upload/outlet_photoact';
      $filename = str_random(6).'_'.$file->getClientOriginalName();
      $file->move($destinationPath, $filename);

      $photoAct->kd_outlet = $request->input('kd_outlet');
      $photoAct->nm_photo = $request->input('nm_photo');
      $photoAct->jenis_photo = $request->input('jenis_photo');
      $photoAct->date_take_photo = $request->input('date_take_photo');
      $photoAct->date_upload_photo = $request->input('date_upload_photo');
      $photoAct->keterangan = $request->input('keterangan');
      $photoAct->path_photo = $filename;
      $photoAct->save();
      $log = new AdminController;
      $log->getLogHistory('Make New Photo Display');
      return redirect()->route('admin.photoAct.form');
    }
  }

  public function edit($id)
  {
    $data['content'] = PhotoActivity::find($id);

    $role = Auth::user()->kd_role;
    $area = Auth::user()->kd_area;
    $id = Auth::user()->id;

    $tipe = [''=>''] + TipePhoto::pluck('nama_tipe', 'id')->toArray();

    return view('pages.photo.edit')->with('data',$data)->with('tipe',$tipe);
  }

  public function update(PhotoRequest $request, $id)
  {
    $photoAct = PhotoActivity::find($id);

      $photoAct->update($request->all());

    if(Input::hasFile('path_photo')){
      File::delete('image_upload/outlet_photoact/'.$photoAct->path_photo);
      $file = $request->file('path_photo');
      $destinationPath = 'image_upload/outlet_photoact';
      $filename = str_random(6).'_'.$file->getClientOriginalName();
      $file->move($destinationPath, $filename);
      $photoAct->path_photo=$filename;
    $photoAct->save();
    }

     $log = new AdminController;
      $log->getLogHistory('Update Photo Display with ID'.$id);

      return redirect()->route('admin.photoAct.form');

  }

  public function destroy($id)
  {
    $data = PhotoActivity::find($id);
    $data->delete();
    $log = new AdminController;
    $log->getLogHistory('Delete Photo Display with ID'.$id);
    return redirect()->route('admin.photoAct.form');
  }
  public function downloadPhoto($sales, $osales, $tipe, $otipe, $area, $oarea, $dist, $odist, $dateFrom, $dateTo)
  {
    $log = new AdminController;
    $log->getLogHistory('Download Data Photo Display');

    $role = Auth::user()->kd_role;
    $areas = Auth::user()->kd_area;
    if($areas == 100)
    {
      ob_end_clean();
      ob_start();
      $photo = PhotoActivity::select(DB::raw("
        user.nama as SALES,
        photo_activity.kd_outlet as KD_OUTLET,
        outlet.nm_outlet as OUTLET,
        tipe.nm_tipe as TIPE_OUTLET,
        tp_photo.nama_tipe as TIPE,
        photo_activity.nm_photo as NM_PHOTO,
        photo_activity.note as NOTE,
        photo_activity.date_take_photo as DATE_TAKE,
        photo_activity.date_upload_photo as DATE_UPLOAD,
        photo_activity.path_photo as PATH_PHOTO
        ")
      )
      ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
      ->join('user','user.id','=','outlet.kd_user')
      ->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')
      ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.kd_jenis')
      ->where('user.id', $osales, $sales)
      ->where('outlet.kd_area', $oarea, $area)
      ->where('outlet.kd_tipe', $otipe, $tipe)
      ->where('outlet.kd_dist', $odist, $dist)
      ->whereBetween('photo_activity.date_upload_photo',[$dateFrom, $dateTo])
      ->groupBy('photo_activity.id')
      ->orderBy('photo_activity.date_upload_photo', 'desc')
      ->get();

    }
    else if(Auth::user()->kd_role == 3){

      $id =Auth::user()->id;
      $photo = PhotoActivity::select(DB::raw("
        user.nama as SALES,
        photo_activity.kd_outlet as KD_OUTLET,
        outlet.nm_outlet as OUTLET,
        tipe.nm_tipe as TIPE_OUTLET,
        tp_photo.nama_tipe as TIPE,
        photo_activity.nm_photo as NM_PHOTO,
        photo_activity.note as NOTE,
        photo_activity.date_take_photo as DATE_TAKE,
        photo_activity.date_upload_photo as DATE_UPLOAD,
        photo_activity.path_photo as PATH_PHOTO
        ")
      )
      ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')

      ->join('user','user.id','=','outlet.kd_user')
      ->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')
      ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.kd_jenis')
      ->where('user.id', '=', $id)
      ->where('outlet.kd_area', $oarea, $area)
      ->where('outlet.kd_tipe', $otipe, $tipe)
      ->where('outlet.kd_dist', $odist, $dist)
      ->whereBetween('photo_activity.date_upload_photo',[$dateFrom, $dateTo])
      ->groupBy('photo_activity.id')
      ->orderBy('photo_activity.date_upload_photo', 'desc')
      ->get();


    }
    else
    {

      $photo = PhotoActivity::select(DB::raw("
        user.nama as SALES,
        photo_activity.kd_outlet as KD_OUTLET,
        outlet.nm_outlet as OUTLET,
        tipe.nm_tipe as TIPE_OUTLET,
        tp_photo.nama_tipe as TIPE,
        photo_activity.nm_photo as NM_PHOTO,
        photo_activity.note as NOTE,
        photo_activity.date_take_photo as DATE_TAKE,
        photo_activity.date_upload_photo as DATE_UPLOAD,
        photo_activity.path_photo as PATH_PHOTO
        ")
      )
      ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')

      ->join('user','user.id','=','outlet.kd_user')
      ->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')
      ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.kd_jenis')
      ->where('user.id', $osales, $sales)
      ->where('outlet.kd_area', '=', $areas)
      ->where('outlet.kd_tipe', $otipe, $tipe)
      ->where('outlet.kd_dist', $odist, $dist)
      ->whereBetween('photo_activity.date_upload_photo',[$dateFrom, $dateTo])
      ->groupBy('photo_activity.id')
      ->orderBy('photo_activity.date_upload_photo', 'desc')
      ->get();


    }
    ob_end_clean();
    ob_start();
    date_default_timezone_set("Asia/Jakarta");
    $d= strtotime("now");
    $time =date("d-m-Y H:i:s", $d);
    $filename = $time."_PHOTO_ACT";
    Excel::create($filename, function($excel) use($photo) {
      $excel->sheet('Photo Activity', function($sheet) use($photo) {
        $sheet->fromArray($photo);
      });
    })->download('xls');
  }


}
