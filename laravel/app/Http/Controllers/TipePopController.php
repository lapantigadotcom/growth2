<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\TipePopRequest;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;
use App\TipePop;

class TipePopController extends Controller
{
    public function index()
    {
        $data = TipePop::all();
        return view('pages.tipePop.index', compact('data'));
    }

    public function create()
    {
        
        return view('pages.tipePop.create');
    }

    public function store(TipePopRequest $request)
    {
        TipePop::create($request->all());
        $log = new AdminController;
        $log->getLogHistory('Add Tipe POP');
        return redirect()->route('admin.tipePop.index');
    }

    public function edit($id)
    {
        $data['content'] = TipePop::find($id);
        return view('pages.tipePop.edit')->with('data',$data);
    }

    public function update(TipePopRequest $request, $id)
    {
        $tipePop = TipePop::find($id);       
        $tipePop->update($request->all());
        $log = new AdminController;
        $log->getLogHistory('Update Tipe POP with ID '.$id);
        return redirect()->route('admin.tipePop.index');
    }

    public function destroy($id)
    {
        $data = TipePop::find($id);
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Tipe POP with ID '.$id);
        return redirect()->route('admin.tipePop.index'); 
    }
}
