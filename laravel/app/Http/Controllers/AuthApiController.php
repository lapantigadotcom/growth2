<?php

namespace App\Http\Controllers;

use Auth;
use Input;

use App\User;
use App\Area;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\UserTransformer;

class AuthApiController extends Controller
{
    public function login(Request $request, User $user)
    {
    	if(!Auth::attempt(['username' => $request->username, 'password' => $request->password])){
    		$response = [
    			'status'	=> 'error',
			    'message' 	=> 'Your credential is wrong',
			    'data'		=> ''
    		];

    		return response()->json($response, 200);
    	}
    	$user = $user->find(Auth::user()->id);
    	$token = bcrypt($user->username.$user->password);
        $user->id_gcm = $request->id_gcm;
        $user->imei = $request->imei;
    	$user->api_token = $token;

    	if($user->save())
        {
            $data = $user;
            $data['token'] = $user->api_token;
            $area = Area::where('id','=',$data->kd_area)->first();
            $data['area_code'] = $area->kd_area;
        	$response = [
        		'status'	=> 'success',
        		'message'	=> 'login success',
                'data'      => $data
        	];

        	return response()->json($response, 200);
        }
    }

    //checking credential => success
    public function profile(User $user)
    {
        $user = $user->find(Auth::user()->id);
        return fractal()
            ->item($user)
            ->transformWith(new Usertransformer)
            ->toArray();
    }
}
