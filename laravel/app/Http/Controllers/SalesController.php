<?php
/*
PT. Trikarya Teknologi Indonesia
Tenggilis raya 127
Office Complex Apartment Metropolis MKB 206
Surabaya, Jawa timur, Indonesia
Phone : +6231-8420384 / +6281235537717
*/
namespace App\Http\Controllers;
use Stok;
use DB;
use Auth;
use Session;
use Validator;
use Redirect;
use Input;
use App\User;
use App\Role;
use App\Kota;
use App\Area;
use App\VisitPlan;
use App\TakeOrder;
use App\PhotoActivity;
use App\CompetitorActivity;
use App\Outlet;
use App\Logging;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Http\Controllers\AdminController;


class SalesController extends Controller
{
    public function index()
    {
       if(Auth::user()->kd_area == 100){
            $data=User::where('user.kd_role','=','3')
            ->select('user.id','user.nik','user.nama','user.id_gcm','user.alamat','user.telepon','user.join_date','area.kd_area','role.type_role')
            ->leftJoin('area','area.id','=','user.kd_area')
            ->leftJoin('role','role.kd_role','=','user.kd_role')
            ->get();
            return view('pages.sales.index')->with('data',$data);
        }
        else
        {
            $area =Auth::user()->kd_area;
            $data=User::where('user.kd_role','=','3')
            ->select('user.id','user.nik','user.nama','user.id_gcm','user.alamat','user.telepon','user.join_date','area.kd_area','role.type_role')
            ->where('user.kd_area','=',$area)
            ->leftJoin('area','area.id','=','user.kd_area')
            ->leftJoin('role','role.kd_role','=','user.kd_role')
            ->get();
            return view('pages.sales.index')->with('data',$data);
        }
    }

    public function create()
    {
        $area = [''=>''] + Area::pluck('nm_area', 'id')->toArray();
        return view('pages.sales.create')->with('area',$area);
    }

    public function store(UserRequest $request)
    {
            $rules = array(
            'nama'             => 'required',                       
            'username'         => 'required|unique:user',    
            'email_user'       => 'required|email|unique:user',    
            'password'         => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {

            $messages = $validator->messages();

            $roles = [''=>''] + Role::pluck('type_role', 'kd_role')->toArray();
            $area = [''=>''] + Area::pluck('nm_area', 'id')->toArray();
            return view('pages.sales.create')->withErrors($validator)->with('roles',$roles)->with('area',$area);

        } else {
            $user = new User;
            if(Input::hasFile('foto'))
            {
                $file = $request->file('foto');
                $destinationPath = 'userphoto/';
                $filename = str_random(6).'_'.$file->getClientOriginalName();
                $file->move($destinationPath, $filename);
            }
            else {$filename = '';}
          
            $user->nik = $request->input('nik');
            $user->nama = $request->input('nama');
            $user->alamat = $request->input('alamat');
            $user->telepon = $request->input('telepon');
            $user->kd_role = $request->input('kd_role');
            $user->kd_area = $request->input('kd_area');
            $user->username = $request->input('username');
            $user->password = $request->input('password');
            $user->email_user = $request->input('email_user');
            $user->join_date = $request->input('join_date');
            $user->foto = $filename;
            $user->save();        
            $log = new AdminController;
            $log->getLogHistory('Make New Sales Force');

            return redirect()->route('admin.sales.index');
        }  
    }

    public function show($id)
    {
        $page = 'show';
        $data['content'] = User::where('user.kd_role','=','3')
        ->select('user.*','role.type_role', 'user.username','user.email_user','area.nm_area')
        ->join('role','role.kd_role','=','user.kd_role')
        ->leftJoin('area','area.id','=','user.kd_area')
        ->where('user.id','=',$id)
        ->first();

         $data['outletVisit'] = Visitplan::join('outlet','outlet.kd_outlet', '=', 'visit_plan.kd_outlet')
        ->join('user','user.id', '=', 'outlet.kd_user')
        ->where('user.id', '=', $id)
        ->where('visit_plan.status_visit', '=', 1)
        ->count();

        $data['workingTime'] = DB::select(DB::raw("SELECT HOUR(TIMEDIFF( MAX(visit_plan.date_visiting),MIN(visit_plan.date_visiting))) AS `working` from visit_plan, user, outlet WHERE visit_plan.kd_outlet=outlet.kd_outlet AND DATE(visit_plan.date_visiting) = CURDATE() AND user.id = outlet.kd_user AND user.id= :id ") , array('id' => $id,));

        $data['takeOrder'] = TakeOrder::join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
        ->join('outlet','outlet.kd_outlet', '=', 'visit_plan.kd_outlet')
        ->join('user','user.id', '=', 'outlet.kd_user')
        ->where('user.id', '=', $id)
        ->where('take_order.status_order', '=', 1)
        ->distinct('kd_to')
        ->count('kd_to');



$data['competitorPhoto'] = CompetitorActivity::join('outlet','outlet.kd_outlet', '=', 'comp_activity.kd_outlet')
        ->join('user','user.id', '=', 'outlet.kd_user')
        ->where('user.id', '=', $id)
        ->count();



        $data['takePhoto'] = PhotoActivity::join('outlet','outlet.kd_outlet', '=', 'photo_activity.kd_outlet')
        ->join('user','user.id', '=', 'outlet.kd_user')
        ->where('user.id', '=', $id)
        ->count();
        
        $data['logs'] = Logging::join('user', 'user.id', '=', 'logging.kd_user')->orderBy('log_time', 'desc')->where('logging.kd_user','=',$id)->get();

        return view('pages.sales.'.$page,compact('data'));
    }

    public function edit($id)
    {
        $data['content'] = User::find($id);
        $area = [''=>''] + Area::pluck('nm_area', 'id')->toArray();
        return view('pages.sales.edit')->with('data',$data)->with('area',$area);
    }

    public function update(UserRequest $request, $id)
    {
        $user = User::find($id); 
        if(Input::hasFile('foto')){
            $file = $request->file('foto');
            $destinationPath = 'userphoto/';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $file->move($destinationPath, $filename);
        }
        else
        {
            $filename = $user->foto;
        }

        if ($request->input('password') == '')
        {
            $user->nik = $request->input('nik');
            $user->nama = $request->input('nama');
            $user->alamat = $request->input('alamat');
            $user->telepon = $request->input('telepon');
            $user->kd_area = $request->input('kd_area');
            $user->username = $request->input('username');
            $user->email_user = $request->input('email_user');
            $user->join_date = $request->input('join_date');
            $user->foto = $filename;
            $user->save();        
        }
        else
        {
            $user->nik = $request->input('nik');
            $user->nama = $request->input('nama');
            $user->alamat = $request->input('alamat');
            $user->telepon = $request->input('telepon');
            $user->kd_area = $request->input('kd_area');
            $user->username = $request->input('username');
            $user->email_user = $request->input('email_user');
            $user->password = $request->input('password');
            $user->join_date = $request->input('join_date');
            $user->foto = $filename;
            $user->save();        
        }
       $log = new AdminController;
       $log->getLogHistory('Update Sales Force with ID '.$id);
       return redirect()->route('admin.sales.index');
    }

    public function destroy($id)
    {
         $data = User::with(
            'outlets',
            'outlets.visitPlans',
            'outlets.photoActivities',
            'logs'   
        )->find($id);

        foreach ($data->outlets as $row) {
            foreach ($row->visitPlans as $value) {
                $value->delete();
            }
            foreach ($row->photoActivities as $value) {
               $value->delete();
            }
            $row->delete();
        }
        foreach ($data->logs as $row) {
               $row->delete();
        }
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Sales Force with ID '.$id);
        return redirect()->route('admin.sales.index');
        
    }

    public function myProfile()
    {
        $page = 'show';
        $id= Auth::user()->id;
        $data['content'] = User::where('user.kd_role','=','3')
        ->select('user.*','role.type_role', 'user.username','user.email_user')
        ->join('role','role.kd_role','=','user.kd_role')
        ->where('user.id','=',$id)
        ->first();

        $data['outletVisit'] = Visitplan::join('outlet','outlet.kd_outlet', '=', 'visit_plan.kd_outlet')
        ->join('user','user.id', '=', 'outlet.kd_user')
        ->where('user.id', '=', $id)
        ->where('visit_plan.status_visit', '=', 1)
        ->count();
        
        $data['workingTime'] = DB::select(DB::raw("SELECT HOUR(TIMEDIFF( MAX(visit_plan.date_visiting),MIN(visit_plan.date_visiting))) AS `working` from visit_plan, user, outlet WHERE visit_plan.kd_outlet=outlet.kd_outlet AND DATE(visit_plan.date_visiting) = CURDATE() AND user.id = outlet.kd_user AND user.id= :id ") , array('id' => $id,));

        $data['takeOrder'] = TakeOrder::join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
        ->join('outlet','outlet.kd_outlet', '=', 'visit_plan.kd_outlet')
        ->join('user','user.id', '=', 'outlet.kd_user')
        ->where('user.id', '=', $id)
        ->where('visit_plan.status_visit', '=', 1)
        ->where('take_order.status_order', '=', 1)
        ->count();

        $data['competitoractSF'] = PhotoActivity::join('outlet','outlet.kd_outlet', '=', 'photo_activity.kd_outlet')
        ->join('user','user.id', '=', 'outlet.kd_user')
        ->where('user.id', '=', $id)
        ->where('photo_activity.jenis_photo','=', 3)
        ->count();
        return view('pages.salesProfile.'.$page,compact('data'));

        $data['takePhoto'] = PhotoActivity::join('outlet','outlet.kd_outlet', '=', 'photo_activity.kd_outlet')
        ->join('user','user.id', '=', 'outlet.kd_user')
        ->where('user.id', '=', $id)
        ->count();
        return view('pages.salesProfile.'.$page,compact('data'));
    }

    public function editProfile()
    {
        $id= Auth::user()->id;
        $data['content'] = User::find($id);
        $area = [''=>''] + Area::pluck('nm_area', 'id')->toArray();
        return view('pages.sales.edit')->with('data',$data)->with('area',$area);
    }
}
