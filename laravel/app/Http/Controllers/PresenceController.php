<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Sessions;
use Input;
use Validator;
use Redirect;
use Excel;

use App\Distributor;
use App\Area;
use App\Kota;
use App\Tipe;
use App\User;
use App\Office;
use App\Presence;
use App\BusinessTrip;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Datatables;
use File;

class PresenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $area = Auth::user()->kd_area;
        if($area==100)
        {
          $data['areas'] = [''=>'All Area'] + Area::pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'All Sales'];
        }
        else
        {
          $data['areas'] =  Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'All Sales']+User::where('kd_area','=',$area)->where('kd_role', '=','3')->pluck('nama', 'id')->toArray();
        }

        return view('pages.presence.index', [
            'data' => $data,
        ]);
    }

    public function ajax(Request $request)
    {

        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;

        $data['dateTo']     = $request->input('dateTo');
        $data['dateFrom']   = $request->input('dateFrom');
        $sales              = $request->input('sales');
        $area               = $request->input('area');

        if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
        if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");

        $osales = $oarea = '=';

        if($sales==''){
          $sales='null';
          $osales='!=';
        }
        if($area==''||$area=='100'){
          $area='null';
          $oarea='!=';
        }

        $dateFrom = date("Y-m-d",  strtotime($data['dateFrom']));
        $dateTo =  date("Y-m-d", strtotime('+1 day', strtotime($data['dateTo'])));
       
        
        if(Auth::user()->kd_area == 100){
            $data = DB::table('presence')
                ->select('presence.id',
                    'presence.date',
                    'presence.date_checkin_presence',
                    'presence.date_checkin_visit',
                    'presence.date_checkout_visit',
                    'presence.date_checkout_presence',
                    'user.nama',
                    'area.nm_area',
                    'office.nm_office')
                ->leftJoin('office', 'office.id', '=', 'presence.kd_office')
                ->leftJoin('user', 'user.id', '=', 'presence.kd_user')
                ->leftJoin('area', 'area.id', '=', 'user.kd_area')
                ->where('user.id', $osales, $sales)
                ->where('area.id', $oarea, $area)
                ->whereBetween('presence.date', [$dateFrom, $dateTo]);
        }
        else if(Auth::user()->kd_role == 3){
            $id =Auth::user()->id;
            $data = DB::table('presence')
                ->select('presence.id',
                    'presence.date',
                    'presence.date_checkin_presence',
                    'presence.date_checkin_visit',
                    'presence.date_checkout_visit',
                    'presence.date_checkout_presence',
                    'user.nama',
                    'area.nm_area',
                    'office.nm_office')
                ->leftJoin('office', 'office.id', '=', 'presence.kd_office')
                ->leftJoin('user', 'user.id', '=', 'presence.kd_user')
                ->leftJoin('area', 'area.id', '=', 'user.kd_area')
                ->where('user.id', '=', $id)
                ->whereBetween('presence.date', [$dateFrom, $dateTo]);
        }else{
            $area =Auth::user()->kd_area;
            $data = DB::table('presence')
                ->select('presence.id',
                    'presence.date',
                    'presence.date_checkin_presence',
                    'presence.date_checkin_visit',
                    'presence.date_checkout_visit',
                    'presence.date_checkout_presence',
                    'user.nama',
                    'area.nm_area',
                    'office.nm_office')
                ->leftJoin('office', 'office.id', '=', 'presence.kd_office')
                ->leftJoin('user', 'user.id', '=', 'presence.kd_user')
                ->leftJoin('area', 'area.id', '=', 'user.kd_area')
                ->where('user.id', $osales, $sales)
                ->where('area.id', $oarea, $area)
                ->whereBetween('presence.date', [$dateFrom, $dateTo]);
        }
        

        return Datatables::of($data)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.presence.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pages.presence.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Presence::find($id);
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Presence with ID '.$id);
        return redirect()->route('admin.presence.index');
    }

    public function download(Request $request)
    {
        $data['dateTo']     = $request->input('dateTo');
        $data['dateFrom']   = $request->input('dateFrom');
        $sales              = $request->input('sales');
        $area              = $request->input('area');

        if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
        if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");

        $osales = $oarea = '=';

        if($sales==''){
          $sales='null';
          $osales='!=';
        }
        if($area==''||$area=='100'){
          $area='null';
          $oarea='!=';
        }

        $dateFrom = date("Y-m-d",  strtotime($data['dateFrom']));
        $dateTo =  date("Y-m-d", strtotime('+1 day', strtotime($data['dateTo'])));
        
        $log = new AdminController;
        $log->getLogHistory('Download Data Visit Plan');

        if(Auth::user()->kd_role == 6)
        {   
            ob_start();
            $presences = Presence::select('presence.id as ID_PRESENCE',
                    'presence.date AS DATE_PRESENCE',
                    'presence.date_checkin_presence AS DATE_CHECKIN_PRESENCE',
                    'presence.date_checkin_visit AS DATE_CHECKIN_VISIT',
                    'presence.date_checkout_visit AS DATE_CHECKOUT_VISIT',
                    'presence.date_checkout_presence AS DATE_CHECKOUT_PRESENCE',
                    'user.nama AS SALES_FORCE',
                    'area.nm_area AS AREA_SF',
                    'office.nm_office AS NAMA_OFFICE')
                ->leftJoin('office', 'office.id', '=', 'presence.kd_office')
                ->leftJoin('user', 'user.id', '=', 'presence.kd_user')
                ->leftJoin('area', 'area.id', '=', 'user.kd_area')
                ->where('user.id', $osales, $sales)
                ->where('area.id', $oarea, $area)
                ->whereBetween('presence.date', [$dateFrom, $dateTo])
                ->get();
        }
        elseif(Auth::user()->kd_role == 3)
        {
            $id =Auth::user()->id;
            ob_start();
            $presences = Presence::select('presence.id as ID_PRESENCE',
                    'presence.date AS DATE_PRESENCE',
                    'presence.date_checkin_presence AS DATE_CHECKIN_PRESENCE',
                    'presence.date_checkin_visit AS DATE_CHECKIN_VISIT',
                    'presence.date_checkout_visit AS DATE_CHECKOUT_VISIT',
                    'presence.date_checkout_presence AS DATE_CHECKOUT_PRESENCE',
                    'user.nama AS SALES_FORCE',
                    'area.nm_area AS AREA_SF',
                    'office.nm_office AS NAMA_OFFICE')
                ->leftJoin('office', 'office.id', '=', 'presence.kd_office')
                ->leftJoin('user', 'user.id', '=', 'presence.kd_user')
                ->leftJoin('area', 'area.id', '=', 'user.kd_area')
                ->where('user.id', '=', $id)
                ->whereBetween('presence.date', [$dateFrom, $dateTo])
                ->get();
        }else{
            $area =Auth::user()->kd_area;
            ob_start();
            $presences = Presence::select('presence.id as ID_PRESENCE',
                    'presence.date AS DATE_PRESENCE',
                    'presence.date_checkin_presence AS DATE_CHECKIN_PRESENCE',
                    'presence.date_checkin_visit AS DATE_CHECKIN_VISIT',
                    'presence.date_checkout_visit AS DATE_CHECKOUT_VISIT',
                    'presence.date_checkout_presence AS DATE_CHECKOUT_PRESENCE',
                    'user.nama AS SALES_FORCE',
                    'area.nm_area AS AREA_SF',
                    'office.nm_office AS NAMA_OFFICE')
                ->leftJoin('office', 'office.id', '=', 'presence.kd_office')
                ->leftJoin('user', 'user.id', '=', 'presence.kd_user')
                ->leftJoin('area', 'area.id', '=', 'user.kd_area')
                ->where('user.id', $osales, $sales)
                ->where('area.id', $oarea, $area)
                ->whereBetween('presence.date', [$dateFrom, $dateTo])
                ->get();
        }

        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_PRESENCE";
        if(!$presences->isEmpty())
        {
            Excel::create($filename, function($excel) use($presences) {
                $excel->sheet('Master Outlet', function($sheet) use($presences) {
                    $sheet->fromArray($presences);
                });
            })->download('xls');
        }
    }
}
