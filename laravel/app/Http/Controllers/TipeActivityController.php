<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\TipeActivityRequest;
use App\TipeActivity;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;

class TipeActivityController extends Controller
{
    public function index()
    {
        $data = TipeActivity::all();
        return view('pages.tipeActivity.index', compact('data'));
    }

    public function create()
    {
        return view('pages.tipeActivity.create');
    }

    public function store(TipeActivityRequest $request)
    {
        TipeActivity::create($request->all());
        $log = new AdminController;
        $log->getLogHistory('Make New Tipe Activity');
        return redirect()->route('admin.tipeActivity.index');
    }

    public function edit($id)
    {
        $data['content'] = TipeActivity::find($id);
        return view('pages.tipeActivity.edit')->with('data',$data);
    }

    public function update(TipeActivityRequest $request, $id)
    {
        $tipeAct = TipeActivity::find($id);       
        $tipeAct->update($request->all());
        $log->getLogHistory('Update Tipe Activity with ID '.$id);
        return redirect()->route('admin.tipeActivity.index');
    }

    public function destroy($id)
    {
        $data = TipeActivity::find($id);
        $data->delete();
        $tipeAct->update($request->all());
        $log->getLogHistory('Delete Tipe Activity with ID'.$id);
        return redirect()->route('admin.tipeActivity.index'); 
    }
}
