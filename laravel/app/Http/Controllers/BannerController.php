<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
    use Input;
use Image;
use File;
use App\Banner;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{

    public function index()
    {
        $data = Banner::orderBy('date_active', 'desc')->get();
        return view('pages.banner.index', compact('data'));
    }

    public function create()
    {
    	
        return view('pages.banner.create');
    }

    public function up($id)
    {
    	$banner = Banner::find($id);
    	$banner->date_active = date('Y-m-d H:i:s');
    	$banner->save();
        $log = new AdminController;
        $log->getLogHistory('Up Banner');
        return redirect()->route('admin.banner.index');
    }


    public function store(Request $request)
    {
    	$filename='';
        
	    $file = $request->file('path_image');
        $destinationPath = 'image_upload/banner/';
        $filename = str_random(6).'_'.$file->getClientOriginalName();
        $img = Image::make($file)->resize(600, null, function($constraint){
            $constraint->aspectRatio();
        })->encode('png')->save($destinationPath.$filename);
	    
        $banner = new Banner();
        
        
        $banner->path_image = $filename;
        $banner->date_active = date('Y-m-d H:i:s');
        $banner->status = $request->input('status');

        $banner->save();	
          
        $log = new AdminController;
        $log->getLogHistory('Add Banner');
        
        return redirect()->route('admin.banner.index');
    }

    public function edit($id)
    {
        $data['content'] = Banner::find($id);

        
        return view('pages.banner.edit')->with('data',$data);
    }

    public function update(Request $request, $id)
    {
        $banner = Banner::find($id);
        $banner->update($request->all()); 
        
        if(Input::hasFile('path_image')){
         File::delete('image_upload/banner/'.$banner->path_image);    
            $file = $request->file('path_image');
            $destinationPath = 'image_upload/banner/';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $img = Image::make($file)->resize(600, null, function($constraint){
                $constraint->aspectRatio();
            })->encode('png')->save($destinationPath.$filename);
            
            $banner->path_image=$filename;
            $banner->save();
        }
        $log = new AdminController;
        $log->getLogHistory('Update Banner with ID '.$id);


        return redirect()->route('admin.banner.index');
    }

    public function destroy($id)
    {
        $data = Banner::find($id);
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Banner with ID '.$id);


        return redirect()->route('admin.banner.index'); 
    }

     

}
