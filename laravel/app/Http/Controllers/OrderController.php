<?php
/*
    PT. Trikarya Teknologi Indonesia
    Tenggilis raya 127
    Office Complex Apartment Metropolis MKB 206
    Surabaya, Jawa timur, Indonesia
    Phone : +6231-8420384 / +6281235537717
    Code By : Novita Nata Wardanie
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Input;
use Excel;
use App\VisitPlan;
use App\Produk;
use App\Satuan;
use App\TakeOrder;
use App\Area;
use App\Tipe;
use App\Distributor;
use App\User;
use App\Kota;
  use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;
use App\Http\Controllers\AdminController;

class OrderController extends Controller
{

          public function formOrder(OrderRequest $request)
    {
        $area = Auth::user()->kd_area;
        // if(Auth::user()->kd_area== 100){
        //      $data['content']= DB::table('take_order')
        //     ->select('produk.nm_produk',
        //         'user.nama',
        //         'take_order.kd_to',
        //         'take_order.kd_visitplan',
        //         'visit_plan.kd_outlet',
        //         'outlet.nm_outlet',

        //         'take_order.*')
        //     ->selectRaw('produk.harga_produk*take_order.qty_order as total_value')
        //     ->join('produk','produk.id','=','take_order.kd_produk')
        //     ->join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
        //     ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')
        //     ->join('user', 'user.id', '=', 'outlet.kd_user')
        //     ->whereBetween('take_order.date_order',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
        //     ->orderBy('take_order.date_order', 'desc')
        //     ->get();

        // }
        if($area==100)
        {
          $data['areas'] = [''=>'All Area'] + Area::pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'All Sales'];
        }
        else
        {
          $data['areas'] =  Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'All Sales']+User::where('kd_area','=',$area)->where('kd_role', '=','3')->pluck('nama', 'id')->toArray();
        }

        $data['tipe'] =  [''=>'All Tipe'] + Tipe::pluck( 'nm_tipe','id')->toArray();
        $data['distributor'] =  [''=>'All Distributor'] + Distributor::pluck('kd_dist','id' )->toArray();

        if(Auth::user()->kd_area== 100){
              
           $data['dateTo'] = $request->input('dateTo');
           $data['dateFrom'] = $request->input('dateFrom');
           if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
           if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");

           $dateFrom = date("Y-m-d", strtotime($data['dateFrom']));
           $dateTo =  date("Y-m-d", strtotime('+1 day', strtotime($data['dateTo'])));

           $data['content'] = TakeOrder::orderBy('date_order', 'desc')
           ->with('outlets')
           ->with('satuans')
           ->with('users')
           ->whereBetween('take_order.date_order',[$dateFrom,$dateTo])
           ->whereBetween('take_order.date_order',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])

           ->get();

        }
        else if(Auth::user()->kd_role == 3){
            $id =Auth::user()->id;
            $data['content']= DB::table('take_order')
            ->select('produk.nm_produk',
                'user.nama',
                'take_order.kd_to',
                'take_order.kd_visitplan',
                'visit_plan.kd_outlet',
                'outlet.nm_outlet',

                'take_order.*')
            ->selectRaw('produk.harga_produk*take_order.qty_order as total_value')
            ->join('produk','produk.id','=','take_order.kd_produk')
            ->join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
            ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')

            ->where('outlet.kd_user','=',$id)
            ->whereBetween('take_order.date_order',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
            ->orderBy('take_order.date_order', 'desc')
            ->get();
        }
        else {
            $area =Auth::user()->kd_area;
            $data['content']= DB::table('take_order')
            ->select('produk.nm_produk',
                'user.nama',
                'take_order.kd_to',
                'take_order.kd_visitplan',
                'visit_plan.kd_outlet',
                'outlet.nm_outlet',

                'take_order.*')
            ->selectRaw('produk.harga_produk*take_order.qty_order as total_value')
            ->join('produk','produk.id','=','take_order.kd_produk')
            ->join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
            ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')

            ->where('outlet.kd_area','=',$area)
            ->whereBetween('take_order.date_order',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
            ->orderBy('take_order.date_order', 'desc')
            ->get();
        }
        return view('pages.order.order')->with('data',$data);
        
    }

   public function index(OrderRequest $request)
    {
        $data['dateTo'] = $request->input('dateTo');
        $data['dateFrom'] = $request->input('dateFrom');

    $sales = $request->input('sales');
    $area = $request->input('kd_area');
    $tipe = $request->input('nm_tipe');
    $dist = $request->input('kd_dist');
    $osales=$oarea=$otipe=$odist='=';
    if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
    if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");
    if($sales==''){
      $sales='null';
      $osales='!=';
    }
    if($area==''||$area=='100'){
      $area='null';
      $oarea='!=';
    }
    if($tipe==''){
      $tipe='null';
      $otipe='!=';
    }
    if($dist==''){
      $dist='null';
      $odist='!=';
    }


    $dateFrom = date("Y-m-d", strtotime($data['dateFrom']));
    $dateTo =  date("Y-m-d", strtotime('+1 day', strtotime($data['dateTo'])));

        if(Input::get('show')) {
            if(Auth::user()->kd_area == 100) {
             $data['dateTo'] = $request->input('dateTo');
             $data['dateFrom'] = $request->input('dateFrom');
             if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
             if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");

             $dateFrom = date("Y-m-d", strtotime($data['dateFrom']));
             $dateTo =  date("Y-m-d", strtotime('+1 day', strtotime($data['dateTo'])));

             $data['content'] = TakeOrder::orderBy('date_order', 'desc')
             ->with('outlets')
             ->with('satuans')
             ->with('users')
             ->whereBetween('take_order.date_order',[$dateFrom,$dateTo])
             ->whereBetween('take_order.date_order',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])

             ->get();
            }
            else if(Auth::user()->kd_role == 3){
                $id =Auth::user()->id;
                $data['content']= DB::table('take_order')
                ->select('produk.nm_produk',
                'user.nama',
                'take_order.kd_to',
                'take_order.kd_visitplan',
                'visit_plan.kd_outlet',
                'outlet.nm_outlet',

                'take_order.*')
            ->selectRaw('produk.harga_produk*take_order.qty_order as total_value')
            ->join('produk','produk.id','=','take_order.kd_produk')
            ->join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
            ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')

              ->where('outlet.kd_area', $oarea, $area)
              ->where('outlet.kd_tipe', $otipe, $tipe)
              ->where('outlet.kd_dist', $odist, $dist)
                ->where('outlet.kd_user','=',$id)
                 ->whereBetween('take_order.date_order',[$dateFrom,$dateTo])
                 ->orderBy('take_order.date_order', 'desc')
                ->get();
            }
            else {
                $area =Auth::user()->kd_area;
                $data['content']= DB::table('take_order')
                ->select('produk.nm_produk',
                'user.nama',
                'take_order.kd_to',
                'take_order.kd_visitplan',
                'visit_plan.kd_outlet',
                'outlet.nm_outlet',

                'take_order.*')
            ->selectRaw('produk.harga_produk*take_order.qty_order as total_value')
            ->join('produk','produk.id','=','take_order.kd_produk')
            ->join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
            ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->where('user.id', $osales, $sales)
              ->where('outlet.kd_tipe', $otipe, $tipe)
              ->where('outlet.kd_dist', $odist, $dist)
                ->where('outlet.kd_area','=',$area)
                ->whereBetween('take_order.date_order',[$dateFrom,$dateTo])
                ->orderBy('take_order.date_order', 'desc')
                ->get();
            }
            return view('pages.order.index')->with('data',$data);
        } else if(Input::get('download')) {
            $this->exportTakeOrder($sales, $osales, $tipe, $otipe, $area, $oarea, $dist, $odist, $dateFrom, $dateTo);
        }
    }

    public function exportTakeOrder($sales, $osales, $tipe, $otipe, $area, $oarea, $dist, $odist, $dateFrom, $dateTo)
    {
        $log = new AdminController;
        $log->getLogHistory('Download Data Take Order');

        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        if($area == 100)
        {
            // ob_end_clean();
            ob_start();
            $order = TakeOrder::selectRaw(
                    "
                    user.nama AS NAMA_SF,
                    take_order.kd_to AS KD_ORDER,
                    visit_plan.id AS KD_VISIT,
                    outlet.kd_outlet AS KD_OUTLET,
                    outlet.nm_outlet AS OUTLET,
                    tipe.nm_tipe as TIPE_OUTLET,
                    produk.nm_produk AS NAMA_PRODUK,
                    take_order.qty_order as QUANTITY,
                    produk.harga_produk*take_order.qty_order as TOTAL_VALUE,
                    (CASE WHEN (take_order.sku_baru = 1) THEN 'YES' ELSE 'NO' END) as SKU_BARU,
                    take_order.date_order as DATE_ORDER"
                    )
               ->join('produk','produk.id','=','take_order.kd_produk')
            ->join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
            ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')
            ->where('user.id', $osales, $sales)
              ->where('outlet.kd_area', $oarea, $area)
              ->where('outlet.kd_tipe', $otipe, $tipe)
              ->where('outlet.kd_dist', $odist, $dist)
                ->whereBetween('take_order.date_order',[$dateFrom,$dateTo])
                ->orderBy('take_order.date_order', 'desc')
                ->get();
            date_default_timezone_set("Asia/Jakarta");
            $d= strtotime("now");
            $time =date("d-m-Y H:i:s", $d);
            $filename = $time."_TAKE_ORDER";
            Excel::create($filename, function($excel) use($order) {
                $excel->sheet('Take Order', function($sheet) use($order) {
                    $sheet->fromArray($order);
                });
            })->download('xls');
        }
        else
        {
            $this->exportTakeOrderArea($area,$sales, $osales, $tipe, $otipe, $dist, $odist, $dateFrom, $dateTo);
        }

    }

    public function exportTakeOrderArea($area,$sales, $osales, $tipe, $otipe, $dist, $odist, $dateFrom, $dateTo)
    {
        $area = Auth::user()->kd_area;
        ob_start();
       $order = TakeOrder::selectRaw(
                    "
                    user.nama AS NAMA_SF,
                    take_order.kd_to AS KD_ORDER,
                    visit_plan.id AS KD_VISIT,
                    outlet.kd_outlet AS KD_OUTLET,
                    outlet.nm_outlet AS OUTLET,
                    tipe.nm_tipe as TIPE_OUTLET,
                    produk.nm_produk AS NAMA_PRODUK,
                    take_order.qty_order as QUANTITY,
                    produk.harga_produk*take_order.qty_order as TOTAL_VALUE,
                    (CASE WHEN (take_order.sku_baru = 1) THEN 'YES' ELSE 'NO' END) as SKU_BARU,
                    take_order.date_order as DATE_ORDER"
                    )
               ->join('produk','produk.id','=','take_order.kd_produk')
            ->join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
            ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')
            ->where('user.id', $osales, $sales)

              ->where('outlet.kd_tipe', $otipe, $tipe)
              ->where('outlet.kd_dist', $odist, $dist)
            ->where('outlet.kd_area','=',$area)
            ->whereBetween('take_order.date_order',[$dateFrom,$dateTo])
            ->orderBy('take_order.date_order', 'desc')
            ->get();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_TAKE_ORDER";
        Excel::create($filename, function($excel) use($order) {
            $excel->sheet('Take Order', function($sheet) use($order) {
                $sheet->fromArray($order);
            });
        })->download('xls');
    }

    public function create()
    {
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        $id = Auth::user()->id;
        if($area == 100)
        {
            $data['visit'] = [''=>''] + VisitPlan::join('outlet','outlet.kd_outlet','=','visit_plan.kd_outlet')
            ->pluck('outlet.nm_outlet', 'visit_plan.id')
            ->toArray();
        }
        else if($role==3)
        {
            $data['visit'] = [''=>''] + VisitPlan::join('outlet','outlet.kd_outlet','=','visit_plan.kd_outlet')
            ->where('outlet.kd_user','=',$id)
            ->pluck('outlet.nm_outlet', 'visit_plan.id')
            ->toArray();
        }
        else
        {
             $data['visit'] = [''=>''] + VisitPlan::join('outlet','outlet.kd_outlet','=','visit_plan.kd_outlet')
            ->where('outlet.kd_area','=',$area)
            ->pluck('outlet.nm_outlet', 'visit_plan.id')
            ->toArray();
        }
        $data['produk'] = [''=>''] + Produk::pluck('nm_produk', 'id')->toArray();
        return view('pages.order.create', compact('data'));
    }

    public function store(OrderRequest $request)
    {
        TakeOrder::create($request->all());
        $log = new AdminController;
        $log->getLogHistory('Make New Order');
        return redirect()->route('admin.takeOrder.form');
    }

    public function edit($id)
   /* {
        $data ['content']= TakeOrder::find($id);
        if($area == 100)
        {
            $data['visit'] = [''=>''] + VisitPlan::join('outlet','outlet.kd_outlet','=','visit_plan.kd_outlet')
            ->pluck('outlet.nm_outlet', 'visit_plan.id')
            ->toArray();
        }
        else if($role==3)
        {
            $data['visit'] = [''=>''] + VisitPlan::join('outlet','outlet.kd_outlet','=','visit_plan.kd_outlet')
            ->where('outlet.kd_user','=',$id)
            ->pluck('outlet.nm_outlet', 'visit_plan.id')
            ->toArray();
        }
        else
        {
             $data['visit'] = [''=>''] + VisitPlan::join('outlet','outlet.kd_outlet','=','visit_plan.kd_outlet')
            ->where('outlet.kd_area','=',$area)
            ->pluck('outlet.nm_outlet', 'visit_plan.id')
            ->toArray();
        }
        $data['produk'] = [''=>''] + Produk::pluck('nm_produk', 'id')->toArray();
        return view('pages.order.edit')->with('data',$data);
    } */

    {
        $data['visit'] = [''=>''] + VisitPlan::join('outlet','outlet.kd_outlet','=','visit_plan.kd_outlet')
        ->pluck('outlet.nm_outlet', 'visit_plan.id')->toArray();
        $data['produk'] = [''=>''] + Produk::pluck('nm_produk', 'id')->toArray();
        $data ['content']= TakeOrder::find($id);
        return view('pages.order.edit')->with('data',$data);
    }

    public function update(OrderRequest $request, $id)
    {
        $data = TakeOrder::find($id);
        $data->update($request->all());
        $log = new AdminController;
        $log->getLogHistory('Update Order');
        return redirect()->route('admin.takeOrder.form');
    }

    public function destroy($id)
    {
        $data = TakeOrder::find($id);
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Order');
        return redirect()->route('admin.takeOrder.form');

    }


}
