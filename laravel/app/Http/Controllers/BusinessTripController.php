<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Sessions;
use Input;
use Validator;
use Redirect;
use Excel;

use App\Area;
use App\Kota;
use App\User;
use App\BusinessTrip;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Http\FormRequest;

use Datatables;
use File;

class BusinessTripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $area = Auth::user()->kd_area;
        if($area==100)
        {
          $data['areas'] = [''=>'All Area'] + Area::pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'All Sales'];
        }
        else
        {
          $data['areas'] =  Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'All Sales']+User::where('kd_area','=',$area)->where('kd_role', '=','3')->pluck('nama', 'id')->toArray();
        }

        return view('pages.businessTrip.index', [
            'data' => $data,
        ]);

    }

    public function ajax(Request $request)
    {
        $data['dateTo']     = $request->input('dateTo');
        $data['dateFrom']   = $request->input('dateFrom');
        $sales              = $request->input('sales');
        $area               = $request->input('area');

        $osales = $oarea = '=';
        
        if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
        if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");

        if($sales==''){
          $sales='null';
          $osales='!=';
        }
        if($area==''||$area=='100'){
          $area='null';
          $oarea='!=';
        }

        $dateFrom = date("Y-m-d",  strtotime($data['dateFrom']));
        $dateTo =  date("Y-m-d", strtotime('+1 day', strtotime($data['dateTo'])));

        if(Auth::user()->kd_role == 100){
            $data = DB::table('business_trip')
                ->select(
                    'business_trip.id',
                    'business_trip.date_request',
                    'user.nama',
                    'area.nm_area',
                    'kota.nm_kota',
                    'business_trip.date_start',
                    'business_trip.date_end')
                ->leftJoin('user', 'user.id', '=', 'business_trip.kd_user')
                ->leftJoin('area', 'area.id', '=', 'user.kd_area')
                ->leftJoin('kota', 'kota.id', '=', 'business_trip.kd_kota')
                ->where('user.id', $osales, $sales)
                ->whereBetween('business_trip.date_request', [$dateFrom, $dateTo]);
        }
        elseif(Auth::user()->kd_role == 3 ){
            $id =Auth::user()->id;
            $data = DB::table('business_trip')
                ->select(
                    'business_trip.id',
                    'business_trip.date_request',
                    'user.nama',
                    'area.nm_area',
                    'kota.nm_kota',
                    'business_trip.date_start',
                    'business_trip.date_end')
                ->leftJoin('user', 'user.id', '=', 'business_trip.kd_user')
                ->leftJoin('area', 'area.id', '=', 'user.kd_area')
                ->leftJoin('kota', 'kota.id', '=', 'business_trip.kd_kota')
                ->where('user.id', '=', $id)
                ->whereBetween('business_trip.date_request', [$dateFrom, $dateTo]);
        }
        else{
            $area =Auth::user()->kd_area;
            $data = DB::table('business_trip')
                ->select(
                    'business_trip.id',
                    'business_trip.date_request',
                    'user.nama',
                    'area.nm_area',
                    'kota.nm_kota',
                    'business_trip.date_start',
                    'business_trip.date_end')
                ->leftJoin('user', 'user.id', '=', 'business_trip.kd_user')
                ->leftJoin('area', 'area.id', '=', 'user.kd_area')
                ->leftJoin('kota', 'kota.id', '=', 'business_trip.kd_kota')
                ->where('user.id', $osales, $sales)
                ->where('area.id', $oarea, $area)
                ->whereBetween('business_trip.date_request', [$dateFrom, $dateTo]);
        }
        return Datatables::of($data)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data= BusinessTrip::select(
                    'businessTrip.id',
                    'businessTrip.kd_kota',
                    'businessTrip.date_request',
                    'user.nama',
                    'area.nm_area',
                    'kota.nm_kota',
                    'businessTrip.date_start',
                    'businessTrip.date_end')
                ->leftJoin('user', 'user.id', '=', 'businessTrip.kd_user')
                ->leftJoin('area', 'area.id', '=', 'user.kd_area')
                ->leftJoin('kota', 'kota.id', '=', 'businessTrip.kd_kota')
                ->where('businessTrip.id', '=', $id)
                ->get()->first();
        $kotas = [''=>'Select Kota'] + Kota::pluck('nm_kota', 'id')->toArray();

        return view('pages.businessTrip.edit', [
            'businessTrip' => $data,
            'kotas' => $kotas
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'date_start'  => 'required',
            'date_end' => 'required',
            'kd_kota' => 'required',
        );

        $messages = array(
            'date_start.required'   => 'Date Start field is required',
            'date_end.required'     => 'Date End field is required',
            'kd_kota.required'      => 'Kota field is required',
        );

        $validator = Validator::make(Input::all(), $rules, $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }else{
            $data= BusinessTrip::find($id);
            $data->date_start   =   $request->input('date_start');
            $data->date_end     =   $request->input('date_end');
            $data->kd_kota      =   $request->input('kd_kota');
            $data->save();
            
            $log = new AdminController;
            $log->getLogHistory('Update Business Trip with ID '.$id);
            return redirect()->route('admin.businessTrip.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = BusinessTrip::find($id);
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Business Trip with ID '.$id);
        return redirect()->route('admin.businessTrip.index');
    }

    public function download(Request $request)
    {
        $data['dateTo']     = $request->input('dateTo');
        $data['dateFrom']   = $request->input('dateFrom');
        $sales              = $request->input('sales');
        $area               = $request->input('area');

        $osales = $oarea = '=';
        
        if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
        if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");

        if($sales==''){
          $sales='null';
          $osales='!=';
        }
        if($area==''||$area=='100'){
          $area='null';
          $oarea='!=';
        }

        $dateFrom = date("Y-m-d",  strtotime($data['dateFrom']));
        $dateTo =  date("Y-m-d", strtotime('+1 day', strtotime($data['dateTo'])));

        ob_start();
        if(Auth::user()->kd_role == 100){
            $trips = BusinessTrip::select(
                    'businessTrip.id as ID_BUSINESS_TRIP',
                    'businessTrip.date_request as DATE_REQUEST',
                    'user.nama as SALES_FORCE',
                    'area.nm_area as AREA_SF',
                    'kota.nm_kota as KOTA',
                    'businessTrip.date_start as DATE_START',
                    'businessTrip.date_end as DATE_END')
                ->leftJoin('user', 'user.id', '=', 'businessTrip.kd_user')
                ->leftJoin('area', 'area.id', '=', 'user.kd_area')
                ->leftJoin('kota', 'kota.id', '=', 'businessTrip.kd_kota')
                ->where('user.id', $osales, $sales)
                ->whereBetween('businessTrip.date_request', [$dateFrom, $dateTo])
                ->get();
        }
        elseif(Auth::user()->kd_role == 3 ){
            $id =Auth::user()->id;
            $trips = BusinessTrip::select(
                    'businessTrip.id as ID_BUSINESS_TRIP',
                    'businessTrip.date_request as DATE_REQUEST',
                    'user.nama as SALES_FORCE',
                    'area.nm_area as AREA_SF',
                    'kota.nm_kota as KOTA',
                    'businessTrip.date_start as DATE_START',
                    'businessTrip.date_end as DATE_END')
                ->leftJoin('user', 'user.id', '=', 'businessTrip.kd_user')
                ->leftJoin('area', 'area.id', '=', 'user.kd_area')
                ->leftJoin('kota', 'kota.id', '=', 'businessTrip.kd_kota')
                ->where('user.id', '=', $id)
                ->whereBetween('businessTrip.date_request', [$dateFrom, $dateTo])
                ->get();
        }
        else{
            $area =Auth::user()->kd_area;
            $trips = BusinessTrip::select(
                    'businessTrip.id as ID_BUSINESS_TRIP',
                    'businessTrip.date_request as DATE_REQUEST',
                    'user.nama as SALES_FORCE',
                    'area.nm_area as AREA_SF',
                    'kota.nm_kota as KOTA',
                    'businessTrip.date_start as DATE_START',
                    'businessTrip.date_end as DATE_END')
                ->leftJoin('user', 'user.id', '=', 'businessTrip.kd_user')
                ->leftJoin('area', 'area.id', '=', 'user.kd_area')
                ->leftJoin('kota', 'kota.id', '=', 'businessTrip.kd_kota')
                ->where('user.id', $osales, $sales)
                ->where('area.id', $oarea, $area)
                ->whereBetween('businessTrip.date_request', [$dateFrom, $dateTo])
                ->get();
        }

        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_BUSINESS TRIP";
        if(!$trips->isEmpty())
        {
            Excel::create($filename, function($excel) use($trips) {
                $excel->sheet('Master Outlet', function($sheet) use($trips) {
                    $sheet->fromArray($trips);
                });
            })->download('xls');
        }
    }
}
