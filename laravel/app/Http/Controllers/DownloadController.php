<?php
/*
PT. Trikarya Teknologi Indonesia
Tenggilis raya 127
Office Complex Apartment Metropolis MKB 206
Surabaya, Jawa timur, Indonesia
Phone : +6231-8420384 / +6281235537717
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Response;
use Excel;
use Input;
use Validator;
use App\Area;
use App\Kota;
use App\Outlet;
use App\Distributor;
use App\Produk;
use App\VisitPlan;
use App\TakeOrder;
use App\User;
use App\Presence;
use App\BusinessTrip;

use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;

class DownloadController extends Controller
{
    public function index()
    {
        return view('pages.download.index');
    }

    public function exportArea()
    {
        $log = new AdminController;
        $log->getLogHistory('Download Master Data Area');
        ob_start();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_AREA";
        $area = Kota::select('kota.id as ID',
                            'kota.nm_kota as NAMA_KOTA',
                            'area.kd_area as KODE_AREA',
                            'area.nm_area as NAMA_AREA')
        ->leftJoin('area', 'kota.kd_area','=','area.id')
        ->orderBy('kota.id','asc')
        ->get();
        if(!$area->isEmpty())
        {
            Excel::create($filename, function($excel) use($area) {
                    $excel->sheet('Sheet 1', function($sheet) use($area) {
                        $sheet->fromArray($area);
                    });
                })->download('xls');
        }
    }

    public function exportOutlet()
    {
	    $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        if($role == 1 || $area == 100)
        {
            $log = new AdminController;
            $log->getLogHistory('Download Master Data Outlet');
	        ob_start();
	        $outlets = Outlet::select('outlet.kd_outlet as KODE_OUTLET',
                    'outlet.nm_outlet as NAMA_OUTLET',
                    'outlet.almt_outlet as ALAMAT',
                    'kota.nm_kota AS KOTA',
                    'outlet.kodepos AS KODEPOS',
                    'outlet.rank_outlet AS RANK',
                    'outlet.nm_pic AS NAMA_PIC',
                    'outlet.tlp_pic AS TLP_PIC',
                    'tipe.nm_tipe AS TIPE',
                    'area.kd_area AS AREA',
                    'tipe.nm_tipe AS TIPE',
                    'distributor.nm_dist AS DISTRIBUTOR',
                    'user.nama AS SALES')
            ->leftJoin('area', 'area.id', '=', 'outlet.kd_area')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->leftJoin('tipe', 'tipe.id', '=', 'outlet.kd_tipe')
            ->leftJoin('distributor', 'distributor.id', '=', 'outlet.kd_dist')
            ->leftJoin('user', 'user.id', '=', 'outlet.kd_user')
            ->get();
	        date_default_timezone_set("Asia/Jakarta");
	        $d= strtotime("now");
	        $time =date("d-m-Y H:i:s", $d);
	        $filename = $time."_OUTLET";
	        if(!$outlets->isEmpty())
            {
                Excel::create($filename, function($excel) use($outlets) {
                    $excel->sheet('Master Outlet', function($sheet) use($outlets) {
                        $sheet->fromArray($outlets);
                    });
                })->download('xls');
            }
        }
        else
        {
            $this->exportOutletAreaOrSales();
        }
    }


     

    public function exportDistributor()
    {
        $log = new AdminController;
        $log->getLogHistory('Download Master Data Distributor');
        ob_start();
        $distributors =  Distributor::select('distributor.kd_dist as KODE_DISTRIBUTOR',
                'distributor.nm_dist AS NAMA_DISTRIBUTOR',
                'distributor.almt_dist AS ALAMAT',
                'kota.nm_kota AS KOTA',
                'distributor.telp_dist AS TELEPON')
        ->join('kota', 'kota.id', '=', 'distributor.kd_kota')
        ->get();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_DISTRIBUTOR";
        if(!$distributors->isEmpty())
        {
            Excel::create($filename, function($excel) use($distributors) {
                $excel->sheet('Sheet 1', function($sheet) use($distributors) {
                    $sheet->fromArray($distributors);
                });
            })->download('xls');
        }
    }

    public function exportProduk()
    {
        $log = new AdminController;
        $log->getLogHistory('Download Master Data Produk');
        ob_start();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_PRODUK";
        $prod = Produk::select('kd_produk as KODE_PRODUK', 'nm_produk as NAMA_PRODUK')->get();
        if(!$prod->isEmpty())
        {
            Excel::create($filename, function($excel) use($prod) {
                $excel->sheet('Sheet 1', function($sheet) use($prod) {
                    $sheet->fromArray($prod);
                });
            })->download('xls');
        }
    }

    public function exportVisitPlan()
    {
        $log = new AdminController;
        $log->getLogHistory('Download Data Visit Plan');
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        if($role == 1 || $area == 100)
        {
	        ob_start();
	        $visit = VisitPlan::select(DB::raw("outlet.kd_outlet as KODE_OUTLET,
	          outlet.nm_outlet as NAMA_OUTLET,
	          outlet.almt_outlet as ALAMAT_OUTLET,
	          kota.nm_kota as KOTA,
              outlet.kodepos AS KODEPOS,
	          area.nm_area as AREA,
	          (CASE WHEN (visit_plan.approve_visit = 1) THEN 'Approved'
	          WHEN (visit_plan.approve_visit = 2) THEN 'Pending' ELSE 'Denied' END) as VISIT_APPROVAL,
	          visit_plan.date_create_visit as DATE_CREATE_VISIT,
	          visit_plan.date_visiting as PLAN_DATE_VISIT,
	          user.nama as SALES")
	        )
	        ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
	        ->join('user', 'user.id', '=', 'outlet.kd_user')
	        ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
	        ->join('area', 'area.id', '=', 'outlet.kd_area')
	        ->get();
	        date_default_timezone_set("Asia/Jakarta");
	        $d= strtotime("now");
	        $time =date("d-m-Y H:i:s", $d);
	        $filename = $time."_VISIT_PLAN";
            if(!$visit->isEmpty())
            {
    	        Excel::create($filename, function($excel) use($visit) {
    	            $excel->sheet('Visit Plan', function($sheet) use($visit) {
    	                $sheet->fromArray($visit);
    	            });
    	        })->download('xls');
            }
        }
        else
        {
            $this->exportVisitPlanArea();
        }
    }

   public function exportVisitMonitor()
    {
        $log = new AdminController;
    $log->getLogHistory('Download Data Visit Monitor');
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        if($role == 1 || $area == 100)
        {
            ob_start();
            $visit = VisitPlan::select(DB::raw("outlet.kd_outlet as KODE_OUTLET,
              outlet.nm_outlet as NAME_OUTLET,
              outlet.almt_outlet as ALAMAT_OUTLET,
              kota.nm_kota as KOTA,
              outlet.kodepos AS KODEPOS,
              area.nm_area as AREA,
              (CASE WHEN (visit_plan.status_visit = 1) THEN 'Finished'
              WHEN (visit_plan.status_visit = 2) THEN 'Pending' ELSE 'Skip' END) as STATUS_VISIT,
              visit_plan.skip_order_reason as SKIP_ORDER_REASON,
                visit_plan.skip_reason as UNVISITED_REASON,

              visit_plan.date_visit as PLAN_DATE_VISIT,
              visit_plan.date_visiting as DATE_ACTUAL_VISIT,
              user.nama as SALES")
            )
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->join('area', 'area.id', '=', 'outlet.kd_area')

            ->get();
            date_default_timezone_set("Asia/Jakarta");
            $d= strtotime("now");
            $time =date("d-m-Y H:i:s", $d);
            $filename = $time."_VISIT_MONITOR";
            if(!$visit->isEmpty())
            {
                Excel::create($filename, function($excel) use($visit) {
                    $excel->sheet('Visit Monitor', function($sheet) use($visit) {
                        $sheet->fromArray($visit);
                    });
                })->download('xls');
            }
        }
        else
        {
            $this->exportVisitMonitorArea();
        }
    }


    public function exportTakeOrder()
    {
        $log = new AdminController;
        $log->getLogHistory('Download Data Take Order');
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        if($role == 1 || $area == 100)
        {
	        ob_start();
	        $order = TakeOrder::select(
                 'take_order.kd_to AS KD_ORDER',
                    'visit_plan.id AS KD_VISIT',
                    'user.nama AS NAMA_SF',
                     'produk.kd_produk as KODE_PRODUK',
                    'produk.nm_produk AS NAMA_PRODUK',
                    'outlet.kd_outlet AS KD_OUTLET',
                    'outlet.nm_outlet AS OUTLET',
                    'outlet.almt_outlet AS ALAMAT',
                    'outlet.kodepos AS KODEPOS',
                    'kota.nm_kota AS KOTA',
                    'area.nm_area AS AREA',

                    'take_order.date_order as DATE_ORDER',
                    'take_order.qty_order as QUANTITY',
                    'take_order.satuan as SATUAN')
	           ->join('produk','produk.id','=','take_order.kd_produk')

                ->join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
                ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')
              ->join('user', 'user.id', '=', 'outlet.kd_user')
                ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
                ->join('area','area.id','=','user.kd_area')
	            ->get();
	        date_default_timezone_set("Asia/Jakarta");
	        $d= strtotime("now");
	        $time =date("d-m-Y H:i:s", $d);
	        $filename = $time."_TAKE_ORDER";
            if(!$order->isEmpty())
            {
    	        Excel::create($filename, function($excel) use($order) {
    	            $excel->sheet('Take Order', function($sheet) use($order) {
    	                $sheet->fromArray($order);
    	            });
    	        })->download('xls');
            }
        }
        else
        {
            $this->exportTakeOrderArea();
        }
    }

    public function exportVisitPlanArea()
    {
        $area = Auth::user()->kd_area;
        ob_start();
        $visit = VisitPlan::select(DB::raw("outlet.kode as KODE_OUTLET,
          outlet.nm_outlet as NAMA_OUTLET,
          outlet.almt_outlet as ALAMAT_OUTLET,
          kota.nm_kota as KOTA,
          area.nm_area as AREA,
          (CASE WHEN (visit_plan.approve_visit = 1) THEN 'Approved'
          WHEN (visit_plan.approve_visit = 2) THEN 'Pending' ELSE 'Denied' END) as VISIT_APPROVAL,
          visit_plan.date_create_visit as DATE_CREATE_VISIT,
          visit_plan.date_visiting as PLAN_DATE_VISIT,
          user.nama as SALES")
        )
        ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
        ->join('user', 'user.id', '=', 'outlet.kd_user')
        ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
        ->join('area', 'area.id', '=', 'outlet.kd_area')
        ->where('outlet.kd_area','=',$area)
        ->get();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_VISIT_PLAN";
        if(!$visit->isEmpty())
        {
            Excel::create($filename, function($excel) use($visit) {
                $excel->sheet('Visit Plan Area', function($sheet) use($visit) {
                    $sheet->fromArray($visit);
                });
            })->download('xls');
        }
    }

    public function exportVisitMonitorArea()
    {
        $area = Auth::user()->kd_area;
        ob_start();
        $visit = VisitPlan::select(DB::raw("outlet.kd_outlet as KODE_OUTLET,
          outlet.nm_outlet as NAMA_OUTLET,
          outlet.almt_outlet as ALAMAT_OUTLET,
          kota.nm_kota as KOTA,
          area.nm_area as AREA,
          (CASE WHEN (visit_plan.status_visit = 1) THEN 'Finished' ELSE 'Skip' END) as STATUS_VISIT,

          visit_plan.date_visit as PLAN_DATE_VISIT,
          visit_plan.date_visiting as DATE_ACTUAL_VISIT,
          user.nama as SALES")
        )
        ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
        ->join('user', 'user.id', '=', 'outlet.kd_user')
        ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
        ->join('area', 'area.id', '=', 'outlet.kd_area')
        ->where('outlet.kd_area','=',$area)
        ->get();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_VISIT_MONITOR";
        if(!$visit->isEmpty())
        {
            Excel::create($filename, function($excel) use($visit) {
                $excel->sheet('Sheet 1', function($sheet) use($visit) {
                    $sheet->fromArray($visit);
                });
            })->download('xls');
        }
    }

    public function exportTakeOrderArea()
    {
        $area = Auth::user()->kd_area;
        ob_start();
        $order = TakeOrder::select('produk.kd_produk as KODE_PRODUK',
                'produk.nm_produk AS NAMA_PRODUK',
                'outlet.nm_outlet AS OUTLET',
                'take_order.date_order as DATE_ORDER',
                'take_order.qty_order as QUANTITY',
                'take_order.satuan as SATUAN')
            ->join('produk','produk.id','=','take_order.kd_produk')
            ->join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
            ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')
            ->where('outlet.kd_area','=',$area)
            ->get();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_TAKE_ORDER";
        if(!$order->isEmpty())
        {
            Excel::create($filename, function($excel) use($order) {
                $excel->sheet('Take Order', function($sheet) use($order) {
                    $sheet->fromArray($order);
                });
            })->download('xls');
        }
    }

    public function exportVisitBukti()
    {
        $area = Auth::user()->kd_area;
        ob_start();
        $order = TakeOrder::select('produk.kd_produk as KODE_PRODUK',
                'produk.nm_produk AS NAMA_PRODUK',
                'outlet.nm_outlet AS OUTLET',
                'take_order.date_order as DATE_ORDER',
                'take_order.qty_order as QUANTITY',
                'take_order.satuan as SATUAN')
            ->join('produk','produk.id','=','take_order.kd_produk')
            ->join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
            ->join('outlet','visit_plan.kd_outlet','=','outlet.kd_outlet')
            ->where('outlet.kd_area','=',$area)
            ->get();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_VISIT_REPORT_PMP";
        if(!$order->isEmpty())
        {
            Excel::create($filename, function($excel) use($order) {
                $excel->sheet('Take Order', function($sheet) use($order) {
                    $sheet->fromArray($order);
                });
            })->download('xls');
        }
    }

    public function exportSalesForce()
    {
        $log = new AdminController;
        $log->getLogHistory('Download Master Data Sales Force');
        ob_start();
        $sf = User::where('user.kd_role','=','3')
        ->select('user.nik AS NIK','user.nama AS NAMA_SALES','user.alamat AS ALAMAT','user.telepon AS TELEPON','area.nm_area AS AREA')
        ->leftJoin('area','area.id','=','user.kd_area')
        ->join('role','role.kd_role','=','user.kd_role')
        ->get();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_SALES_FORCE";
        if(!$sf->isEmpty())
        {
            Excel::create($filename, function($excel) use($sf) {
                $excel->sheet('SF Hisamitsu', function($sheet) use($sf) {
                    $sheet->fromArray($sf);
                });
            })->download('xls');
        }
    }

    public function exportNonSF()
    {
        $log = new AdminController;
        $log->getLogHistory('Download Master Data User non SF');
        ob_start();
        $users = User::where('user.kd_role','!=','3')
        ->select('user.nik as NIK',
                 'user.nama as NAMA',
                 'user.alamat as ALAMAT',
                 'user.telepon as TELEPON',
                 'area.nm_area as AREA',
                 'role.type_role as POSISI')
        ->leftJoin('area','area.id','=','user.kd_area')
        ->join('role','role.kd_role','=','user.kd_role')
        ->get();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_USER_NONSF";
        if(!$users->isEmpty())
        {
                Excel::create($filename, function($excel) use($users) {
                    $excel->sheet('NON SF Hisamitsu', function($sheet) use($users) {
                    $sheet->fromArray($users);
                    });
                })->download('xls');
        }
    }

    public function exportSalesDetail($id)
    {
        $log = new AdminController;
	$log->getLogHistory('Download Data Sales Detail');
        ob_start();
        $sfDetail = User::where('user.kd_role','=','3')
        ->select('user.nik as NIK',
        'user.nama as NAMA',
        'user.alamat as ALAMAT',
        'user.telepon as TELEPON',
        'kota.nm_kota as KOTA',
        'role.type_role as ROLE', 'area.nm_area as AREA')
        ->leftJoin('kota','kota.id','=','user.kd_kota')
        ->leftJoin('role','role.kd_role','=','user.kd_role')
         ->leftJoin('area','area.id','=','user.kd_area')
        ->where('user.id','=',$id)
        ->get();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_SF_DETAIL";
        if(!$sfDetail->isEmpty())
        {
            Excel::create($filename, function($excel) use($sfDetail) {
                $excel->sheet('Sheet 1', function($sheet) use($sfDetail) {
                    $sheet->fromArray($sfDetail);
                });
            })->download('xls');
        }

    }

    public function exportOutletAreaOrSales()
    {
      $log = new AdminController;
      $outlet = Outlet::select(
                  'outlet.kd_outlet as Kode_Outlet',
                  'outlet.nm_outlet as Nama_Outlet',
                  'outlet.almt_outlet as Alamat_Outlet',
                  'outlet.kodepos as Kodepos',
                  'outlet.nm_pic AS Nama_PIC',
                  'outlet.tlp_pic AS Tlp_PIC',
                  'tipe.nm_tipe as Tipe',
                  'area.kd_area as Kode_Area',
                  'kota.nm_kota as Nama_Kota',
                  'user.nama as Sales_Force')
      ->leftJoin('area', 'area.id', '=', 'outlet.kd_area')
      ->leftJoin('kota', 'kota.id', '=', 'outlet.kd_kota')
      ->leftJoin('tipe', 'tipe.id', '=', 'outlet.kd_tipe')
      ->leftJoin('user', 'user.id', '=', 'outlet.kd_user');

      if(Auth::user()->kd_role == 3){
	      $log->getLogHistory('Download Data Outlet Sales');
        ob_start();
        $id=Auth::user()->id;
        $outlet=$outlet->where('outlet.kd_user','=',$id)->get();
        if(!$outlet->isEmpty())
        {
            Excel::create('outletSales', function($excel) use($outlet) {
                $excel->sheet('Sheet 1', function($sheet) use($outlet) {
                    $sheet->fromArray($outlet);
                });
            })->download('xls');
        }
      }else{
        $log->getLogHistory('Download Data Outlet in Area');
        ob_start();
        $area =Auth::user()->kd_area;
        $outlet=$outlet->where('outlet.kd_area','=',$area)->get();
        if(!$outlet->isEmpty())
        {
            Excel::create('outletArea', function($excel) use($outlet) {
                $excel->sheet('Sheet 1', function($sheet) use($outlet) {
                    $sheet->fromArray($outlet);
                });
            })->download('xls');
        }
      }

    }

     public function uploadOutlet()
    {
         if(Input::hasFile('fileUpload')){
            $path = Input::file('fileUpload')->getRealPath();
            $data = Excel::load($path, function($reader) { })->get();

            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    $insert[] = ['kode' => $value->kode,
                                'kd_dist' => $value->kd_dist,
                                'kd_area' => $value->kd_area,
                                'kd_kota' => $value->kd_kota,
                                'kd_user' => $value->kd_user,
                                'kd_tipe' => $value->kd_tipe,
                                'nm_outlet' => $value->nm_outlet,
                                'almt_outlet' => $value->almt_outlet,
                                'rank_outlet' => $value->rank_outlet,
                                'kodepos' => $value->kodepos,
                                'reg_status' => $value->reg_status,
                                'latitude' => $value->latitude,
                                'longitude' => $value->longitude,
                                ];
                }

                if(!empty($insert)){
                    Outlet::insert($insert);
                    $log = new AdminController;
		    $log->getLogHistory('Upload Master Data Outlet');
                    return redirect()->intended('admin/outlet');
                }
            }
        }
    }

    public function uploadProduk()
    {
        if(Input::hasFile('fileUpload')){
            $path = Input::file('fileUpload')->getRealPath();
            $data = Excel::load($path, function($reader) { })->get();

            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    $insert[] = ['kd_produk' => $value->kd_produk,
                                'nm_produk' => $value->nm_produk,
                                ];
                }

                if(!empty($insert)){
                    Produk::insert($insert);
                    $log = new AdminController;
		    $log->getLogHistory('Upload Master Data Produk');
                    return redirect()->intended('admin/produk');
                }
            }
        }
    }

    public function uploadDistributor()
    {
        if(Input::hasFile('fileUpload')){
            $path = Input::file('fileUpload')->getRealPath();
            $data = Excel::load($path, function($reader) { })->get();

            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    $insert[] = ['kd_dist' => $value->kd_dist,
                                'kd_kota' => $value->kd_kota,
                                'kd_tipe' => $value->kd_tipe,
                                'nm_dist' => $value->nm_dist,
                                'almt_dist' => $value->almt_dist,
                                'telp_dist' => $value->telp_dist,
                                ];
                }
                if(!empty($insert)){
                    Distributor::insert($insert);
                    $log = new AdminController;
		    $log->getLogHistory('Upload Master Data Distributor');
                    return redirect()->intended('admin/distributor');
                }
            }
        }
    }

    public function uploadArea()
    {
        if(Input::hasFile('fileUpload')){
            $path = Input::file('fileUpload')->getRealPath();
            $data = Excel::load($path, function($reader) { })->get();

            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    $insert[] = ['kd_area' => $value->kd_area,
                                'nm_area' => $value->nm_area,
                                ];
                }
                if(!empty($insert)){
                    Area::insert($insert);
                    $log = new AdminController;
		    $log->getLogHistory('Upload Master Data Area');
                    return redirect()->intended('admin/area');
                }
            }
        }
    }

    public function uploadUser()
    {
        if(Input::hasFile('fileUpload')){
            $path = Input::file('fileUpload')->getRealPath();
            $data = Excel::load($path, function($reader) { })->get();

            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    $insert[] = ['nik' => $value->nik,
                                'nama' => $value->nama,
                                'alamat' => $value->alamat,
                                'telepon' => $value->telepon,
                                'foto' => $value->foto,
                                'kd_role' => $value->kd_role,
                                'kd_area' => $value->kd_area,
                                'username' => $value->username,
                                'email_user' => $value->email_user,
                                'join_date' => $value->join_date,
                                ];
                }
                if(!empty($insert)){
                    User::insert($insert);
                    $log = new AdminController;
		    $log->getLogHistory('Upload Master Data User/Sales');
                    return redirect()->intended('admin/sales');
                }
            }
        }
    }

}
