<?php
/*
PT. Trikarya Teknologi Indonesia
Tenggilis raya 127
Office Complex Apartment Metropolis MKB 206
Surabaya, Jawa timur, Indonesia
Phone : +6231-8420384 / +6281235537717
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Input;
use Excel;
use App\Billing;
use App\Satuan;
use App\VisitPlan;
use App\VisitBukti;
use App\Outlet;
use App\Distributor;
use App\Area;
use App\Tipe;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\VisitBuktiRequest;
use App\Http\Requests\BillingRequest;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;


class SatuanController extends Controller
{
     

     
   public function index(BillingRequest $request)
    {
 

        $data = Satuan::orderBy('created_at', 'desc')
         ->get();
        return view('pages.satuan.index', compact('data'));
     }
    
      
     public function store(Request $request)
    {
         
        $satuan = new Satuan();
        $satuan->nama = $request->input('nama');
        $satuan->kode = $request->input('kode');
        $satuan->created_at = date('Y-m-d H:i:s'); 
        $satuan->save();  
        
        $data['tipe']='Satuan';
        $data['status']='new';
        $data['message']=json_encode($satuan);  
        
        $log = new AdminController;
        $log->getLogHistory('Add Satuan');
        
        
        return redirect()->route('admin.satuan.index');
    }

    public function create()
    {
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        $id = Auth::user()->id;
        if($area == 100)
        {
            $dist=Distributor::all();
            $outlet = Outlet::where('reg_status','=', 'YES')->pluck('kd_outlet', 'nm_outlet')->toArray();
        }
        else if($role == 3)
        {
            $dist=Distributor::all();
            $outlet= Outlet::where('kd_user','=',$id)->get();
        }
        else
        {
            $dist=Distributor::all();
            $outlet = Outlet::select('kd_outlet', 'nm_outlet')->where('kd_area', '=', $area)->get()->toArray();
        }

        return view('pages.satuan.create')->with('dist',$dist)->with('outlet',$outlet);
    } 

    public function edit($id)
    {
        $data['content'] = Satuan::find($id);

        
        return view('pages.satuan.edit')->with('data',$data);
    }

    public function update(Request $request, $id)
    {
        $satuan = Satuan::find($id);
        $satuan->update($request->all()); 
        $log = new AdminController;
        $log->getLogHistory('Update Satuan with ID '.$id);

        return redirect()->route('admin.satuan.index');
    }
 
     public function destroy($id)
    {
        $data = Satuan::find($id);
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Satuan with ID '.$id);
        return redirect()->route('admin.satuan.index');
    }

    
}
