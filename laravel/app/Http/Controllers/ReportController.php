<?php
/*
PT. Trikarya Teknologi Indonesia
Tenggilis raya 127
Office Complex Apartment Metropolis MKB 206
Surabaya, Jawa timur, Indonesia
Phone : +6231-8420384 / +6281235537717
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Validator;
use Input;
use Excel;
use Datatables;
use PDF;
use QrCode;

use App\User;
use App\HistoryHarga;
use App\Area;
use App\Sample;
use App\Konfigurasi;
use App\Role;
use App\Outlet;
use App\Distributor;
use App\VisitPlan;
use App\TipeActivity;
use App\TakeOrder;
use App\PhotoActivity;
use App\TipePop;
use App\Pop;
use App\Competitor;
use App\BusinessTrip;

use App\TipePhoto;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReportRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ReportController extends Controller
{
/**
* Display a listing of the resource.
*
* @return Response
*/
public function getReport()
{
    return view('pages.report.index');
}


public function outletVisit()
{
    $role = Auth::user()->kd_role;
    $area = Auth::user()->kd_area;

    $dist = Distributor::pluck('kd_dist', 'id')->toArray();

    if($area==100)
    {
        $areas = [''=>'All Area'] + Area::pluck('kd_area', 'id')->toArray();
        $sales =  [''=>'All Sales'];
    }
    else
    {
        $sales =  [''=>'All Sales']+User::where('kd_area','=',$area)->where('kd_role', '=','3')->pluck('nama', 'id')->toArray();
        $areas =[''=>'All Area'] + Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
    }
    return view('pages.report.outletVisit')->with('areas',$areas)->with('dist', $dist)->with('sales', $sales);
}

public function resultReport(ReportRequest $request)
{
        $page = '';

        $data['kd_area'] = $request->input('kd_area');
        $data['kd_sales'] = $request->input('sales');
        $data['dateFrom'] = $request->input('dateFrom');
        $data['dateTo'] = $request->input('dateTo');
        $data['kd_dist'] = $request->input('kd_dist');
        $data['dist'] = Distributor::find($data['kd_dist'])->kd_dist;
        $data['targetVisiting'] = Distributor::find($data['kd_dist'])->target_visit;

        if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
        if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");

        $osales='=';
        $dateFrom = date("Y-m-d", strtotime($data['dateFrom']));
        $dateTo =  date("Y-m-d", strtotime('+1 day', strtotime($data['dateTo'])));

        //echo $dateFrom.$dateTo.'||'.$data['dateFrom'].$data['dateTo'];
        if($data['kd_sales']==''){
            $osales='!=';
            $data['kd_sales']='null';
        }


        if($data['kd_area']==''||$data['kd_area']==100){
            $page = 'resultNasional';
            $data['result'] = DB::select(DB::raw("
                SELECT 1 as al,  c.sales as sales, a.tgl/c.sales as hkerja, b.jmlh as visit_total, (a.tgl* :targetVisiting1 ) as visit_tg, b.jmlh/(a.tgl*:targetVisiting2)*100 as visit_ac, b.jmlh/a.tgl as visit_rata
                from
                (   select 1 as al, count(vp.tgl) as tgl
                from
                (   select distinct outlet.kd_user, date(visit_plan.date_visiting) as tgl
                from visit_plan
                join outlet on outlet.kd_outlet = visit_plan.kd_outlet
                where outlet.kd_dist = :dist1 and
                visit_plan.date_visiting between :dateFrom1 and :dateTo1
                ) as vp
                group by al
                ) as a

                join
                (   select 1 as al, count(vp.id) as jmlh
                from visit_plan vp
                join outlet on outlet.kd_outlet = vp.kd_outlet
                where outlet.kd_dist = :dist2 and
                vp.date_visiting between :dateFrom2 and :dateTo2
                group by al
                ) as b
                on a.al = b.al

                join

                (
                select 1 as al, count(us.kd_user) as sales
                from
                (
                select distinct outlet.kd_user
                from visit_plan
                join outlet on outlet.kd_outlet = visit_plan.kd_outlet
                where outlet.kd_dist = :dist3 and
                visit_plan.date_visiting between :dateFrom3 and :dateTo3
            ) as us
            group by al
        ) as c
        on a.al = c.al



        "

        ),
            array('dist1' => $data['kd_dist'],'dist2' => $data['kd_dist'],'dist3' => $data['kd_dist'],'dateFrom1' => $dateFrom,'dateFrom2' => $dateFrom,'dateFrom3' => $dateFrom,'dateTo1' => $dateTo,'dateTo2' => $dateTo, 'dateTo3' => $dateTo,'targetVisiting1'=> $data['targetVisiting'], 'targetVisiting2'=> $data['targetVisiting'],));
        }else{
            $page = 'result';
            $data['result'] = DB::select(DB::raw("
                SELECT b.nm_area as area,  b.nama as sales, a.tgl as hkerja, b.jmlh as visit_total, (a.tgl* :targetVisiting1 ) as visit_tg, b.jmlh/(a.tgl*:targetVisiting2)*100 as visit_ac, b.jmlh/a.tgl as visit_rata
                from
                (   select vp.kd_user, count(vp.tgl) as tgl
                from
                (   select distinct outlet.kd_user, date(visit_plan.date_visiting) as tgl
                from visit_plan
                join outlet on outlet.kd_outlet = visit_plan.kd_outlet
                where outlet.kd_dist = :dist1 and
                outlet.kd_area = :area1 and
                outlet.kd_user $osales :sales1 and
                visit_plan.date_visiting between :dateFrom1 and :dateTo1
                ) as vp
                group by vp.kd_user
                ) as a
                join
                (   select user.nama, area.nm_area, outlet.kd_user, count(vp.id) as jmlh
                from visit_plan vp
                join outlet on outlet.kd_outlet = vp.kd_outlet
                join user on user.id = outlet.kd_user
                join area on area.id = outlet.kd_area
                where outlet.kd_dist = :dist2 and
                outlet.kd_area = :area2 and
                outlet.kd_user $osales :sales2 and
                vp.date_visiting between :dateFrom2 and :dateTo2
                group by outlet.kd_user, user.nama, area.nm_area
                ) as b
                on a.kd_user = b.kd_user"),
            array('area1' => $data['kd_area'],'area2' => $data['kd_area'],'dist1' => $data['kd_dist'],'dist2' => $data['kd_dist'],'sales1' => $data['kd_sales'],'sales2' => $data['kd_sales'],'dateFrom1' => $dateFrom,'dateFrom2' => $dateFrom,'dateTo1' => $dateTo,'dateTo2' => $dateTo, 'targetVisiting1'=> $data['targetVisiting'], 'targetVisiting2'=> $data['targetVisiting'],));

            $data['hkerja'] =  0; $data['visit_total'] =  0; $data['visit_tg'] =  0; $data['visit_ac'] =  0; $data['visit_rata'] =  0;
            $data['sales_total']=0;
            if(!empty($data['result'])){
                foreach ($data['result'] as $value) {
                    $data['sales_total']+=1;
                    $data['hkerja'] += $value->hkerja;
                    $data['visit_total'] += $value->visit_total;
                    $data['visit_tg'] += $value->visit_tg;
                }

                $data['hkerja'] = $data['hkerja']/$data['sales_total'];
                $data['visit_ac']        = $data['visit_total']/$data['visit_tg']*100;
                $data['visit_rata'] = $data['visit_total']/$data['hkerja'];
            }
            $data['area']=Area::select('nm_area')->where('id', '=', $data['kd_area'])->get();
        }

    if(Input::get('calc')) {

        return view('pages.report.'.$page)->with('data',$data);

    } elseif(Input::get('down')) {
        ob_end_clean();
        ob_start();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);

        $filename = $time."_".$page."_Outlet_Visit";
        Excel::create($filename, function($excel) use($data, $page){
            $excel->sheet('Sheet 1', function($sheet) use($data, $page) {
                $sheet->loadView('pages.report.'.$page.'Download')->with('data',$data);
                $sheet->fromArray($data);
                $sheet->setOrientation('landscape');
            });
        })->download('xls');
    }
}


public function workingTime()
{
    $role = Auth::user()->kd_role;
    $area = Auth::user()->kd_area;
    if($area==100)
    {
        $areas =  [''=>'Select Area']+Area::pluck('kd_area', 'id')->toArray();
        $sales=[''=>'Select Sales'];
    }
    else
    {
        $areas = Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
        $sales = User::where('kd_area','=',$area)->where('kd_role', '=', 3)->pluck('nama', 'id')->toArray();
    }
    return view('pages.workTime.workingTime')->with('areas',$areas)->with('sales', $sales);
}

public function resultWorkTime(ReportRequest $request)
{

    $page = 'result';
    $data['kd_area'] = $request->input('kd_area');
    $data['kd_sales'] = $request->input('sales');
    $data['dateFrom'] = $request->input('dateFrom');
    $data['dateTo'] = $request->input('dateTo');

    if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
    if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");

    $dateFrom = date("Y-m-d", strtotime($data['dateFrom']));
    $dateTo =  date("Y-m-d", strtotime('+1 day', strtotime($data['dateTo'])));

    $data['sales'] = User::select('nama','nik')->where('id','=', $data['kd_sales'])->groupBy('id')->get();
    $data['area']= Area::select('nm_area')->where('id','=', $data['kd_area'])->get();

    $data['workingTime'] = DB::select(DB::raw("
        SELECT date(visit_plan.date_visiting) as tgl, TIME_FORMAT(MIN(visit_plan.date_visiting), '%H:%i') as `CheckIn`, TIME_FORMAT(MAX(visit_plan.date_visiting), '%H:%i') as `CheckOut`,
        HOUR(TIMEDIFF( MAX(visit_plan.date_visiting),MIN(visit_plan.date_visiting))) AS `WorkingHours`, MINUTE(TIMEDIFF( MAX(visit_plan.date_visiting),MIN(visit_plan.date_visiting))) AS `WorkingMinutes`
        FROM visit_plan, user, outlet
        WHERE visit_plan.kd_outlet=outlet.kd_outlet
        AND visit_plan.date_visiting between :dateFrom and :dateTo
        AND user.id = outlet.kd_user
        AND user.id= :id
        GROUP BY DATE(visit_plan.date_visiting)"),
    array('id' => $data['kd_sales'], 'dateFrom' => $dateFrom, 'dateTo' => $dateTo, ));

    $data['totalWorkTimeMonth'] = 0;
    $data['totalWorkTimeMonthMinute'] = 0;
    foreach ($data['workingTime'] as $value) {
        $data['totalWorkTimeMonth'] += $value->WorkingHours;
        $data['totalWorkTimeMonthMinute'] += $value->WorkingMinutes;
    }

    $data['totalWorkTimeMonth'] += floor($data['totalWorkTimeMonthMinute']/60);
    $data['totalWorkTimeMonthMinute'] = $data['totalWorkTimeMonthMinute'] % 60;


    if(Input::get('show')) {
        return view('pages.workTime.'.$page)->with('data',$data);
    }
    elseif(Input::get('down')) {
        ob_end_clean();
        ob_start();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        foreach($data['sales'] as $val){
            $namaSales = $val->nama;
        }
        $filename = $time."_".$namaSales."_Working_Time";
        Excel::create($filename, function($excel) use($data){
            $excel->sheet('Sheet 1', function($sheet) use($data) {
                $sheet->loadView('pages.workTime.resultDownload')->with('data',$data);
                $sheet->fromArray($data);
                $sheet->setOrientation('landscape');
            });
        })->download('xls');
    }

}


public function effectiveCall()
{
    $role = Auth::user()->kd_role;
    $area = Auth::user()->kd_area;
    $dist = Distributor::pluck('kd_dist', 'id')->toArray();
    if($area==100)
    {
        $areas = [''=>'All Area'] + Area::pluck('kd_area', 'id')->toArray();
    }
    else
    {
        $areas = [''=>''] + Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
    }
    return view('pages.reportEc.effectiveCall')->with('areas',$areas)->with('dist',$dist);
}

public function resultEffectiveCall(Request $request){
    $rules = array();
    $validator = Validator::make(Input::all(), $rules);


    if ($validator->fails()) {
        $messages = $validator->messages();
    } else {

        $data['kd_area'] = $request->input('kd_area');
        $data['kd_sales'] = $request->input('sales');
        $data['dateFrom'] = $request->input('dateFrom');
        $data['dateTo'] = $request->input('dateTo');
        $data['kd_dist'] = $request->input('kd_dist');
        $data['dist'] = Distributor::find($data['kd_dist'])->kd_dist;
        $data['targetEc'] = Distributor::find($data['kd_dist'])->target_ec;

        if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
        if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");


        $osales='=';
        $dateFrom = date("Y-m-d", strtotime($data['dateFrom']));
        $dateTo =  date("Y-m-d", strtotime('+1 day', strtotime($data['dateTo'])));

        $page='';
        if($data['kd_sales']==''){
            $osales='!=';
            $data['kd_sales']='null';
        }


        if($data['kd_area']==''||$data['kd_area']==100){
            $page = 'resultNasional';
            $data['result'] = DB::select(DB::raw("
                SELECT c.sales as sales, b.ec as ec_total, a.wd/c.sales as working_days, a.wd*:target1 as ec_target, b.ec/(a.wd*:target2)*100 as ec_ac, b.ec/a.wd as ec_avg, b.tv as to_value, b.tv/b.ec as ec_value
                from
                (
                select 1 as al, count(tgl) as wd
                from
                (
                select distinct 1 as al, outlet.kd_user, date(visit_plan.date_visiting) as tgl
                from visit_plan
                join outlet on outlet.kd_outlet = visit_plan.kd_outlet
                where outlet.kd_dist = :dist1 and
                visit_plan.date_visiting between :dateFrom1 and :dateTo1
                ) as hk
                group by al
                ) as a
                join
                (
                select 1 as al, count(kd_to) as ec, sum(harga) as tv
                from (
                select outlet.kd_user, take_order.kd_to, sum(take_order.qty_order*produk.harga_produk) as harga
                from take_order
                join visit_plan on visit_plan.id = take_order.kd_visitplan
                join outlet on outlet.kd_outlet = visit_plan.kd_outlet
                join produk on produk.id = take_order.kd_produk
                where outlet.kd_dist = :dist2 and
                take_order.date_order between :dateFrom2 and :dateTo2
                group by outlet.kd_user, take_order.kd_to
                ) as ec
                group by al
                ) as b

                on a.al = b.al
                join

                (
                select 1 as al, count(us.kd_user) as sales
                from
                (
                select distinct outlet.kd_user
                from visit_plan
                join take_order on take_order.kd_visitplan = visit_plan.id
                join outlet on outlet.kd_outlet = visit_plan.kd_outlet
                where outlet.kd_dist = :dist3 and
                take_order.date_order between :dateFrom3 and :dateTo3
                    ) as us
                    group by al
                ) as c
                on a.al = c.al

                "), array('dist1' => $data['kd_dist'],'dist2' => $data['kd_dist'],'dist3' => $data['kd_dist'],'dateFrom1' => $dateFrom,'dateFrom2' => $dateFrom,'dateFrom3' => $dateFrom,'dateTo1' => $dateTo,'dateTo2' => $dateTo,'dateTo3' => $dateTo, 'target1'=> $data['targetEc'], 'target2'=> $data['targetEc'],));


        }
        else{
            $page = 'result';
            $data['result'] = DB::select(DB::raw("
                SELECT a.nm_area as area, a.nama as sales, b.ec as ec_total, a.wd as working_days, a.wd*:target1 as ec_target, b.ec/(a.wd*:target2)*100 as ec_ac, b.ec/a.wd as ec_avg, b.tv as to_value, b.tv/b.ec as ec_value
                from
                (
                select kd_user, nama, nm_area, count(tgl) as wd
                from
                (
                select distinct user.nama, area.nm_area, outlet.kd_user, date(visit_plan.date_visiting) as tgl
                from visit_plan
                join outlet on outlet.kd_outlet = visit_plan.kd_outlet
                join user on outlet.kd_user = user.id
                join area on outlet.kd_area = area.id
                where outlet.kd_dist = :dist1 and
                outlet.kd_area = :area1 and
                outlet.kd_user $osales :sales1 and
                visit_plan.date_visiting between :dateFrom1 and :dateTo1
                ) as hk
                group by kd_user
                ) as a
                join
                (
                select kd_user, count(kd_to) as ec, sum(harga) as tv
                from (
                select outlet.kd_user, take_order.kd_to, sum(take_order.qty_order*produk.harga_produk) as harga
                from take_order
                join visit_plan on visit_plan.id = take_order.kd_visitplan
                join outlet on outlet.kd_outlet = visit_plan.kd_outlet
                join produk on produk.id = take_order.kd_produk
                where outlet.kd_dist = :dist2 and
                outlet.kd_area = :area2 and
                outlet.kd_user $osales :sales2 and
                take_order.date_order between :dateFrom2 and :dateTo2
                group by outlet.kd_user, take_order.kd_to
                ) as ec
                group by kd_user
            ) as b
            on a.kd_user = b.kd_user"),
            array('area1' => $data['kd_area'],'area2' => $data['kd_area'],'dist1' => $data['kd_dist'],'dist2' => $data['kd_dist'],'sales1' => $data['kd_sales'],'sales2' => $data['kd_sales'],'dateFrom1' => $dateFrom,'dateFrom2' => $dateFrom,'dateTo1' => $dateTo,'dateTo2' => $dateTo, 'target1'=> $data['targetEc'], 'target2'=> $data['targetEc'],));


            $data['sales_total'] =  0; $data['ec_total'] =  0; $data['ec_target'] =  0;$data['working_days'] =  0; $data['ec_ac'] =  0; $data['ec_avg'] =  0;
            $data['to_value']=0; $data['ec_value']=0;
            if(!empty($data['result'])){
                foreach ($data['result'] as $value) {
                    $data['sales_total']+=1;
                    $data['ec_total'] += $value->ec_total;
                    $data['to_value'] += $value->to_value;
                    $data['working_days'] += $value->working_days;
                    $data['ec_target'] += $value->ec_target;
                }

                $data['working_days'] = $data['working_days']/$data['sales_total'];
                $data['ec_ac']        = $data['ec_total']/$data['ec_target']*100;
                $data['ec_avg'] = $data['ec_total']/$data['working_days'];
                $data['ec_value'] = $data['to_value']/$data['ec_total'];

            }
            $data['area']=Area::select('nm_area')->where('id', '=', $data['kd_area'])->get();
            //echo $dateFrom.$dateTo;

        }
          //$data['history']=HistoryHarga::join('produk', 'produk.id', '=', 'history_harga.kd_produk')->whereBetween('history_harga.date_change', [$dateFrom, $dateTo])->orderBy('history_harga.date_change', 'desc')->get();
        if(Input::get('calc')) {

            return view('pages.reportEc.'.$page)->with('data',$data);

        } elseif(Input::get('down')) {
            ob_end_clean();
            ob_start();
            date_default_timezone_set("Asia/Jakarta");
            $d= strtotime("now");
            $time =date("d-m-Y H:i:s", $d);

            $filename = $time."_".$page."_Effective_Call";
            Excel::create($filename, function($excel) use($data, $page){
                $excel->sheet('Sheet 1', function($sheet) use($data, $page) {
                    $sheet->loadView('pages.reportEc.'.$page.'Download')->with('data',$data);
                    $sheet->fromArray($data);
                    $sheet->setOrientation('landscape');
                });
            })->download('xls');
        }
    }
}

public function sampling()
{
    $role = Auth::user()->kd_role;
    $area = Auth::user()->kd_area;

    if($area==100)
    {
        $areas =  [''=>'All Area']+Area::pluck('kd_area', 'id')->toArray();
        $sales=[''=>'All Sales'];
    }
    else
    {
        $areas = Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
        $sales = [''=>'All Sales']+User::where('kd_area','=',$area)->where('kd_role', '=', 3)->pluck('nama', 'id')->toArray();
    }
    return view('pages.reportSampling.sampling')->with('areas',$areas)->with('sales', $sales);
}


private function buildSamplingQuery($type, $data){
    $sample=$data['sample'];
    $a='SELECT area, outlet,  user, ';
    $i=0;

    foreach($sample as $row => $id){
        $row =preg_replace('/\W+/', '', $row);
        $a=$a.' sum('.$row.') as '.$row;
        if($i+1!=count($sample)){
            $a=$a.',';
        }
        $i++;
    }
    $atas=$a.' from (';
    $a='';

    $i=0;
    foreach($sample as $row => $id){
        $a=$a.'SELECT area.kd_area as area, outlet.nm_outlet as outlet, user.nama as user, ';
        $j=0;
        foreach($sample as $roww => $idd){
            $roww =preg_replace('/\W+/', '', $roww);
            if($i==$j){
                $a = $a.' sum(sampling.'.$type.') as '.$roww.' ';
            }
            else{
                $a= $a.' 0 as '.$roww.' ';


            }
            if($j+1!=count($sample)){
                $a=$a.',';
            }
            $j++;

        }
        $a=$a.' from sampling join visit_plan on visit_plan.id = sampling.kd_visit join outlet on visit_plan.kd_outlet = outlet.kd_outlet join user on user.id = outlet.kd_user join area on area.id=outlet.kd_area where sampling.kd_sample ='.$id.' and outlet.kd_area '.$data['oarea'].' '.$data['kd_area'].' and outlet.kd_user '.$data['osales'].' '.$data['kd_user'].' and DATE_FORMAT(sampling.date_upload, "%Y-%m") = "'.$data['periode'].'" group by outlet,area,user ';
        if($i+1!=count($sample)){
            $a=$a.'union ';
        }
        $i++;
    }

    $query = $atas.$a.') as alah group by outlet, user, area';

    //print_r($query);
    return $query;
}

public function pop()
{
    $role = Auth::user()->kd_role;
    $area = Auth::user()->kd_area;
    $dist = Distributor::pluck('kd_dist', 'id')->toArray();
    if($area==100)
    {
        $areas =  [''=>'All Area']+Area::pluck('kd_area', 'id')->toArray();
        $sales=[''=>'All Sales'];
    }
    else
    {
        $areas = Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
        $sales = [''=>'All Sales']+User::where('kd_area','=',$area)->where('kd_role', '=', 3)->pluck('nama', 'id')->toArray();
    }
    return view('pages.reportPop.pop')->with('areas',$areas)->with('sales', $sales)->with('dist', $dist);
}

public function resultSampling(Request $request)
{

    $data['periode'] = $request->input('periode');
    $data['kd_area'] = $request->input('kd_area');
    $data['kd_user'] = $request->input('sales');
    $data['periode']= date("Y-m", strtotime($data['periode']));

    $data['osales'] = $data['oarea'] = '=';

    if($data['kd_user']==''){
        $data['osales']=' is not ';
        $data['kd_user']='null';
    }

    if($data['kd_area']==100||$data['kd_area']==''){
        $data['oarea']=' is not ';
        $data['kd_area']='null';
    }

    $page='';

    $data['sample']=Sample::pluck('id', 'nm_sample')->toArray();

    $data['l']=DB::select(DB::raw($this->buildSamplingQuery('qty_l', $data)));
    //print_r($this->buildSamplingQuery('qty_l', $data));
    $data['p']=DB::select(DB::raw($this->buildSamplingQuery('qty_p', $data)));

    $data['t']=DB::select(DB::raw($this->buildSamplingQuery('qty', $data)));

    //print_r($data['l']);
    //print_r($data['p']);
    //print_r($data['t']);
        if(Input::get('calc')) {

            return view('pages.reportSampling.result', compact('data'));

        } elseif(Input::get('down')) {
            ob_end_clean();
            ob_start();
            date_default_timezone_set("Asia/Jakarta");
            $d= strtotime("now");
            $time =date("d-m-Y H:i:s", $d);

            $filename = $time."_result_Sampling_Usage";
            Excel::create($filename, function($excel) use($data, $page){
                $excel->sheet('Sheet 1', function($sheet) use($data, $page) {
                    $sheet->loadView('pages.reportSampling.resultDownload')->with('data',$data);
                    $sheet->fromArray($data);
                    $sheet->setOrientation('landscape');
                });
            })->download('xls');
        }

}

public function resultPOP(Request $request)
{
      /*select nm_outlet, sum(a), sum(b)
    from (
    select outlet.nm_outlet,sampling.qty as a, 0 as b from sampling join visit_plan on visit_plan.id = sampling.kd_visit join outlet on outlet.kd_outlet = visit_plan.kd_outlet where outlet.kd_area = 675 and month(sampling.date_upload) = 5 and sampling.kd_sample=1
    UNION
    select outlet.nm_outlet,0 as a, sampling.qty as b from sampling join visit_plan on visit_plan.id = sampling.kd_visit join outlet on outlet.kd_outlet = visit_plan.kd_outlet where outlet.kd_area = 675 and month(sampling.date_upload) = 5 and sampling.kd_sample=2
        ) as c group by nm_outlet

    select nm_outlet, sum(a), sum(b)
    from (
    select outlet.nm_outlet,sampling.qty_l as a, 0 as b from sampling join visit_plan on visit_plan.id = sampling.kd_visit join outlet on outlet.kd_outlet = visit_plan.kd_outlet where outlet.kd_area = 675 and month(sampling.date_upload) = 5 and sampling.kd_sample=1
    UNION
    select outlet.nm_outlet,0 as a, sampling.qty_l as b from sampling join visit_plan on visit_plan.id = sampling.kd_visit join outlet on outlet.kd_outlet = visit_plan.kd_outlet where outlet.kd_area = 675 and month(sampling.date_upload) = 5 and sampling.kd_sample=2
        ) as c group by nm_outlet



    select nm_outlet, sum(a), sum(b) from ( select outlet.nm_outlet,sampling.qty_p as a, 0 as b from sampling join visit_plan on visit_plan.id = sampling.kd_visit join outlet on outlet.kd_outlet = visit_plan.kd_outlet where outlet.kd_area = 675 and month(sampling.date_upload) = 5 and sampling.kd_sample=1
    UNION
    select outlet.nm_outlet,0 as a, sampling.qty_p as b from sampling join visit_plan on visit_plan.id = sampling.kd_visit join outlet on outlet.kd_outlet = visit_plan.kd_outlet where outlet.kd_area = 675 and month(sampling.date_upload) = 5 and sampling.kd_sample=2 ) as c group by nm_outlet
    */
    $data['periode'] = $request->input('periode');
    $data['kd_area'] = $request->input('kd_area');
    $data['kd_user'] = $request->input('sales');
    $data['periode']= date("Y-m", strtotime($data['periode']));

    $data['osales'] = $data['oarea'] = '=';

    if($data['kd_user']==''){
        $data['osales']=' is not ';
        $data['kd_user']='null';
    }

    if($data['kd_area']==100||$data['kd_area']==''){
        $data['oarea']=' is not ';
        $data['kd_area']='null';
    }

    $page='';

    $data['tpop']=TipePop::pluck('id', 'nm_pop')->toArray();


    $sample=$data['tpop'];
    $a='SELECT area, outlet,  user, ';
    $i=0;

    foreach($sample as $row => $id){
        $row = preg_replace('/\W+/', '', $row);

        $a=$a." sum(".$row.") as '".$row."'";
        if($i+1!=count($sample)){
            $a=$a.',';
        }
        $i++;
    }
    $atas=$a.' from (';
    $a='';

    $i=0;
    foreach($sample as $row => $id){
        $a=$a.'SELECT area.kd_area as area, outlet.nm_outlet as outlet, user.nama as user, ';
        $j=0;
        foreach($sample as $roww => $idd){
            $roww =preg_replace('/\W+/', '', $roww);

            if($i==$j){
                $a = $a." sum(pop.qty) as '".$roww."' ";
            }
            else{
                $a= $a." 0 as '".$roww."' ";


            }
            if($j+1!=count($sample)){
                $a=$a.',';
            }
            $j++;

        }

        $a=$a.' from pop join visit_plan on visit_plan.id = pop.kd_visit join outlet on visit_plan.kd_outlet = outlet.kd_outlet join user on user.id = outlet.kd_user join area on area.id=outlet.kd_area where pop.jenis_pop ='.$id.' and outlet.kd_area '.$data['oarea'].' '.$data['kd_area'].' and outlet.kd_user '.$data['osales'].' '.$data['kd_user'].' and date_format(pop.date_upload, "%Y-%m") ="'.$data['periode'].'" group by area,outlet,user ';
        if($i+1!=count($sample)){
            $a=$a.'union ';
        }
        $i++;
    }

    $query = $atas.$a.') as alah group by outlet';
    //echo $query;



    $data['pop']=DB::select(DB::raw($query));
     if(Input::get('calc')) {

            return view('pages.reportPop.result', compact('data'));

        } elseif(Input::get('down')) {
            ob_end_clean();
            ob_start();
            date_default_timezone_set("Asia/Jakarta");
            $d= strtotime("now");
            $time =date("d-m-Y H:i:s", $d);

            $filename = $time."_result_Sampling_Usage";
            Excel::create($filename, function($excel) use($data, $page){
                $excel->sheet('Sheet 1', function($sheet) use($data, $page) {
                    $sheet->loadView('pages.reportPop.resultDownload')->with('data',$data);
                    $sheet->fromArray($data);
                    $sheet->setOrientation('landscape');
                });
            })->download('xls');
        }
}

public function photoActivity()
{
    $role = Auth::user()->kd_role;
    $area = Auth::user()->kd_area;
    $tact = [''=>'Select Tipe']+TipePhoto::pluck('nama_tipe', 'id')->toArray();
    if($area==100)
    {
        $areas = [''=>'Select Area']+Area::pluck('kd_area', 'id')->toArray();

    }
    else
    {
        $areas =  [''=>'Select Area']+Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
    }
    return view('pages.reportPhoto.photoActivity')->with('areas',$areas)->with('tact', $tact);
}

// competitor Report


public function resultPhotoActivity(ReportRequest $request)
{
    $page = 'result';
        $data['periode'] = $request->input('periode');
        $data['kd_area'] = $request->input('kd_area');
        $data['kd_sales'] = $request->input('sales');
        $data['kd_jenis'] = $request->input('kd_jenis');
        $data['periode']= date("Y-m", strtotime($data['periode']));
        $data['sales'] = User::select('nama','nik')->where('id','=', $data['kd_sales'])->get();
        $data['area']= Area::select('nm_area')->where('id','=', $data['kd_area'])->get();
        $data['tact']=TipePhoto::select('nama_tipe')->where('id', '=', $data['kd_jenis'])->get();
        $data['photoAct'] = PhotoActivity::select('photo_activity.*', 'outlet.nm_outlet', 'outlet.almt_outlet'
            )
        ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
        ->where('outlet.kd_area','=', $data['kd_area'])
        ->where('outlet.kd_user','=', $data['kd_sales'])
        ->where('photo_activity.kd_jenis','=', $data['kd_jenis'])
        ->whereRaw("DATE_FORMAT(photo_activity.date_take_photo, '%Y-%m')=?",array($data['periode']))

        ->get();
    if(Input::get('calc')) {

        return view('pages.reportPhoto.'.$page)->with('data',$data);
    }
    elseif(Input::get('down')) {
        ob_end_clean();
    ob_start();
    date_default_timezone_set("Asia/Jakarta");
    $d= strtotime("now");
    $time =date("d-m-Y H:i:s", $d);

    $filename = $time."_".$data['sales'][0]->nama."_Photo_Activity";
    Excel::create($filename, function($excel) use($data){
        $excel->sheet('Photo Outlet', function($sheet) use($data) {
            $sheet->loadView('pages.reportPhoto.resultDownload')->with('data',$data);
            $sheet->fromArray($data);
            $sheet->setOrientation('landscape');
        });
    })->download('xls');
    }
}

public function downloadPhotoActivity(ReportRequest $request)
{
    $page = 'result';
    $data['kd_area'] = $request->input('kd_area');
    $data['kd_sales'] = $request->input('sales');
    $data['periode'] = $request->input('periode');
    $periode = substr($data['periode'], 1,2);
    $tahun = substr($data['periode'], 3,4);
    $data['sales'] = User::select('nama','nik')->where('id','=', $data['kd_sales'])->groupBy('id')->get();
    $data['area']= Area::select('nm_area')->where('id','=', $data['kd_area'])->get();

    $data['photoAct'] = PhotoActivity::select('photo_activity.*',
        'outlet.*',
        'tp_photo.nama_tipe'
        )
    ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
    ->join('user','user.id','=','outlet.kd_user')
    ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.jenis_photo')
    ->where('photo_activity.kd_competitor' , '=', 0)
    ->where('outlet.kd_user','=', $data['kd_sales'])
    ->where(DB::raw('MONTH(photo_activity.date_upload_photo)'),'=',$periode)
    ->where(DB::raw('YEAR(photo_activity.date_upload_photo)'),'=',$tahun)
    ->orderBy('tp_photo.id','desc')
    ->get();


}
public function downloadCompetitorActivity(ReportRequest $request)
{
    $data['kd_area'] = $request->input('kd_area');
    $data['kd_sales'] = $request->input('sales');
    $data['periode'] = $request->input('periode');
    $periode = substr($data['periode'], 1,2);
    $tahun = substr($data['periode'], 3,4);
    $data['sales'] = User::select('nama','nik')->where('id','=', $data['kd_sales'])->groupBy('id')->get();
    $data['area']= Area::select('nm_area')->where('id','=', $data['kd_area'])->get();

    $data['competitorAct'] = PhotoActivity::select('photo_activity.*',
        'competitor.*',
        'outlet.*',
        'user.nama'
        )
    ->join('competitor','competitor.id','=','photo_activity.kd_competitor')
    ->join('outlet','outlet.kd_outlet','=','photo_activity.kd_outlet')
    ->join('user','user.id','=','photo_activity.kd_user')
    ->leftJoin('tp_photo','tp_photo.id','=','photo_activity.jenis_photo')
    ->where('photo_activity.kd_user','=', $data['kd_sales'])
    ->where(DB::raw('MONTH(photo_activity.date_upload_photo)'),'=',$periode)
    ->where(DB::raw('YEAR(photo_activity.date_upload_photo)'),'=',$tahun)
    ->orderBy('tp_photo.id','desc')
    ->get();

    ob_end_clean();
    ob_start();
    date_default_timezone_set("Asia/Jakarta");
    $d= strtotime("now");
    $time =date("d-m-Y H:i:s", $d);
    $filename = $time."_"."Competitor_Photo_Activity";
    Excel::create($filename, function($excel) use($data){
        $excel->sheet('Competitor Activity', function($sheet) use($data) {
            $sheet->loadView('pages.reportCompetitor.resultDownload')->with('data',$data);
            $sheet->fromArray($data);
            $sheet->setOrientation('landscape');
        });
    })->download('xls');
}
    
    public function presence(Request $request)
    {
        $area = Auth::user()->kd_area;
        if($area==100)
        {
          $data['areas'] = [''=>'Select Area'] + Area::pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'Select Sales'];
        }
        else
        {
          $data['areas'] =  Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'Select Sales']+User::where('kd_area','=',$area)->where('kd_role', '=','3')->pluck('nama', 'id')->toArray();
        }

        return view('pages.reportPresence.presence', [
            'data' => $data,
        ]);
    }

    public function resultPresence(Request $request)
    {
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;

        $data['dateTo']     = $request->input('dateTo');
        $data['dateFrom']   = $request->input('dateFrom');
        $sales              = $request->input('sales');

        if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
        if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");


        $dateFrom = date("Y-m-d",  strtotime($data['dateFrom']));
        $dateTo =  date("Y-m-d", strtotime($data['dateTo']));

        $presences = DB::table('presence')
            ->select(
                'presence.date',
                'presence.date_checkin_presence',
                'presence.date_checkout_presence',
                'user.id',
                'user.nama',
                'user.nik',
                'area.nm_area',
                'office.nm_office')
            ->leftJoin('office', 'office.id', '=', 'presence.kd_office')
            ->leftJoin('user', 'user.id', '=', 'presence.kd_user')
            ->leftJoin('area', 'area.id', '=', 'user.kd_area')
            ->where('user.id', '=' , $sales)
            ->whereBetween('presence.date', [$dateFrom, $dateTo])->get();

        $page='';
        if(Input::get('calc')) {
            return view('pages.reportPresence.result', [
                'presences' => $presences,
                'dateFrom' => $dateFrom,
                'dateTo' => $dateTo
            ]);
        } elseif(Input::get('down')) {
            ob_end_clean();
            ob_start();
            date_default_timezone_set("Asia/Jakarta");
            $d= strtotime("now");
            $time =date("d-m-Y H:i:s", $d);

            $filename = $time."_result_Presence";
            Excel::create($filename, function($excel) use($presences, $dateFrom, $dateTo, $page){
                $excel->sheet('Sheet 1', function($sheet) use($presences, $dateFrom, $dateTo, $page) {
                    $sheet->loadView('pages.reportPresence.resultDownload')->with('presences', $presences)->with('dateFrom', $dateFrom)->with('dateTo', $dateTo);
                    $sheet->fromArray($presences->toArray());
                    $sheet->setOrientation('landscape');
                });
            })->download('xls');
        } elseif(Input::get('pdf')) {
            ob_end_clean();
            ob_start();
            date_default_timezone_set("Asia/Jakarta");
            $d= strtotime("now");
            $time =date("d-m-Y H:i:s", $d);

            $filename = $time."_result_Presence";
            $pdf = PDF::loadView('pages.reportPresence.resultDownloadPDF', array('presences' => $presences, 'dateFrom' => $dateFrom, 'dateTo' => $dateTo));
            return $pdf->stream($filename.'.pdf');
        }
    }
    
    public function businessTrip(Request $request)
    {
        $area = Auth::user()->kd_area;
        if($area==100)
        {
          $data['areas'] = [''=>'Select Area'] + Area::pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'Select Sales'];
        }
        else
        {
          $data['areas'] =  Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'Select Sales']+User::where('kd_area','=',$area)->where('kd_role', '=','3')->pluck('nama', 'id')->toArray();
        }

        return view('pages.reportbusinessTrip.businessTrip', [
            'data' => $data,
        ]);
    }

    public function resultBusinessTrip(Request $request)
    {
        $data['dateTo']     = $request->input('dateTo');
        $data['dateFrom']   = $request->input('dateFrom');
        $sales              = $request->input('sales');

        $osales = '=';
        
        if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
        if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");

        $dateFrom = date("Y-m-d",  strtotime($data['dateFrom']));
        $dateTo =  date("Y-m-d", strtotime($data['dateTo']));

        $trips = DB::table('business_trip')
            ->select(
                'business_trip.id',
                'business_trip.date_request',
                'user.id as kd_user',
                'user.nama',
                'user.nik',
                'area.nm_area',
                'kota.nm_kota',
                'business_trip.date_start',
                'business_trip.date_end')
            ->leftJoin('user', 'user.id', '=', 'business_trip.kd_user')
            ->leftJoin('area', 'area.id', '=', 'user.kd_area')
            ->leftJoin('kota', 'kota.id', '=', 'business_trip.kd_kota')
            ->where('user.id', "=", $sales)
            ->whereBetween('business_trip.date_request', [$dateFrom, $dateTo])->get();

        $page='';
        if(Input::get('calc')) {
            return view('pages.reportbusinessTrip.result', [
                'trips' => $trips,
                'dateFrom' => $dateFrom,
                'dateTo' => $dateTo
            ]);
        } elseif(Input::get('down')) {
            ob_end_clean();
            ob_start();
            date_default_timezone_set("Asia/Jakarta");
            $d= strtotime("now");
            $time =date("d-m-Y H:i:s", $d);

            $filename = $time."_result_Business_Trip";
            Excel::create($filename, function($excel) use($trips, $dateFrom, $dateTo, $page){
                $excel->sheet('Sheet 1', function($sheet) use($trips, $dateFrom, $dateTo, $page) {
                    $sheet->loadView('pages.reportbusinessTrip.resultDownload')->with('trips', $trips)->with('dateFrom', $dateFrom)->with('dateTo', $dateTo);
                    $sheet->fromArray($trips->toArray());
                    $sheet->setOrientation('landscape');
                });
            })->download('xls');
        } elseif(Input::get('pdf')) {
            ob_end_clean();
            ob_start();
            date_default_timezone_set("Asia/Jakarta");
            $d= strtotime("now");
            $time =date("d-m-Y H:i:s", $d);
            $qr = base64_encode(QrCode::format('png')
                        ->size(500)->errorCorrection('H')
                        ->generate($sales));

            $filename = $time."_result_Presence";
            $pdf = PDF::loadView('pages.reportBusinessTrip.resultDownloadPDF', array('trips' => $trips, 'dateFrom' => $dateFrom, 'dateTo' => $dateTo, 'qr' => $qr));
            return $pdf->stream($filename.'.pdf');
        }
    }

}
