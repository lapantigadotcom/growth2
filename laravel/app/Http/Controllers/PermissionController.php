<?php 
/*
PT. Trikarya Teknologi Indonesia
Tenggilis raya 127
Office Complex Apartment Metropolis MKB 206
Surabaya, Jawa timur, Indonesia
Phone : +6231-8420384 / +6281235537717
*/
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\AdminRoleRequest;
use App\Http\Requests\PermissionRequest;
use App\Permission;
use Illuminate\Http\Request;
use Input;
use DB;

class PermissionController extends Controller 
{
	public function index()
	{
		$data = Permission::with('permissionRoles')->get();
		return view('pages.permission.index', compact('data'));
	}

	public function create()
	{
		return view('pages.permission.create');
	}

	public function store(PermissionRequest $request)
	{
		$data = Permission::create($request->all());
                $log = new AdminController;
       	        $log->getLogHistory('Make New Permission');
		return redirect()->route('admin.permission.index');
	}

	public function edit($id)
	{
		$data['content'] = Permission::find($id);
		return view('pages.permission.edit', compact('data'));
	}

	public function update(PermissionRequest $request, $id)
	{
		$data = Permission::find($id);
		$data->update($request->all());
                $log = new AdminController;
       	        $log->getLogHistory('Delete Permission with ID'.$id);
		return redirect()->route('admin.permission.index');
	}

	public function destroy($id)
	{
		$data = Permission::find($id);
		$data->permissionRoles()->detach();
		$data->delete();
		$log = new AdminController;
       	        $log->getLogHistory('Delete Permission with ID'.$id);
		return redirect()->route('admin.permission.index');
	}
}
