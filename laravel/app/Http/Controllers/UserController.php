<?php
/*
PT. Trikarya Teknologi Indonesia
Tenggilis raya 127
Office Complex Apartment Metropolis MKB 206
Surabaya, Jawa timur, Indonesia
Phone : +6231-8420384 / +6281235537717
*/
namespace App\Http\Controllers;
use DB;
use Auth;
use Session;
use Input;
use File;
use Validator;
use App\User;
use App\Role;
use App\Kota;
use App\Area;
use App\Outlet;
use App\VisitPlan;
use App\TakeOrder;
use App\PhotoActivity;
use App\CompetitorActivity;
use App\Logging;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data=User::where('user.kd_role','!=','3')
        ->select('user.id','user.nik','user.nama','user.alamat','user.telepon','user.foto','area.nm_area','role.type_role')
        ->leftJoin('area','area.id','=','user.kd_area')
        ->join('role','role.kd_role','=','user.kd_role')
        ->get();
        return view('pages.user.index')->with('data',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
       $roles = [''=>''] + Role::pluck('type_role', 'kd_role')->toArray();
        $area = [''=>''] + Area::pluck('nm_area', 'id')->toArray();
        return view('pages.user.create')->with('roles',$roles)->with('area',$area);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(UserRequest $request)
    {
       $rules = array(
            'nama'             => 'required',
            'username'         => 'required|unique:user',
            'email_user'       => 'required|email|unique:user',
            'password'         => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();

            $roles = [''=>''] + Role::pluck('type_role', 'kd_role')->toArray();
            $area = [''=>''] + Area::pluck('nm_area', 'id')->toArray();
            return view('pages.user.create')->withErrors($validator)->with('roles',$roles)->with('area',$area);
        } else {
            $user = new User;
            if(Input::hasFile('foto')){
                 $file = $request->file('foto');
                 $destinationPath = 'userphoto/';
                 $filename = str_random(6).'_'.$file->getClientOriginalName();
                 $file->move($destinationPath, $filename);
            }
            else { $filename = '';}

            $user->nik = $request->input('nik');
            $user->nama = $request->input('nama');
            $user->alamat = $request->input('alamat');
            $user->telepon = $request->input('telepon');
            $user->kd_role = $request->input('kd_role');
            $user->kd_area = $request->input('kd_area');
            $user->username = $request->input('username');
            $user->password = $request->input('password');
            $user->api_token = bcrypt($request->input('username').$request->input('password'));
            $user->email_user = $request->input('email_user');
            $user->join_date = $request->input('join_date');
            $user->foto = $filename;
            $user->save();
            $log = new AdminController;
            $log->getLogHistory('Make New User');
            return redirect()->route('admin.user.index');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $page = 'show';
        $data['content'] =User::where('user.kd_role','!=','3')
        ->select('user.*','area.nm_area','role.type_role')
        ->leftJoin('area','area.id','=','user.kd_area')
        ->join('role','role.kd_role','=','user.kd_role')
        ->where('user.id','=',$id)
        ->first();

        $data['logs']=Logging::join('user', 'user.id', '=', 'logging.kd_user')->orderBy('log_time', 'desc')->where('logging.kd_user','=',$id)->get();
        return view('pages.user.'.$page,compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $data['content'] = User::find($id);
        $roles = [''=>''] + Role::pluck('type_role', 'kd_role')->toArray();
        $area = [''=>''] + Area::pluck('nm_area', 'id')->toArray();
        return view('pages.user.edit')->with('data',$data)->with('roles',$roles)->with('area',$area);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::find($id);
        if(Input::hasFile('foto')){
	      	$file = $request->file('foto');
	        $destinationPath = 'userphoto/';
	        $filename = str_random(6).'_'.$file->getClientOriginalName();
	        $file->move($destinationPath, $filename);
        }
        else
        {
            $filename = $user->foto;
        }

        if ($request->input('password') == '')
        {
	        $user->nik = $request->input('nik');
	        $user->nama = $request->input('nama');
	        $user->alamat = $request->input('alamat');
	        $user->telepon = $request->input('telepon');
	        $user->kd_role = $request->input('kd_role');
	        $user->kd_area = $request->input('kd_area');
	        $user->username = $request->input('username');
	        $user->email_user = $request->input('email_user');
	        $user->join_date = $request->input('join_date');
	        $user->foto = $filename;
	        $user->save();
        }
        else
        {
            $user->nik = $request->input('nik');
            $user->nama = $request->input('nama');
            $user->alamat = $request->input('alamat');
            $user->telepon = $request->input('telepon');
            $user->kd_role = $request->input('kd_role');
            $user->kd_area = $request->input('kd_area');
            $user->username = $request->input('username');
            $user->email_user = $request->input('email_user');
            $user->password = $request->input('password');
            $user->join_date = $request->input('join_date');
            $user->foto = $filename;
            $user->save();
        }
        $log = new AdminController;
        $log->getLogHistory('Update User with ID '.$id);
	return redirect()->route('admin.user.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $data = User::with(
           'outlets',
           'outlets.visitPlans',
           'outlets.photoActivities',
           'logs'
        )->find($id);

        foreach ($data->outlets as $row) {
            foreach ($row->visitPlans as $value) {
                $value->delete();
            }
            foreach ($row->photoActivities as $value) {
               $value->delete();
            }
            $row->delete();
        }
        foreach ($data->logs as $row) {
               $row->delete();
        }
        $data->delete();
        $log = new AdminController;
	$log->getLogHistory('Delete User with ID '.$id);
        return redirect()->route('admin.user.index');

    }

    public function myProfile()
    {
        $page = 'show';
        $id= Auth::user()->id;
        $data['content'] = User::select('user.*','role.type_role','area.nm_area')
        ->join('role','role.kd_role','=','user.kd_role')
        ->leftJoin('area','area.id','=','user.kd_area')
        ->where('user.id','=',$id)
        ->first();


        //working time/day
        if(Auth::user()->kd_role == 3){
            $data['outletVisit'] = Visitplan::join('outlet','outlet.kd_outlet', '=', 'visit_plan.kd_outlet')
            ->join('user','user.id', '=', 'outlet.kd_user')
            ->where('user.id', '=', $id)
            ->where('visit_plan.status_visit', '=', 1)
            ->count();

            $data['workingTime'] = DB::select(DB::raw("SELECT HOUR(TIMEDIFF( MAX(visit_plan.date_visiting),MIN(visit_plan.date_visiting))) AS `working`
            FROM visit_plan, user, outlet
            WHERE visit_plan.kd_outlet=outlet.kd_outlet
            AND DATE(visit_plan.date_visiting) = CURDATE()
            AND user.id = outlet.kd_user
            AND user.id= :id ") , array('id' => $id,));

            $data['takeOrder'] = TakeOrder::join('visit_plan','visit_plan.id','=','take_order.kd_visitplan')
            ->join('outlet','outlet.kd_outlet', '=', 'visit_plan.kd_outlet')
            ->join('user','user.id', '=', 'outlet.kd_user')
            ->where('user.id', '=', $id)
            ->where('visit_plan.status_visit', '=', 1)
            ->where('take_order.status_order', '=', 1)
            ->count();

            $data['competitorPhoto'] = CompetitorActivity::join('outlet','outlet.kd_outlet', '=', 'comp_activity.kd_outlet')
            ->join('user','user.id', '=', 'outlet.kd_user')
            ->where('user.id', '=', $id)
            ->count();

            $data['takePhoto'] = PhotoActivity::join('outlet','outlet.kd_outlet', '=', 'photo_activity.kd_outlet')
            ->join('user','user.id', '=', 'outlet.kd_user')
            ->where('user.id', '=', $id)
            ->count();
        }

        return view('pages.userProfile.'.$page,compact('data'));
    }

    public function editProfile()
    {
        $id= Auth::user()->id;
        $data['content'] = User::find($id);
        $area = [''=>''] + Area::pluck('nm_area', 'id')->toArray();
        return view('pages.userProfile.edit')->with('data',$data)->with('area',$area);
    }

    public function updateProfile(UserRequest $request)
    {
        $id = Auth::user()->id;
        $user = User::find($id);
        if(Input::hasFile('foto')){
            $file = $request->file('foto');
            $destinationPath = 'userphoto/';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $file->move($destinationPath, $filename);

            $user->nik = $request->input('nik');
            $user->nama = $request->input('nama');
            $user->alamat = $request->input('alamat');
            $user->telepon = $request->input('telepon');
            $user->kd_role = $request->input('kd_role');
            $user->kd_area = $request->input('kd_area');
            $user->username = $request->input('username');
            $user->email_user = $request->input('email_user');
            $user->join_date = $request->input('join_date');
            $user->foto = $filename;
            $user->save();
            $log = new AdminController;
	    $log->getLogHistory('Update My Profile');
            return redirect()->route('admin.profile',[Auth::user()->username]);
        }
        else
        {
            $user->update($request->all());
            $log = new AdminController;
	    $log->getLogHistory('Update My Profile');
            return redirect()->route('admin.profile',[Auth::user()->username]);

        }
    }
}
