<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\User;
use DB;
use Auth;
use File;
use Session;
use Input;
use Excel;
use App\VisitProblem;
use App\VisitPlan;
use App\Http\Requests\VisitPlanRequest;
use App\Http\Requests\VisitProblemRequest;
use App\Outlet;
use App\Kota;
use App\Area;
use App\Tipe;
use App\Distributor;

class VisitProblemController extends Controller
{
  public function form()
  {

    $area = Auth::user()->kd_area;
    if($area==100)
    {
      $data['areas'] = [''=>'All Area'] + Area::pluck('kd_area', 'id')->toArray();
      $data['sales'] =  [''=>'All Sales'];

    }
    else
    {
      $data['areas'] = [''=>'All Area'] + Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
      $data['sales'] =  [''=>'All Sales']+User::where('kd_area','=',$area)->where('kd_role', '=','3')->pluck('nama', 'id')->toArray();
    }

    $data['tipe'] =  [''=>'All Tipe'] + Tipe::pluck( 'nm_tipe','id')->toArray();
    $data['distributor'] =  [''=>'All Distributor'] + Distributor::pluck('kd_dist','id' )->toArray();

    if(Auth::user()->kd_area==100){
      $data['data'] = VisitProblem::select('outlet.nm_outlet',
        'user.nama',
        'visit_plan.kd_outlet',
        'visit_problem.*')
      ->join('visit_plan', 'visit_problem.kd_visit','=','visit_plan.id')
      ->leftJoin('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
      ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
      ->leftJoin('distributor', 'distributor.id','=','outlet.kd_dist')
      ->whereBetween('visit_problem.date_take_photo', [date('Y-m-d', strtotime("-7 day")), date('Y-m-d', strtotime("+1 day"))])
      ->where('status', 1)
      ->orderBy('visit_problem.date_take_photo', 'desc')
      ->get();
      return view('pages.visit.viewFormProblem')->with('data',$data);
    }
    else if(Auth::user()->kd_role == 3){
      $id = Auth::user()->id;
      $data['data'] = VisitProblem::select('outlet.nm_outlet',
        'user.nama',
        'visit_plan.kd_outlet',
        'visit_problem.*')
      ->join('visit_plan', 'visit_problem.kd_visit','=','visit_plan.id')
      ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
      ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
      ->leftJoin('distributor', 'distributor.id','=','outlet.kd_dist')
      ->where('outlet.kd_user','=', $id)
      ->whereBetween('visit_problem.date_take_photo', [date('Y-m-d', strtotime("-7 day")), date('Y-m-d', strtotime("+1 day"))])
      ->where('status', 1)
      ->orderBy('visit_problem.date_take_photo', 'desc')
      ->get();
      return view('pages.visit.viewFormProblem')->with('data',$data);

    }
    else{
      $area = Auth::user()->kd_area;
      $data ['data']= VisitProblem::select('outlet.nm_outlet',
        'user.nama',
        'visit_plan.kd_outlet',
        'visit_problem.*')
      ->join('visit_plan', 'visit_problem.kd_visit','=','visit_plan.id')
      ->join('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
      ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
      ->leftJoin('distributor', 'distributor.id','=','outlet.kd_dist')
      ->where('outlet.kd_area','=', $area)
      ->whereBetween('visit_problem.date_take_photo', [date('Y-m-d', strtotime("-7 day")), date('Y-m-d', strtotime("+1 day"))])
      ->where('status', 1)
      ->orderBy('visit_problem.date_take_photo', 'desc')
      ->get();
      return view('pages.visit.viewFormProblem')->with('data',$data);
    }
  }


  public function index(VisitProblemRequest $request){
    $data['dateFrom'] = $request->input('dateFrom');
    $data['dateTo'] = $request->input('dateTo');
    $sales = $request->input('sales');
    $area = $request->input('kd_area');
    $tipe = $request->input('nm_tipe');
    $dist = $request->input('kd_dist');
    $osales=$oarea=$otipe=$odist='=';
    if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
    if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");
    if($sales==''){
      $sales='null';
      $osales='!=';
    }
    if($area==''||$area=='100'){
      $area='null';
      $oarea='!=';
    }
    if($tipe==''){
      $tipe='null';
      $otipe='!=';
    }
    if($dist==''){
      $dist='null';
      $odist='!=';
    }


    $dateFrom = date("Y-m-d", strtotime($data['dateFrom']));
    $dateTo =  date("Y-m-d", strtotime('+1 day', strtotime($data['dateTo'])));

    if(Input::get('show')){
      if(Auth::user()->kd_area==100){
        $data['data'] = VisitProblem::select('outlet.nm_outlet',
          'user.nama',
          'visit_plan.kd_outlet',
          'visit_problem.*')
        ->join('visit_plan', 'visit_problem.kd_visit','=','visit_plan.id')
        ->leftJoin('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
        ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
        ->leftJoin('distributor', 'distributor.id','=','outlet.kd_dist')
        ->where('user.id', $osales, $sales)
        ->where('outlet.kd_area', $oarea, $area)
        ->where('outlet.kd_tipe', $otipe, $tipe)
        ->where('outlet.kd_dist', $odist, $dist)
        ->whereBetween('visit_problem.date_take_photo',  [$dateFrom, $dateTo])
        ->where('status', 1)
        ->orderBy('visit_problem.date_take_photo', 'desc')
        ->get();
        return view('pages.visitProblem.index')->with('data',$data);
      }
      else if(Auth::user()->kd_role ==3){
        $id = Auth::user()->id;
        $data['data'] = VisitProblem::select('outlet.nm_outlet',
          'user.nama',
          'visit_plan.kd_outlet',
          'visit_problem.*')
        ->join('visit_plan', 'visit_problem.kd_visit','=','visit_plan.id')
        ->leftJoin('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
        ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
        ->leftJoin('distributor', 'distributor.id','=','outlet.kd_dist')
        ->where('outlet.kd_user','=', $id)
        ->where('outlet.kd_area', $oarea, $area)
        ->where('outlet.kd_tipe', $otipe, $tipe)
        ->where('outlet.kd_dist', $odist, $dist)
        ->whereBetween('visit_problem.date_take_photo', [$dateFrom, $dateTo])
        ->where('status', 1)
        ->orderBy('visit_problem.date_take_photo', 'desc')
        ->get();
        return view('pages.visitProblem.index')->with('data',$data);
      }
      else{
        $area = Auth::user()->kd_area;
        $data['data'] = VisitProblem::select('outlet.nm_outlet',
          'user.nama',
          'visit_plan.kd_outlet',
          'visit_problem.*')
        ->join('visit_plan', 'visit_problem.kd_visit','=','visit_plan.id')
        ->leftJoin('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
        ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
        ->leftJoin('distributor', 'distributor.id','=','outlet.kd_dist')
        ->where('outlet.kd_area','=', $area)
        ->where('user.id', $osales, $sales)
        ->where('outlet.kd_tipe', $otipe, $tipe)
        ->where('outlet.kd_dist', $odist, $dist)
        ->whereBetween('visit_problem.date_take_photo', [$dateFrom, $dateTo])
        ->where('status', 1)
        ->orderBy('visit_problem.date_take_photo', 'desc')
        ->get();
        return view('pages.visitProblem.index')->with('data',$data);
      }
    }
    else if(Input::get('download')){
      $this->downloadVisitProblem($sales, $osales, $tipe, $otipe, $area, $oarea, $dist, $odist, $dateFrom, $dateTo);
    }
  }

  public function formapproval(){
    $area = Auth::user()->kd_area;
    $role = Auth::user()->kd_role;
    if($area==100){
      $data['data']=VisitProblem::join('visit_plan', 'visit_plan.id', '=', 'visit_problem.kd_visit')
      ->join('outlet', 'outlet.kd_outlet','=', 'visit_plan.kd_outlet')
      ->join('user', 'user.id', '=', 'outlet.kd_user')
      ->where('visit_problem.status', 0)->get();
    }
    else{
      echo 'ehe';
      $data['data']=VisitProblem::join('visit_plan', 'visit_plan.id', '=', 'visit_problem.kd_visit')
      ->join('outlet', 'outlet.kd_outlet','=', 'visit_plan.kd_outlet')
      ->join('user', 'user.id', '=', 'outlet.kd_user')
      ->where('visit_problem.status',0)->where('outlet.kd_outlet', $area)
      ->get();
    }
    return view('pages.visitProblem.formApprove')->with('data', $data);
  }

  public function approvedProblem(Request $request){
    $id = $request->input('approve_problem');
    $data = VisitProblem::findMany($id);
    foreach ($data as $value)
    {
      $value->update(['status' => 1]);
      $value->save();
      $visit = VisitPlan::find($value->kd_visit);
      $visit->update(['date_visiting' => date("Y-m-d H:i:s"), 'status_visit' => 1]);
      $visit->save();
      $dataa['status']='new';
      $dataa['tipe']='VisitProblem';
      $dataa['message']=$visit;
      $fcm_id=User::select('user.id_gcm')->join('outlet', 'outlet.kd_user', '=', 'user.id')
      ->join('visit_plan', 'visit_plan.kd_outlet', '=','outlet.kd_outlet')
      ->where('visit_plan.id', $visit->id)->get();
      $this->sendNotif($dataa, $fcm_id[0]->id_gcm);
    }

    $id = $request->input('denied_problem');
    $data = VisitProblem::findMany($id);
    foreach ($data as $value)
    {
      $value->update(['status' => -1]);
      $value->save();
      $visit = VisitPlan::find($value->kd_visit);

      $dataa['status']='reject';
      $dataa['tipe']='VisitProblem';
      $dataa['message']=$visit;
      $fcm_id=User::select('user.id_gcm')->join('outlet', 'outlet.kd_user', '=', 'user.id')
      ->join('visit_plan', 'visit_plan.kd_outlet', '=','outlet.kd_outlet')
      ->where('visit_plan.id', $visit->id)->get();
      $this->sendNotif($dataa, $fcm_id[0]->id_gcm);
    }

    $log = new AdminController;
    $log->getLogHistory('Approve Visit Problem');
    return redirect()->route('admin.visitProblem.form');
  }

   

  public function destroy($id)
  {
    $data = VisitProblem::find($id);
    //$data->delete();
    $data->update(array('status' => 0));
    $data->save();

    $visit = VisitPlan::find($data->kd_visit);
    $visit->update(['date_visiting' => date("Y-m-d H:i:s"), 'status_visit' => 0]);
    $visit->save();

    $dataa['status']='reject';
      $dataa['tipe']='VisitProblem';
      $dataa['message']=$visit;

      $fcm_id=User::select('user.id_gcm')->join('outlet', 'outlet.kd_user', '=', 'user.id')
      ->join('visit_plan', 'visit_plan.kd_outlet', '=','outlet.kd_outlet')
      ->where('visit_plan.id', $visit->id)->get();

      $this->sendNotif($dataa, $fcm_id[0]->id_gcm);

    $log = new AdminController;
    $log->getLogHistory('Delete Visit Problem with ID '.$id);
    return redirect()->route('admin.visitProblem.form');

  }
  public function store(VisitProblemRequest $request)
  {
    $log = new AdminController;
    VisitProblem::create($request->all());
    $log->getLogHistory('Make Visit Problem');
    return redirect()->route('admin.visitProblem.form');
  }

  public function update(VisitProblemRequest $request, $id)
  {
    $data = VisitProblem::find($id);

    $data->update($request->all());
    if(Input::hasFile('nm_photo')){
      File::delete('image_upload/visit_problem/'.$data->nm_photo);
      $file = $request->file('nm_photo');
      $destinationPath = 'image_upload/visit_problem/';
      $filename = str_random(6).'_'.$file->getClientOriginalName();
      $file->move($destinationPath, $filename);
      $data->nm_photo=$filename;

    }
    $data->save();

    $log = new AdminController;
    $log->getLogHistory('Update Visit Problem with ID '.$id);
    return redirect()->route('admin.visitProblem.form');
  }



  public function downloadVisitProblem($sales, $osales, $tipe, $otipe, $area, $oarea, $dist, $odist, $dateFrom, $dateTo)
  {
    $log = new AdminController;
    $log->getLogHistory('Download Data Visit Problem');

    $role = Auth::user()->kd_role;
    $areas = Auth::user()->kd_area;
    if($areas == 100)
    {

      $visit = VisitProblem::select(DB::raw("
        user.nama as SALES,
        visit_problem.kd_problem as KODE_PROBLEM,
        visit_problem.kd_visit as KODE_VISIT,
        outlet.kd_outlet as KODE_OUTLET,
        outlet.nm_outlet as NAMA_OUTLET,
        tipe.nm_tipe as TIPE_OUTLET,
        visit_problem.detail_problem as DETAIL_PROBLEM,
        visit_problem.nm_photo as THUMBNAIL,
        visit_problem.date_upload_photo as DATE_UPLOAD_PHOTO,
        visit_problem.date_take_photo as DATE_TAKE_PHOTO
        ")
      )
      ->join('visit_plan', 'visit_problem.kd_visit','=','visit_plan.id')
      ->leftJoin('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
      ->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')
      ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
      ->leftJoin('distributor', 'distributor.id','=','outlet.kd_dist')
      ->where('user.id', $osales, $sales)
      ->where('outlet.kd_area', $oarea, $area)
      ->where('outlet.kd_tipe', $otipe, $tipe)
      ->where('outlet.kd_dist', $odist, $dist)
      ->whereBetween('visit_problem.date_take_photo',  [$dateFrom, $dateTo])
      ->get();

    }
    else if(Auth::user()->kd_role == 3){

      $id =Auth::user()->id;
      $visit = VisitProblem::select(DB::raw("
        user.nama as SALES,
        visit_problem.kd_problem as KODE_PROBLEM,
        visit_problem.kd_visit as KODE_VISIT,
        outlet.kd_outlet as KODE_OUTLET,
        outlet.nm_outlet as NAMA_OUTLET,
        tipe.nm_tipe as TIPE_OUTLET,
        visit_problem.detail_problem as DETAIL_PROBLEM,
        visit_problem.nm_photo as THUMBNAIL,
        visit_problem.date_upload_photo as DATE_UPLOAD_PHOTO,
        visit_problem.date_take_photo as DATE_TAKE_PHOTO
        ")
      )
      ->join('visit_plan', 'visit_problem.kd_visit','=','visit_plan.id')
      ->leftJoin('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
      ->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')
      ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
      ->leftJoin('distributor', 'distributor.id','=','outlet.kd_dist')
      ->where('user.id', '=', $id)
      ->where('outlet.kd_area', $oarea, $area)
      ->where('outlet.kd_tipe', $otipe, $tipe)
      ->where('outlet.kd_dist', $odist, $dist)
      ->whereBetween('visit_problem.date_take_photo',  [$dateFrom, $dateTo])
      ->get();

    }
    else
    {

      $visit = VisitProblem::select(DB::raw("
        user.nama as SALES,
        visit_problem.kd_problem as KODE_PROBLEM,
        visit_problem.kd_visit as KODE_VISIT,
        outlet.kd_outlet as KODE_OUTLET,
        outlet.nm_outlet as NAMA_OUTLET,
        tipe.nm_tipe as TIPE_OUTLET,
        visit_problem.detail_problem as DETAIL_PROBLEM,
        visit_problem.nm_photo as THUMBNAIL,
        visit_problem.date_upload_photo as DATE_UPLOAD_PHOTO,
        visit_problem.date_take_photo as DATE_TAKE_PHOTO
        ")
      )
      ->join('visit_plan', 'visit_problem.kd_visit','=','visit_plan.id')
      ->leftJoin('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
      ->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')
      ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
      ->leftJoin('distributor', 'distributor.id','=','outlet.kd_dist')
      ->where('user.id', $osales, $sales)
      ->where('outlet.kd_area', '=', $areas)
      ->where('outlet.kd_tipe', $otipe, $tipe)
      ->where('outlet.kd_dist', $odist, $dist)
      ->whereBetween('visit_problem.date_take_photo',  [$dateFrom, $dateTo])
      ->get();

    }
    ob_start();
    ob_end_clean();
    date_default_timezone_set("Asia/Jakarta");
    $d= strtotime("now");
    $time =date("d-m-Y H:i:s", $d);
    $filename = $time."_BAD_SIGNAL_VISIT";
    Excel::create($filename, function($excel) use($visit) {
      $excel->sheet('Visit Problem', function($sheet) use($visit) {
        $sheet->fromArray($visit);
      });
    })->download('xls');
  }

  public function sendNotif($data, $fcm_id){
        $ch = curl_init("https://fcm.googleapis.com/fcm/send");
        $header = array
        (
            'Authorization: key=AAAA_xS6-L0:APA91bEYaR-O3xM8FvcFqo6BOuOWNHaYId6Th0KExWi9dHfWlGWLAfnsUaQpLaVrZKqKIjWZD2ExvfxlOf2gZlPPA0msaGcilVik5mYEOA95TDriQ7plaZwZkes7-3tm4fWHJNINdr4W',
            'Content-Type: application/json'
        );
        $msg = array
        (
            'to'        => $fcm_id,
            'priority'      => "high",
            'data'  => $data
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($msg));

        $response = curl_exec($ch);
        //echo 'a';
        //print_r($data);
        //print_r($response);
        curl_close($ch);
    }

}
