<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use File;
use Auth;
use Session;
use Image;
use App\News;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    public function index()
    {
        $data = News::orderBy('date_upload', 'desc')->get();
        return view('pages.news.index', compact('data'));
    }

    public function create()
    {
    	
        return view('pages.news.create');
    }


    public function store(Request $request)
    {
    	$filename='';
        if(Input::hasFile('path_image')){
            
            $file = $request->file('path_image');
            $destinationPath = 'image_upload/news/';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $img = Image::make($file)->resize(600, null, function($constraint){
                $constraint->aspectRatio();
            })->encode('png')->save($destinationPath.$filename);
            
            
        }
        $news = new News();
        $news->judul = $request->input('judul');
        $news->headline = $request->input('headline');
        $news->content = $request->input('content');
        $news->path_image = $filename;
        $news->date_upload = date('Y-m-d H:i:s');
        $news->status = $request->input('status');

        $news->save();	
        
        $data['tipe']='News';
        $data['status']='new';
        $data['message']=json_encode($news);  
        
        $log = new AdminController;
        $log->getLogHistory('Add News');
        $this->sendNotif($data);
        //echo 'berhasil';
        return redirect()->route('admin.news.index');
    }

    public function edit($id)
    {
        $data['content'] = News::find($id);

        
        return view('pages.news.edit')->with('data',$data);
    }

    public function update(Request $request, $id)
    {
        $news = News::find($id);
        $news->update($request->all()); 
        
        if(Input::hasFile('path_image')){
         File::delete('image_upload/news/'.$news->path_image);    
            $file = $request->file('path_image');
            $destinationPath = 'image_upload/news/';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $img = Image::make($file)->resize(600, null, function($constraint){
                $constraint->aspectRatio();
            })->encode('png')->save($destinationPath.$filename);
            $news->path_image=$filename;
            $news->save();
        }
        $log = new AdminController;
        $log->getLogHistory('Update News with ID '.$id);

        return redirect()->route('admin.news.index');
    }

    public function sendNotif($data){
        $ch = curl_init("https://fcm.googleapis.com/fcm/send");
        $header = array
        (
            'Authorization: key=AAAA_xS6-L0:APA91bEYaR-O3xM8FvcFqo6BOuOWNHaYId6Th0KExWi9dHfWlGWLAfnsUaQpLaVrZKqKIjWZD2ExvfxlOf2gZlPPA0msaGcilVik5mYEOA95TDriQ7plaZwZkes7-3tm4fWHJNINdr4W',
            'Content-Type: application/json'
        );
        $msg = array
        (
            'to'        => "/topics/allDevices",
            'priority'      => "high",
            'data'  => $data
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($msg));

        $response = curl_exec($ch);
        //echo 'a';
        //print_r($data);
        print_r($response);
        curl_close($ch);
    }

    public function destroy($id)
    {
        $data = News::find($id);
        $id = $data->id;
        $data->delete();
        $dataa['tipe']='News';
        $dataa['status']='reject';
        $dataa['message']=json_encode(array('id'=>$id));  
        $this->sendNotif($dataa);
        $log = new AdminController;
        $log->getLogHistory('Delete News with ID '.$id);
        return redirect()->route('admin.news.index'); 
    }

}
