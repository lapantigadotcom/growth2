<?php
/*
PT. Trikarya Teknologi Indonesia
Tenggilis raya 127
Office Complex Apartment Metropolis MKB 206
Surabaya, Jawa timur, Indonesia
Phone : +6231-8420384 / +6281235537717
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use App\Produk;
use App\Brand;
use App\Area;
use App\HistoryHarga;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProdukRequest;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;

class ProdukController extends Controller
{
    public function index()
    {   
         $data['content'] = Produk::with('areas','brand')->orderBy('id', 'DESC')->get();
        return view('pages.produk.index', compact('data'));
        // return  compact('data');
    }

    public function historyIndex(){
      $data['nm_produk']="";
        $data['history'] = HistoryHarga::join('produk', 'produk.id','=', 'history_harga.kd_produk' )->orderBy('history_harga.date_change', 'desc')->select('produk.nm_produk', "history_harga.*")->get();
        return view('pages.produk.history', compact('data'));
    }

    public function ahistory($id){
      $data['nm_produk']=Produk::find($id)->nm_produk;
      $data['history'] = HistoryHarga::join('produk', 'produk.id', '=', 'history_harga.kd_produk')->where('history_harga.kd_produk', $id)->orderBy('history_harga.date_change', 'desc')->select('produk.nm_produk', 'history_harga.*')->get();
      return view('pages.produk.history', compact('data'));
    }



    public function create()
    {   
       $area = [''=>''] + Area::pluck('nm_area', 'id')->toArray();
       $brand = [''=>''] + Brand::pluck('nama', 'id')->toArray();
       return view('pages.produk.create')
       ->with('area',$area)
       ->with('brand',$brand);
    }

    public function store(ProdukRequest $request)
    {   
        // $produk = new Produk();
        // Produk::create($request->all());
        $produk = new Produk();
        $produk->nm_produk = $request->input('nm_produk');
        $produk->kd_area = $request->input('kd_area');
        $produk->id_brand = $request->input('id_brand');
        $produk->kd_produk = $request->input('kd_produk');
        $produk->harga_produk = $request->input('harga_produk');
        $produk->save();  

        $log = new AdminController;
        $log->getLogHistory('New Product added');
        return redirect()->route('admin.produk.index');
    }

    public function edit($id)
    {
        $data['content'] = Produk::find($id);
        $area = [''=>''] + Area::pluck('nm_area', 'id')->toArray();
        $brand = [''=>''] + Brand::pluck('nama', 'id')->toArray();
        return view('pages.produk.edit', compact('data'))
        ->with('brand',$brand)
        ->with('area',$area);
    }

    public function update(ProdukRequest $request, $id)
    {
        $data = Produk::find($id);
        $history = new HistoryHarga;
        $history->harga_before = $data->harga_produk;
        $history->kd_produk = $data->id;

        $data->update($request->all());

        $history->harga_after = $request->input('harga_produk');

        $history->date_change = date('Y-m-d H:i:s');
        $history->save();

        $log = new AdminController;
        $log->getLogHistory('Update Product with ID '.$id);
        return redirect()->route('admin.produk.index');
    }

    public function destroy($id)
    {
        $data = Produk::with('takeOrders')->find($id);
        foreach($data->takeOrders as $row)
        {
            $row->delete();
        }
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Product with ID '.$id);
        return redirect()->route('admin.produk.index');
    }


}
