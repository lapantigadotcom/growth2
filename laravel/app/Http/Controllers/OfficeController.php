<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Sessions;
use Input;
use Validator;
use Redirect;

use App\Distributor;
use App\Area;
use App\Kota;
use App\Tipe;
use App\User;
use App\Office;
use App\Konfigurasi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Http\FormRequest;

use Datatables;
use File;

class OfficeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.office.index');
    }

    public function ajax(Request $request)
    {
        
        // if(Auth::user()->kd_role == 1 || Auth::user()->kd_area == 100){
            $data = DB::table('office')
                ->select(
                    'office.id',
                    'office.nm_office',
                    'office.almt_office',
                    'office.latitude',
                    'office.longitude',
                    'user.nama',
                    'office.status_office',
                    'area.nm_area')
                ->leftJoin('area', 'area.id', '=', 'office.kd_area')
                ->leftJoin('user', 'user.id', '=', 'office.kd_user');
            // dd($data->get());
        // }
        // else if(Auth::user()->kd_role == 3){
        //     $id =Auth::user()->id;
        //     $data = DB::table('office')
        //         ->select(
        //             'office.id',
        //             'office.nm_office',
        //             'office.almt_office',
        //             'office.latitude',
        //             'office.longitude',
        //             'user.nama',
        //             'office.status_office',
        //             'area.nm_area')
        //         ->where('office.kd_user', '=', 'user.id')
        //         ->leftJoin('area', 'area.id', '=', 'office.kd_area')
        //         ->leftJoin('user', 'user.id', '=', $id);
        // }else
        // {
        //     $area = $area =Auth::user()->kd_area;
        //     $data = DB::table('office')
        //         ->select(
        //             'office.id',
        //             'office.nm_office',
        //             'office.almt_office',
        //             'office.latitude',
        //             'office.longitude',
        //             'user.nama',
        //             'office.status_office',
        //             'area.nm_area')
        //         ->where('office.kd_area', '=', $area)
        //         ->leftJoin('area', 'area.id', '=', 'office.kd_area')
        //         ->leftJoin('user', 'user.id', '=', 'office.kd_user');
        // }
       
        return Datatables::of($data)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->kd_role == 1 || Auth::user()->kd_area == 100)
        {
            $dist = [''=>''] + Distributor::pluck('nm_dist', 'id')->toArray();
            $areas = [''=>''] + Area::pluck('kd_area', 'id')->toArray();
            $users = [''=>''] + User::where('kd_role','=','3')->pluck('nama', 'id')->toArray();
        }
        else
        {
            $dist = [''=>''] + Distributor::pluck('nm_dist', 'id')->toArray();
            $areas = [''=>''] + Area::where('id','=',Auth::user()->kd_area)->pluck('kd_area', 'id')->toArray();
            $users = [''=>''] + User::where('kd_role','=','3')->where('kd_area','=',Auth::user()->kd_area)->pluck('nama', 'id')->toArray();
        }
        return view('pages.office.create')->with('dist',$dist)->with('areas',$areas)->with('users',$users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'nm_office'  => 'required|unique:office',
            'almt_office' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'kd_area'   => 'required',
            'kd_user'   => 'required',
            'path_photo' => 'required'
        );

        $messages = array(
            'nm_office.unique'      => 'Nama Office has already been taken!',
            'nm_office.required'    => 'Nama Office field is required',
            'almt_office.required'  => 'Alamat Office field is required',
            'latitude.required'     => 'Latitude field is required',
            'longitude.required'    => 'Longitude field is required',
            'kd_area.required'      => 'Area field is required',
            'kd_user.required'      => 'Registered Sales Force field is required',
            'path_photo.required'   => 'Foto field is required',
        );

        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            $data = $this->create();
            return $data->withInput(Input::all())->withErrors($validator);
        }else{
            $data = new Office;
            $data->nm_office    =$request->input('nm_office');
            $data->almt_office  =$request->input('almt_office');
            $data->latitude    =$request->input('latitude');
            $data->longitude    =$request->input('longitude');
            $data->kd_area      =$request->input('kd_area');
            $data->kd_user      =$request->input('kd_user');
            $data->status_office =$request->input('status');
            $data->save();

            $file = $request->file('path_photo');
            $destinationPath = 'image_upload/office';
            $filename = date('YmdHis').'_'.$data->id.'_'. $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $data->update(array('foto_office' => $filename));
            $log = new AdminController;
            $log->getLogHistory('Make New Office');
            return redirect()->route('admin.office.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = 'show';
        $office = DB::table('office')
        ->select('office.*', 'user.nama', 'area.nm_area')
        ->leftJoin('user', 'user.id', '=', 'office.kd_user')
        ->leftJoin('area', 'area.id', '=', 'office.kd_area')
        ->where('office.id', '=', $id)
        ->first();
        return view('pages.office.'.$page,compact('office'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data= Office::find($id);
        $allStatus = array(
            '0' => 'Belum Aktif',
            '1' => 'Aktif'
        );
        
        $areas = [''=>'Select Area'] + Area::pluck('kd_area', 'id')->toArray();
        $kodeArea = Office::select('kd_area')->where('id','=',$id)->first();
        $value = $kodeArea->kd_area;
        $users = [''=>'Select Sales'] + User::where('kd_role','=',3)->where('kd_area','=',$value)->pluck('nama', 'id')->toArray();
        
        return view('pages.office.edit', [
            'office' => $data,
            'areas' => $areas,
            'users' => $users,
            'allStatus' => $allStatus,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'nm_office'  => 'required',
            'almt_office' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'kd_area'   => 'required',
            'kd_user'   => 'required'
        );

        $messages = array(
            'nm_office.required'    => 'Nama Office field is required',
            'almt_office.required'  => 'Alamat Office field is required',
            'latitude.required'     => 'Latitude field is required',
            'longitude.required'    => 'Longitude field is required',
            'kd_area.required'      => 'Area field is required',
            'kd_user.required'      => 'Registered Sales Force field is required'
        );

        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            $data = $this->create();
            
            $office= Office::find($id);
            $allStatus = array(
                '0' => 'Belum Aktif',
                '1' => 'Aktif'
            );
            
            $areas = [''=>'Select Area'] + Area::pluck('kd_area', 'id')->toArray();

            return $data->withInput(Input::all())->withErrors($validator)->with('areas', $areas)->with('office', $office);
        }else{
            $data= Office::find($id);
            $data->nm_office    =$request->input('nm_office');
            $data->almt_office  =$request->input('almt_office');
            $data->latitude    =$request->input('latitude');
            $data->longitude    =$request->input('longitude');
            $data->kd_area      =$request->input('kd_area');
            $data->kd_user      =$request->input('kd_user');
            $data->status_office =$request->input('status');
            $data->save();
            if(!is_null($request->file('path_photo')))
            {
                $file = $request->file('path_photo');
                $destinationPath = 'image_upload/office';
                $filename = date('YmdHis').'_'.$data->id.'_'. $file->getClientOriginalName();
                $file->move($destinationPath, $filename);
                $data->update(array('foto_office' => $filename));
            }
            $log = new AdminController;
            $log->getLogHistory('Update Office with ID '.$id);
            return redirect()->route('admin.office.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Office::with('presences')->find($id);
        foreach($data->presences as $row)
        {
            $row->delete();
        }
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Office with ID '.$id);
        return redirect()->route('admin.office.index');
    }

    public function formToleransi()
    {
        $konfigurasi = Konfigurasi::find(1);
        return view('pages.office.formToleransi', [
            'konfigurasi' => $konfigurasi
        ]);
    }

    public function updateToleransi(Request $request)
    {
        $toleransi = $request->input('toleransi_max_office');
        DB::table('konfigurasi')->update(['toleransi_max_office' => $toleransi]);
        DB::table('office')->update(['toleransi_max_office' => $toleransi]);

        $log = new AdminController;
        $log->getLogHistory('Update Toleransi Max Office');
        return redirect()->route('admin.officeManage.index');
    }
}
