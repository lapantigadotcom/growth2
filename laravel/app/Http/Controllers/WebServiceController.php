<?php

namespace App\Http\Controllers;
use Hash;
use DB;
use Auth;
use Session;
use Response;
use Input;
use Mail;
use App\Exceptions\SymfonyDisplayer;
use App\User;
use App\TakeOrder;
use App\Produk;
use App\Role;
use App\Banner;
use App\Kota;
use App\Konfigurasi;
use App\Counter;
use App\Sampling;
use App\Satuan;
use App\Brand;
use App\Sample;
use App\Stok;
use App\Office;
use App\Presence;
use App\BusinessTrip;
use App\BlacklistApp;

use App\TipePop;
use App\Pop;
use App\News;

use App\Outlet;
use App\Distributor;
use App\Tipe;
use App\TipeActivity;
use App\TipePhoto;
use App\Area;
use App\Logging;
use App\Competitor;
use App\VisitPlan;
use App\VisitBukti;
use App\Billing;
use App\VisitProblem;
use App\PhotoActivity;
use App\CompetitorActivity;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Image;
use Datatables;

class WebServiceController extends Controller
{

    public function store()
    {
        $data=new User;
        $data->author=Input::get('username');
        $data->text=Input::get('password');
        $success=$data->save();

        if(!$success)
        {
            $out['status']='error';
            $out['message']='error saving';
            $out['data']='';
            return Response::json($out,200);
        }
        $out['status']='success';
        $out['message']='success saving';
        $out['data']='';

        return Response::json($out,200);
    }

    public function getUser($username,$password,$fcmID)
    {
        $data = User::where('username', '=', $username)->first();
        if(is_null($data))
        {
            $out['status']='error';
            $out['message']='not found';
            $out['data']='';
            return Response::json($out,200);
        }
        else
        {
            if(Hash::check($password,$data->password))
            {
                $data->id_gcm = $fcmID;
                if($data->save()){
                    $area = Area::where('id','=',$data->kd_area)->first();
                    $data->area_code = $area->kd_area;
                    $out['status']='success';
                    $out['message']='login success';
                    $out['data']=$data;
                    return Response::json($out,200);
                }
            }
            else{
                $out['status']='error';
                $out['message']='wrong password';
                $out['data']='';
                return Response::json($out,200);
            }

        }
    }

    public function getFoto(Request $request)
    {
        $data = Outlet::where('kd_outlet', '=', $request->kd_outlet)->first();
        if(is_null($data) || is_null($data->foto_outlet) )
        {

            $out['status']='error';
            $out['message']='no fetched data';
            $out['data']='';
            return Response::json($out,200);
        }
        else
        {
            $path = 'image_upload/outlet/';
            $imagedata = file_get_contents($path.$data->foto_outlet);

            $out['status']='success';
            $out['message']='fetched data';
            $out['data']=base64_encode($imagedata);
            return Response::json($out,200);
        }
    }

    public function getArea()
    {
        $data = Area::all();
        if(is_null($data))
        {
            $out['status']='error';
            $out['message']='not found';
            $out['data']='';
            return Response::json($out,200);
        }
        else
        {
            $out['status']='success';
            $out['message']='success';
            $out['data']=$data;
            return Response::json($data,200);
        }
    }

    public function setPhoto(Request $request)
    {
        $data = new PhotoActivity;
        $data->kd_outlet = Input::get('kd_outlet');
        $data->nm_photo = Input::get('nm_photo');
        $data->kd_jenis = Input::get('kd_jenis');
        $data->date_take_photo = Input::get('tgl_take');
        $data->note = Input::get('note');
        $data->date_upload_photo = Input::get('tgl_upload');
        $data->save();
        
        $jenis = "_pmp_market_update1";
        $path = 'image_upload/outlet_photoact/';

         $jenis2 = "_pmp_market_update2";
        $path2 = 'image_upload/outlet_photoact/';

         $jenis3 = "_pmp_market_update3";
        $path3 = 'image_upload/outlet_photoact/';

       

        $base =Input::get('path_photo');
        if(strlen($base)>0){
            $binary = base64_decode($base);
            header('Content-Type: bitmap; charset=utf-8');
            $f = finfo_open();
            $mime_type = finfo_buffer($f, $binary, FILEINFO_MIME_TYPE);
            $mime_type = str_ireplace('image/', '', $mime_type);
            $filename =  strval($data->id).$jenis.date('_YmdHis'). '.' . $mime_type;
            $file = fopen($path . $filename, 'wb');
            if (fwrite($file, $binary)) {
                $data->update(array('path_photo' => $filename));
                fclose($file);
            }
        }

        $base2 =Input::get('path_photo2');
        if(strlen($base2)>0){
            $binary2 = base64_decode($base2);
            $f2 = finfo_open();
            $mime_type2 = finfo_buffer($f2, $binary2, FILEINFO_MIME_TYPE);
            $mime_type2 = str_ireplace('image/', '', $mime_type2);
            $filename2 =  strval($data->id).$jenis2.date('_YmdHis'). '.' . $mime_type2;
            $file2 = fopen($path2 . $filename2, 'wb');
            if (fwrite($file2, $binary2)) {

                $data->update(array('path_photo2' => $filename2));

                fclose($file2);
            }
        }
        $base3 =Input::get('path_photo3');
        if(strlen($base3)>0){
            $binary3 = base64_decode($base3);
            $f3 = finfo_open();
            $mime_type3 = finfo_buffer($f3, $binary3, FILEINFO_MIME_TYPE);        
            $mime_type3 = str_ireplace('image/', '', $mime_type3);
            $filename3 =  strval($data->id).$jenis3.date('_YmdHis'). '.' . $mime_type3;
            $file3 = fopen($path3 . $filename3, 'wb');
            if (fwrite($file3, $binary3)) {

                $data->update(array('path_photo3' => $filename3));

                fclose($file3);
            }
        }

        $logging = new Logging;
        $logging->kd_user = Input::get("kd_user");
        $logging->description = "Upload Market Update berhasil".$jenis;
        $logging->log_time = Input::get('tgl_take');
        $logging->detail_akses = "PMP Android App";
        $logging->save();

        $out['status']='success';
        $out['message']='Upload Market Update berhasil';
        $out['data']='';
            return Response::json($out,200);//return FALSE;

        
    }

    public function setCompetitor()
    {
        $data = new CompetitorActivity;
        $data->kd_outlet = Input::get('kd_outlet');
        $data->kd_comp = Input::get('kd_kompetitor');
        $data->nm_photo = Input::get('nm_photo');

        $data->date_take_photo = Input::get('tgl_take');
        $data->note = Input::get('note');
        $data->date_upload_photo = Input::get('tgl_upload');
        $data->save();


        $jenis = "_kompetitor";
        $path = 'image_upload/competitor/';


        $base =Input::get('foto');
        $binary = base64_decode($base);
        header('Content-Type: bitmap; charset=utf-8');

        $f = finfo_open();
        $mime_type = finfo_buffer($f, $binary, FILEINFO_MIME_TYPE);
        $mime_type = str_ireplace('image/', '', $mime_type);

        $filename =  strval($data->id).$jenis.date('_YmdHis'). '.' . $mime_type;
        $file = fopen($path . $filename, 'wb');
        if (fwrite($file, $binary)) {
            $data->update(array('path_photo' => $filename));
            fclose($file);
            $logging = new Logging;
            $logging->kd_user = Input::get("kd_user");
            $logging->description = "Upload foto ".$jenis;
            $logging->log_time = Input::get('tgl_upload');
            $logging->detail_akses = "PMP Android App";
            $logging->save();

            $out['status']='success';
            $out['message']='upload image success';
            $out['data']='';
            return Response::json($out,200);//return FALSE;
        } else {
            $out['status'] = "error";
            $out['message']='upload image failed';
            $out['data']='';
            return Response::json($out,200);//return FALSE;
        }
    }

    public function setGCM()
    {
        $id = Input::get('kd_user');
        $data = User::find($id);
        $success = $data->update(['id_gcm' => Input::get('id_gcm')]);
        if($success)
        {
          $out['status'] = "success";
          $out['message']='GCM fetched';
          $out['data']='';

          return Response::json($out,200);
      }
      else
      {

        $out['status']='error';
        $out['message']='error saving';
        $out['data']='';
        return Response::json($out,200);
    }
}

    // api post visit bukti
public function setVisitBukti (Request $request) 

{ 
    {
        $data = new VisitBukti;
        $data->kd_outlet = Input::get('kd_outlet');
        $data->note = Input::get('note');
        $data->kd_user = Input::get('kd_user');
        $data->created_at = Input::get('visitreport_at');

        $data->save(); 
        $jenis = "_visitbukti";
        $path = 'image_upload/visitbukti/';

        $jenis2 = "_visitbukti2";
        $path2 = 'image_upload/visitbukti/';

        $jenis3 = "_visitbukti3";
        $path3 = 'image_upload/visitbukti/';

        $base =Input::get('img_path1');
        if(strlen($base)>0){
            $binary = base64_decode($base);
            header('Content-Type: bitmap; charset=utf-8');
            $f = finfo_open();
            $mime_type = finfo_buffer($f, $binary, FILEINFO_MIME_TYPE);
            $mime_type = str_ireplace('image/', '', $mime_type);
            $filename =  strval($data->id).$jenis.date('_YmdHis'). '.' . $mime_type;
            $file = fopen($path . $filename, 'wb');
            if (fwrite($file, $binary)) {
                $data->update(array('img_path1' => $filename));
                fclose($file);
            }
        }

        $base2 =Input::get('img_path2');
        if(strlen($base2)>0){
            $binary2 = base64_decode($base2);
            $f2 = finfo_open();
            $mime_type2 = finfo_buffer($f2, $binary2, FILEINFO_MIME_TYPE);
            $mime_type2 = str_ireplace('image/', '', $mime_type2);
            $filename2 =  strval($data->id).$jenis2.date('_YmdHis'). '.' . $mime_type2;
            $file2 = fopen($path2 . $filename2, 'wb');
            if (fwrite($file2, $binary2)) {

                $data->update(array('img_path2' => $filename2));

                fclose($file2);
            }
        }
        $base3 =Input::get('img_path3');
        if(strlen($base3)>0){
            $binary3 = base64_decode($base3);
            $f3 = finfo_open();
            $mime_type3 = finfo_buffer($f3, $binary3, FILEINFO_MIME_TYPE);        
            $mime_type3 = str_ireplace('image/', '', $mime_type3);
            $filename3 =  strval($data->id).$jenis3.date('_YmdHis'). '.' . $mime_type3;
            $file3 = fopen($path3 . $filename3, 'wb');
            if (fwrite($file3, $binary3)) {

                $data->update(array('img_path2' => $filename3));

                fclose($file3);
            }
        }

        $logging = new Logging;
        $logging->kd_user = Input::get("kd_user");
        $logging->description = "Upload foto Visit Bukti  ".$jenis;
        $logging->log_time = Input::get('created_at');
        $logging->detail_akses = "PMP Android App";
        $logging->save();

        $out['status']='success';
        $out['message']='upload image visit report success';
        $out['data']='';
            return Response::json($out,200);//return FALSE;

        }
    }
    

    public function setOutlet(Request $request)
    {
        $id = -1;
        $data=new Outlet;
        $kota = Kota::where("id",Input::get('kd_kota'))->first();
        if(!Input::has('nm_outlet'))
        {
          $out['status']='error';
          $out['message']='no data sent';
          $out['data']='';
          return Response::json($out,200);
      }
      $konfig = Konfigurasi::first();
      $data->kd_dist=Input::get('kd_dist');
      $data->kd_kota=Input::get('kd_kota');
      $data->kd_area=$kota->kd_area;
      $data->kd_user=Input::get('kd_user');
      $data->nm_outlet=Input::get('nm_outlet');
      $data->almt_outlet=Input::get('almt_outlet');
      $data->kd_tipe=Input::get('kd_tipe');
      $data->rank_outlet=Input::get('rank_outlet');
      $data->kodepos=Input::get('telp_outlet');
      $data->reg_status=Input::get('reg_status');
      $data->latitude=Input::get('latitude');
      $data->longitude=Input::get('longitude');
      $data->nm_pic=Input::get('nama_pic');
      $data->tlp_pic=Input::get('tlp_pic');
      $data->toleransi_long=$konfig->toleransi_max;
      $success = $data->save();
      $path = 'image_upload/outlet/';
      $base =Input::get('foto');
      $binary = base64_decode($base);
      header('Content-Type: bitmap; charset=utf-8');

      $f = finfo_open();
      $mime_type = finfo_buffer($f, $binary, FILEINFO_MIME_TYPE);
      $mime_type = str_ireplace('image/', '', $mime_type);

      $filename =  strval($data->kd_outlet).date('_YmdHis'). '.' . $mime_type;
      $file = fopen($path . $filename, 'wb');
      if (fwrite($file, $binary)) {
        $data->update(array('foto_outlet' => $filename));
        fclose($file);
    }
    $logging = new Logging;
    $logging->kd_user = Input::get("kd_user");
    $logging->description = "Register outlet ".Input::get("nm_outlet");
    $logging->log_time = Input::get('tgl_upload');
    $logging->detail_akses = "PMP Android App";
    $logging->save(); 

    if(Input::get('take_order') == 1)
    {
        $vp=new VisitPlan;
        $vp->kd_outlet=$data->kd_outlet;
        $vp->approve_visit=1;
        $vp->status_visit=0;
        $vp->date_visit = Input::get('tgl_upload');
        $success = $vp->save();
        $id = $vp->id;
        $logging = new Logging;
        $logging->kd_user = Input::get("kd_user");
        $logging->description = "Register visit plan to outlet ".Input::get("nm_outlet");
        $logging->log_time = Input::get('tgl_upload');
        $logging->detail_akses = "PMP Android App";
        $logging->save();
    }
    if(!$success)
    {
      $out['status']='error';
      $out['message']='error saving';
      $out['data']='';

      return Response::json($out,200);
  }

  $dt['id'] = $data->kd_outlet;
  $dt['kd_visit'] = $id;
  $dt['toleransi'] = $konfig->toleransi_max;
  $out['status']='success';
  $out['message']='success saving';
  $out['data']=$dt;
  return Response::json($out,200);
}

public function deleteVisit(){

  $id =  Input::get('id');



  $res = VisitPlan::where('id', $id)->delete();

  $data['id']=$id;
  if($res){
    $out['status']='success';
    $out['message']='Visit Plan '.$id.' deleted';
    $out['data']=$data;
}
else{
    $out['status']='error';
    $out['message']='delete error';
    $out['data']=$data;
}
return Response::json($out,200);
}



public function getTO($id){
    $to_visit = VisitPlan::select('visit_plan.kd_outlet', 'visit_plan.id')
    ->join('outlet', 'outlet.kd_outlet', '=', 'visit_plan.kd_outlet')
    ->leftJoin('take_order', 'take_order.kd_visitplan', '=', 'visit_plan.id')
    ->whereRaw('DATEDIFF(NOW(),visit_plan.date_visiting)<1')
    ->where('visit_plan.skip_order_reason', 'waiting')
    ->whereNull('take_order.id')
    ->where('visit_plan.status_visit',1)
    ->where('outlet.kd_user',$id)->get();

    if(is_null($to_visit)){
        $out['status']='error';
        $out['message']='no pending to of visit plan today';
        $out['data']='';
    } else{
        $out['status']='success';
        $out['message']='fetched TO';
        $out['data']=$to_visit;
    }
    return Response::json($out,200);
}

public function submitBilling (Request $request){

   { 
    $data = new Billing;
    $data->kd_outlet = Input::get('kd_outlet');
    $data->kd_user = Input::get('kd_user');
    $data->status = Input::get('status');
    $data->transfer_value = Input::get('transfer_value');
    $data->cash_value = Input::get('cash_value');
    $data->nominal_giro = Input::get('nominal_giro');
    $data->giro_no = Input::get('giro_no');
    $data->bank_name = Input::get('bank_name');
    $data->method_payment = Input::get('method_payment');
    $data->submit_at = Input::get('submit_at');
    $data->due_date = Input::get('due_date');

    $data->save(); 
    $logging = new Logging;
    $logging->kd_user = Input::get("kd_user");
    $logging->description = "Submit Billing berhasil dilakukan ";
    $logging->log_time = Input::get('created_at');
    $logging->detail_akses = "PMP Android App";
    $logging->save();

    $out['status']='success';
    $out['message']='Submit Billing Success';
    $out['data']='';
            return Response::json($out,200);//return FALSE;
        }

    }

    public function getSatuan ()

    {
       $data = Satuan::get();
       if(is_null($data))
       {
        $out['status'] = "error";
        $out['message']='not found';
        $out['data']='';
        return Response::json($out,200);
    }
    else
    {
        $out['status'] = "success";
        $out['message']='success';
        $out['data']=$data;
        return Response::json($out,200);
    }
}

public function getLogBilling(Request $request)
{   
    $loguser = Billing::where('kd_user','=',$request->kd_user)
    ->orderBy('submit_at', 'desc')
    ->get();

    if(is_null($request->kd_user))
    {
      $out['status'] = "error";
      $out['message']='Kode User tidak ditemukan';
      $out['data']='';
      return Response::json($out,200);
  }
  else
  {
      $out['status'] = "success";
      $out['message']='success';
      $out['data'] = $loguser;
      return Response::json($out,200);
  }
}


public function getLogOrder(Request $request)
{   
    $loguser = TakeOrder::where('kd_user','=',$request->kd_user)
    ->orderBy('date_order', 'desc')
    ->get();

    if(is_null($request->kd_user))
    {
      $out['status'] = "error";
      $out['message']='Kode User tidak ditemukan';
      $out['data']='';
      return Response::json($out,200);
  }
  else
  {
      $out['status'] = "success";
      $out['message']='success';
      $out['data'] = $loguser;
      return Response::json($out,200);
  }
}



public function getBrand ()

{
   $data = Brand::orderBy('urutan', 'ASC')->get();
   if(is_null($data))
   {
    $out['status'] = "error";
    $out['message']='not found';
    $out['data']='';
    return Response::json($out,200);
}
else
{
    $out['status'] = "success";
    $out['message']='success';
    $out['data']=$data;
    return Response::json($out,200);
}
}

public function getVisitToday(Request $request){
    $id = $request->kd_user;
    $visit = VisitPlan::select('visit_plan.kd_outlet', 'visit_plan.id')
    ->join('outlet', 'outlet.kd_outlet', '=', 'visit_plan.kd_outlet')
    ->whereRaw('DATEDIFF(NOW(),visit_plan.date_visiting)<1')
    ->where('visit_plan.status_visit',1)
    ->where('outlet.kd_user',$id)->get();
    if(is_null($visit)){
        $out['status']='error';
        $out['message']='no submit visit plan today';
        $out['data']='';
    } else{
        $out['status']='success';
        $out['message']='fetched Visit';
        $out['data']=$visit;
    }

    return Response::json($out,200);
}

public function getVisitPlanToday(Request $request){
        // $id = $request->kd_user;

        //checking if user checkin
    $token  = $request->bearerToken();
    $user = User::where('api_token', '=', $token)
    ->get()->first();

    $statusCheckin = Presence::where('kd_user', '=', $user->id)
    ->where('status_checkin', '=', '1')
    ->whereRaw("DATE(date) = DATE(NOW())")->get();

    if(is_null($statusCheckin))
    {
        $out['status']  ='error';
        $out['message'] ='please checkin first your presence today';
        $out['data']   ='';
    }else{            
        $visit = VisitPlan::select('visit_plan.*')
        ->whereRaw('DATE(visit_plan.date_visit) = DATE(NOW())')
        ->leftJoin('outlet', 'outlet.kd_outlet', '=', 'visit_plan.kd_outlet')
        ->where('outlet.kd_user', '=', $user->id )
        ->get();

        if(is_null($visit)){
            $out['status']  ='error';
            $out['message'] ='no submit visit plan today';
            $out['data']   ='';
        } else{
            $out['status']  ='success';
            $out['message'] ='fetched Visit';
            $out['data']    =$visit;
        }
    }
    return Response::json($out,200);
}

public function getPop(Request $request){
    $visit = VisitPlan::select('visit_plan.kd_outlet', 'visit_plan.id')
    ->leftJoin('pop', 'pop.kd_visit', '=', 'visit_plan.id')
    ->join('outlet', 'outlet.kd_outlet', '=', 'visit_plan.kd_outlet')
    ->where('outlet.kd_user',$request->kd_user)
    ->whereNull('pop.kd_pop')
    ->whereRaw('DATEDIFF(NOW(),visit_plan.date_visiting)<1')
    ->where('visit_plan.status_visit',1)
    ->get();
    if(is_null($visit)){
        $out['status']='error';
        $out['message']='no pending pop today';
        $out['data']='';
    } else{
        $out['status']='success';
        $out['message']='fetched POP pending Visit';
        $out['data']=$visit;
    }

    return Response::json($out,200);
}

public function getStok($id){

    $visit = VisitPlan::select('visit_plan.kd_outlet', 'visit_plan.id')
    ->leftJoin('stok', 'stok.kd_visit', '=', 'visit_plan.id')
    ->join('outlet', 'outlet.kd_outlet', '=', 'visit_plan.kd_outlet')
    ->where('outlet.kd_user',$id)
    ->whereNull('stok.kd_stok')
    ->whereRaw('DATEDIFF(NOW(),visit_plan.date_visiting)<1')
    ->where('visit_plan.status_visit',1)
    ->get();

    if(is_null($visit)){
        $out['status']='error';
        $out['message']='no pending stok today';
        $out['data']='';
    } else{
        $out['status']='success';
        $out['message']='fetched stok pending Visit';
        $out['data']=$visit;
    }

    return Response::json($out,200);
}

public function getSampling($id){
    $visit = VisitPlan::select('visit_plan.kd_outlet', 'visit_plan.id')
    ->leftJoin('sampling', 'sampling.kd_visit', '=', 'visit_plan.id')
    ->join('outlet', 'outlet.kd_outlet', '=', 'visit_plan.kd_outlet')
    ->where('outlet.kd_user',$id)
    ->whereNull('sampling.kd_sampling')
    ->whereRaw('DATEDIFF(NOW(),visit_plan.date_visiting)<1')
    ->where('visit_plan.status_visit',1)
    ->get();

    if(is_null($visit)){
        $out['status']='error';
        $out['message']='no pending sampling today';
        $out['data']='';
    } else{
        $out['status']='success';
        $out['message']='fetched stok sampling Visit';
        $out['data']=$visit;
    }

    return Response::json($out,200);
}

public function submitStok(){
    $data = Input::json()->all();

    try{
        $kd_visit = $data['kd_visit'];

        foreach($data['stok'] as $stoki){

            $stok = new Stok;

            $stok->kd_visit = $kd_visit;
            $stok->kd_produk = $stoki['kd_produk'];
            $stok->qty_gudang = $stoki['qty_gudang'];
            $stok->qty_pajangan = $stoki['qty_pajangan'];
            $stok->expired = $stoki['expired'];
            $stok->date_upload = date('Y-m-d H:i:s');
            $success=$stok->save();
        }

        $kd_outlet = VisitPlan::find($kd_visit)->kd_outlet;
        $kd_user = Outlet::find($kd_outlet)->kd_user;

        $logging = new Logging;
        $logging->kd_user = $kd_user;
        $logging->description = "Submit Stok";
        $logging->log_time = date("Y-m-d H:i:s");
        $logging->detail_akses = "PMP Android App";
        $logging->save();

        $out['status'] = "success";
        $out['message']='submit stok success';
        $out['data']='';
    }
    catch(Exception $e){
        $out['status'] = "error";
        $out['message']='submit stok error';
        $out['data']='';
    }

    return Response::json($out,200);
}

public function submitPop(){
    $data = Input::json()->all();

    try{
        $kd_visit = $data['kd_visit'];

        foreach($data['pop'] as $popi){

            $pop = new Pop;

            $pop->kd_visit = $kd_visit;
            $pop->jenis_pop = $popi['jenis_pop'];
            $pop->qty = $popi['qty'];
            $pop->date_upload = date('Y-m-d H:i:s');
            $success=$pop->save();
        }

        $kd_outlet = VisitPlan::find($kd_visit)->kd_outlet;
        $kd_user = Outlet::find($kd_outlet)->kd_user;

        $logging = new Logging;
        $logging->kd_user = $kd_user;
        $logging->description = "Submit POP";
        $logging->log_time = date("Y-m-d H:i:s");
        $logging->detail_akses = "PMP Android App";
        $logging->save();

        $out['status'] = "success";
        $out['message']='submit pop success';
        $out['data']='';
    }
    catch(Exception $e){
        $out['status'] = "error";
        $out['message']='submit pop error';
        $out['data']='';
    }

    return Response::json($out,200);
}
public function submitTO(){
    try{
        $counter_to = Counter::where("id",1)->first();
        $counter = $counter_to->counter;
        $counter_to->update(array('counter' => $counter+1));

        $data = Input::json()->all();

        $kd_visit = '00';
        $status_order = Input::get('status_order');
        $date_order = Input::get('date_order');

        foreach($data['order'] as $order){
            $produk = new TakeOrder;
            $produk->kd_user = Input::get('kd_user');
            $produk->kd_outlet = Input::get('kd_outlet');
            $produk->id_satuan = $order['id_satuan'];
            $produk->kd_to = "PMP_Order_".strval($counter);
            // $produk->id_satuan = Input::get('id_satuan');
            $produk->kd_visitplan = $kd_visit;
            $produk->kd_produk = $order['kd_produk'];
            $produk->other_produk = $order['other_produk'];
            $produk->qty_order = $order['qty_order'];
            $produk->sku_baru = '0';
            $produk->satuan = 'satuan';
            $produk->date_order = date('Y-m-d H:i:s');
            $produk->status_order = 1;
            $success=$produk->save();
        }


        $kd_user = Input::get('kd_user');
        $kd_outlet = Input::get('kd_outlet');


        $logging = new Logging;
        $logging->kd_user = $kd_user;
        $logging->description = "Submit Take Order";
        $logging->log_time = date("Y-m-d H:i:s");
        $logging->detail_akses = "PMP Android App";
        $logging->save();

        $out['status'] = "success";
        $out['message']='submit order success';
        $out['data']='';
    }
    catch(Exception $e){
        $out['status'] = "error";
        $out['message']='submit to error';
        $out['data']='';
    }
    return Response::json($out,200);
}

public function submitSampling(){

    try{
        $data = Input::json()->all();
        $kd_visit = $data['kd_visit'];

        foreach($data['sampling'] as $sampl){

            $sampling = new Sampling;

            $sampling->kd_visit = $kd_visit;
            $sampling->kd_sample = $sampl['kd_sample'];
            $sampling->qty = $sampl['qty'];
            $sampling->qty_l = $sampl['qty_l'];
            $sampling->qty_p = $sampl['qty_p'];

            $sampling->date_upload = date('Y-m-d H:i:s');

            $success=$sampling->save();
        }

        $kd_outlet = VisitPlan::find($kd_visit)->kd_outlet;
        $kd_user = Outlet::find($kd_outlet)->kd_user;

        $logging = new Logging;
        $logging->kd_user = $kd_user;
        $logging->description = "Submit Sampling";
        $logging->log_time = date("Y-m-d H:i:s");
        $logging->detail_akses = "PMP Android App";
        $logging->save();

        $out['status'] = "success";
        $out['message']='submit sampling success';
        $out['data']='';


    }
    catch(Exception $e){
        $out['status'] = "error";
        $out['message']='submit sampling error';
        $out['data']='';
    }

    return Response::json($out,200);


}

// public function submitTO(){
//         try{
//             $counter_to = Counter::where("id",1)->first();
//             $counter = $counter_to->counter;
//             $counter_to->update(array('counter' => $counter+1));

//             $data = Input::json()->all();

//             // $kd_outlet = $data['kd_outlet'];

//                  $produk = new TakeOrder;
//                 $produk->kd_to = "PMP_Order_".strval($counter);
//                 $produk->kd_outlet=Input::get('kd_outlet');
//                 $produk->kd_user=Input::get('kd_user');
//                 $produk->kd_produk=Input::get('kd_produk');
//                 $produk->qty_order = Input::get('kd_produk');
//                 $produk->sku_baru = '0';
//                 $produk->date_order = Input::get('date_order');
//                 $produk->status_order = Input::get('status_order');
//                 $success=$produk->save(); 
//                 $logging = new Logging;
//                 $logging->kd_user = $produk->kd_user;
//                 $logging->description = "Submit Take Order Berhasil";
//                 $logging->log_time = date("Y-m-d H:i:s");
//                 $logging->detail_akses = "PMP Android App";
//                 $logging->save();

//             $out['status'] = "success";
//             $out['message']='Submit Take Order Berhasil';
//             $out['data']='';
//         }
//         catch(Exception $e){
//             $out['status'] = "error";
//             $out['message']='Submit Take Order Gagal';
//             $out['data']='';
//         }
//         return Response::json($out,200);
//     }

public function setVisit()
{
  if(!Input::has('kd_outlet'))
  {

    $out['status']='error';
    $out['message']='no data sent';
    $out['data']='';
    return Response::json($out,200);
}

$outlets = Input::get('kd_outlet');
$ids = array();

try{
    foreach($outlets as $row){
      $data=new VisitPlan;
          //$data->kd_outlet=Input::get('kd_outlet');
      $outlet = Outlet::where('kd_outlet','=',$row)->first();
      $area = Area::where('id','=',$outlet->kd_area)->first();
      $data->kd_outlet=$row;
      $data->date_visit=Input::get('date_visit');
      $data->date_create_visit=Input::get('date_create_visit');
      $data->status_visit = 0;
      $success=$data->save();

      if($area->status_aproval == 0){
        $data->approve_visit=0;
        $ids[]=0;
    }

    elseif($area->status_aproval == 1){
      $data->approve_visit=2;
      $ids[]=$data->id;
  }

  $success=$data->save();

}


}

catch(Exception $e){
    $out['status'] = "error";
    $out['message']='set visit error';
    $out['data']='';
    return Response::json($out,200);
}

$logging = new Logging;
$logging->kd_user = Input::get("kd_user");
$logging->description = "Register visit plan success";
$logging->log_time = Input::get('tgl_upload');
$logging->detail_akses = "PMP Android App";
$logging->save();

      /*if($area->status_aproval == 0)
        $dt['id'] = 0;
      else if($area->status_aproval == 1)
      $dt['id'] = $data->id;*/
      $dt['id']=$ids;
      $out['status']='success';
      $out['message']='success';
      $out['data']=$dt;
      return Response::json($out,200);
  }

  public function submitcheckOut(Request $request)

  {   

    $data= VisitPlan::find($request->input('kd_visit'));
    $data->date_checkout = Input::get('date_checkout');
    $data->save();

    $out['status']='success';
    $out['message']='success in checkout outlet';
    $out['data']= $data->id;
    return Response::json($out,200);
}

public function submitVisit()
{   

    $data=new VisitPlan;
    if(!Input::has('kd_outlet'))
    {
        $out['status'] = "no data sent";
        return Response::json($out,500);
    }
    $data->kd_outlet=Input::get('kd_outlet');
    $data->date_visit=Input::get('date_visiting');
    $data->date_create_visit=Input::get('date_visiting');
    $data->approve_visit= '1';
    $data->status_visit = 0;
    $success=$data->save();

    if(!$success)
    {
        $out['status'] = "error saving";
        return Response::json($out,501);
    }
    $logging = new Logging;
    $logging->kd_user = Input::get("kd_user");
    $logging->description = "Register visit plan to outlet ".Outlet::where("kd_outlet",Input::get("kd_outlet"))->first()->nm_outlet;
    $logging->log_time = Input::get('tgl_upload');
    $logging->detail_akses = "PMP Android App";
    $logging->save();
    $out['status'] = "success";



    $user = $data;
    if(is_null($user)){
        $out['status'] = "error";
        $out['message']='kd_visit not found';
        $out['data']='';
        return Response::json($out,200);
    }

    else{
        $ifClose = Input::get('if_close');
        if($ifClose == 1)
        {
            $new_user_data = array(
                "status_visit"      =>  Input::get('status_visit'),
                "date_visiting"     =>  Input::get('date_visiting'),
                "skip_order_reason" =>  Input::get('skip_order_reason'),
                "if_close"          =>  Input::get('if_close'),
                "keterangan_if_close"=> Input::get('keterangan_if_close')
            );

            $path = 'image_upload/outlet_photoClose/';
            $base =Input::get('foto');
            $binary = base64_decode($base);
            header('Content-Type: bitmap; charset=utf-8');

            $f = finfo_open();
            $mime_type = finfo_buffer($f, $binary, FILEINFO_MIME_TYPE);
            $mime_type = str_ireplace('image/', '', $mime_type);

            $filename =  strval($user->id).date('_YmdHis'). '.' . $mime_type;
            $file = fopen($path . $filename, 'wb');
            if (fwrite($file, $binary)) {
                $user->update(array('foto_if_close' => $filename));
                fclose($file);
            }
            $success = $user->update($new_user_data);
        }else{
            $new_user_data = array(
             "status_visit"      =>  Input::get('status_visit'),
             "date_visiting"     =>  Input::get('date_visiting'),
             "skip_order_reason" =>  Input::get('skip_order_reason'),
             "if_close"          =>  Input::get('if_close'),
             "keterangan_if_close"=> Input::get('keterangan_if_close')
         );
            $success = $user->update($new_user_data);
        }

        if(!$success)
        {
          $out['status'] = "error";
          $out['message']='error submit visit';
          $out['data']='';
          return Response::json($out,200);
      }

      $logging = new Logging;
      $logging->kd_user = Input::get("kd_user");
      $logging->description = "Submit visit plan to outlet ".Outlet::where("kd_outlet",$user->kd_outlet)->first()->nm_outlet;
      $logging->log_time = Input::get('date_visiting');
      $logging->detail_akses = "PMP Android App";
      $logging->save();

      $out['status'] = "success";
      $out['message']='submit visit success';
      $out['data']= $user;
      return Response::json($out,200);
  }
}

public function getAllData($kd_user,$kd_area)
{
    $kota = Kota::where('kd_area','=',$kd_area)->get();
    $competitor = Competitor::all();
    $outlet = Outlet::where('kd_user','=',$kd_user)->get();
    $tipePhoto = TipePhoto::all();
        /*$distributor = Distributor::select('distributor.id','distributor.kd_dist','distributor.kd_tipe','distributor.kd_kota','distributor.nm_dist','distributor.almt_dist','distributor.telp_dist')
        ->join('kota','kota.id','=','distributor.kd_kota')->where('kota.kd_area','=',$kd_area)
        ->get();*/
        $distributor = Distributor::all();
        $tipe = Tipe::all();
        $tipeActivity = TipeActivity::all();
        if(is_null($outlet))
        {
           $visits = "";
       }
       else
       {
        $visits = array();
        foreach($outlet as $outlet1)
        {
            $visit=VisitPlan::select('visit_plan.id','visit_plan.kd_outlet','visit_plan.date_visit','visit_plan.date_create_visit','visit_plan.approve_visit','visit_plan.status_visit','visit_plan.date_visiting','visit_plan.skip_order_reason','visit_plan.skip_reason')
            ->join('outlet','outlet.kd_outlet','=','visit_plan.kd_outlet')->where('outlet.kd_outlet','=',$outlet1->kd_outlet)->where('approve_visit','=',1)->where('status_visit', '=', 0)->get();
            foreach($visit as $vis)
            {
                array_push($visits,$vis);
            }
        }
    }
    $produk = Produk::all();
    $dataUser = array("kota"=>$kota,"competitor"=>$competitor,"outlet"=>$outlet,"distributor"=>$distributor,
        "tipe"=>$tipe,"visitplan"=>$visits,"produk"=>$produk,"tipe_photo"=>$tipePhoto, "tipe_activity"=>$tipeActivity);

    $out['status']='success';
    $out['message']='fetched data';
    $out['data']=$dataUser;
    return Response::json($out,200);
}

public function getKota(Request $request)
{
    $kota = Kota::where('kd_area','=',$request->kd_area)->get();
    if(is_null($kota))
    {
      $out['status'] = "error";
      $out['message']='not found';
      $out['data']='';
      return Response::json($out,200);
  }
  else
  {
      $out['status'] = "success";
      $out['message']='success';
      $out['data'] = $kota;
      return Response::json($out,200);
  }
}

public function getTipePhoto()
{
    $tipePhoto = TipePhoto::all();
    if(is_null($tipePhoto))
    {
      $out['status'] = "error";
      $out['message']='not found';
      $out['data']='';
      return Response::json($out,200);
  }
  else
  {
      $out['status'] = "success";
      $out['message']='success';
      $out['data']=$tipePhoto;
      return Response::json($out,200);
  }
}

public function getDetailNews($id)
{
    $data = News::where('id',$id)->first();
    if($data->path_image!='' ){
        header('Content-Type: application/json');
        $im=file_get_contents('image_upload/news/'.$data->path_image);
        $image=base64_encode($im);
    }


    if(is_null($image))
    {
        $out['status'] = "error";
        $out['message']='not found';
        $out['data']='';
        return Response::json($out,200);
    }
    else
    {
        $out['status'] = "success";
        $out['message']='success';
        $out['data']=$image;
        return Response::json($out,200);
    }
}

public function getBanner()
{
    $data = Banner::orderBy('date_active', 'desc')->where('status', 1)->take(3)->get();
    foreach($data as $row){
        header('Content-Type: application/json');
        $im=file_get_contents('image_upload/banner/'.$row->path_image);

        $row->path_image=base64_encode($im);
    }


    if(is_null($data))
    {
      $out['status'] = "error";
      $out['message']='not found';
      $out['data']='';
      return Response::json($out,200);
  }
  else
  {
      $out['status'] = "success";
      $out['message']='success';
      $out['data']=$data;
      return Response::json($out,200);
  }
}

public function getCompetitor()
{
    $data = Competitor::all();
    if(is_null($data))
    {
      $out['status'] = "error";
      $out['message']='not found';
      $out['data']='';
      return Response::json($out,200);
  }
  else
  {
      $out['status'] = "success";
      $out['message']='success';
      $out['data']=$data;
      return Response::json($out,200);
  }
}

public function getSample()
{
    $data = Sample::all();
    if(is_null($data))
    {
      $out['status'] = "error";
      $out['message']='not found';
      $out['data']='';
      return Response::json($out,200);
  }
  else
  {
      $out['status'] = "success";
      $out['message']='success';
      $out['data']=$data;
      return Response::json($out,200);
  }
}


public function getTPop()
{
    $data = TipePop::all();
    if(is_null($data))
    {
      $out['status'] = "error";
      $out['message']='not found';
      $out['data']='';
      return Response::json($out,200);
  }
  else
  {
      $out['status'] = "success";
      $out['message']='success';
      $out['data']=$data;
      return Response::json($out,200);
  }
}

public function getOutlet(Request $request)
{
        // $data = Outlet::where('kd_user','=',$request->kd_user)->get();
    $data = Outlet::get();
    if(is_null($data))
    {
        $out['status'] = "error";
        $out['message']='not found';
        $out['data']='';
        return Response::json($out,200);
    }
    else
    {
        $out['status'] = "success";
        $out['message']='success';
        $out['data']=$data;
        return Response::json($out,200);
    }
}

public function getDistributor(Request $request)
{
        //$kotaUser = Kota::where('id','=',$kdKota)->first();
        //if(is_null($kotaUser))
        //{
        //     return Response::json("your city is not registered",404);
        //}

    $data=Distributor::select('distributor.id','distributor.kd_dist','distributor.kd_tipe','distributor.kd_kota','distributor.nm_dist','distributor.almt_dist','distributor.telp_dist')
    ->join('kota','kota.id','=','distributor.kd_kota')->where('kota.kd_area','=',$request->kd_area)
    ->get();
    if(is_null($data))
    {
      $out['status'] = "error";
      $out['message']='not found';
      $out['data']='';
      return Response::json($out,200);
  }
  else
  {
      $out['status'] = "success";
      $out['message']='success';
      $out['data']=$data;
      return Response::json($out,200);
  }
}
public function getTipe()
{
    $data = Tipe::all();
    if(is_null($data))
    {
      $out['status'] = "error";
      $out['message']='not found';
      $out['data']='';
      return Response::json($out,200);
  }
  else
  {
      $out['status'] = "success";
      $out['message']='success';
      $out['data']=$data;
      return Response::json($out,200);
  }
}
public function getVisit(Request $request)
{
    $data = Outlet::where('kd_user','=',$request->kd_user)->get();
    if(is_null($data))
    {
      $out['status']='error';
      $out['message']='not found';
      $out['data']='';
      return Response::json($out,200);
  }
  else
  {
    $visits = array();
    foreach($data as $outlet)
    {
        $visit=VisitPlan::select('visit_plan.id','visit_plan.kd_outlet','visit_plan.date_visit','visit_plan.date_create_visit','visit_plan.approve_visit','visit_plan.status_visit','visit_plan.date_visiting','visit_plan.skip_order_reason','visit_plan.skip_reason')
        ->join('outlet','outlet.kd_outlet','=','visit_plan.kd_outlet')
        ->where('outlet.kd_outlet','=',$outlet->kd_outlet)
        ->where('approve_visit','=',1)
        ->where('date_visit', date('Y-m-d'))
        ->where('status_visit','=', 0)->get();
        foreach($visit as $vis)
        {
            array_push($visits,$vis);
        }
    }
    $out['status']='success';
    $out['message']='fetched data';
    $out['data']=$visits;
    return Response::json($out,200);
}
}

public function getProduk()
{
    $data = Produk::all();
    if(is_null($data))
    {
      $out['status']='error';
      $out['message']='not found';
      $out['data']='';
      return Response::json($out,200);
  }
  else
  {
      $out['status']='success';
      $out['message']='fetched data';
      $out['data']=$data;
      return Response::json($out,200);
  }
}

public function getOffice()
{
    $data = Office::all();
    if(is_null($data))
    {
      $out['status'] = "error";
      $out['message']='not found';
      $out['data']='';
      return Response::json($out,200);
  }
  else
  {
      $out['status'] = "success";
      $out['message']='success';
      $out['data']=$data;
      return Response::json($out,200);
  }
}

public function getPresence(Request $request)
{
    $token  = $request->bearerToken();
    $user = User::where('api_token', '=', $token)->get()->first();

    $data = Presence::select('*')
    ->where('kd_user', '=', $user->id)
    ->get();

    if(is_null($data))
    {
        $out['status'] = "error";
        $out['message']='not found';
        $out['data']='';
        return Response::json($out,200);
    }
    else
    {
        $out['status'] = "success";
        $out['message']='success';
        $out['data']=$data;
        return Response::json($out,200);
    }
}


public function getBlacklistApp()
{
    $data = blacklistApp::all();
    if(is_null($data))
    {
      $out['status'] = "error";
      $out['message']='not found';
      $out['data']='';
      return Response::json($out,200);
  }
  else
  {
      $out['status'] = "success";
      $out['message']='success';
      $out['data']=$data;
      return Response::json($out,200);
  }
}

public function getBusinessTrip(Request $request)
{
    $token  = $request->bearerToken();
    $user = User::where('api_token', '=', $token)->get()->first();

    $data = BusinessTrip::select('*')
    ->where('kd_user', '=', $user->id)
    ->get();

    if(is_null($data))
    {
        $out['status'] = "error";
        $out['message']='not found';
        $out['data']='';
        return Response::json($out,200);
    }
    else
    {
        $out['status'] = "success";
        $out['message']='success';
        $out['data']=$data;
        return Response::json($out,200);
    }
}

public function sendEmailReminder($email,$nik,$telp)
{
    $user = User::where('email_user', '=', $email)->first();
    if(is_null($data))
    {
      $out['status']='error';
      $out['message']='not found';
      $out['data']='';

      return Response::json($out,200);
  }
  else
  {
   if(!($user->nik == $nik && $user->telepon == $telp))
   {
     $out['status']='error';
     $out['message']='not found';
     $out['data']='';
     return Response::json($out,200);
 }
}
$newPass = str_random(8);
if($user->update(array('password' => $newPass))){
    Mail::send('email.ubahPass', ['newPass' => $newPass], function($message) use ($user)
    {
        $message->from('trikaryateknologi@gmail.com', 'PMP Sales Report');
        $message->to($user->email_user, $user->nama)->subject('Reset Password!');
    });
}
$out['status']='success';
$out['message']='email sent';
$out['data']='';
return Response::json($out,200);
}

public function setVisitProblem()
{   

    $datavisit=new VisitPlan;
    if(!Input::has('kd_outlet'))
    {
        $out['status'] = "no data sent";
        return Response::json($out,500);
    }

    $datavisit->kd_outlet=Input::get('kd_outlet');
    $datavisit->date_visit=Input::get('date_take_photo');
    $datavisit->date_create_visit=Input::get('date_take_photo');
    $datavisit->date_visiting=Input::get('date_take_photo');
    $datavisit->is_checkout='1';
    $datavisit->status_visit='1';
    $datavisit->approve_visit= '1';
    $datavisit->status_visit = 0;
    $success=$datavisit->save(); 

    $data = new VisitProblem;
        // $data['kd_visit'] = $id; 

        // $data->kd_visit = $data['kd_visit'];
    $data->kd_visit = $datavisit->id;
        // $data->kd_visit = Input::get('kd_visit');
    $data->detail_problem = Input::get('detail_problem');
    $data->date_upload_photo = Input::get('date_upload_photo');
    $data->date_take_photo = Input::get('date_take_photo');
    $data->status = 1;

    $path = 'image_upload/visit_problem/';

    $base =Input::get('foto');
    $binary = base64_decode($base);
    header('Content-Type: bitmap; charset=utf-8');

    $f = finfo_open();
    $mime_type = finfo_buffer($f, $binary, FILEINFO_MIME_TYPE);
    $mime_type = str_ireplace('image/', '', $mime_type);

    $filename =  strval(Input::get("kd_user")).'problem_visit'.date('_YmdHis'). '.' . $mime_type;
    $file = fopen($path . $filename, 'wb');
    if (fwrite($file, $binary)) {
        $data->nm_photo = $filename;
        $data->save();
        fclose($file);

        $visit = VisitPlan::find($data->kd_visit);
        $visit->update(['date_visiting' => Input::get('date_take_photo'), 'status_visit' => 1, 'skip_order_reason' => 'Bad signal visit']);
        $visit->save();

        $logging = new Logging;
        $logging->kd_user = Input::get("kd_user");
        $logging->description = "Upload problem visit";
        $logging->log_time = Input::get('date_upload_photo');
        $logging->detail_akses = "PMP Android App";
        $logging->save();

        $out['status']='success';
        $out['message']='upload image success';
        $out['data']='';
            return Response::json($out,200);//return FALSE;
        } else {
            $out['status'] = "error";
            $out['message']='upload image failed';
            $out['data']='';
            return Response::json($out,200);//return FALSE;
        }
    }

    public function setOffice()
    {
        $data = new Office;
        $data->nm_office            = Input::get('nm_office');
        $data->almt_office          = Input::get('almt_office');
        $data->latitude             = Input::get('latitude');
        $data->longitude            = Input::get('longitude');
        $data->status_office        = Input::get('status_office');
        $data->kd_user              = Input::get('kd_user');
        $data->kd_area              = Input::get('kd_area');
        $data->status_office        = 0;
        $data->save();

        $path = 'image_upload/office/';
        $base =Input::get('foto');
        $binary = base64_decode($base);
        header('Content-Type: bitmap; charset=utf-8');

        $f = finfo_open();
        $mime_type = finfo_buffer($f, $binary, FILEINFO_MIME_TYPE);
        $mime_type = str_ireplace('image/', '', $mime_type);

        $filename =  strval(Input::get("kd_user")).'office'.date('_YmdHis'). '.' . $mime_type;
        $file = fopen($path . $filename, 'wb');
        if (fwrite($file, $binary)) {
            $data->foto_office = $filename;
            $data->save();
            fclose($file);

            $logging = new Logging;
            $logging->kd_user = Input::get("kd_user");
            $logging->description = "Upload problem visit";
            $logging->log_time = Input::get('date_upload_photo');
            $logging->detail_akses = "PMP Android App";
            $logging->save();

            $out['status']='success';
            $out['message']='upload image success';
            $out['data']='';
            return Response::json($out,200);
        }else {
            $out['status'] = "error";
            $out['message']='upload image failed';
            $out['data']='';
            return Response::json($out,200);
        }
    }

    public function setPresence(Request $request){
        $status = Input::get('status');
        $token  = $request->bearerToken();

        $user = User::where('api_token', '=', $token)->get()->first();
        
        if($status == 'checkin_presence'){

            $office = Office::find(Input::get('id'));
            $konfigurasi = Konfigurasi::first();

            if(is_null($office)){
                $out['status']='error';
                $out['message']='id office not found';
                $out['data']= '';
                return Response::json($out,200);
            }else{
                if(Input::get('jarak') > $konfigurasi->toleransi_max_office){
                    $out['status']='error';
                    $out['message']='melebihi jarak toleransi';
                    $out['data']= '';
                    return Response::json($out,200);
                }
                else{
                    $data = new Presence;
                    $data->kd_user              = $user->id;
                    $data->kd_office            = Input::get('id');
                    $data->date                 = Input::get('date');
                    $data->status_checkin       = 1;
                    $data->date_checkin_presence = Input::get('time');
                    $data->save();

                    $out['status']='success';
                    $out['message']='success in checkin presence';
                    $out['data']= $data->id;
                    return Response::json($out,200);
                }
            }
        }
        elseif($status == 'checkout_presence')
        {
            $konfigurasi = Konfigurasi::first();
            if(Input::get('jarak') > $konfigurasi->toleransi_max_office){
                $out['status']='error';
                $out['message']='melebihi jarak toleransi';
                $out['data']= '';
                return Response::json($out,200);
            }else{
                $data= Presence::find($request->input('kd_presence'));
                $data->date_checkout_presence = Input::get('time');
                $data->save();

                $out['status']='success';
                $out['message']='success in checkout presence';
                $out['data']= $data->id;
                return Response::json($out,200);
            }
        }
        elseif($status == 'checkin_visit')
        {
            $data= Presence::find($request->input('kd_presence'));
            $data->date_checkin_visit = Input::get('time');
            $data->save();

            $out['status']='success';
            $out['message']='success in checkin visit';
            $out['data']= $data->id;
            return Response::json($out,200);
        }
        elseif($status == 'checkout_visit')
        {
            $data= Presence::find($request->input('kd_presence'));
            $data->date_checkout_visit = Input::get('time');
            $data->save();

            $out['status']='success';
            $out['message']='success in checkout presence';
            $out['data']= $data->id;
            return Response::json($out,200);
        }
    }

    public function setBusinessTrip(Request $request)
    {
        try{
            $token  = $request->bearerToken();
            $user = User::where('api_token', '=', $token)->get()->first();
            $data = Input::json()->all();

            foreach($data['kota'] as $trips){
                $trip               = new BusinessTrip;
                $trip->kd_user      = $user->id;
                $trip->kd_kota      = $trips['kd_kota'];
                $trip->date_request = Input::get('date_request');
                $trip->date_start   = Input::get('date_start');
                $trip->date_end     = Input::get('date_end');
                $success = $trip->save();
            }

            $logging = new Logging;
            $logging->kd_user = $user->id;
            $logging->description = "Request Business Trip";
            $logging->log_time = date("Y-m-d H:i:s");
            $logging->detail_akses = "PMP Android App";
            $logging->save();

            $out['status'] = "success";
            $out['message']='submit business trip success';
            $out['data']='';
        }
        catch(Exception $e){
            $out['status'] = "error";
            $out['message']='submit sampling error';
            $out['data']='';
        }

        return Response::json($out,200);

    }

    public function getNewsImage($id){
        $data = News::find($id);

        $image = file_get_contents('image_upload/news/'.$data->path_image);

        return Image::make($image)->response();

    }

}
