<?php
/*
PT. Trikarya Teknologi Indonesia
Tenggilis raya 127
Office Complex Apartment Metropolis MKB 206
Surabaya, Jawa timur, Indonesia
Phone : +6231-8420384 / +6281235537717
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Input;
use Excel;
use App\VisitPlan;
use App\Outlet;
use App\Distributor;
use App\Area;
use App\Tipe;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\VisitPlanRequest;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;

class VisitPlanController extends Controller
{
    public function formVisit()
    {
        $area = Auth::user()->kd_area;
        if($area==100)
        {
          $data['areas'] = [''=>'All Area'] + Area::pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'All Sales'];
        }
        else
        {
          $data['areas'] =  Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'All Sales']+User::where('kd_area','=',$area)->where('kd_role', '=','3')->pluck('nama', 'id')->toArray();
        }

        $data['tipe'] =  [''=>'All Tipe'] + Tipe::pluck( 'nm_tipe','id')->toArray();
        $data['distributor'] =  [''=>'All Distributor'] + Distributor::pluck('kd_dist','id' )->toArray();

        if(Auth::user()->kd_area == 100){
            $data['content'] = DB::table('visit_plan')
            ->select('outlet.nm_outlet',
                    'kota.nm_kota',
                    'user.nama',
                    'visit_plan.id',
                    'area.kd_area',
                    'tipe.nm_tipe',
                    'distributor.kd_dist',
                    'visit_plan.date_create_visit',
                    'visit_plan.date_visit',
                    'visit_plan.approve_visit')
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->join('tipe', 'tipe.id', '=', 'outlet.kd_tipe')
            ->join('distributor', 'distributor.id', '=', 'outlet.kd_dist')
            ->join('area', 'area.id', '=', 'outlet.kd_area')
            ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->whereBetween('visit_plan.date_create_visit',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
            ->orderBy('visit_plan.date_create_visit', 'desc')
            ->get();
            return view('pages.visit.viewForm')->with('data',$data);
         }
        else if(Auth::user()->kd_role == 3){
            $id =Auth::user()->id;
            $data['content'] = DB::table('visit_plan')
            ->select('outlet.nm_outlet',
                    'kota.nm_kota',
                    'user.nama',
                    'visit_plan.id',
                    'area.kd_area',
                    'tipe.nm_tipe',
                    'distributor.kd_dist',
                    'visit_plan.date_create_visit',
                    'visit_plan.date_visit',
                    'visit_plan.approve_visit')
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->join('tipe', 'tipe.id', '=', 'outlet.kd_tipe')
            ->join('distributor', 'distributor.id', '=', 'outlet.kd_dist')
            ->join('area', 'area.id', '=', 'outlet.kd_area')
            ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->where('outlet.kd_user','=',$id)
            ->whereBetween('visit_plan.date_create_visit',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
            ->orderBy('visit_plan.date_create_visit', 'desc')
            ->get();
            return view('pages.visit.viewForm')->with('data',$data);
        }
        else
        {
            $area =Auth::user()->kd_area;
            $data['content'] = DB::table('visit_plan')
            ->select('outlet.nm_outlet',
                    'kota.nm_kota',
                    'user.nama',
                    'visit_plan.id',
                    'area.kd_area',
                    'tipe.nm_tipe',
                    'distributor.kd_dist',
                    'visit_plan.date_create_visit',
                    'visit_plan.date_visit',
                    'visit_plan.approve_visit')
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->join('tipe', 'tipe.id', '=', 'outlet.kd_tipe')
            ->join('distributor', 'distributor.id', '=', 'outlet.kd_dist')
            ->join('area', 'area.id', '=', 'outlet.kd_area')
            ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->where('outlet.kd_area','=',$area)
            ->whereBetween('visit_plan.date_create_visit',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
            ->orderBy('visit_plan.date_create_visit', 'desc')
            ->get();
            return view('pages.visit.viewForm')->with('data',$data);
        }
    }

    public function index(VisitPlanRequest $request)
    {

       $data['dateTo'] = $request->input('dateTo');
       $data['dateFrom'] = $request->input('dateFrom');

       $sales = $request->input('sales');
       $area = $request->input('kd_area');
       $tipe = $request->input('nm_tipe');
       $dist = $request->input('kd_dist');
       $osales=$oarea=$otipe=$odist='=';
       if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
       if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");
       if($sales==''){
         $sales='null';
         $osales='!=';
       }
       if($area==''||$area=='100'){
         $area='null';
         $oarea='!=';
       }
       if($tipe==''){
         $tipe='null';
         $otipe='!=';
       }
       if($dist==''){
         $dist='null';
         $odist='!=';
       }


       $dateFrom = date("Y-m-d",  strtotime($data['dateFrom']));
       $dateTo =  date("Y-m-d", strtotime('+1 day', strtotime($data['dateTo'])));

        if(Input::get('show')) {
            if(Auth::user()->kd_area == 100){
            $data['content'] = DB::table('visit_plan')
            ->select('outlet.nm_outlet',
                    'kota.nm_kota',
                    'user.nama',
                    'visit_plan.id',
                    'area.kd_area',
                    'tipe.nm_tipe',
                    'distributor.kd_dist',
                    'visit_plan.date_create_visit',
                    'visit_plan.date_visit',
                    'visit_plan.approve_visit')
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->join('tipe', 'tipe.id', '=', 'outlet.kd_tipe')
            ->join('distributor', 'distributor.id', '=', 'outlet.kd_dist')
            ->join('area', 'area.id', '=', 'outlet.kd_area')
            ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->where('user.id', $osales, $sales)
              ->where('outlet.kd_area', $oarea, $area)
              ->where('outlet.kd_tipe', $otipe, $tipe)
              ->where('outlet.kd_dist', $odist, $dist)
              ->whereBetween('visit_plan.date_create_visit', [$dateFrom, $dateTo])
              ->orderBy('visit_plan.date_create_visit', 'desc')
            ->get();
             return view('pages.visit.index')->with('data',$data);
             }
             else if(Auth::user()->kd_role == 3){
                $id =Auth::user()->id;
                $data['content'] = DB::table('visit_plan')
                ->select('outlet.nm_outlet',
                    'kota.nm_kota',
                    'user.nama',
                    'visit_plan.id',
                    'area.kd_area',
                    'tipe.nm_tipe',
                    'distributor.kd_dist',
                    'visit_plan.date_create_visit',
                    'visit_plan.date_visit',
                    'visit_plan.approve_visit')
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->join('tipe', 'tipe.id', '=', 'outlet.kd_tipe')
            ->join('distributor', 'distributor.id', '=', 'outlet.kd_dist')
            ->join('area', 'area.id', '=', 'outlet.kd_area')
            ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
                ->where('outlet.kd_user','=',$id)

              ->where('outlet.kd_area', $oarea, $area)
              ->where('outlet.kd_tipe', $otipe, $tipe)
              ->where('outlet.kd_dist', $odist, $dist)
              ->whereBetween('visit_plan.date_create_visit', [$dateFrom, $dateTo])
              ->orderBy('visit_plan.date_create_visit', 'desc')
                ->get();
                 return view('pages.visit.index')->with('data',$data);
            }
             else
            {
                $area =Auth::user()->kd_area;
                $data['content'] = DB::table('visit_plan')
                ->select('outlet.nm_outlet',
                    'kota.nm_kota',
                    'user.nama',
                    'visit_plan.id',
                    'area.kd_area',
                    'tipe.nm_tipe',
                    'distributor.kd_dist',
                    'visit_plan.date_create_visit',
                    'visit_plan.date_visit',
                    'visit_plan.approve_visit')
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->join('tipe', 'tipe.id', '=', 'outlet.kd_tipe')
            ->join('distributor', 'distributor.id', '=', 'outlet.kd_dist')
            ->join('area', 'area.id', '=', 'outlet.kd_area')
            ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
                ->where('outlet.kd_area','=',$area)
                ->where('user.id', $osales, $sales)
              ->where('outlet.kd_tipe', $otipe, $tipe)
              ->where('outlet.kd_dist', $odist, $dist)
              ->whereBetween('visit_plan.date_create_visit', [$dateFrom, $dateTo])
              ->orderBy('visit_plan.date_create_visit', 'desc')
                ->get();
                 return view('pages.visit.index')->with('data',$data);
            }
        } else if(Input::get('download')) {
            $this-> downloadVisitPlan($sales, $osales, $tipe, $otipe, $area, $oarea, $dist, $odist, $dateFrom, $dateTo);
        }
    }

    public function downloadVisitPlan($sales, $osales, $tipe, $otipe, $area, $oarea, $dist, $odist, $dateFrom, $dateTo)
    {


        $log = new AdminController;
        $log->getLogHistory('Download Data Visit Plan');

        $role = Auth::user()->kd_role;
        $areas = Auth::user()->kd_area;
        if($areas == 100) {
            ob_end_clean();
            ob_start();
            $visit = VisitPlan::select(DB::raw("user.nama as SALES_FORCE,
              outlet.nm_outlet as NAMA_OUTLET,
              kota.nm_kota as KOTA,
              area.kd_area AS AREA,
              tipe.nm_tipe as TIPE,
              distributor.kd_dist as DIST,
              visit_plan.date_create_visit as DATE_CREATE_VISIT,
              visit_plan.date_visit as PLAN_DATE_VISIT,
              (CASE WHEN (visit_plan.approve_visit = 1) THEN 'Approved' WHEN (visit_plan.approve_visit = 2) THEN 'Pending' ELSE 'Denied' END) as VISIT_APPROVAL
              ")
            )
            ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->join('tipe', 'tipe.id', '=', 'outlet.kd_tipe')
            ->join('distributor', 'distributor.id', '=', 'outlet.kd_dist')
            ->join('area', 'area.id', '=', 'outlet.kd_area')
            ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->where('user.id', $osales, $sales)
              ->where('outlet.kd_area', $oarea, $area)
              ->where('outlet.kd_tipe', $otipe, $tipe)
              ->where('outlet.kd_dist', $odist, $dist)
              ->whereBetween('visit_plan.date_create_visit', [$dateFrom, $dateTo])
              ->orderBy('visit_plan.date_create_visit', 'desc')
            ->get();
            date_default_timezone_set("Asia/Jakarta");
            $d= strtotime("now");
            $time =date("d-m-Y H:i:s", $d);
            $filename = $time."_VISIT_PLAN";
            Excel::create($filename, function($excel) use($visit) {
                $excel->sheet('Visit Plan List', function($sheet) use($visit) {
                    $sheet->fromArray($visit);
                });
            })->download('xls');
        }
        else
        {

            $this->downloadVisitPlanArea($areas,$dateFrom,$dateTo, $sales, $osales, $dist, $odist, $tipe, $otipe);
        }
    }

    public function downloadVisitPlanArea($area,$dateFrom,$dateTo, $sales, $osales, $dist, $odist, $tipe, $otipe)
    {
        ob_end_clean();
        ob_start();
        $visit = VisitPlan::select(DB::raw("user.nama as SALES_FORCE,
              outlet.nm_outlet as NAMA_OUTLET,
              kota.nm_kota as KOTA,
              area.kd_area AS AREA,
              tipe.nm_tipe as TIPE,
              distributor.kd_dist as DIST,
              visit_plan.date_create_visit as DATE_CREATE_VISIT,
              visit_plan.date_visit as PLAN_DATE_VISIT,
              (CASE WHEN (visit_plan.approve_visit = 1) THEN 'Approved' WHEN (visit_plan.approve_visit = 2) THEN 'Pending' ELSE 'Denied' END) as VISIT_APPROVAL")
        )
       ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
            ->join('user', 'user.id', '=', 'outlet.kd_user')
            ->join('tipe', 'tipe.id', '=', 'outlet.kd_tipe')
            ->join('distributor', 'distributor.id', '=', 'outlet.kd_dist')
            ->join('area', 'area.id', '=', 'outlet.kd_area')
            ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
            ->where('outlet.kd_area', '=', $area)
            ->where('user.id', $osales, $sales)
              ->where('outlet.kd_tipe', $otipe, $tipe)
              ->where('outlet.kd_dist', $odist, $dist)

              ->whereBetween('visit_plan.date_create_visit', [$dateFrom, $dateTo])
              ->orderBy('visit_plan.date_create_visit', 'desc')
              ->get();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_VISIT_PLAN";
        Excel::create($filename, function($excel) use($visit) {
            $excel->sheet('Sheet 1', function($sheet) use($visit) {
                $sheet->fromArray($visit);
            });
        })->download('xls');
    }

    public function create()
    {
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        $id = Auth::user()->id;
        if($area == 100)
        {
            $dist=Distributor::all();
            $outlet = Outlet::where('reg_status','=', 'YES')->pluck('kd_outlet', 'nm_outlet')->toArray();
        }
        else if($role == 3)
        {
            $dist=Distributor::all();
            $outlet= Outlet::where('kd_user','=',$id)->get();
        }
        else
        {
            $dist=Distributor::all();
            $outlet = Outlet::select('kd_outlet', 'nm_outlet')->where('kd_area', '=', $area)->get()->toArray();
        }

        return view('pages.visit.create')->with('dist',$dist)->with('outlet',$outlet);
    }

    public function store(VisitPlanRequest $request)
    {
        $log = new AdminController;
        VisitPlan::create($request->all());
        $log->getLogHistory('Make Visit Plan');
        return redirect()->route('admin.visitPlan.form');
    }

    public function edit($id)
    {
        $data['content'] = VisitPlan::find($id);
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        $id = Auth::user()->id;
        if($area == 100)
        {
            $outlet = [''=>''] + Outlet::pluck('nm_outlet', 'kd_outlet')->toArray();
        }
        else if($role==3)
        {
            $outlet = [''=>''] + Outlet::where('kd_user','=',$id)->pluck('nm_outlet', 'kd_outlet')->toArray();
        }
        else
        {
             $outlet = [''=>''] + Outlet::where('kd_area','=',$area)->pluck('nm_outlet', 'kd_outlet')->toArray();
        }
        return view('pages.visit.edit')->with('data',$data)->with('outlet',$outlet);
    }

    public function update(VisitPlanRequest $request, $id)
    {
        $data = VisitPlan::find($id);
        $data->update($request->all());
        $log = new AdminController;
        $log->getLogHistory('Update Visit Plan with ID '.$id);
        return redirect()->route('admin.visitPlan.form');
    }

    public function editApprove()
    {
    $user = Auth::user();
    if($user->kd_role == 6){
        $data = DB::table('visit_plan')
        ->select('outlet.nm_outlet',
                'outlet.almt_outlet',
                'kota.nm_kota',
                'user.nama',
                'visit_plan.id',
                'visit_plan.date_create_visit',
                'visit_plan.date_visit',
                'visit_plan.approve_visit')
        ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
        ->join('user', 'user.id', '=', 'outlet.kd_user')
        ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
        ->where('visit_plan.approve_visit','=',0)
        ->get();
    }
    else{
        $data = DB::table('visit_plan')
        ->select('outlet.nm_outlet',
                'outlet.almt_outlet',
                'kota.nm_kota',
                'user.nama',
                'visit_plan.id',
                'visit_plan.date_create_visit',
                'visit_plan.date_visit',
                'visit_plan.approve_visit')
        ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
        ->join('user', 'user.id', '=', 'outlet.kd_user')
        ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
        ->where('visit_plan.approve_visit','=',0)
        ->where('outlet.kd_area','=',$user->kd_area)
        ->get();
    }
         return view('pages.visit.editApprove')->with('data',$data);
    }

    public function approvedVisit(VisitPlanRequest $request)
    {
        $id = $request->input('approve_visit');
        $data = VisitPlan::findMany($id);
        foreach ($data as $value)
        {
            $value->update(['approve_visit' => 1]);
            $value->save();
            $dataa['status']='new';
            $dataa['tipe']='VisitPlan';
            $dataa['message']=$value;
            $fcm_id=User::select('user.id_gcm')->join('outlet', 'outlet.kd_user', '=', 'user.id')
            ->join('visit_plan', 'visit_plan.kd_outlet', '=','outlet.kd_outlet')
            ->where('visit_plan.id', $value->id)->get();

            $this->sendNotif($dataa, $fcm_id[0]->id_gcm);
        }

        $log = new AdminController;
        $log->getLogHistory('Approve Visit');
        return redirect()->route('admin.visitPlan.form');
    }

    public function editApproveHO()
    {
      $user = Auth::user();
         if($user->kd_role == 6){
            $data = DB::table('visit_plan')
                    ->select('outlet.nm_outlet',
                            'outlet.almt_outlet',
                            'kota.nm_kota',
                            'user.nama',
                            'visit_plan.id',
                            'visit_plan.date_create_visit',
                            'visit_plan.date_visit',
                            'visit_plan.approve_visit')
                    ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
                    ->join('user', 'user.id', '=', 'outlet.kd_user')
                    ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
                    ->where('visit_plan.approve_visit','=',1)
                    ->get();
         }
         else{
            $data = DB::table('visit_plan')
                    ->select('outlet.nm_outlet',
                            'outlet.almt_outlet',
                            'kota.nm_kota',
                            'user.nama',
                            'visit_plan.id',
                            'visit_plan.date_create_visit',
                            'visit_plan.date_visit',
                            'visit_plan.approve_visit')
                    ->join('outlet', 'visit_plan.kd_outlet', '=', 'outlet.kd_outlet')
                    ->join('user', 'user.id', '=', 'outlet.kd_user')
                    ->join('kota', 'kota.id', '=', 'outlet.kd_kota')
                    ->where('visit_plan.approve_visit','=',1)
                    ->where('outlet.kd_area','=',$user->kd_area)
                    ->get();
         }
         return view('pages.visit.editApproveHO')->with('data',$data);
    }

    public function approvedVisitHO(VisitPlanRequest $request)
    {
        $id = $request->input('approve_visit');
        $data = VisitPlan::findMany($id);
        foreach ($data as $value)
        {
            $value->update(['approve_visit' => 3]);
            $value->save();
            $dataa['status']='new';
            $dataa['tipe']='VisitPlan';
            $dataa['message']=$value;
            $fcm_id=User::select('user.id_gcm')->join('outlet', 'outlet.kd_user', '=', 'user.id')
            ->join('visit_plan', 'visit_plan.kd_outlet', '=','outlet.kd_outlet')
            ->where('visit_plan.id', $value->id)->get();

            $this->sendNotif($dataa, $fcm_id[0]->id_gcm);
        }

        $log = new AdminController;
        $log->getLogHistory('Approve Visit');
        return redirect()->route('admin.visitPlan.form');
    }

    public function destroy($id)
    {
        $data = VisitPlan::with('takeOrders')->find($id);
        foreach ($data->takeOrders as $row) {
            $row->delete();
        }
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Visit Plan');
        return redirect()->route('admin.visitPlan.form');
    }

    public function sendNotif($data, $fcm_id){
        $ch = curl_init("https://fcm.googleapis.com/fcm/send");
        $header = array
        (
            'Authorization: key=AAAA_xS6-L0:APA91bEYaR-O3xM8FvcFqo6BOuOWNHaYId6Th0KExWi9dHfWlGWLAfnsUaQpLaVrZKqKIjWZD2ExvfxlOf2gZlPPA0msaGcilVik5mYEOA95TDriQ7plaZwZkes7-3tm4fWHJNINdr4W',
            'Content-Type: application/json'
        );
        $msg = array
        (
            'to'        => $fcm_id,
            'priority'      => "high",
            'data'  => $data
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($msg));

        $response = curl_exec($ch);
        //echo 'a';
        //print_r($data);
        //print_r($response);
        curl_close($ch);
    }
}
