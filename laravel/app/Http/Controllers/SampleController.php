<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\SampleRequest;
use App\Sample;

class SampleController extends Controller
{
    public function index()
    {
        $data = Sample::all();
        return view('pages.sample.index', compact('data'));
    }

    public function create()
    {
        return view('pages.sample.create');
    }

    public function store(SampleRequest $request)
    {
        Sample::create($request->all());
        $log = new AdminController;
        $log->getLogHistory('Add Sample');
        return redirect()->route('admin.sample.index');
    }

    public function edit($id)
    {
        $data['content'] = Sample::find($id);
        return view('pages.sample.edit')->with('data',$data);
    }

    public function update(SampleRequest $request, $id)
    {
        $sample = Sample::find($id);       
        $sample->update($request->all());
        $log = new AdminController;
        $log->getLogHistory('Update Sample with ID'.$id);
        return redirect()->route('admin.sample.index');
    }

    public function destroy($id)
    {
        $data = Sample::find($id);
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Sample with ID'.$id);
        return redirect()->route('admin.sample.index'); 
    }
}
