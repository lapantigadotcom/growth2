<?php

namespace App\Http\Controllers;

use Response;

use App\News;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class GuestApiController extends Controller
{
    public function getNews()
    {

        $data = News::where('status',1)->orderBy('date_upload', 'desc')->get();

        if(is_null($data))
        {
          $out['status'] = "error";
          $out['message']='not found';
          $out['data']='';
            return Response::json($out,200);
        }
        else
        {
          $out['status'] = "success";
          $out['message']='success';
          $out['data']=$data;
            return Response::json($out,200);
        }
    }
}
