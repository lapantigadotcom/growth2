<?php
/*
PT. Trikarya Teknologi Indonesia
Tenggilis raya 127
Office Complex Apartment Metropolis MKB 206
Surabaya, Jawa timur, Indonesia
Phone : +6231-8420384 / +6281235537717
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Input;
use Validator;
use App\Outlet;
use App\Tipe;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;
use App\Http\Requests\OutletRequest;

use Illuminate\Http\Request;

class TipeOutletController extends Controller
{
    public function index()
    {
        $data = Tipe::all();
        return view('pages.outletManage.edit', compact('data'));
    }

    public function create()
    {
        return view('pages.outletManage.create');
    }

    public function store(OutletRequest $request)
    {
        Tipe::create($request->all());
        $log = new AdminController;
	$log->getLogHistory('Make New Tipe Outlet');
        return redirect()->route('admin.outletManage.index');
    }

    public function edit($id)
    {
        $data= Tipe::find($id);
        return view('pages.outletManage.editTipe')->with('data',$data);
    }

    public function update(OutletRequest $request, $id)
    {
        $data = Tipe::find($id);       
        $data->update($request->all());
        $log = new AdminController;
	$log->getLogHistory('Update Tipe Outlet with ID '.$id);
        return redirect()->route('admin.outletManage.index');
    }

    public function destroy($id)
    {
        $data = Tipe::find($id);
        $data->delete();
        $log = new AdminController;
	$log->getLogHistory('Delete Tipe Outlet with ID '.$id);
        return redirect()->route('admin.outletManage.index');
    }

    
}
