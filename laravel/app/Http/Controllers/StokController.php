<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use DB;
use Auth;
use Session;
use Input;
use Excel;
use App\VisitPlan;
use App\Http\Requests\VisitPlanRequest;
use App\Http\Requests\StokRequest;
use App\Outlet;
use App\Kota;
use App\Area;
use App\Tipe;
use App\Stok;
use App\Produk;
use App\Distributor;

class StokController extends Controller
{
  public function formStok()
  {

    $area = Auth::user()->kd_area;
        if($area==100)
        {
          $data['areas'] = [''=>'All Area'] + Area::pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'All Sales'];
        }
        else
        {
          $data['areas'] =  Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'All Sales']+User::where('kd_area','=',$area)->where('kd_role', '=','3')->pluck('nama', 'id')->toArray();
        }

        $data['tipe'] =  [''=>'All Tipe'] + Tipe::pluck( 'nm_tipe','id')->toArray();
        $data['distributor'] =  [''=>'All Distributor'] + Distributor::pluck('kd_dist','id' )->toArray();

    if(Auth::user()->kd_area==100){
      $data['data'] = Stok::select('outlet.nm_outlet', 'outlet.kd_outlet',
      'user.nama',
      'produk.nm_produk',
      'stok.*')
      ->join('visit_plan', 'stok.kd_visit','=','visit_plan.id')
      ->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
      ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
      ->leftJoin('produk', 'produk.id', '=', 'stok.kd_produk')
      ->whereBetween('stok.date_upload', [date('Y-m-d', strtotime("-7 day")), date('Y-m-d', strtotime("+1 day"))])
      ->orderBy('stok.date_upload', 'desc')
      ->get();
      return view('pages.stok.viewFormStok')->with('data',$data);
    }
    else if(Auth::user()->kd_role == 3){
      $id = Auth::user()->id;
      $data['data'] = Stok::select('outlet.nm_outlet', 'outlet.kd_outlet',
      'user.nama',
      'produk.nm_produk',
      'stok.*')
      ->join('visit_plan', 'stok.kd_visit','=','visit_plan.id')
      ->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
      ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
      ->leftJoin('produk', 'produk.id', '=', 'stok.kd_produk')
      ->where('outlet.kd_user','=', $id)
      ->whereBetween('stok.date_upload', [date('Y-m-d', strtotime("-7 day")), date('Y-m-d', strtotime("+1 day"))])
      ->orderBy('stok.date_upload', 'desc')
      ->get();
      return view('pages.stok.viewFormStok')->with('data',$data);

    }
    else{
      $area = Auth::user()->kd_area;
      $data['data'] = Stok::select('outlet.nm_outlet', 'outlet.kd_outlet',
      'user.nama',
      'produk.nm_produk',
      'stok.*')
      ->join('visit_plan', 'stok.kd_visit','=','visit_plan.id')
      ->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
      ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
      ->leftJoin('produk', 'produk.id', '=', 'stok.kd_produk')
      ->where('outlet.kd_area','=', $area)
      ->whereBetween('stok.date_upload', [date('Y-m-d', strtotime("-7 day")), date('Y-m-d', strtotime("+1 day"))])
      ->orderBy('stok.date_upload', 'desc')
      ->get();
      return view('pages.stok.viewFormStok')->with('data',$data);
    }
  }


  public function index(StokRequest $request){
    $data['dateFrom'] = $request->input('dateFrom');
    $data['dateTo'] = $request->input('dateTo');
    $sales = $request->input('sales');
    $area = $request->input('kd_area');
    $tipe = $request->input('nm_tipe');
    $dist = $request->input('kd_dist');
    $osales=$oarea=$otipe=$odist='=';
    if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
    if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");
    if($sales==''){
      $sales='null';
      $osales='!=';
    }
    if($area==''||$area=='100'){
      $area='null';
      $oarea='!=';
    }
    if($tipe==''){
      $tipe='null';
      $otipe='!=';
    }
    if($dist==''){
      $dist='null';
      $odist='!=';
    }


    $dateFrom = date("Y-m-d", strtotime($data['dateFrom']));
    $dateTo =  date("Y-m-d", strtotime('+1 day', strtotime($data['dateTo'])));

    if(Input::get('show')){
      if(Auth::user()->kd_area==100){
        $data['data'] = Stok::select('outlet.nm_outlet', 'outlet.kd_outlet',
        'user.nama',
        'produk.nm_produk',
        'stok.*')
        ->join('visit_plan', 'stok.kd_visit','=','visit_plan.id')
        ->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
        ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
        ->leftJoin('produk', 'produk.id', '=', 'stok.kd_produk')
        ->where('user.id', $osales, $sales)
        ->where('outlet.kd_area', $oarea, $area)
        ->where('outlet.kd_tipe', $otipe, $tipe)
        ->where('outlet.kd_dist', $odist, $dist)
        ->whereBetween('stok.date_upload',  [$dateFrom, $dateTo])
        ->orderBy('stok.date_upload', 'desc')
        ->get();
        return view('pages.stok.index')->with('data',$data);
      }
      else if(Auth::user()->kd_role ==3){
        $id = Auth::user()->id;
        $data['data'] = Stok::select('outlet.nm_outlet', 'outlet.kd_outlet',
        'user.nama',
        'produk.nm_produk',
        'stok.*')
        ->join('visit_plan', 'stok.kd_visit','=','visit_plan.id')
        ->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
        ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
        ->leftJoin('produk', 'produk.id', '=', 'stok.kd_produk')
        ->where('outlet.kd_user','=', $id)

        ->where('outlet.kd_area', $oarea, $area)
        ->where('outlet.kd_tipe', $otipe, $tipe)
        ->where('outlet.kd_dist', $odist, $dist)
        ->whereBetween('stok.date_upload', [$dateFrom, $dateTo])
        ->orderBy('stok.date_upload', 'desc')
        ->get();
        return view('pages.stok.index')->with('data',$data);
      }
      else{
        $area = Auth::user()->kd_area;
        $data['data'] = Stok::select('outlet.nm_outlet', 'outlet.kd_outlet',
        'user.nama',
        'produk.nm_produk',
        'stok.*')
        ->join('visit_plan', 'stok.kd_visit','=','visit_plan.id')
        ->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
        ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
        ->leftJoin('produk', 'produk.id', '=', 'stok.kd_produk')
        ->where('outlet.kd_area','=', $area)
        ->where('user.id', $osales, $sales)

        ->where('outlet.kd_tipe', $otipe, $tipe)
        ->where('outlet.kd_dist', $odist, $dist)
        ->whereBetween('stok.date_upload', [$dateFrom, $dateTo])
        ->orderBy('stok.date_upload', 'desc')
        ->get();
        return view('pages.stok.index')->with('data',$data);
      }
    }
    else if(Input::get('download')){
      $this->downloadStok($sales, $osales, $tipe, $otipe, $area, $oarea, $dist, $odist, $dateFrom, $dateTo);
    }
  }

  public function edit($id){
    $data['content']=Stok::find($id);
    $produk=Produk::pluck('nm_produk', 'id');
    $role = Auth::user()->kd_role;
    $area = Auth::user()->kd_area;
    $id= Auth::user()->id;

    return view('pages.stok.edit')->with('data',$data)->with('produk',$produk);
  }

  public function destroy($id)
  {
    $data = Stok::find($id);

    $data->delete();
    $log = new AdminController;
    $log->getLogHistory('Delete Stok with ID'.$id);
    return redirect()->route('admin.stok.form');

  }
  public function store(StokRequest $request)
  {
    $log = new AdminController;
    Stok::create($request->all());
    $log->getLogHistory('Make Stok');
    return redirect()->route('admin.stok.form');
  }

  public function update(StokRequest $request, $id)
  {
    $data = Stok::find($id);

    $data->update($request->all());

    $log = new AdminController;
    $log->getLogHistory('Update Stok with ID'.$id);
    return redirect()->route('admin.stok.form');
  }

  public function downloadStok($sales, $osales, $tipe, $otipe, $area, $oarea, $dist, $odist, $dateFrom, $dateTo)
  {
    $log = new AdminController;
    $log->getLogHistory('Download Data Stok');

    $role = Auth::user()->kd_role;
    $areas = Auth::user()->kd_area;
    if($areas == 100)
    {
      ob_end_clean();
      ob_start();
      $stok = Stok::select(DB::raw("
      user.nama as SALES,
      stok.kd_stok as KODE_STOK,
      stok.kd_visit as KODE_VISIT,
      visit_plan.kd_outlet as KODE_OUTLET,
      outlet.nm_outlet as NAMA_OUTLET,
      tipe.nm_tipe as TIPE_OUTLET,
      produk.nm_produk as NAMA_PRODUK,
      stok.qty as QTY,
      stok.date_upload as DATE_UPLOAD
      ")
      )
      ->join('visit_plan', 'stok.kd_visit','=','visit_plan.id')
      ->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
      ->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')
      ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
      ->leftJoin('produk', 'produk.id', '=', 'stok.kd_produk')
      ->where('user.id', $osales, $sales)
      ->where('outlet.kd_area', $oarea, $area)
      ->where('outlet.kd_tipe', $otipe, $tipe)
      ->where('outlet.kd_dist', $odist, $dist)
      ->whereBetween('stok.date_upload',  [$dateFrom, $dateTo])
      ->orderBy('stok.date_upload', 'desc')
      ->get();

    }
    else if(Auth::user()->kd_role == 3){
      ob_end_clean();
      ob_start();
      $id =Auth::user()->id;
      $stok = Stok::select(DB::raw("
      user.nama as SALES,
      stok.kd_stok as KODE_STOK,
      stok.kd_visit as KODE_VISIT,
      visit_plan.kd_outlet as KODE_OUTLET,
      outlet.nm_outlet as NAMA_OUTLET,
      tipe.nm_tipe as TIPE_OUTLET,
      produk.nm_produk as NAMA_PRODUK,
      stok.qty as QTY,
      stok.date_upload as DATE_UPLOAD
      ")
      )
      ->join('visit_plan', 'stok.kd_visit','=','visit_plan.id')
      ->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
      ->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')
      ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
      ->leftJoin('produk', 'produk.id', '=', 'stok.kd_produk')
      ->where('user.id', '=', $id)
      ->where('outlet.kd_area', $oarea, $area)
      ->where('outlet.kd_tipe', $otipe, $tipe)
      ->where('outlet.kd_dist', $odist, $dist)
      ->whereBetween('stok.date_upload',  [$dateFrom, $dateTo])
      ->orderBy('stok.date_upload', 'desc')
      ->get();

    }
    else
    {
      ob_end_clean();
      ob_start();
      $stok = Stok::select(DB::raw("
      user.nama as SALES,
      stok.kd_stok as KODE_STOK,
      stok.kd_visit as KODE_VISIT,
      visit_plan.kd_outlet as KODE_OUTLET,
      outlet.nm_outlet as NAMA_OUTLET,
      tipe.nm_tipe as TIPE_OUTLET,
      produk.nm_produk as NAMA_PRODUK,
      stok.qty as QTY,
      stok.date_upload as DATE_UPLOAD
      ")
      )
      ->join('visit_plan', 'stok.kd_visit','=','visit_plan.id')
      ->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
      ->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')
      ->leftJoin('user', 'user.id','=', 'outlet.kd_user')
      ->leftJoin('produk', 'produk.id', '=', 'stok.kd_produk')
      ->where('user.id', $osales, $sales)
      ->where('outlet.kd_area', '=', $areas)
      ->where('outlet.kd_tipe', $otipe, $tipe)
      ->where('outlet.kd_dist', $odist, $dist)
      ->whereBetween('stok.date_upload',  [$dateFrom, $dateTo])
      ->orderBy('stok.date_upload', 'desc')
      ->get();

    }
    date_default_timezone_set("Asia/Jakarta");
      $d= strtotime("now");
      $time =date("d-m-Y H:i:s", $d);
      $filename = $time."_STOK";

      Excel::create($filename, function($excel) use($stok) {
        $excel->sheet('Stok', function($sheet) use($stok) {
          $sheet->fromArray($stok);
        });
      })->download('xls');
  }
}
