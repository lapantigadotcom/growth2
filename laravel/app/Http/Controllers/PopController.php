<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use DB;
use Auth;
use Session;
use Input;
use Excel;
use App\VisitPlan;
use App\Http\Requests\PopRequest;
use App\Http\Requests\StokRequest;
use App\Outlet;
use App\Kota;
use App\Area;
use App\Tipe;
use App\TipePop;
use App\Stok;
use App\Distributor;
use App\Pop;

class PopController extends Controller
{
	public function formPop()
	{

		 $area = Auth::user()->kd_area;
        if($area==100)
        {
          $data['areas'] = [''=>'All Area'] + Area::pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'All Sales'];
        }
        else
        {
          $data['areas'] =  Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
          $data['sales'] =  [''=>'All Sales']+User::where('kd_area','=',$area)->where('kd_role', '=','3')->pluck('nama', 'id')->toArray();
        }

        $data['tipe'] =  [''=>'All Tipe'] + Tipe::pluck( 'nm_tipe','id')->toArray();
        $data['distributor'] =  [''=>'All Distributor'] + Distributor::pluck('kd_dist','id' )->toArray();

		if(Auth::user()->kd_area==100){
			$data['data'] = Pop::select('outlet.nm_outlet', 'outlet.kd_outlet',
				'user.nama',
				'tp_pop.nm_pop',
				'pop.*')
			->join('visit_plan', 'pop.kd_visit','=','visit_plan.id')
			->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
			->leftJoin('user', 'user.id','=', 'outlet.kd_user')
			->leftJoin('tp_pop', 'pop.jenis_pop','=', 'tp_pop.id')
			->whereBetween('pop.date_upload', [date('Y-m-d', strtotime("-7 day")), date('Y-m-d', strtotime("+1 day"))])
			->orderBy('pop.date_upload', 'desc')
			->get();
			return view('pages.pop.viewFormPop')->with('data',$data);
		}
		else if(Auth::user()->kd_role == 3){
			$id = Auth::user()->id;
			$data['data'] = Pop::select('outlet.nm_outlet', 'outlet.kd_outlet',
				'user.nama',
				'tp_pop.nm_pop',
				'pop.*')
			->join('visit_plan', 'pop.kd_visit','=','visit_plan.id')
			->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
			->leftJoin('user', 'user.id','=', 'outlet.kd_user')
			->leftJoin('tp_pop', 'pop.jenis_pop','=', 'tp_pop.id')
			->where('outlet.kd_user','=', $id)
			->whereBetween('pop.date_upload', [date('Y-m-d', strtotime("-7 day")), date('Y-m-d', strtotime("+1 day"))])
			->orderBy('pop.date_upload', 'desc')
			->get();
			return view('pages.pop.viewFormPop')->with('data',$data);

		}
		else{
			$area = Auth::user()->kd_area;
			$data['data'] = Pop::select('outlet.nm_outlet', 'outlet.kd_outlet',
				'user.nama',
				'tp_pop.nm_pop',
				'pop.*')
			->join('visit_plan', 'pop.kd_visit','=','visit_plan.id')
			->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
			->leftJoin('user', 'user.id','=', 'outlet.kd_user')
			->leftJoin('tp_pop', 'pop.jenis_pop','=', 'tp_pop.id')
			->where('outlet.kd_area','=', $area)
			->whereBetween('pop.date_upload', [date('Y-m-d', strtotime("-7 day")), date('Y-m-d', strtotime("+1 day"))])
			->orderBy('pop.date_upload', 'desc')
			->get();
			return view('pages.pop.viewFormPop')->with('data',$data);
		}
	}


	public function index(PopRequest $request){
		$data['dateFrom'] = $request->input('dateFrom');
		$data['dateTo'] = $request->input('dateTo');
		$sales = $request->input('sales');
		$area = $request->input('kd_area');
		$tipe = $request->input('nm_tipe');
		$dist = $request->input('kd_dist');
		$osales=$oarea=$otipe=$odist='=';
		if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
		if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");
		if($sales==''){
			$sales='null';
			$osales='!=';
		}
		if($area==''||$area=='100'){
			$area='null';
			$oarea='!=';
		}
		if($tipe==''){
			$tipe='null';
			$otipe='!=';
		}
		if($dist==''){
			$dist='null';
			$odist='!=';
		}


		$dateFrom = date("Y-m-d",  strtotime($data['dateFrom']));
		$dateTo =  date("Y-m-d", strtotime('+1 day', strtotime($data['dateTo'])));

		if(Input::get('show')){
			if(Auth::user()->kd_area==100){
				$data['data'] = Pop::select('outlet.nm_outlet', 'outlet.kd_outlet',
					'user.nama',
					'tp_pop.nm_pop',
					'pop.*')
				->join('visit_plan', 'pop.kd_visit','=','visit_plan.id')
				->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
				->leftJoin('user', 'user.id','=', 'outlet.kd_user')
				->leftJoin('tp_pop', 'pop.jenis_pop','=', 'tp_pop.id')
				->where('user.id', $osales, $sales)
				->where('outlet.kd_area', $oarea, $area)
				->where('outlet.kd_tipe', $otipe, $tipe)
				->where('outlet.kd_dist', $odist, $dist)
				->whereBetween('pop.date_upload',  [$dateFrom, $dateTo])
				->orderBy('pop.date_upload', 'desc')
				->get();
				return view('pages.pop.index')->with('data',$data);
			}
			else if(Auth::user()->kd_role ==3){
				$id = Auth::user()->id;
				$data['data'] = Pop::select('outlet.nm_outlet', 'outlet.kd_outlet',
					'user.nama',
					'tp_pop.nm_pop',
					'pop.*')
				->join('visit_plan', 'pop.kd_visit','=','visit_plan.id')
				->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
				->leftJoin('user', 'user.id','=', 'outlet.kd_user')
				->leftJoin('tp_pop', 'pop.jenis_pop','=', 'tp_pop.id')
				->where('outlet.kd_user','=', $id)

				->where('outlet.kd_area', $oarea, $area)
				->where('outlet.kd_tipe', $otipe, $tipe)
				->where('outlet.kd_dist', $odist, $dist)
				->whereBetween('pop.date_upload', [$dateFrom, $dateTo])
				->orderBy('pop.date_upload', 'desc')
				->get();
				return view('pages.pop.index')->with('data',$data);
			}
			else{
				$area = Auth::user()->kd_area;
				$data['data'] = Pop::select('outlet.nm_outlet', 'outlet.kd_outlet',
					'user.nama',
					'tp_pop.nm_pop',
					'pop.*')
				->join('visit_plan', 'pop.kd_visit','=','visit_plan.id')
				->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
				->leftJoin('user', 'user.id','=', 'outlet.kd_user')
				->leftJoin('tp_pop', 'pop.jenis_pop','=', 'tp_pop.id')
				->where('outlet.kd_area','=', $area)
				->where('user.id', $osales, $sales)

				->where('outlet.kd_tipe', $otipe, $tipe)
				->where('outlet.kd_dist', $odist, $dist)
				->whereBetween('pop.date_upload', [$dateFrom, $dateTo])
				->orderBy('pop.date_upload', 'desc')
				->get();
				return view('pages.pop.index')->with('data',$data);
			}
		}
		else if(Input::get('download')){
			$this->downloadPop($sales, $osales, $tipe, $otipe, $area, $oarea, $dist, $odist, $dateFrom, $dateTo);
		}
	}

	public function edit($id){
		$data['content']=Pop::find($id);
		$tp_pop = TipePop::pluck('nm_pop', 'id')->toArray();
		$role = Auth::user()->kd_role;
		$area = Auth::user()->kd_area;
		$id= Auth::user()->id;
		/*if($area == 100)
		{
			$outlet = [''=>''] + Outlet::pluck('nm_outlet', 'kd_outlet')->toArray();
		}
		else if($role==3)
		{
			$outlet = [''=>''] + Outlet::where('kd_user','=',$id)->pluck('nm_outlet', 'kd_outlet')->toArray();
		}
		else
		{
			$outlet = [''=>''] + Outlet::where('kd_area','=',$area)->pluck('nm_outlet', 'kd_outlet')->toArray();
		}*/
		return view('pages.pop.edit')->with('data',$data)->with('jenis_pop',$tp_pop);
	}

	public function destroy($id)
	{
		$data = Pop::find($id);

		$data->delete();
		$log = new AdminController;
		$log->getLogHistory('Delete Pemakaian POP with ID '.$id);
		return redirect()->route('admin.pop.form');

	}
	public function store(PopRequest $request)
	{
		$log = new AdminController;
		Pop::create($request->all());
		$log->getLogHistory('Make Pemakaian POP');
		return redirect()->route('admin.pop.form');
	}

	public function update(PopRequest $request, $id)
	{
		$data = Pop::find($id);

		$data->update($request->all());

		$log = new AdminController;
		$log->getLogHistory('Update Pemakaian Pop with ID '.$id);
		return redirect()->route('admin.pop.form');
	}



	public function downloadPop($sales, $osales, $tipe, $otipe, $area, $oarea, $dist, $odist, $dateFrom, $dateTo)
	{
		$log = new AdminController;
		$log->getLogHistory('Download Data Pemakaian POP');

		$role = Auth::user()->kd_role;
		$areas = Auth::user()->kd_area;
		if($areas == 100)
		{
			ob_end_clean();
			ob_start();
			$pop = Pop::select(DB::raw("
				user.nama as SALES,
				pop.kd_pop as KODE_POP,
				pop.kd_visit as KODE_VISIT,
				visit_plan.kd_outlet as KODE_OUTLET,
				outlet.nm_outlet as NAMA_OUTLET,
				tipe.nm_tipe as TIPE_OUTLET,
				tp_pop.nm_pop as JENIS_POP,
				pop.qty as QTY,
				pop.date_upload as DATE_UPLOAD
				")
			)
			->join('visit_plan', 'pop.kd_visit','=','visit_plan.id')
			->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
			->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')
			->leftJoin('user', 'user.id','=', 'outlet.kd_user')

			->leftJoin('tp_pop', 'pop.jenis_pop','=', 'tp_pop.id')
			->where('user.id', $osales, $sales)
			->where('outlet.kd_area', $oarea, $area)
			->where('outlet.kd_tipe', $otipe, $tipe)
			->where('outlet.kd_dist', $odist, $dist)
			->whereBetween('pop.date_upload',  [$dateFrom, $dateTo])
			->orderBy('pop.date_upload', 'desc')
			->get();

		}
		else if(Auth::user()->kd_role == 3){
			ob_end_clean();
			ob_start();
			$id =Auth::user()->id;
			$pop = Pop::select(DB::raw("
				user.nama as SALES,
				pop.kd_pop as KODE_POP,
				pop.kd_visit as KODE_VISIT,
				visit_plan.kd_outlet as KODE_OUTLET,
				outlet.nm_outlet as NAMA_OUTLET,
				tipe.nm_tipe as TIPE_OUTLET,
				tp_pop.nm_pop as JENIS_POP,
				pop.qty as QTY,
				pop.date_upload as DATE_UPLOAD
				")
			)
			->join('visit_plan', 'pop.kd_visit','=','visit_plan.id')
			->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
			->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')
			->leftJoin('user', 'user.id','=', 'outlet.kd_user')

			->leftJoin('tp_pop', 'pop.jenis_pop','=', 'tp_pop.id')
			->where('user.id', '=', $id)
			->where('outlet.kd_area', $oarea, $area)
			->where('outlet.kd_tipe', $otipe, $tipe)
			->where('outlet.kd_dist', $odist, $dist)
			->whereBetween('pop.date_upload',  [$dateFrom, $dateTo])
			->orderBy('pop.date_upload', 'desc')
			->get();

		}
		else
		{
			ob_end_clean();
			ob_start();
			$pop = Pop::select(DB::raw("
				user.nama as SALES,
				pop.kd_pop as KODE_POP,
				pop.kd_visit as KODE_VISIT,
				visit_plan.kd_outlet as KODE_OUTLET,
				outlet.nm_outlet as NAMA_OUTLET,
				tipe.nm_tipe as TIPE_OUTLET,
				tp_pop.nm_pop as JENIS_POP,
				pop.qty as QTY,
				pop.date_upload as DATE_UPLOAD
				")
			)
			->join('visit_plan', 'pop.kd_visit','=','visit_plan.id')
			->leftJoin('outlet', 'visit_plan.kd_outlet','=','outlet.kd_outlet')
			->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')
			->leftJoin('user', 'user.id','=', 'outlet.kd_user')

			->leftJoin('tp_pop', 'pop.jenis_pop','=', 'tp_pop.id')
			->where('user.id', $osales, $sales)
			->where('outlet.kd_area', '=', $areas)
			->where('outlet.kd_tipe', $otipe, $tipe)
			->where('outlet.kd_dist', $odist, $dist)
			->whereBetween('pop.date_upload',  [$dateFrom, $dateTo])
			->orderBy('pop.date_upload', 'desc')
			->get();

		}
		date_default_timezone_set("Asia/Jakarta");
			$d= strtotime("now");
			$time =date("d-m-Y H:i:s", $d);
			$filename = $time."_POP";
			Excel::create($filename, function($excel) use($pop) {
				$excel->sheet('Pop', function($sheet) use($pop) {
					$sheet->fromArray($pop);
				});
			})->download('xls');
	}
}
