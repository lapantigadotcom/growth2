<?php
/*
PT. Trikarya Teknologi Indonesia
Tenggilis raya 127
Office Complex Apartment Metropolis MKB 206
Surabaya, Jawa timur, Indonesia
Phone : +6231-8420384 / +6281235537717
*/
namespace App\Http\Controllers;

use DB;
use Auth;
use Session;
use Input;
use Excel;
use App\VisitPlan;
use App\VisitBukti;
use App\Outlet;
use App\Distributor;
use App\Area;
use App\Tipe;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\VisitBuktiRequest;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;

class VisitBuktiController extends Controller
{
     

     
   public function index(VisitBuktiRequest $request)
    {
          $data['dateTo'] = $request->input('dateTo');
        $data['dateFrom'] = $request->input('dateFrom');
        if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
    if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");
    $dateFrom = date("Y-m-d",strtotime($data['dateFrom']));
    $dateTo =  date("Y-m-d", strtotime('+1 day', strtotime($data['dateTo'])));

        $data = VisitBukti::orderBy('created_at', 'desc')
        ->with('outlets')
        ->with('users')
        ->whereBetween('created_at',[$dateFrom,$dateTo])
        ->get();
        return view('pages.visitbukti.index', compact('data'));
    }
    public function formbukti(VisitBuktiRequest $request)
    {
       
        $data = VisitBukti::orderBy('created_at', 'desc')
        ->with('outlets')
        ->with('users')
          ->whereBetween('created_at',[date('Y-m-d',strtotime("-7 day")),date('Y-m-d',strtotime("+1 day"))])
        ->take(100)->get();
        return view('pages.visitbukti.visitbukti', compact('data'));
        
    }

    public function create()
    {
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        $id = Auth::user()->id;
        if($area == 100)
        {
            $dist=Distributor::all();
            $outlet = Outlet::where('reg_status','=', 'YES')->pluck('kd_outlet', 'nm_outlet')->toArray();
        }
        else if($role == 3)
        {
            $dist=Distributor::all();
            $outlet= Outlet::where('kd_user','=',$id)->get();
        }
        else
        {
            $dist=Distributor::all();
            $outlet = Outlet::select('kd_outlet', 'nm_outlet')->where('kd_area', '=', $area)->get()->toArray();
        }

        return view('pages.visitbukti.create')->with('dist',$dist)->with('outlet',$outlet);
    }

    public function store(VisitPlanRequest $request)
    {
        $log = new AdminController;
        VisitPlan::create($request->all());
        $log->getLogHistory('Make Visit Plan');
        return redirect()->route('admin.visitPlan.form');
    }

    public function edit($id)
    {
        $data['content'] = VisitPlan::find($id);
        $role = Auth::user()->kd_role;
        $area = Auth::user()->kd_area;
        $id = Auth::user()->id;
        if($area == 100)
        {
            $outlet = [''=>''] + Outlet::pluck('nm_outlet', 'kd_outlet')->toArray();
        }
        else if($role==3)
        {
            $outlet = [''=>''] + Outlet::where('kd_user','=',$id)->pluck('nm_outlet', 'kd_outlet')->toArray();
        }
        else
        {
             $outlet = [''=>''] + Outlet::where('kd_area','=',$area)->pluck('nm_outlet', 'kd_outlet')->toArray();
        }
        return view('pages.visitbukti.edit')->with('data',$data)->with('outlet',$outlet);
    }

    public function update(VisitPlanRequest $request, $id)
    {
        $data = VisitPlan::find($id);
        $data->update($request->all());
        $log = new AdminController;
        $log->getLogHistory('Update Visit Plan with ID '.$id);
        return redirect()->route('admin.visitPlan.form');
    }

     

     

     public function destroy($id)
    {
        $data = VisitBukti::find($id);
         
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Visit Bukti with ID '.$id);
        return redirect()->route('admin.visitbukti.form');
    }

    
}
