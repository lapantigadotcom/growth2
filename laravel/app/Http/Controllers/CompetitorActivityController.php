<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Auth;
use Excel;
use Session;
use Input;
use Validator;
use App\TipeActivity;
use App\Area;
use App\Outlet;
use App\Competitor;
use App\Distributor;
use App\Tipe;
use App\CompetitorActivity;
use App\User;
use File;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\PhotoRequest;
use App\Http\Requests\CompetitorActivityRequest;
use App\Http\Controllers\AdminController;


class CompetitorActivityController extends Controller
{

    public function form()
    {

        $area = Auth::user()->kd_area;
        if($area==100)
        {
            $data['areas'] = [''=>'All Area'] + Area::pluck('kd_area', 'id')->toArray();
            $data['sales'] =  [''=>'All Sales'];
        }
        else
        {
            $data['areas'] =  Area::where('id','=',$area)->pluck('kd_area', 'id')->toArray();
            $data['sales'] =  [''=>'All Sales']+User::where('kd_area','=',$area)->where('kd_role', '=','3')->pluck('nama', 'id')->toArray();
        }

        $data['tipe'] =  [''=>'All Tipe'] + Tipe::pluck( 'nm_tipe','id')->toArray();
        $data['distributor'] =  [''=>'All Distributor'] + Distributor::pluck('kd_dist','id' )->toArray();

        if(Auth::user()->kd_area == 100){
            $data['competitorAct'] = CompetitorActivity::select('comp_activity.*',
                'competitor.nm_competitor','outlet.nm_outlet',

                'user.nama'
                )
            ->leftJoin('outlet','outlet.kd_outlet','=','comp_activity.kd_outlet')
            ->leftJoin('user','user.id','=','outlet.kd_user')
            ->leftJoin('competitor','competitor.id','=','comp_activity.kd_comp')

            ->whereBetween('comp_activity.date_upload_photo', [date('Y-m-d', strtotime("-7 day")), date('Y-m-d', strtotime("+1 day"))])
            ->groupBy('comp_activity.kd_comp_act')
            ->orderBy('comp_activity.date_upload_photo', 'desc')
            ->get();
        }
        else if(Auth::user()->kd_role == 3){
            $id =Auth::user()->id;
            $data['competitorAct'] = CompetitorActivity::select('comp_activity.*',
                'competitor.nm_competitor','outlet.nm_outlet',

                'user.nama'
                )
            ->leftJoin('outlet','outlet.kd_outlet','=','comp_activity.kd_outlet')
            ->leftJoin('user','user.id','=','outlet.kd_user')
            ->leftJoin('competitor','competitor.id','=','comp_activity.kd_comp')

            ->where('outlet.kd_user','=',$id)
            ->whereBetween('comp_activity.date_upload_photo', [date('Y-m-d', strtotime("-7 day")), date('Y-m-d', strtotime("+1 day"))])
            ->groupBy('comp_activity.kd_comp_act')
            ->orderBy('comp_activity.date_upload_photo', 'desc')
            ->get();


        }
        else {
            $area =Auth::user()->kd_area;
            $data['competitorAct'] = CompetitorActivity::select('comp_activity.*',
                'competitor.nm_competitor','outlet.nm_outlet',

                'user.nama'
                )
            ->leftJoin('outlet','outlet.kd_outlet','=','comp_activity.kd_outlet')
            ->leftJoin('user','user.id','=','outlet.kd_user')
            ->leftJoin('competitor','competitor.id','=','comp_activity.kd_comp')

            ->where('outlet.kd_area','=',$area)
            ->whereBetween('comp_activity.date_upload_photo', [date('Y-m-d', strtotime("-7 day")), date('Y-m-d', strtotime("+1 day"))])
            ->groupBy('comp_activity.kd_comp_act')
            ->orderBy('comp_activity.date_upload_photo', 'desc')
            ->get();

        }
//$data['competitor'] = Competitor::all();
        return view('pages.competitor.viewForm', compact('data'));
    }

    public function index(CompetitorActivityRequest $request)
    {
        $data['dateFrom'] = $request->input('dateFrom');
        $data['dateTo'] = $request->input('dateTo');
        $sales = $request->input('sales');
        $area = $request->input('kd_area');
        $tipe = $request->input('nm_tipe');
        $dist = $request->input('kd_dist');
        $osales=$oarea=$otipe=$odist='=';

        if($data['dateFrom']=='') $data['dateFrom']='1970-01-01';
        if($data['dateTo']=='') $data['dateTo']=date("Y-m-d");
        if($sales==''){
            $sales='null';
            $osales='!=';
        }
        if($area==''||$area=='100'){
            $area='null';
            $oarea='!=';
        }
        if($tipe==''){
            $tipe='null';
            $otipe='!=';
        }
        if($dist==''){
            $dist='null';
            $odist='!=';
        }


        $dateFrom = date("Y-m-d", strtotime($data['dateFrom']));
        $dateTo =  date("Y-m-d", strtotime('+1 day', strtotime($data['dateTo'])));
        if(Input::get('show')) {
            if(Auth::user()->kd_area == 100){
                $data['competitorAct'] = CompetitorActivity::select('comp_activity.*',
                    'competitor.nm_competitor','outlet.nm_outlet',

                    'user.nama'
                    )
                ->leftJoin('outlet','outlet.kd_outlet','=','comp_activity.kd_outlet')
                ->leftJoin('user','user.id','=','outlet.kd_user')

                ->leftJoin('competitor','competitor.id','=','comp_activity.kd_comp')

                ->where('user.id', $osales, $sales)
                ->where('outlet.kd_area', $oarea, $area)
                ->where('outlet.kd_tipe', $otipe, $tipe)
                ->where('outlet.kd_dist', $odist, $dist)
                ->whereBetween('comp_activity.date_upload_photo', [$dateFrom, $dateTo])
                ->groupBy('comp_activity.kd_comp_act')
                ->orderBy('comp_activity.date_upload_photo', 'desc')
                ->get();
            }
            else if(Auth::user()->kd_role == 3){
                $id =Auth::user()->id;
                $data['competitorAct'] = CompetitorActivity::select('comp_activity.*',
                    'competitor.nm_competitor','outlet.nm_outlet',

                    'user.nama'
                    )
                ->leftJoin('outlet','outlet.kd_outlet','=','comp_activity.kd_outlet')
                ->leftJoin('user','user.id','=','outlet.kd_user')
                ->leftJoin('competitor','competitor.id','=','comp_activity.kd_comp')

                ->where('outlet.kd_user','=',$id)

                ->where('outlet.kd_area', $oarea, $area)
                ->where('outlet.kd_tipe', $otipe, $tipe)
                ->where('outlet.kd_dist', $odist, $dist)
                ->whereBetween('comp_activity.date_upload_photo', [$dateFrom, $dateTo])
                ->groupBy('comp_activity.kd_comp_act')
                ->orderBy('comp_activity.date_upload_photo', 'desc')
                ->get();
            }
            else {
                $area =Auth::user()->kd_area;
                $data['competitorAct'] = CompetitorActivity::select('comp_activity.*',
                    'competitor.nm_competitor','outlet.nm_outlet',

                    'user.nama'
                    )
                ->leftJoin('outlet','outlet.kd_outlet','=','comp_activity.kd_outlet')
                ->leftJoin('user','user.id','=','outlet.kd_user')
                ->leftJoin('competitor','competitor.id','=','comp_activity.kd_comp')

                ->where('outlet.kd_area','=',$area)
                ->where('user.id', $osales, $sales)

                ->where('outlet.kd_tipe', $otipe, $tipe)
                ->where('outlet.kd_dist', $odist, $dist)
                ->whereBetween('comp_activity.date_upload_photo', [$dateFrom, $dateTo])
                ->groupBy('comp_activity.kd_comp_act')
                ->orderBy('comp_activity.date_upload_photo', 'desc')
                ->get();

            }
//$data['competitor'] = Competitor::all();
            return view('pages.competitor.index')->with('data', $data);
        } else if(Input::get('download')) {
            $this->downloadCompAct($sales, $osales, $tipe, $otipe, $area, $oarea, $dist, $odist, $dateFrom, $dateTo);
        }
    }

    public function create()
    {
        $outlets = [''=>''] + Outlet::pluck('nm_outlet', 'kd_outlet')->toArray();
        $competitors = [''=>''] + Competitor::pluck('nm_competitor', 'id')->toArray();
        $users = [''=>''] + User::where('kd_role','=','3')->pluck('nama', 'id')->toArray();
        return view('pages.competitor.create')->with('outlets',$outlets)->with('competitors',$competitors)->with('users',$users);
//return view('pages.competitor.create')->with('competitors',$competitors)->with('users',$users);
    }

    public function store(PhotoRequest $request)
    {
        $rules = array(
            'nm_photo'         => 'required',
            'path_photo'       => 'required',
            );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {

            $messages = $validator->messages();

            $outlets = [''=>''] + Outlet::pluck('nm_dist', 'id')->toArray();
            $competitors = [''=>''] + Competitor::pluck('kd_area', 'id')->toArray();
            $users = [''=>''] + User::where('kd_role','=','3')->pluck('nama', 'id')->toArray();

            return view('pages.competitor.create')
            ->with('outlets',$outlets)
            ->with('competitors',$competitors)
            ->with('users',$users)
            ->withErrors($validator);

        } else {
            $photoAct = new CompetitorActivity;

            $file = $request->file('path_photo');
            $destinationPath = 'image_upload/competitor';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $outlet = Outlet::where('kd_outlet','=',$request->input('kd_outlet'))->first();
            $photoAct->kd_outlet = 0;
            $photoAct->kd_competitor = $request->input('kd_competitor');
            $photoAct->kd_user = $outlet->kd_user;
            $photoAct->nm_photo = $request->input('nm_photo');

            $photoAct->date_take_photo = $request->input('date_take_photo');
            $photoAct->date_upload_photo = $request->input('date_upload_photo');

            $photoAct->note = $request->input('note');
            $photoAct->path_photo = $filename;
            $photoAct->save();
            $log = new AdminController;
            $log->getLogHistory('Make Competitor Activity');
            return redirect()->route('admin.competitor.index');
        }
    }

    public function edit($id)
    {
        $data['content'] = CompetitorActivity::find($id);

        $competitors = [''=>''] + Competitor::pluck('nm_competitor', 'id')->toArray();

        return view('pages.competitor.edit')->with('data',$data)->with('competitors',$competitors)->with('tp_act', $tp_act);
    }

    public function update(CompetitorActivityRequest $request, $id)
    {

        //echo 'keren';
        $photoAct = CompetitorActivity::find($id);
        $photoAct->update($request->all());
        if(Input::hasFile('path_photo')){
            File::delete('image_upload/competitor/'.$photoAct->path_photo);
            $file = $request->file('path_photo');
            $destinationPath = 'image_upload/competitor';
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $file->move($destinationPath, $filename);

            $photoAct->path_photo = $filename;
            $photoAct->save();

        }

        $log = new AdminController;
        $log->getLogHistory('Update Competitor Activity with ID '.$id);
        return redirect()->route('admin.competitor.form');

    }

    public function destroy($id)
    {
        $data = CompetitorActivity::find($id);
        $data->delete();
        $log = new AdminController;
        $log->getLogHistory('Delete Competitor Activity with ID '.$id);
        return redirect()->route('admin.competitor.index');
    }

    public function downloadCompAct($sales, $osales, $tipe, $otipe, $area, $oarea, $dist, $odist, $dateFrom, $dateTo)
    {
        $log = new AdminController;
        $log->getLogHistory('Download Data Competior Activity');

        $role = Auth::user()->kd_role;
        $areas = Auth::user()->kd_area;
        if($areas == 100)
        {
            ob_end_clean();
            ob_start();
            $comp = CompetitorActivity::select(DB::raw("
                user.nama as SALES,
                comp_activity.kd_outlet as KD_OUTLET,
                outlet.nm_outlet as OUTLET,
                tipe.nm_tipe as TIPE_OUTLET,
                competitor.nm_competitor as COMPETITOR,

                comp_activity.note as NOTE,
                comp_activity.date_take_photo as DATE_TAKE,
                comp_activity.date_upload_photo as DATE_UPLOAD,
                comp_activity.path_photo as PATH_PHOTO
                ")
            )
            ->leftJoin('outlet','outlet.kd_outlet','=','comp_activity.kd_outlet')
            ->leftJoin('user','user.id','=','outlet.kd_user')
            ->leftJoin('competitor','competitor.id','=','comp_activity.kd_comp')
            ->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')
            ->where('user.id', $osales, $sales)
            ->where('outlet.kd_area', $oarea, $area)
            ->where('outlet.kd_tipe', $otipe, $tipe)
            ->where('outlet.kd_dist', $odist, $dist)
            ->whereBetween('comp_activity.date_upload_photo', [$dateFrom, $dateTo])
            ->groupBy('comp_activity.kd_comp_act')
            ->orderBy('comp_activity.date_upload_photo', 'desc')
            ->get();

        }
        else if(Auth::user()->kd_role == 3){

            $id =Auth::user()->id;
            $comp = PhotoActivity::select(DB::raw("
                user.nama as SALES,
                comp_activity.kd_outlet as KD_OUTLET,
                outlet.nm_outlet as OUTLET,
                tipe.nm_tipe as TIPE_OUTLET,
                competitor.nm_competitor as COMPETITOR,

                comp_activity.note as NOTE,
                comp_activity.date_take_photo as DATE_TAKE,
                comp_activity.date_upload_photo as DATE_UPLOAD,
                comp_activity.path_photo as PATH_PHOTO
                ")
            )
            ->leftJoin('outlet','outlet.kd_outlet','=','comp_activity.kd_outlet')
            ->leftJoin('user','user.id','=','outlet.kd_user')
            ->leftJoin('competitor','competitor.id','=','comp_activity.kd_comp')
            ->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')

            ->where('outlet.kd_user','=',$id)

            ->where('outlet.kd_area', $oarea, $area)
            ->where('outlet.kd_tipe', $otipe, $tipe)
            ->where('outlet.kd_dist', $odist, $dist)
            ->whereBetween('comp_activity.date_upload_photo', [$dateFrom, $dateTo])
            ->groupBy('comp_activity.kd_comp_act')
            ->get();


        }
        else
        {

            $comp = PhotoActivity::select(DB::raw("
                user.nama as SALES,
                comp_activity.kd_outlet as KD_OUTLET,
                outlet.nm_outlet as OUTLET,
                tipe.nm_tipe as TIPE_OUTLET,
                competitor.nm_competitor as COMPETITOR,

                comp_activity.note as NOTE,
                comp_activity.date_take_photo as DATE_TAKE,
                comp_activity.date_upload_photo as DATE_UPLOAD,
                comp_activity.path_photo as PATH_PHOTO
                ")
            )
            ->leftJoin('outlet','outlet.kd_outlet','=','comp_activity.kd_outlet')
            ->leftJoin('user','user.id','=','outlet.kd_user')
            ->leftJoin('competitor','competitor.id','=','comp_activity.kd_comp')
            ->leftJoin('tipe', 'outlet.kd_tipe', '=', 'tipe.id')

            ->where('outlet.kd_area','=',$area)
            ->where('user.id', $osales, $sales)

            ->where('outlet.kd_tipe', $otipe, $tipe)
            ->where('outlet.kd_dist', $odist, $dist)
            ->whereBetween('comp_activity.date_upload_photo', [$dateFrom, $dateTo])
            ->groupBy('comp_activity.kd_comp_act')
            ->orderBy('comp_activity.date_upload_photo', 'desc')
            ->get();


        }
        ob_end_clean();
        ob_start();
        date_default_timezone_set("Asia/Jakarta");
        $d= strtotime("now");
        $time =date("d-m-Y H:i:s", $d);
        $filename = $time."_COMP_ACT";
        Excel::create($filename, function($excel) use($comp) {
            $excel->sheet('Competitor Activity', function($sheet) use($comp) {
                $sheet->fromArray($comp);
            });
        })->download('xls');
    }

}
