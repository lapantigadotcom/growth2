<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Kota;

class Area extends Model
{
    //
    protected $table = 'area';
    protected $guarded = ['id'];
    protected $fillable = ['kd_area','nm_area','status_aproval'];
    public $timestamps = false;

    public function kota()
    {
    	return $this->hasMany('App\Kota', 'kd_area', 'id');
    }
}
