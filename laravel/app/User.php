<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use App\Role;
use App\Outlet;
use App\VisitPlan;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract 
{
    //
    use Authenticatable, CanResetPassword, Notifiable;
    protected $table = 'user';
    protected $guarded = ['id'];
    protected $fillable = [
                    'kd_role',
                    'kd_kota',
                    'kd_area',
                    'nik',
                    'nama',
                    'alamat',
                    'telepon',
                    'foto',
                    'username',
                    'password',
                    'email_user',
     				'join_date',
                    'status_area',
                    'id_gcm'
                    ];

    public $timestamps = false;
    protected $effectiveMenu = null;
    protected $effectivePermission = null;
    protected $groupRole = null;

    public function setPasswordAttribute($password)
    {   
        $this->attributes['password'] = bcrypt($password);
    }

    public function roles()
    {
    	return $this->belongsTo('App\Role', 'kd_role', 'kd_role');
    }

    public function outlets()
    {
    	return $this->hasMany('App\Outlet', 'kd_user', 'id');
    }
    public function logs()
    {
        return $this->hasMany('App\Logging', 'kd_user', 'id');
    }

    public function listRole()
    {
        return $this->roles();
    }

    public function hasAccess($route)
    {
        $permissions = $this->getEffectivePermission();
        foreach ($permissions as $permission) 
        {
            if ($permission->nm_permission == $route)
            {
                return true;
            }

        }
        return false;
    }

    public function getEffectiveMenu()
    {
        if ($this->effectiveMenu != null)
        {
            return $this->effectiveMenu;
        }
        else
        {
            $role = Role::with(array('adminmenu' => function($query){
                $query->orderBy('parent_menu')->orderBy('urutan');
            }))->find($this->getRoleId());
            if ($role != null)
            {
                $effectiveMenu = array();
                foreach ($role->adminmenu as $adminmenu) 
                {
                    if ($adminmenu->parent_menu == 0 && $adminmenu->enable==1)
                    {
                        array_push($effectiveMenu, $adminmenu);
                        $adminmenu->_child = array();
                        foreach ($role->adminmenu as $child) 
                        {
                            // echo "<pre>";
                            // print_r($child->parent_menu . " - " . $adminmenu->id );
                            // echo "</pre>";
                            if ($child->parent_menu == $adminmenu->id && $child->enable==1)
                            {
                                array_push($adminmenu->_child, $child);

                                $child->_child = array();
                                foreach ($role->adminmenu as $grandChild) 
                                {
                                    if ($grandChild->parent_menu == $child->id && $grandChild->enable==1)
                                    {
                                        array_push($child->_child, $grandChild);
                                    }
                                }
                            }
                        }
                    }
                }
                $this->effectiveMenu = $effectiveMenu;
            }
            else
            {
                $this->effectiveMenu = array();
            }
            return $this->effectiveMenu;
        }
    }

    public function getEffectivePermission()
    {
        if ($this->effectivePermission != null)
        {
            return $this->effectivePermission;
        }
        else
        {
            $role = Role::with('permission')->find($this->getRoleId());
            
            if ($role != null)
            {
                $this->effectivePermission = $role->permission;
            }
            else
            {
                $this->effectivePermission = array();
            }
            return $this->effectivePermission;
        }
    }

    public function setRoleId($id)
    {
        $role = Role::find($id);
        if ($role != null)
        {
            $this->kd_role = $id;
            $this->save();
        }
    }

    public function getRoleId()
    {
        if ($this->kd_role == 0)
        {
            if ($this->roles->count() > 0)
            {
                $id = $this->roles->sortByDesc('kd_role')->first()->id;
                $this->setRoleId($id);
                return $this->kd_role;
            }
            else
            {
                return 0;
            }            
        }
        else
        {
            return $this->kd_role;
        }
    }
}