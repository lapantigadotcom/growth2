<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\VisitPlan;
use App\Produk;

class TakeOrder extends Model
{
    protected $table = 'take_order';
    protected $guarded = ['id'];
    protected $fillable = ['kd_visitplan', 'kd_produk', 'qty_order', 'satuan','date_order','status_order', 'sku_baru'];

    public $timestamps = false;

    public function products()
    {
        return $this->belongsTo('App\Produk', 'kd_produk', 'id');
    }

    public function visitPlans()
    {
        return $this->belongsTo('App\VisitPlan', 'kd_visitplan', 'id');
    }
    public function outlets()
    {
        return $this->belongsTo('App\Outlet', 'kd_outlet', 'kd_outlet');
    }
    
     public function satuans()
    {
        return $this->belongsTo('App\Satuan', 'id_satuan', 'id');
    }

     public function users()
    {
        return $this->belongsTo('App\User', 'kd_user', 'id');
    }
}
