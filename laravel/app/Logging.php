<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Logging extends Model
{
    //
    protected $table = 'logging';
    protected $guarded = ['id'];
    protected $fillable = ['kd_user','description','log_time','detail_akses'];
    public $timestamps = false;
}
