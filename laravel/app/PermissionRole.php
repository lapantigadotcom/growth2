<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Role;
use App\Permission;

class PermissionRole extends Model 
{
    //
    protected $table = 'permission_role';
    protected $guarded = ['id'];
    public $timestamps = false;

    public function roles()
    {
        return $this->belongsTo('App\Role', 'kd_role', 'id');
    }

    public function permissions()
    {
    	return $this->belongsTo('App\Permission', 'kd_permission', 'id');
    }
}
