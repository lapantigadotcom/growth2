  <div class="modal modal-default" id="reset-modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Anda Yakin?</h4>
        </div>
        <div class="modal-body">
          <p>
          Password baru akan dikirimkan melalui email : <strong> <span id="email-reset"></span></strong>
          </p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" id="reset-modal-confirm">Reset Password</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
<script type="text/javascript">
  function resetPasswordModal (x) {
    var email = x.getAttribute('data-email');
    $('#email-reset').text(email);
    var href = x.getAttribute('data-href');
    console.log(href);
    $('#reset-modal').modal({ backdrop: 'static', keyboard: false })
    .one('click', '#reset-modal-confirm', function (e) {
      window.location.href = href;
    });
    
  }
</script>