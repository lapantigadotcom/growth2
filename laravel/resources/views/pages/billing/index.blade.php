 @extends('layouts.master')

@section('title', 'Billing List')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Billing Filter Result</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Billing</span></a></li>
                            <li class="active"><span>Filter Result</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-money"></i>&nbsp;&nbsp;Filter Billing Data</h6>
                                </div>

                                @include('partials.panel')

                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap"> 
                                     
                                        <div class="table-responsive">
                                            <table id="billingtable" class="table datable_1 table-hover display table-bordered mb-30">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align:center;">#</th>
                                                        <th style="text-align:center;">Sales Name</th>
                                                        <th style="text-align:center;">Outlet</th> 
                                                        <th style="text-align:center;">Status</th>
                                                        <th style="text-align:center;">Bank Name</th>
                                                        <th style="text-align:center;">No. Giro</th>
                                                        <th style="text-align:center;">Date Submit</th>
                                                        <th style="text-align:center;">Due Submit</th>
                                                        @if(Auth::user()->kd_role != 3)   
                                                            <th style="text-align:center;">Actions</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data as $row)
                                                        <tr>
                                                             <td>{{ $row->id}}</td>
                                                             <td>{{ $row->users->nama}}</td>
                                                             <td><span style="color: green">
                                                                <i class="fa fa-circle"></i> {{ $row->kd_outlet }}</span> 
                                                                <br>{{ $row->outlets->nm_outlet}}
                                                            </td>
                                                            <td>@if($row->status == 0)
                                                        Non Visit 
                                                        @else($row->status == 1)
                                                        Visit
                                                        @endif
                                                    </td>
                                                                  
                                                             
                                                            <td><center>
                                                                @if ($row->bank_name != '')
                                                                {{ $row->bank_name }}
                                                                @else (!$row->bank_name->isEmpty())
                                                                <i class="fa fa-circle-o text-merah"></i>
                                                                @endif</center>
                                                            </td> 
                                                            <td><center>
                                                                @if ($row->giro_no != '')
                                                                {{ $row->giro_no }}
                                                                @else (!$row->giro_no->isEmpty())
                                                                <i class="fa fa-circle-o text-merah"></i>
                                                                @endif</center>
                                                            </td> 
                                                            <td>{{ date('d-F-Y H:i:s', strtotime($row->submit_at))  }}</td>
                                                            <td>{{ date('d-F-Y H:i:s', strtotime($row->due_date))  }}</td>
                                                            @if(Auth::user()->kd_role != 3)
                                                                <td style="min-width: 50px">
                                                                    <center>
                                                                        <div class="button-list">
                                                                           
                                                                            @if(Auth::user()->hasAccess('admin.billing.delete'))      
                                                                               <a href="{!! route('admin.billing.delete',[$row->id]) !!}" onclick="return confirm('Are you sure?')"  class="btn btn-xs btn-danger text-red"><i class="fa fa-trash"></i></a>
                                                                            @endif
                                                                        </div>
                                                                    </center>    
                                                                </td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')
                 @section('custom-js')
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('#billingtable').DataTable( {
                            "paging":   true,
                            "ordering": true,
                            "info":     true,
                            "autoWidth": true,
                            "search" :true
                        } );
                    } );
                </script>
                @endsection
            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush

@endsection