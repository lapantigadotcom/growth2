<div class="box-body">
	<div class="row">
			<div class="col-sm-12 col-md-6 col-lg-6">
				<div class="box-header with-border">
		          <h3 class="box-title">Date Range</h3>
		        </div>
		        <div class="box-body">
					<div class="form-group">
						{!! Form::text('dateFrom', null, array('id' => 'datepicker1' , 'class' => 'form-control' ,'required' => 'required', 'placeholder' => 'Tanggal Awal')) !!}
					</div>

					<div class="form-group">
						{!! Form::text('dateTo', null, array('id' => 'datepicker2' , 'class' => 'form-control' ,'required' => 'required', 'placeholder' => 'Tanggal Akhir')) !!}
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-6 col-lg-6">
				<div class="box-header with-border">
		    		<h3 class="box-title">Select Data</h3>
		    	</div>
				<div class="box-body">
					<div class="form-group row">
			    		<div class="col-sm-6 col-md-3 col-lg-3">
							{!! Form::select('kd_area',$data['areas'], null, array('id'=>'area','class' => 'selects form-control')) !!}
						</div>
						<div class="col-sm-6 col-md-3 col-lg-3">
							{!! Form::select('sales',$data['sales'], null, array('id'=>'sales','class' => 'selects form-control')) !!}
						</div>
						<div class="col-sm-6 col-md-3 col-lg-3">
							{!! Form::select('nm_tipe',$data['tipe'], null, array('id'=>'tipe','class' => 'selects form-control')) !!}
						</div>
						<div class="col-sm-6 col-md-3 col-lg-3">
							{!! Form::select('kd_dist',$data['distributor'], null, array('id'=>'distributor','class' => 'selects form-control')) !!}
						</div>
					</div>
					<div class="form-group">
							{!! Form::submit('Show', array('name'=>'show','class' => 'btn btn-primary')) !!}
							@if(Auth::user()->hasAccess('admin.download.visitMonitor'))
							{!! Form::submit('Download', array('name'=>'download','class' => 'btn btn-primary')) !!}
							@endif
					</div>
				</div>
		</div>
	</div>
</div>


<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
	$(function() {
		$('#datepicker1').datepicker({ dateFormat: 'dd-M-yy'}).val();
		$('#datepicker2').datepicker({ dateFormat: 'dd-M-yy'}).val();
	});

	$(window).load(function() {
		$('form').get(0).reset(); //clear form data on page load
	});
</script>
<script>




	$('#area').on('change', function(e) {
		console.log(e);
		var id = e.target.value;
				//ajax
				$.getJSON('{{url('/ajax-sales?id=')}}'+id, function (data) {
		 //console.log(data);
		 $('#sales').empty();
		 $('#sales').append('<option value="">All Sales</option>');
		 $.each(data, function(index, salesObj){
		 	$('#sales').append('<option value="'+salesObj.id+'">'+salesObj.nama+'</option>');
		 });
		});
			});
		</script>
