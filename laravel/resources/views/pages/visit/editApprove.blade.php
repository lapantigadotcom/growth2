@extends('layouts.master')

<!-- Bootstrap table CSS -->
@section('link','')

@section('title', 'Visit Plan List')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Visit Plan management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Visit Plan</span></a></li>
                            <li><a href="#"><span>Visit Plan List</span></a></li>
                            <li class="active"><span>Approval Visit Plan</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;Approval Visit Plan</h6>
                                </div>
                                
                                @include('partials.panel')

                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="form-wrap">

                                        {!! Form::model($data,array('route' =>'admin.visit.approved', 'method' => 'POST')) !!}
                                            @include('pages.visit.formApprove',array('submit' => 'Perbarui'))
                                        {!! Form::close() !!}

                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
                    
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

@endsection