<div class="form-group">
    <div class="col-sm-12">
        <div class="row">
            
            <div class="col-sm-4">
                <label class="control-label mb-10">From</label>
                <div class='input-group date datetimepicker1'>
                    <input type='text' name="dateFrom" class="form-control" required placeholder="select date..."/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
               </div>    
            </div>
            <div class="col-sm-4">
                <label class="control-label mb-10">To</label>
                <div class='input-group date datetimepicker1'>
                    <input type='text' name="dateTo" class="form-control" required placeholder="select date..."/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>  
            </div>
            <div class="col-sm-4">
                <div class="form-group ">
                    <label class="control-label mb-10">&nbsp;</label>
                     <center>
                        {!! Form::submit('Show', array('name'=>'show','class' => 'btn btn-success')) !!}
                        @if(Auth::user()->hasAccess('admin.download.visitPlan'))        
                        {!! Form::submit('Download', array('name'=>'download','class' => 'btn btn-primary')) !!}
                        @endif
                    </center>
                </div>
            </div>
        </div>
    </div>  
</div>



