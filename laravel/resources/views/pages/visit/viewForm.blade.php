@extends('layouts.master')

@section('link', '<link rel="stylesheet" href="/doodle/vendors/bower_components/summernote/dist/summernote.css" />
    <link href="/doodle/vendors/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" type="text/css"/>
    <link href="/doodle/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css"/>
  <link href="/doodle/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>')

@section('title', 'Visit Plan List')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Visit Plan management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Visit Plan</span></a></li>
                            <li class="active"><span>Visit Plan Filter</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-warning card-view panel-refresh">
                                <div class="refresh-container">
                                    <div class="la-anim-1"></div>
                                </div>
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-light"><i class="fa fa-filter"></i>&nbsp;&nbsp;Filter Data Date Visit Plan</h6>
                                    </div>
                                    @include('partials.panel')
                                    <div class="clearfix"></div>
                                </div>
                                <div id="collapse_1" class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-wrap">

                                                    {!! Form::open(array('route' => 'admin.visitPlan.result', 'method' => 'POST', 'data-toggle' => 'validator', 'role' => 'form', 'class' => 'form-horizontal' )) !!}
                                                        @include('pages.visit.filterVisit')
                                                    {!! Form::close() !!}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- /Row -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view panel-refresh">
                            <div class="refresh-container">
                                <div class="la-anim-1"></div>
                            </div>
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-paper-plane"></i>&nbsp;&nbsp;Visit Plan List 1 Minggu Terakhir</h6>
                                </div>

                                @include('partials.panel2')

                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_2" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">

                                        @if(Auth::user()->hasAccess('admin.distributor.create')) 
                                            <a href="{!! route('admin.visit.editApprove') !!}" class="btn btn-primary"><i class="fa fa-check-square-o">&nbsp;&nbsp;Approval</i></a>
                                        @endif

                                        <div class="table-responsive">
                                            <table class="table datable_1 table-hover table-bordered display mb-30" >
                                                <thead>
                                                    <tr>
                                                        <th><center>Sales Name</center></th>
                                                        <th><center>Nama Outlet</center></th>
                                                        <th><center>Alamat Outlet</center></th>
                                                        <th><center>Kota Outlet</center></th>
                                                        <th><center>Date Create Visit Plan</center></th>
                                                        <th><center>Date Visit Plan</center></th>
                                                        <th><center>Status Approval</center></th>
                                                        @if(Auth::user()->kd_role != 3)   
                                                            <th><center>Action</center></th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data as $row)
                                                        <tr>
                                                            <td>{{ $row->nama}}</td>
                                                            <td>{{ $row->nm_outlet }}</td>
                                                            <td>{{ $row->almt_outlet }}</td>
                                                            <td>{{ $row->nm_kota}}</td>
                                                            <td>{{ date('d-F-Y H:i:s', strtotime($row->date_create_visit))  }}</td>
                                                            <td>{{ date('d-F-Y H:i:s', strtotime($row->date_visit)) }}</td>
                                                            @if($row->approve_visit == 1)
                                                                <td class="center">
                                                                    <span class="label label-success">Approved</span>
                                                                </td>
                                                            @elseif($row->approve_visit == 0)
                                                                <td class="center">
                                                                    <span class="label label-important">Denied</span>
                                                                </td>
                                                            @elseif($row->approve_visit == 2)  
                                                                <td class="center">
                                                                    <span class="label label-warning">Pending</span>
                                                                </td>
                                                            @else
                                                            <td class="center">
                                                                <span class="label label-warning">Pending</span>
                                                            </td>
                                                            @endif                             
                                                            
                                                            @if(Auth::user()->kd_role != 3)  
                                                                <td>
                                                                    <center>
                                                                        <div class="button-list">
                                                                            @if(Auth::user()->hasAccess('admin.visit.edit'))
                                                                                <a href="{!! route('admin.visit.edit',[$row->id]) !!}" data-toggle="tooltip" title="Edit" class="btn btn-success btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-pencil" style="font-size: 14px;"></i></a>
                                                                            @endif
                                                                            @if(Auth::user()->hasAccess('admin.visit.delete'))      
                                                                                <a href="{!! route('admin.visit.delete',[$row->id]) !!}" id="delete" data-toggle="tooltip" title="Delete" class="btn btn-danger btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-trash" style="font-size: 14px;"></i></a>
                                                                            @endif
                                                                        </div>
                                                                    </center>    
                                                                </td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush

        <!-- Moment JavaScript -->
        <script type="text/javascript" src="/doodle/vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
        <!-- Bootstrap Colorpicker JavaScript -->
        <script src="/doodle/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
        <!-- Bootstrap Datetimepicker JavaScript -->
        <script type="text/javascript" src="/doodle/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

        <script type="text/javascript">
            $(function () {
                $('.datetimepicker1').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
            });
        </script>


@endsection