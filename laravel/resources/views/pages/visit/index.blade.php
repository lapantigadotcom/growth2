 @extends('layouts.master')

@section('title', 'Visit Plan List')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Visit Plan management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Visit Plan</span></a></li>
                            <li class="active"><span>Result</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-paper-plane"></i>&nbsp;&nbsp;Visit Plan List</h6>
                                </div>

                                @include('partials.panel')

                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                        
                                            <a href="{!! route('admin.visit.editApprove') !!}" class="btn btn-primary"><i class="fa fa-check-square-o">&nbsp;&nbsp;Approval</i></a>
                                     
                                        <div class="table-responsive">
                                            <table class="table datable_1 table-hover display table-bordered mb-30">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align:center;">Sales Name</th>
                                                        <th style="text-align:center;">Nama Outlet</th>
                                                        <th style="text-align:center;">Alamat Outlet</th>
                                                        <th style="text-align:center;">Kota Outlet</th>
                                                        <th style="text-align:center;">Date Create Visit Plan</th>
                                                        <th style="text-align:center;">Date Visit Plan</th>
                                                        <th style="text-align:center;">Status Approval </th>
                                                        @if(Auth::user()->kd_role != 3)   
                                                            <th style="text-align:center;">Actions</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data as $row)
                                                        <tr>
                                                            <td>{{ $row->nama}}</td>
                                                            <td>{{ $row->nm_outlet }}</td>
                                                            <td>{{ $row->almt_outlet }}</td>
                                                            <td>{{ $row->nm_kota}}</td>
                                                            <td>{{ date('d-F-Y H:i:s', strtotime($row->date_create_visit))  }}</td>
                                                            <td>{{ date('d-F-Y H:i:s', strtotime($row->date_visit)) }}</td>
                                                            @if($row->approve_visit == 1)
                                                                <td class="center">
                                                                    <span class="label label-success">Approved</span>
                                                                </td>
                                                            @elseif($row->approve_visit == 0)
                                                                <td class="center">
                                                                    <span class="label label-important">Denied</span>
                                                                </td>
                                                            @elseif($row->approve_visit == 2)  
                                                                <td class="center">
                                                                    <span class="label label-warning">Pending</span>
                                                                </td>
                                                            @else
                                                            <td class="center">
                                                                <span class="label label-warning">Pending</span>
                                                            </td>
                                                            @endif                             
                                                            @if(Auth::user()->kd_role != 3)
                                                                <td style="min-width: 50px">
                                                                    <center>
                                                                        <div class="button-list">
                                                                            @if(Auth::user()->hasAccess('admin.visit.edit'))
                                                                                <a href="{!! route('admin.visit.edit',[$row->id]) !!}" data-toggle="tooltip" title="Edit" class="btn btn-success btn-sm btn-icon-anim btn-square mr-0" style="display: unset;"><i class="fa fa-pencil" style="font-size: 14px;"></i></a>
                                                                            @endif
                                                                            @if(Auth::user()->hasAccess('admin.visit.delete'))      
                                                                                <a href="{!! route('admin.visit.delete',[$row->id]) !!}" id="delete" data-toggle="tooltip" onclick="return confirm('Are you sure you want to delete this item?');" title="Delete" class="btn btn-danger btn-sm btn-icon-anim btn-square mr-0" style="display: unset;"><i class="fa fa-trash" style="font-size: 14px;"></i></a>
                                                                            @endif
                                                                        </div>
                                                                    </center>    
                                                                </td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush

@endsection