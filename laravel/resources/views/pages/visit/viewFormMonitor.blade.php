@extends('layouts.master')

 
@section('title', 'Monitoring Kunjungan Sales')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Monitoring Kunjungan Sales</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Visit Plan</span></a></li>
                            <li class="active"><span>Monitoring Kunjungan Filter</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-warning card-view panel-refresh">
                                <div class="refresh-container">
                                    <div class="la-anim-1"></div>
                                </div>
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-light"><i class="fa fa-filter"></i>&nbsp;&nbsp;Filter Kunjungan Sales</h6>
                                    </div>
                                    @include('partials.panel')
                                    <div class="clearfix"></div>
                                </div>
                                <div id="collapse_1" class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-wrap">

                                                    {!! Form::open(array('route' => 'admin.visitingMonitor.result', 'method' => 'POST', 'data-toggle' => 'validator', 'role' => 'form', 'class' => 'form-horizontal')) !!}
                                                        @include('pages.visit.filterVisit')
                                                    {!! Form::close() !!}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- /Row -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view panel-refresh">
                            <div class="refresh-container">
                                <div class="la-anim-1"></div>
                            </div>
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-paper-plane"></i>&nbsp;&nbsp;List Kunjungan 1 Minggu Terakhir</h6>
                                </div>

                                @include('partials.panel2')

                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_2" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table id="tablevisitmon" class="table datable_1 table-hover table-bordered display mb-30" >
                                                <thead>
                                                    <tr>
                                                        <th style="text-align:center;">KD Visit</th>
                                                         <th style="text-align:center;">Sales Name</th>             
                                                        <th style="text-align:center;">Outlet</th>
                                                        <th style="text-align:center;">Alamat Outlet</th>
                                                        <th style="text-align:center;">Kota</th>
                                                        <th style="text-align:center;">Tgl Kunjungan</th>
                                                        <th style="text-align:center;">Status</th>
                                                        
                                                        @if(Auth::user()->kd_role != 3)   
                                                            <th style="text-align:center;">Action</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data as $row)
                                                        <tr>
                                                            <td>{{ $row->id }}</td>
                                                             <td>{{ $row->nama }}</td>                     
                                                            <td><span style="color: green">
                                                                <i class="fa fa-circle"></i> {{ $row->kd_outlet }}</span> 
                                                                <br>{{ $row->outlets->nm_outlet}}
                                                            </td>
                                                            <td>{{ $row->almt_outlet }}</td>
                                                            <td>{{ $row->nm_kota }}</td>
                                                                 <td>{{ date('d-F-Y H:i:s', strtotime($row->date_visit)) }}</td>
                                                            
                                                            @if($row->status_visit == 1)
                                                                <td class="center">
                                                                    <span class="btn btn-success btn-xs">Finished</span>
                                                                </td>
                                                            @elseif($row->status_visit == 0)
                                                                 
                                                                    <td class="center">
                                                                        <span class="btn btn-warning btn-xs">Waiting</span>
                                                                    </td> 
                                                                @endif                   
                                                            @if(Auth::user()->kd_role != 3)
                                                                <td>
                                                                    <center>
                                                                        <div class="button-list">
                                                                            @if(Auth::user()->hasAccess('admin.visitMonitor.edit'))
                                                                                <a href="{!! route('admin.visitMonitor.edit',[$row->id]) !!}" data-toggle="tooltip" title="Edit" class="btn btn-success btn-sm btn-icon-anim btn-square mr-0" style="display: unset;"><i class="fa fa-pencil" style="font-size: 11px;"></i></a>
                                                                            @endif
                                                                            @if(Auth::user()->hasAccess('admin.visitMonitor.delete'))      
                                                                                <a href="{!! route('admin.visitMonitor.delete',[$row->id]) !!}" id="delete" data-toggle="tooltip" title="Delete" class="btn btn-danger btn-sm btn-icon-anim btn-square mr-0" onclick="return confirm('Are you sure?')" style="display: unset;"><i class="fa fa-trash" style="font-size: 11px;"></i></a> 
                                                                            @endif
                                                                        </div>
                                                                    </center>    
                                                                </td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')
        @section('custom-js')
        <link href="{{asset ('assets/theme/backend/vendors/bower_components/summernote/dist/summernote.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset ('assets/theme/backend/vendors/bower_components/switchery/dist/switchery.min.css')}}" rel="stylesheet" type="text/css" />

        
        <link href="{{asset ('assets/theme/backend/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
         <!-- Moment JavaScript -->
          <script src="{{asset ('assets/theme/backend/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>   
  
        <script type="text/javascript">
            $(function () {
                $('.datetimepicker1').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
            });
        </script>
        <script type="text/javascript">
        $(function() {
                $('#tablevisitmon').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    </script>
        @endsection

        @push('message')
            @include('partials.toastr')
        @endpush

@endsection