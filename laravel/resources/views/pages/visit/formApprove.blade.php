<div class="table-responsive">
    <table class="table datable_1 table-hover display table-bordered mb-30" >
        <thead>
            <tr>
                <th style="text-align:center;">Nama Outlet</th>
                <th style="text-align:center;">Alamat Outlet</th>
                <th style="text-align:center;">Kota Outlet</th>
                <th style="text-align:center;">Date Create Visit Plan</th>
                <th style="text-align:center;">Date Visit Plan</th>
                <th style="text-align:center;">Status Approval </th>
                <th style="text-align:center;">Sales Name</th>
                <th style="text-align:center;">Approval</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $row)
                <tr>
                    <td>{{ $row->nm_outlet }}</td>
                    <td>{{ $row->almt_outlet }}</td>
                    <td>{{ $row->nm_kota}}</td>
                    <td>{{ date('d-F-Y H:i:s', strtotime($row->date_create_visit))  }}</td>
                    <td>{{ date('d-F-Y H:i:s', strtotime($row->date_visit)) }}</td>
                    @if($row->approve_visit == 1)
                        <td class="center">
                            <span class="label label-success">Approved</span>
                        </td>
                    @elseif($row->approve_visit == 0)
                        <td class="center">
                            <span class="label label-important">Denied</span>
                        </td>
                    @elseif($row->approve_visit == 2)  
                        <td class="center">
                            <span class="label label-warning">Pending</span>
                        </td>
                    @else
                    <td class="center">
                        <span class="label label-warning">Pending</span>
                    </td>
                    @endif                             
                    <td>{{ $row->nama}}</td>                          
                    <td>
                        <center>
                            <input type="checkbox" name="approve_visit[]" id="approved" value="{{$row->id}}">
                        </center>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="row pa-30 pb-0">
        <center>
            <input type="button" id="check" class="btn btn-primary" value="Check All" />
            <input type="button" id="uncheck" class="btn btn-primary" value="UnCheck All" />
        </center>
    </div>

    <div class="row pa-10">
        <center>
            {!! Form::submit($submit, array('class' => 'btn btn-success')) !!}
        </center>
    </div>

</div>

<script>
    $(document).ready( function() {
        $('#check').on('click', function () {
            var allInputs = document.getElementsByTagName("input");
            for (var i = 0, max = allInputs.length; i < max; i++){
                if (allInputs[i].type === 'checkbox')
                    allInputs[i].checked = true;
            }
        });
        $('#uncheck').on('click', function () {
            var allInputs = document.getElementsByTagName("input");
            for (var i = 0, max = allInputs.length; i < max; i++){
                if (allInputs[i].type === 'checkbox')
                    allInputs[i].checked = false;
            }
        });
    });
</script>