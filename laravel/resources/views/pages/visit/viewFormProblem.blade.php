 @extends('layouts.master')

@section('title', 'Visit Bad Signal List')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Latest Visit Bad Signal list</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Visit Bad Signal</span></a></li>
                            <li class="active"><span>Result</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-money"></i>&nbsp;&nbsp;Filter Visit Bad Signal</h6>
                                </div>

                                @include('partials.panel')

                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-wrap">

                                                 {!! Form::open(array('route' => 'admin.visitProblem.result', 'method' => 'POST' )) !!}
                                                 @include('pages.visit.filterVisitProblem')
                                                 {!! Form::close() !!}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap"> 
                                     
                                        <div class="table-responsive">
                                            <table id="tablevisitpromble" class="table datable_1 table-hover display table-bordered mb-30">
                                             <thead>
                                               <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center" width="160">Outlet</th>
                                                <th class="text-center">Sales Name</th>
                                                <th class="text-center">ID Visit Bad Signal</th>
                                                <th class="text-center">Kode Visit</th>          
                                                <th class="text-center">Note Kunjungan</th>
                                                <th class="text-center">Waktu Take Photo</th>
                                                <th class="text-center">Waktu Upload Photo</th>
                                                <th class="text-center">Thumbnail</th> 
                                                @if(Auth::user()->kd_role != 3)
                                                <th class="text-center">Action</th>
                                                @else
                                                <th></th>
                                                @endif
                                              </tr>
                                                </thead>
                                                <tbody>
                      <?php $i = 1; ?>
                      @foreach($data['data'] as $row)
                      <tr>
                        <td>{{$i++}}</td>
                        <td><span style="color: green">
                            <i class="fa fa-circle"></i> {{ $row->kd_outlet }}</span> 
                            <br>{{ $row->nm_outlet}}
                        </td>
                        <td>{{ $row->nama }}</td>
                        <td>{{ $row->kd_problem }}</td>
                        <td>{{ $row->kd_visit }}</td>
                        <td>{{ $row->detail_problem }}</td>
                        <td>{{ $row->date_take_photo }}</td>
                        <td>{{ $row->date_upload_photo }}</td>
                        @if(File::exists('image_upload/visit_problem/'.$row->nm_photo) and $row->nm_photo != '')
                        <td> <a href="{!! url('image_upload/visit_problem/'.$row->nm_photo) !!}" target="_blank" title="{{ $row->nm_photo }}">
                          <img style="max-height: 60px; max-width: 60px;" src="{!! url('image_upload/visit_problem/'.$row->nm_photo) !!}" class="img-responsive" /></a>
                        </td>
                        @else
                        <td><img style="max-height: 60px; max-width: 60px;" src="{!! url('/img/no_image_outlet_upload.png') !!}" class="img-responsive" /></td>
                        @endif


                        <td>
                          @if(Auth::user()->hasAccess('admin.visitProblem.edit'))
                          <a href="{!! route('admin.visitProblem.edit',[$row->kd_problem]) !!}" class="fa fa-pencil"></a>
                          @endif
                          &nbsp;
                          @if(Auth::user()->hasAccess('admin.visitProblem.delete'))
                          <a href="{!! route('admin.visitProblem.delete',[$row->kd_problem]) !!}" onclick="return confirm('Are you sure?')"  class="btn btn-xs btn-danger text-red"><i class="fa fa-trash"></i></a>
                         
                          @endif
                        </td>
                      </tr>
                      @endforeach
                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
           
                @include('partials.footer')
                @section('custom-js')
        <link href="{{asset ('assets/theme/backend/vendors/bower_components/summernote/dist/summernote.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset ('assets/theme/backend/vendors/bower_components/switchery/dist/switchery.min.css')}}" rel="stylesheet" type="text/css" />

        
        <link href="{{asset ('assets/theme/backend/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
         <!-- Moment JavaScript -->
          <script src="{{asset ('assets/theme/backend/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>   
  
        <script type="text/javascript">
            $(function () {
                $('.datetimepicker1').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
            });
        </script>
        <script type="text/javascript">
        $(function() {
                $('#tablebilling').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    </script>
        @endsection
            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush

@endsection