<div class="col-xs-6 col-sm-4 col-sm-offset-4">
	<div class="form-group">
		{!! Form::label('kd_outlet','Nama Outlet', array('class' => 'control-label mb-10')) !!}
		<select name="kd_outlet" id="outlet" class="selectpicker" data-style="form-control btn-default btn-outline" required>
		    <option value="0" selected="true" disabled="true">Select Outlet</option>
		    @foreach ($outlet as $outlets)
		      <option value="{{ $outlets->kd_outlet }}">{{ $outlets->nm_outlet}}</option>
		    @endforeach
	    </select>
	</div>
	<div class="form-group">
	    {!! Form::label('username','Nama Sales Name', array('class' => 'control-label mb-10')) !!}
	    <div id="user" name="user" class="control-label mb-10"> </div> 	    	
	    <div id="distributor" name="distributor" class="control-label mb-10"> </div>
	</div>
	<div class="form-group">
		{!! Form::label('date_visit','Tanggal dan Waktu Visit', array('class' => 'control-label mb-10')) !!}
        <div class='input-group date' id='datetimepicker1'>
            <input type='text' name="date_visit" class="form-control" required/>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
        </div>
	</div>
	    {!! Form::hidden('approve_visit', '2', array('class' => 'form-control')) !!}
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>

	<!-- Moment JavaScript -->
	<script type="text/javascript" src="/doodle/vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
	<!-- Bootstrap Colorpicker JavaScript -->
	<script src="/doodle/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
	<!-- Bootstrap Datetimepicker JavaScript -->
	<script type="text/javascript" src="/doodle/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

	<script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
            	format: 'YYYY-MM-DD HH:mm:ss'
            });
        });
    </script>

    <script>
	  $('#outlet').on('change', function(e) {
            console.log(e);
            var kd_outlet = e.target.value;
            //ajax
            $.getJSON('{{url('/ajax-call?kd_outlet=')}}'+kd_outlet, function (data) {
				//console.log(data);
                $('#user').empty();
              	$.each(data, function(index, userObj){
                	$('#user').append('<div><mark>'+userObj.nama+'</mark></div>');
 
 				});
            });
        });

	  $('#distributor').on('change', function(e) {
            console.log(e);
            var kd_dist = e.target.value;
            //ajax
            $.getJSON('{{url('/ajax-call?kd_dist=')}}'+kd_dist, function (data) {
				//console.log(data);
                $('#distributor').empty();
              	$.each(data, function(index, userObj){
 	               	$('#distributor').append('<div>'+userObj.nm_dist+'</div>');

 				});
            });
        });
	</script>