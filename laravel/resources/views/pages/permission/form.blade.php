<div class="col-xs-6 col-sm-4 col-sm-offset-4">
	<div class="form-group">
		{!! Form::label('nm_permission','Route / Permission', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('nm_permission',null, array('class' => 'form-control', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('enable','Availability', array('class' => 'control-label mb-10')) !!}
		{!! Form::select('enable',[
					   '1' => 'Enable',
					   '0' => 'Disable',
					   ], null, array('class' => 'form-control', 'data-style' => 'form-control btn-default btn-outline')) !!}		
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>