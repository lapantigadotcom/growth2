@extends('layouts.master')

@section('title', 'Report Sales Activity')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Report Sales Activity</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li class="active"><span>Report Sales Activity </span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-md-12">
                    <div class="panel panel-warning card-view panel-refresh">
                                <div class="refresh-container">
                                    <div class="la-anim-1"></div>
                                </div>
                                 
                                <div id="collapse_1" class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            
                                            <div class="col-md-4">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-green">
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <a href="{!! route('admin.report.outletVisit') !!}">
                                                                            <div class="text-center data-wrap-right">
                                                                                <i class="fa fa-map txt-light data-right-rep-icon"></i>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <span class="txt-light block pa-0 pb-10">
                                                                                    <strong>Simulasi Billing Activity</strong>
                                                                                </span>
                                                                            </div>
                                                                        </a>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-red">
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <a href="{!! route('admin.report.effectiveCall') !!}">
                                                                            <div class="text-center data-wrap-right">
                                                                                <i class="fa fa-clock-o txt-light data-right-rep-icon"></i>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <span class="txt-light block pa-0 pb-10">
                                                                                    <strong>Report Effective Activity</strong>
                                                                                </span>
                                                                            </div>
                                                                        </a>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             
                                            <div class="col-md-4">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-yellow">
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <a href="{!! route('admin.report.outletVisit') !!}">
                                                                            <div class="text-center data-wrap-right">
                                                                                <i class="fa fa-truck txt-light data-right-rep-icon"></i>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <span class="txt-light block pa-0 pb-10">
                                                                                    <strong>Report Kunjungan Sales</strong>
                                                                                </span>
                                                                            </div>
                                                                        </a>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

@endsection