<div class="row-fluid sortable">       
   <div class="box span12">
    <div class="box-header" data-original-title>
        <h2>Laporan Outlet Visit Per Sales Name</h2>
    </div>
        <div class="box-content">    
         @foreach($data['sales'] as $val)
            <h3>Sales Name : {!! $val->nama !!}</h3>
         @endforeach
         @foreach($data['area'] as $val)
            <h3>Area : {!! $val->nm_area !!}</h3>
         @endforeach  
            <table>
                <thead> 
                    <tr>
                        <th>M1</th>
                        <th>M2</th>
                        <th>M3</th>
                        <th>M4</th>
                        <th>M5</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{!! $data['dateFrom1'] !!} - {!! $data['dateTo1'] !!}</td>
                        <td>{!! $data['dateFrom2'] !!} - {!! $data['dateTo2'] !!}</td>
                        <td>{!! $data['dateFrom3'] !!} - {!! $data['dateTo3'] !!}</td>
                        <td>{!! $data['dateFrom4'] !!} - {!! $data['dateTo4'] !!}</td>
                        <td>{!! $data['dateFrom5'] !!} - {!! $data['dateTo5'] !!}</td>
                    </tr>
                </tbody>
            </table>
            <table>
                <tr>
                    <th>Periode</th>
                    <th>Target Visiting</th>
                    <th>Effective Call</th>
                </tr>
                <tr>
                    <td>{!! $data['periode'] !!}</td>
                    <td>{!! $data['targetVisiting'] !!}</td>
                    <td> {!! $data['targetEc'] !!}</td>
                </tr>
            </table>
               
            <table class="table table-striped table-bordered bootstrap-datatable">
                <thead>
                    <tr>               
                        <th style="text-align:center;" rowspan="2" colspan="1">Kode Outlet</th>
                        <th style="text-align:center;" rowspan="2" colspan="1">Nama Outlet</th>
                        <th style="text-align:center;" rowspan="2" colspan="1">Alamat</th>
                        <th style="text-align:center;" rowspan="2" colspan="1">Kode Pos</th>
                        <th style="text-align:center;" rowspan="2" colspan="1">Kota</th>
                        <th style="text-align:center;" rowspan="2" colspan="1">Rank</th>
                        <th style="text-align:center;" rowspan="2" colspan="1">Tipe</th>
                        <th style="text-align:center;" rowspan="2" colspan="1">Distributor</th>
                        <th  style="text-align:center;" colspan="5">Visiting Count</th>
                        <th  style="text-align:center;" colspan="5">Effective Call</th>
                    </tr>
                    <tr>               
                        <th style="text-align:center;">M1</th>
                        <th style="text-align:center;">M2</th>
                        <th style="text-align:center;">M3</th>
                        <th style="text-align:center;">M4</th>
                        <th style="text-align:center;">M5</th>
                        <th style="text-align:center;">M1</th>
                        <th style="text-align:center;">M2</th>
                        <th style="text-align:center;">M3</th>
                        <th style="text-align:center;">M4</th>
                        <th style="text-align:center;">M5</th>
                    </tr>
                </thead>
                <tbody>
                     @foreach($data['result'] as $row)
                    <tr>                         
                        <td>{{ $row->kode }}</td>
                        <td>{{ $row->nm_outlet }}</td>
                        <td>{{ $row->almt_outlet }}</td>
                        <td>{{ $row->kodepos }}</td>
                        <td>{{ $row->nm_kota }}</td>
                        <td>{{ $row->rank_outlet }}</td>
                        <td>{{ $row->nm_tipe }}</td>
                        <td>{{ $row->nm_dist }}</td>
                        <td>{{ $row->M1 }}</td>
                        <td>{{ $row->M2 }}</td>
                        <td>{{ $row->M3 }}</td>
                        <td>{{ $row->M4 }}</td>
                        <td>{{ $row->M5 }}</td>
                        @if($row->T1=='' || $row->T2=='' ||$row->T3=='' ||$row->T4=='' ||$row->T5=='')
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        @else
                        <td>{{ $row->T1 }}</td>
                        <td>{{ $row->T2 }}</td>
                        <td>{{ $row->T3 }}</td>
                        <td>{{ $row->T4 }}</td>
                        <td>{{ $row->T5 }}</td>
                        @endif
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="8" style="text-align:center;"> Total</td>
                        <td >{{ $data['t1'] }}</td>
                        <td >{{ $data['t2'] }}</td>
                        <td >{{ $data['t3'] }}</td>
                        <td >{{ $data['t4'] }}</td>
                        <td >{{ $data['t5'] }}</td>
                        <td >{{ $data['e1'] }}</td>
                        <td >{{ $data['e2'] }}</td>
                        <td >{{ $data['e3'] }}</td>
                        <td >{{ $data['e4'] }}</td>
                        <td >{{ $data['e5'] }}</td>
                    </tr>
                    <tr>
                        <td colspan="8" style="text-align:center;"> Grand Total</td>
                        <td colspan="5" style="text-align:center;font-size: 18pt;"> {{ $data['grandTotalVisit'] }}</td>
                        <td colspan="5" style="text-align:center;font-size: 18pt;">{{ $data['grandTotalEc'] }} </td>
                      
                    </tr>
                    <tr>
                        <td colspan="8" style="text-align:center;"> Prosentase Visiting dan Effective Call (%)</td>
                        <td colspan="5" style="text-align:center;font-size: 20pt;"> {{ $data['prosentaseVisit'] }} %</td>
                        <td colspan="5" style="text-align:center;font-size: 20pt;">{{ $data['prosentaseEc'] }} %</td>
                       
                    </tr>
                </tbody>
            </table>
        </div>
    </div><!-- /.box-body -->
</div><!-- /.box -->