@extends('layouts.master')

@section('title', 'Visiting Outlet Report')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Report management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Report</span></a></li>
                            <li class="active"><span>Visiting Outlet Report</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-warning card-view panel-refresh">
                            <div class="refresh-container">
                                <div class="la-anim-1"></div>
                            </div>
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-filter"></i>&nbsp;&nbsp;Result Visiting Outlet</h6>
                                </div>
                                @include('partials.panel')
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-wrap">
                                                @foreach($data['sales'] as $val)
                                                    <h4>Sales Name : <span class="label label-danger capitalize-font inline-block" style="font-size: 17px;">{!! $val->nama !!}</span></h4>
                                                 @endforeach
                                                 @foreach($data['area'] as $val)
                                                    <div class="progress-anim mt-10">
                                                        <div class="progress progress-lg">
                                                            <div class="progress-bar progress-bar-success wow animated progress-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="font-size: 15px;" > <span class="sr-only">100% Complete</span>Area : {!! $val->nm_area !!}</div>
                                                            </div>
                                                        </div>
                                                 @endforeach 
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        {!! Form::label('from','M1', array('class' => 'control-label mb-10')) !!}
                                                        <input type="text" class="form-control" value="{!! $data['dateFrom1'] !!} - {!! $data['dateTo1'] !!}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        {!! Form::label('from','M2', array('class' => 'control-label mb-10')) !!}
                                                        <input type="text" class="form-control" value="{!! $data['dateFrom2'] !!} - {!! $data['dateTo2'] !!}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        {!! Form::label('from','M3', array('class' => 'control-label mb-10')) !!}
                                                        <input type="text" class="form-control" value="{!! $data['dateFrom3'] !!} - {!! $data['dateTo3'] !!}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        {!! Form::label('from','M4', array('class' => 'control-label mb-10')) !!}
                                                        <input type="text" class="form-control" value="{!! $data['dateFrom4'] !!} - {!! $data['dateTo4'] !!}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        {!! Form::label('from','M5', array('class' => 'control-label mb-10')) !!}
                                                        <input type="text" class="form-control" value="{!! $data['dateFrom5'] !!} - {!! $data['dateTo5'] !!}" readonly>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-wrap">
                                                <div class="progress-anim mt-10">
                                                    <div class="progress progress-lg">
                                                        <div class="progress-bar progress-bar-info wow animated progress-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="font-size: 15px;" > <span class="sr-only">60% Complete</span>Keterangan</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        {!! Form::label('from','Periode', array('class' => 'control-label mb-10')) !!}
                                                        <input type="text" class="form-control" value="{!! $data['periode'] !!}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        {!! Form::label('from','Target Visiting', array('class' => 'control-label mb-10')) !!}
                                                        <input type="text" class="form-control" value="{!! $data['targetVisiting'] !!}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        {!! Form::label('from','Effective Call', array('class' => 'control-label mb-10')) !!}
                                                        <input type="text" class="form-control" value="{!! $data['targetEc'] !!}" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Row -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-user"></i>&nbsp;&nbsp;Laporan Outlet Visit Per Sales Name</h6>
                                </div>
                                @include('partials.panel2')
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_2" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table class="table datable_2 table-hover table-bordered display mb-30">
                                                <thead>
                                                    <tr>
                                                        <th rowspan="2" colspan="1" >Kode Outlet</th>
                                                        <th rowspan="2" colspan="1" >Nama Outlet</th>
                                                        <th rowspan="2" colspan="1" >Alamat</th>
                                                        <th rowspan="2" colspan="1" >Kode Pos</th>
                                                        <th rowspan="2" colspan="1" >Kota</th>
                                                        <th rowspan="2" colspan="1" >Rank</th>
                                                        <th rowspan="2" colspan="1" >Tipe</th>
                                                        <th rowspan="2" colspan="1" >Distributor</th>
                                                        <th colspan="5" >Visiting Count</th>
                                                        <th colspan="5" >Effective Call</th>
                                                    </tr>
                                                    <tr>
                                                        <th>M1</th>
                                                        <th>M2</th>
                                                        <th>M3</th>
                                                        <th>M4</th>
                                                        <th>M5</th>
                                                        <th>M1</th>
                                                        <th>M2</th>
                                                        <th>M3</th>
                                                        <th>M4</th>
                                                        <th>M5</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data['result'] as $row)
                                                        <tr>                         
                                                            <td>{{ $row->kode }}</td>
                                                            <td>{{ $row->nm_outlet }}</td>
                                                            <td>{{ $row->almt_outlet }}</td>
                                                            <td>{{ $row->kodepos }}</td>
                                                            <td>{{ $row->nm_kota }}</td>
                                                            <td>{{ $row->rank_outlet }}</td>
                                                            <td>{{ $row->nm_tipe }}</td>
                                                            <td>{{ $row->nm_dist }}</td>
                                                            <td>{{ $row->M1 }}</td>
                                                            <td>{{ $row->M2 }}</td>
                                                            <td>{{ $row->M3 }}</td>
                                                            <td>{{ $row->M4 }}</td>
                                                            <td>{{ $row->M5 }}</td>
                                                            @if($row->T1=='' || $row->T2=='' ||$row->T3=='' ||$row->T4=='' ||$row->T5=='')
                                                                <td>0</td>
                                                                <td>0</td>
                                                                <td>0</td>
                                                                <td>0</td>
                                                                <td>0</td>
                                                            @else
                                                                <td>{{ $row->T1 }}</td>
                                                                <td>{{ $row->T2 }}</td>
                                                                <td>{{ $row->T3 }}</td>
                                                                <td>{{ $row->T4 }}</td>
                                                                <td>{{ $row->T5 }}</td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                    <tr>
                                                        <td colspan="8"  style="text-align:center;"> Total</td>
                                                        <td >{{ $data['t1'] }}</td>
                                                        <td >{{ $data['t2'] }}</td>
                                                        <td >{{ $data['t3'] }}</td>
                                                        <td >{{ $data['t4'] }}</td>
                                                        <td >{{ $data['t5'] }}</td>
                                                        <td >{{ $data['e1'] }}</td>
                                                        <td >{{ $data['e2'] }}</td>
                                                        <td >{{ $data['e3'] }}</td>
                                                        <td >{{ $data['e4'] }}</td>
                                                        <td >{{ $data['e5'] }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="8" style="text-align:center;"> Grand Total</td>
                                                        <td colspan="5" style="text-align:center;font-size: 18pt;"> {{ $data['grandTotalVisit'] }}</td>
                                                        <td colspan="5" style="text-align:center;font-size: 18pt;">{{ $data['grandTotalEc'] }} </td>
                                                      
                                                    </tr>
                                                    <tr>
                                                        <td colspan="8" style="text-align:center;"> Prosentase Visiting dan Effective Call (%)</td>
                                                        <td colspan="5" style="text-align:center;font-size: 20pt;"> {{ $data['prosentaseVisit'] }} %</td>
                                                        <td colspan="5" style="text-align:center;font-size: 20pt;">{{ $data['prosentaseEc'] }} %</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

@endsection

@section('script', '
    <!-- Progressbar Animation JavaScript -->
    <script src="/doodle/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script> ')
