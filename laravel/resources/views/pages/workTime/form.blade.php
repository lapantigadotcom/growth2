		<div class="col-xs-6 col-sm-4 col-sm-offset-4">
		 	<div class="form-group">
				{!! Form::label('periode','Periode', array('class' => 'control-label mb-10')) !!}
				<div class='input-group date datetimepicker2'>
			        <input type='text' name="periode" class="form-control" required />
			        <span class="input-group-addon">
			            <span class="glyphicon glyphicon-calendar"></span>
			        </span>
			    </div>
			</div>
			<div class="form-group">
				{!! Form::label('jd_area','Kode Area', array('class' => 'control-label mb-10')) !!}
				{!! Form::select('kd_area',$areas, null, array('id'=>'area','class' => 'form-control')) !!}	
			</div>
			<div class="form-group">
				{!! Form::label('sales','Nama Sales', array('class' => 'control-label mb-10')) !!}
				<select id="sales" name="sales" class="form-control">
				  <option value=""></option>
		        </select>
			</div>
			<div class="form-group ">
					{!! Form::submit('Show', array('class' => 'btn btn-success', 'name' => 'show')) !!}
					{!! Form::submit('Download', array('class' => 'btn btn-primary', 'name' => 'down')) !!}
				</div>
			</div>
		</div>


	<!-- Moment JavaScript -->
	<script type="text/javascript" src="/doodle/vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
		
	<!-- Bootstrap Colorpicker JavaScript -->
	<script src="/doodle/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
	<!-- Bootstrap Datetimepicker JavaScript -->
	<script type="text/javascript" src="/doodle/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

	<script type="text/javascript">
        $(function () {
            $('.datetimepicker2').datetimepicker({
            	format: 'MM-YYYY'
            });
        });
    </script>


	 <script>
		$('#area').on('change', function(e) {
	        console.log(e);
	        var id = e.target.value;
	        //ajax
	        $.getJSON('{{url('/ajax-sales?id=')}}'+id, function (data) {
				//console.log(data);
	            $('#sales').empty();
	          	$.each(data, function(index, salesObj){
	            	 $('#sales').append('<option value="'+salesObj.id+'">'+salesObj.nama+'</option>');
					});
	        });
	    });
	</script>





