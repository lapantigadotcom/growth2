<div class="row-fluid sortable">       
   <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Laporan Working Time Per Sales</h2>
        </div>
        <div class="box-content">    
            @foreach($data['sales'] as $val)
                <h3>Sales Name : {!! $val->nama !!}</h3>
             @endforeach
             @foreach($data['area'] as $val)
                <h3>Area : {!! $val->nm_area !!}</h3>
             @endforeach  
                <h3>Periode : {!! $data['periode'] !!}</h3>     
            <table class="table table-striped table-bordered bootstrap-datatable">
                  <tbody>
                    <tr>                                       
                        <th>Waktu Check In</th>
                        <th>Waktu Check Out</th>
                        <th>Working Time/ Day</th>
                    </tr>
              
                    @foreach($data['workingTime'] as $key => $row)
                    <tr>                         
                        <td>{{ date('d-F-Y H:i:s', strtotime($row->CheckIn ))  }}</td>
                        <td>{{ date('d-F-Y H:i:s', strtotime($row->CheckOut)) }}</td>
                        <td>{{ $row->WorkingHours }}</td>                                 
                    </tr>
                   @endforeach  
                    <tr>
                        <td colspan="2" style="text-align:center;"> Total Working Time / Month</td>
                        <td style="text-align:center;font-size: 18pt"> {{$data['totalWorkTimeMonth']}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>