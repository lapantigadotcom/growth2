@extends('layouts.master')

@section('title', 'Working Time Report')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Report management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Report</span></a></li>
                            <li class="active"><span>Working Time Report</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-warning card-view panel-refresh">
                            <div class="refresh-container">
                                <div class="la-anim-1"></div>
                            </div>
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-filter"></i>&nbsp;&nbsp;Laporan Working Time Per Sales Name</h6>
                                </div>
                                @include('partials.panel')
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-wrap">
                                                @foreach($data['sales'] as $val)
                                                    <h4>Sales Name : <span class="label label-danger capitalize-font inline-block" style="font-size: 17px;">{!! $val->nama !!}</span></h4>
                                                 @endforeach
                                                @foreach($data['sales'] as $val)
                                                    <div class="progress-anim mt-10">
                                                        <div class="progress progress-lg">
                                                            <div class="progress-bar progress-bar-success wow animated progress-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="font-size: 15px;" > <span class="sr-only">100% Complete</span>NIK : {{$val->nik}}</div>
                                                            </div>
                                                        </div>
                                                 @endforeach 
                                                 <div class="col-md-2">
                                                    <div class="form-group">
                                                        @foreach($data['area'] as $val)
                                                        {!! Form::label('area','Area', array('class' => 'control-label mb-10')) !!}
                                                        <input type="text" class="form-control" value="{!! $val->nm_area !!}" readonly>
                                                        @endforeach  
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        {!! Form::label('from','Periode', array('class' => 'control-label mb-10')) !!}
                                                        <input type="text" class="form-control" value="{!! $data['periode'] !!}" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Row -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-user"></i>&nbsp;&nbsp;Laporan Outlet Visit Per Sales Name</h6>
                                </div>
                                @include('partials.panel2')
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_2" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table class="table datable_2 table-hover table-bordered display pb-30">
                                                <thead>
                                                    <tr>
                                                         <th style="text-align:center;" rowspan="2" colspan="1">Waktu Check In</th>
                                                        <th style="text-align:center;" rowspan="2" colspan="1">Waktu Check Out</th>
                                                        <th style="text-align:center;" rowspan="2" colspan="1">Working Time/ Day</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data['workingTime'] as $row)
                                                    <tr>                         
                                                        <td style="text-align:center;">{{ date('d-F-Y H:i:s', strtotime($row->CheckIn ))  }}</td>
                                                        <td style="text-align:center;">{{ date('d-F-Y H:i:s', strtotime($row->CheckOut)) }}</td>
                                                        <td style="text-align:center;">{{ $row->WorkingHours }}</td>                                 
                                                   </tr>
                                                   @endforeach  
                                                    <tr>
                                                        <td colspan="2" style="text-align:center;"> Total Working Time / Month</td>
                                                        <td style="text-align:center;font-size: 18pt"> {{$data['totalWorkTimeMonth']}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

@endsection

@section('script', '
    <!-- Progressbar Animation JavaScript -->
    <script src="/doodle/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js"></script> ')
