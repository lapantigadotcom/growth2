 <div class="box-body">
    <div class="row">
         <div class="col-sm-6 col-md-6 col-lg-6">    
            <div class="box-header with-border">
              <h6 class="box-title">Date Range</h6>
              <br>
            </div>
            <div class="box-body" style="margin-left: 10px">
                 <div class="form-group">
                    <div class="col-sm-11"> 
                <div class="input-group form-group date datetimepicker1" >
                    <input type='text' name="dateFrom" class="form-control" id="datepicker1" placeholder="TGL awal" required/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
               </div>  
               <div class="input-group form-group date datetimepicker1" >
                    <input type='text' name="dateTo" class="form-control" placeholder="TGL akhir" id="datepicker2" required/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
               </div>   
                </div> 
                  </div>   
            </div>
       </div>
        <div class="col-sm-6 col-md-6 col-lg-6">    
            <div class="box-header with-border">
            <h6 class="box-title">Select Data</h6>
            <br>
        </div>
            <div class="box-body">
                <div class="form-group row">
                    <div class="col-sm-6 col-md-3 col-lg-3"> 
                        {!! Form::select('kd_area',$data['areas'], null, array('id'=>'area','class' => 'selects', 'class' => 'form-control')) !!}
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-3"> 
                        {!! Form::select('sales',$data['sales'], null, array('id'=>'sales','class' => 'selects', 'class' => 'form-control')) !!}
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-3"> 
                        {!! Form::select('nm_tipe',$data['tipe'], null, array('id'=>'tipe','class' => 'selects', 'class' => 'form-control')) !!}
                    </div> 
                </div>
                <div class="form-group">
                     {!! Form::submit('Show', array('name'=>'show','class' => 'btn btn-success')) !!}
                        @if(Auth::user()->hasAccess('admin.download.photo'))
                        {!! Form::submit('Download', array('name'=>'download','class' => 'btn btn-warning')) !!}
                        @endif
                        
                </div>
        </div>
     </div>
</div>
