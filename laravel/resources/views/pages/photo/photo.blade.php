@extends('layouts.master')

 
@section('custom-css')
 
<link href="{{asset ('assets/theme/backend/vendors/bower_components/summernote/dist/summernote.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset ('assets/theme/backend/vendors/bower_components/switchery/dist/switchery.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset ('assets/theme/backend/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset ('assets/theme/backend/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('title', 'Market Update')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Market Update</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Photo Activity</span></a></li>
                            <li class="active"><span>Market Update Filter</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->


                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">

                        <div class="panel panel-warning card-view panel-refresh">
                            <div class="refresh-container">
                                <div class="la-anim-1"></div>
                            </div>
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-camera"></i>&nbsp;&nbsp;Market Update 1 Minggu Terakhir</h6>
                                </div>

                                @include('partials.panel2')

                                <div class="clearfix"></div>


                            </div>



                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                   <div class="table-wrap">
                                       <div class="form-wrap">
                                        {!! Form::open(array('route' => 'admin.photoAct.result', 'method' => 'POST', 'data-toggle' => 'validator', 'role' => 'form', 'class' => 'form-horizontal')) !!}
                                        @include('pages.photo.formFilter')
                                        {!! Form::close() !!}
                                        @include('partials.panel')

                                    </div>

                                </div>
                            </div>

                        </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table id="photo" class="table datable_1 table-hover table-bordered display mb-30">
                                                <thead>
                                                    <tr><th>#</th>
                                                        <th width="150">Outlet</th>
                                                        <th>Tipe</th>
                                                        <th>Sales Name </th>
                                                        <th>Nama Foto </th>
                                                        <th>Note</th>
                                                        <th>Date Take Foto</th>
                                                        <!-- <th>Date Upload</th> -->
                                                        <th>Foto 1</th>
                                                        <th>Foto 2</th>
                                                        <th>Foto 3</th>
                                                        @if(Auth::user()->kd_role != 3)   
                                                            <th>Action</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                 <?php $i=1;?>
                                                 @foreach($data['content'] as $row)
                                                 <tr>
                                                    <td>{{$i++}}</td>
                                                    <td><span style="color: green">
                                                        <i class="fa fa-circle"></i> {{ $row->kd_outlet }}</span> 
                                                        <br>{{ $row->outlets->nm_outlet}}
                                                    </td>
                                                     <td>{{ $row->nama_tipe }}</td>
                                                    <td>{{ $row->nama }}</td>
                                                    <td>{{ $row->nm_photo }}</td>
                                                    <td>{{ $row->note }}</td>
                                                    <td>{{ date('d-F-Y H:i:s', strtotime($row->date_take_photo)) }}</td>
                                                    <!-- <td>{{ date('d-F-Y H:i:s', strtotime($row->date_upload_photo)) }}</td> -->
                                                    @if(File::exists('image_upload/outlet_photoact/'.$row->path_photo) and $row->path_photo != '')
                                                    <td> <a href="{!! url('image_upload/outlet_photoact/'.$row->path_photo) !!}" target="_blank" title="{{ $row->nm_photo }}">
                                                        <img style="max-height: 60px; max-width: 60px;" src="{!! url('image_upload/outlet_photoact/'.$row->path_photo) !!}" class="img-responsive" /></a>
                                                    </td>
                                                    @else
                                                    <td>
                                                        <center><i class="fa fa-circle-o text-merah"></i></center>
                                                    </td>
                                                    @endif
                                                     @if(File::exists('image_upload/outlet_photoact/'.$row->path_photo2) and $row->path_photo2 != '')
                                                    <td> <a href="{!! url('image_upload/outlet_photoact/'.$row->path_photo2) !!}" target="_blank" title="{{ $row->nm_photo }}">
                                                        <img style="max-height: 60px; max-width: 60px;" src="{!! url('image_upload/outlet_photoact/'.$row->path_photo2) !!}" class="img-responsive" /></a>
                                                    </td>
                                                    @else
                                                    <td>
                                                        <center><i class="fa fa-circle-o text-merah"></i></center>
                                                    </td>
                                                    @endif
                                                     @if(File::exists('image_upload/outlet_photoact/'.$row->path_photo3) and $row->path_photo3 != '')
                                                    <td> <a href="{!! url('image_upload/outlet_photoact/'.$row->path_photo3) !!}" target="_blank" title="{{ $row->nm_photo }}">
                                                        <img style="max-height: 60px; max-width: 60px;" src="{!! url('image_upload/outlet_photoact/'.$row->path_photo3) !!}" class="img-responsive" /></a>
                                                    </td>
                                                    @else
                                                    <td>
                                                        <center><i class="fa fa-circle-o text-merah"></i></center>
                                                    </td>
                                                    @endif
                                                    @if(Auth::user()->kd_role != 3)   
                                                    <td width="120">
                                                        <center>
                                                            <div class="button-list">         
                                                        <!-- @if(Auth::user()->hasAccess('admin.photo.edit')) 
                                                         <a href="{!! route('admin.photo.edit',[$row->id]) !!}"  class="btn btn-xs btn-primary text-red"><i class="fa fa-pencil"></i></a>
                                                        @endif  -->
                                                        @if(Auth::user()->hasAccess('admin.photo.delete'))
                                                        <a href="{!! route('admin.photo.delete',[$row->id]) !!}" onclick="return confirm('Are you sure?')"  class="btn btn-xs btn-danger text-red"><i class="fa fa-trash"></i></a>   
                                                        @endif          
                                                        </center>
                                                            </div> 
                                                    </td>
                                                    @endif
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush
        @section('custom-js')
        <link href="{{asset ('assets/theme/backend/vendors/bower_components/summernote/dist/summernote.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset ('assets/theme/backend/vendors/bower_components/switchery/dist/switchery.min.css')}}" rel="stylesheet" type="text/css" />

        
        <link href="{{asset ('assets/theme/backend/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
         <!-- Moment JavaScript -->
          <script src="{{asset ('assets/theme/backend/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>   
  
        <script type="text/javascript">
            $(function () {
                $('.datetimepicker1').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
                 $('.datetimepicker2').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
            });
        </script>
        <script type="text/javascript">
        $(function() {
                $('#photo').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    </script>
        @endsection



@endsection