
<div class="row">
	<div class="col-xs-6 col-sm-4 col-sm-offset-4">
		<div class="form-group">
			{!! Form::label('periode','Periode', array('class' => 'control-label mb-10')) !!}
			<div class='input-group date datetimepicker2'>
		        <input type='text' name="periode" class="form-control" required />
		        <span class="input-group-addon">
		            <span class="glyphicon glyphicon-calendar"></span>
		        </span>
		    </div>
		</div>
		<div class="form-group">
			{!! Form::label('kd_area','Kode Area', array('class' => 'control-label mb-10')) !!}
			{!! Form::select('kd_area',$areas, null, array('id'=>'area','class' => 'form-control')) !!}	
		</div>
		<div class="form-group">
			{!! Form::label('sales','Nama Sales', array('class' => 'control-label mb-10')) !!}
			<select id="sales" name="sales" class="form-control">
			  <option value=""></option>
	        </select>
		</div>
	</div>
</div>

<h6 class="txt-dark capitalize-font"><i class="fa fa-calendar mr-10"></i>Date Filter</h6>
<hr class="light-grey-hr"/>
<div class="row">
	<div class="col-md-1 col-md-offset-2">
		<label class="control-label" style="margin-top: 40px;margin-bottom: 28px;">Minggu 1 :</label>
		<label class="control-label" style="margin-top: 40px;margin-bottom: 28px;">Minggu 2 :</label>
		<label class="control-label" style="margin-top: 40px;margin-bottom: 27px;">Minggu 3 :</label>
		<label class="control-label" style="margin-top: 40px;margin-bottom: 27px;">Minggu 4 :</label>
		<label class="control-label" style="margin-top: 40px;">Minggu 5 :</label>
	</div>
	<div class="col-xs-6 col-sm-3">
		<div class="form-group">
			{!! Form::label('from','From', array('class' => 'control-label mb-10')) !!}
			<div class='input-group date datetimepicker1'>
		        <input type='text' name="dateFrom" class="form-control"/>
		        <span class="input-group-addon">
		            <span class="glyphicon glyphicon-calendar"></span>
		        </span>
		    </div>
		</div>
		<div class="form-group">
			{!! Form::label('from','From', array('class' => 'control-label mb-10')) !!}
			<div class='input-group date datetimepicker1'>
		        <input type='text' name="dateFrom2" class="form-control"/>
		        <span class="input-group-addon">
		            <span class="glyphicon glyphicon-calendar"></span>
		        </span>
		    </div>
		</div>
		<div class="form-group">
			{!! Form::label('from','From', array('class' => 'control-label mb-10')) !!}
			<div class='input-group date datetimepicker1'>
		        <input type='text' name="dateFrom3" class="form-control"/>
		        <span class="input-group-addon">
		            <span class="glyphicon glyphicon-calendar"></span>
		        </span>
		    </div>
		</div>
		<div class="form-group">
			{!! Form::label('from','From', array('class' => 'control-label mb-10')) !!}
			<div class='input-group date datetimepicker1'>
		        <input type='text' name="dateFrom4" class="form-control"/>
		        <span class="input-group-addon">
		            <span class="glyphicon glyphicon-calendar"></span>
		        </span>
		    </div>
		</div>
		<div class="form-group">
			{!! Form::label('from','From', array('class' => 'control-label mb-10')) !!}
			<div class='input-group date datetimepicker1'>
		        <input type='text' name="dateFrom5" class="form-control"/>
		        <span class="input-group-addon">
		            <span class="glyphicon glyphicon-calendar"></span>
		        </span>
		    </div>
		</div>
	</div>
	<div class="col-xs-6 col-sm-3">
		<div class="form-group">
			{!! Form::label('to','To', array('class' => 'control-label mb-10')) !!}
			<div class='input-group date datetimepicker1'>
		        <input type='text' name="dateTo" class="form-control"/>
		        <span class="input-group-addon">
		            <span class="glyphicon glyphicon-calendar"></span>
		        </span>
		    </div>
		</div>
		<div class="form-group">
			{!! Form::label('to','To', array('class' => 'control-label mb-10')) !!}
			<div class='input-group date datetimepicker1'>
		        <input type='text' name="dateTo2" class="form-control"/>
		        <span class="input-group-addon">
		            <span class="glyphicon glyphicon-calendar"></span>
		        </span>
		    </div>
		</div>
		<div class="form-group">
			{!! Form::label('to','To', array('class' => 'control-label mb-10')) !!}
			<div class='input-group date datetimepicker1'>
		        <input type='text' name="dateTo3" class="form-control"/>
		        <span class="input-group-addon">
		            <span class="glyphicon glyphicon-calendar"></span>
		        </span>
		    </div>
		</div>
		<div class="form-group">
			{!! Form::label('to','To', array('class' => 'control-label mb-10')) !!}
			<div class='input-group date datetimepicker1'>
		        <input type='text' name="dateTo4" class="form-control"/>
		        <span class="input-group-addon">
		            <span class="glyphicon glyphicon-calendar"></span>
		        </span>
		    </div>
		</div>
		<div class="form-group">
			{!! Form::label('to','To', array('class' => 'control-label mb-10')) !!}
			<div class='input-group date datetimepicker1'>
		        <input type='text' name="dateTo5" class="form-control"/>
		        <span class="input-group-addon">
		            <span class="glyphicon glyphicon-calendar"></span>
		        </span>
		    </div>
		</div>
	</div>
	
	@include('partials.errors')

	<div class="col-md-6 col-md-offset-1">
		<div class="pull-right">
			
			{!! Form::submit('Calculate', array('class' => 'btn btn-success', 'name' => 'calc')) !!}
            {!! Form::submit('Download', array('class' => 'btn btn-primary', 'name' => 'down')) !!}
		
		</div>
	</div>
</div>

	<!-- Moment JavaScript -->
	<script type="text/javascript" src="/doodle/vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
		
	<!-- Bootstrap Colorpicker JavaScript -->
	<script src="/doodle/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
	<!-- Bootstrap Datetimepicker JavaScript -->
	<script type="text/javascript" src="/doodle/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

	<script type="text/javascript">
        $(function () {
            $('.datetimepicker1').datetimepicker({
            	format: 'YYYY-MM-DD'
            });
            $('.datetimepicker2').datetimepicker({
            	format: 'MM-YYYY'
            });
        });
    </script>

    <script>
		$('#area').on('change', function(e) {
	        console.log(e);
	        var id = e.target.value;
	        //ajax
	        $.getJSON('{{url('/ajax-sales?id=')}}'+id, function (data) {
				//console.log(data);
	            $('#sales').empty();
	          	$.each(data, function(index, salesObj){
	            	 $('#sales').append('<option value="'+salesObj.id+'">'+salesObj.nama+'</option>');
					});
	        });
	    });
	</script>
