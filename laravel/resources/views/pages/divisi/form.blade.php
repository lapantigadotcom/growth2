<div class="col-xs-6 col-sm-4 col-sm-offset-4">
	<div class="form-group">
		{!! Form::label('kd_area','Kode Area', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('kd_area',null, array('class' => 'form-control', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('nm_area','Nama Area', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('nm_area',null, array('class' => 'form-control', 'required')) !!}
	</div>
	@include('partials.errors')
	<div class="form-group mb-0">
		{!! Form::submit($submit, array('class' => 'btn btn-primary btn-anim'))!!}
	</div>
</div>