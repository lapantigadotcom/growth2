@extends('layouts.master')

@section('title', 'Divisi List')

@section('content')

       	<!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid">
				
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h5 class="txt-dark">Divisi management</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
						<ol class="breadcrumb">
							<li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
							<li><a href="#"><span>Master Data</span></a></li>
							<li class="active"><span>Divisi</span></li>
						</ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->

				<!-- Row -->
				<div class="row">
					<div class="col-sm-12">
						<div class="panel panel-warning card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-light"><i class="fa fa-th"></i>&nbsp;&nbsp;Divisi List</h6>
								</div>
								<div class="pull-right">
									<div class="tab-struct custom-tab-1">
                                        <ul role="tablist" class="nav nav-tabs" id="myTabs_9">
                                            <li class="active" role="presentation">
                                                <a aria-expanded="true" data-toggle="tab" role="tab" id="home_tab_9" href="#area_city"><p class="panel-title txt-light">Divisi</p></a>
                                            </li>
                                            <li role="presentation" >
                                                <a data-toggle="tab" id="profile_tab_1" role="tab" href="#area_only" aria-expanded="false"><p class="panel-title txt-light">Kota Divisi</p></a>
                                            </li>
                                        </ul>
                                    </div> 
								</div>
								<div class="clearfix"></div>
							</div>

							<div class="tab-content" id="myTabContent_12">
								<div id="area_city" class="tab-pane fade active in" role="tabpanel">
									<div class="panel-wrapper collapse in">
										<div class="panel-body">
											<div class="table-wrap">

											<a href="{!! route('admin.divisi.create') !!}" class="btn btn-success btn-sm" ><i class="fa fa-plus"></i> Tambah Divisi</a>

												<div class="table-responsive">
													<table id="tablearea" class="table table-hover datable_1 table-bordered display mb-30">
														<thead>
															<tr><th width="30"><center>#</center></th>
																<th><center>Kode Divisi</center></th>
																<th><center>Nama Divisi</center></th>
																@if(Auth::user()->kd_role != 3)
																	<th><center>Action</center></th>
		                                                        @endif
															</tr>
														</thead>
														<tbody>
															@foreach($data['area'] as $key=>$row)
															<tr>
																<td><center>
																{{$key+1}}</center></td>
																<td>{{ $row->kd_area }} - <strong>{{ $row->id }}</strong></td>
				                                        		<td>{{ $row->nm_area }}</td>
						                                        @if(Auth::user()->kd_role != 3)
							                                        <td>
							                                        	<center>
							                                        		<div class="button-list">
							                                        			@if(Auth::user()->hasAccess('admin.divisi.edit'))
									                                        	<a href="{!! route('admin.divisi.edit',[$row->id]) !!}" data-toggle="tooltip" title="Edit" class="btn btn-success btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-pencil" style="font-size: 14px;"></i></a>
										                                        @endif
										                                        @if(Auth::user()->hasAccess('admin.divisi.delete'))      
										                                        	<a href="{!! route('admin.divisi.delete',[$row->id]) !!}" id="delete" data-toggle="tooltip" title="Delete" class="btn btn-danger btn-sm btn-icon-anim btn-square mr-5" onclick="return confirm('Are you sure you want to delete this item?');" style="display: unset;"><i class="fa fa-trash" style="font-size: 14px;"></i></a>
										                                        @endif
							                                        		</div>
							                                        	</center>
							                                        </td>
							                                    @endif
															</tr>
															@endforeach
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div id="area_only" class="tab-pane fade" role="tabpanel">
									<div class="panel-wrapper collapse in">
										<div class="panel-body">
											<div class="table-wrap">
												<div class="table-responsive">
													<table id="table" class="table datable_1 table-hover table-bordered display mb-30">
														<thead>
															<tr>
																<th><center>Kode Divisi<center></th>
																<th><center>Nama Divisi</center></th>
																<th><center>Nama Kota</center></th>
																@if(Auth::user()->kd_role != 3)
																	<th><center>Action</center></th>
		                                                        @else
		                                                            <th></th>
		                                                        @endif
															</tr>
														</thead>
														<tbody>
															@foreach($data['all'] as $row)
															<tr>
																@if($row->kd_area != '' )
						                                            <td>{{ $row->kd_area }}</td>
						                                            <td>{{ $row->nm_area }}</td>
						                                        @else
						                                        	<td><span class="label label-warning">Divisi belum tersedia</span></td>
						                                            <td><span class="label label-info">Unregistered</span></td>
						                                        @endif
						                                        <td>{{ $row->nm_kota }}</td>
						                                        <td>
							                                        <center>
							                                        	<div class="button-list">
							                                        		@if(Auth::user()->hasAccess('admin.kota.edit'))
									                                        	<a href="{!! route('admin.kota.edit',[$row->id]) !!}" data-toggle="tooltip" title="Edit" class="btn btn-success btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-pencil" style="font-size: 14px;"></i></a>
									                                        @endif
									                                        @if(Auth::user()->hasAccess('admin.kota.delete'))      
									                                        	<a href="{!! route('admin.kota.delete',[$row->id]) !!}" id="delete" data-toggle="tooltip" title="Delete" class="btn btn-danger btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-trash" style="font-size: 14px;"></i></a>
									                                        @endif
							                                        	</div>
						                                        		
						                                        	</center>
						                                        </td>
															</tr>
															@endforeach
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							
							</div>
						</div>	
					</div>
				</div>
				<!-- /Row -->
			
				@include('partials.footer')
				 @section('custom-js')
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('#table').DataTable( {
                            "paging":   true,
                            "ordering": true,
                            "info":     true,
                            "autoWidth": true,
                            "search" :true
                        } );
                    } );
                </script>
                 <script type="text/javascript">
                    $(document).ready(function() {
                        $('#tablearea').DataTable( {
                            "paging":   true,
                            "ordering": true,
                            "info":     true,
                            "autoWidth": true,
                            "search" :true
                        } );
                    } );
                     </script>
                @endsection
			</div>
		</div>
        <!-- /Main Content -->

        @include('partials.sweetalert')

        @push('message')
        	@include('partials.toastr')
        @endpush
         

@endsection