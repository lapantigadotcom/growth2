<div class="col-xs-6 col-sm-4 col-sm-offset-4">
	<div class="form-group">
		{!! Form::label('nama','Nama', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('nama',null, array('class' => 'form-control', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('kode','Kode', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('kode',null, array('class' => 'form-control', 'required')) !!}
	</div> 
	 
	@include('partials.errors')
	<div class="form-group mb-0">
		{!! Form::submit($submit, array('class' => 'btn btn-primary btn-anim'))!!}
	</div>
</div>

<!-- Moment JavaScript -->
<script type="text/javascript" src="/doodle/vendors/bower_components/moment/min/moment-with-locales.min.js"></script>		