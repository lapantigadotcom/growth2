@extends('layouts.master')

@section('title', 'Sales Name List')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">User management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>User manager</span></a></li>
                            <li class="active"><span>Sales Name</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-user"></i>&nbsp;&nbsp;Sales Name List</h6>
                                </div>
                                @include('partials.panel')
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        @if(Auth::user()->hasAccess('admin.sales.create')) 
                                            <a href="{!! route('admin.sales.create') !!}" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Sales</a>
                                        @endif

                                        <div class="table-responsive">
                                            <table class="table datable_2 table-hover table-bordered display mb-30" id="tablesales">
                                                <thead>
                                                    <tr>
                                                        <th><center>NIK</center></th> 
                                                        <th><center>Nama</center></th>
                                                        <th><center>Alamat</center></th>
                                                        <th><center>Telepon</center></th>
                                                        <th><center>Divisi</center></th>
                                                        <th><center>Join Date</center></th>
                                                        <th><center>Role</center></th>
                                                        @if(Auth::user()->kd_role != 3) 
                                                           <th><center>Action</center></th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data as $row)
                                                        <tr>
                                                            <td>{{ $row->nik }}</td> 
                                                            <td>{{ $row->nama }}</td>
                                                            <td>{{ $row->alamat }}</td>
                                                            <td>{{ $row->telepon }}</td>
                                                            <td>{{ $row->kd_area }}</td>
                                                            <td>{{ date('d-F-Y', strtotime($row->join_date)) }}</td>
                                                            <td>{{ $row->type_role }}</td>
                                                            @if(Auth::user()->kd_role != 3)
                                                                <td>
                                                                    <center>
                                                                        <div class="button-list">
                                                                            @if(Auth::user()->hasAccess('admin.sales.show'))
                                                                                <a href="{!! route('admin.sales.show',[$row->id]) !!}" data-toggle="tooltip" title="Detail Sales" class="btn btn-primary btn-sm btn-icon-anim btn-square mr-0" style="display: unset;"><i class="fa fa-search-plus" style="font-size: 14px;"></i></a>
                                                                            @endif
                                                                            @if(Auth::user()->hasAccess('admin.sales.edit'))
                                                                                <a href="{!! route('admin.sales.edit',[$row->id]) !!}" data-toggle="tooltip" title="Edit" class="btn btn-success btn-sm btn-icon-anim btn-square mr-0" style="display: unset;"><i class="fa fa-pencil" style="font-size: 14px;"></i></a>
                                                                            @endif
                                                                            @if(Auth::user()->hasAccess('admin.sales.delete'))      
                                                                                <a href="{!! route('admin.sales.delete',[$row->id]) !!}" id="delete" data-toggle="tooltip" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn-sm btn-icon-anim btn-square mr-0" style="display: unset;"><i class="fa fa-trash" style="font-size: 14px;"></i></a>
                                                                            @endif
                                                                        </div>
                                                                    </center>     
                                                                </td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->
        @section('custom-js')
         <script type="text/javascript">
        $(function() {
                $('#tablesales').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    </script>
        @endsection
        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush
        
@endsection