	<div class="col-md-6">
		<div class="form-group">
			{!! Form::hidden('kd_role', '3', array('class' => 'form-control')) !!}
			{!! Form::label('nik','NIK', array('class' => 'control-label mb-10')) !!}
			{!! Form::text('nik',null, array('class' => 'form-control', 'required')) !!}
		</div>
		<div class="form-group">
			{!! Form::label('nama','Nama', array('class' => 'control-label mb-10')) !!}
			{!! Form::text('nama',null, array('class' => 'form-control', 'required')) !!}
		</div>
		<div class="form-group">
			{!! Form::label('join_date','Join Date', array('class' => 'control-label mb-10')) !!}
			<div class='input-group date' id='datetimepicker1'>
		        <input type='text' name="join_date" class="form-control"/>
		        <span class="input-group-addon">
		            <span class="glyphicon glyphicon-calendar"></span>
		        </span>
		    </div>
		</div>
		<div class="form-group">
			{!! Form::label('kd_area','Divisi', array('class' => 'control-label mb-10')) !!}
			{!! Form::select('kd_area', $area, null, array('class' => 'form-control', 
		'data-style' => 'data-style="form-control btn-default btn-outline', 'required')) !!}
		</div>
		<div class="form-group">
			{!! Form::label('telepon','Telepon', array('class' => 'control-label mb-10')) !!}
			{!! Form::text('telepon',null, array('class' => 'form-control', 'required')) !!}
		</div>
		<div class="form-group">	
			{!! Form::label('alamat','Alamat', array('class' => 'control-label mb-10')) !!}
			{!! Form::textarea('alamat',null, array('rows' => '2', 'class' => 'form-control', 'required')) !!}
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			{!! Form::label('foto','Upload Foto', array('class' => 'control-label mb-10')) !!}
			<input type="file" id="input-file-now" name="foto" class="dropify" data-height="206"/>
		</div>
		<div class="form-group">		
			{!! Form::label('username','Username', array('class' => 'control-label mb-10')) !!}
			{!! Form::text('username',null, array('class' => 'form-control', 'required')) !!}
		</div>
		<div class="form-group">
			{!! Form::label('password','Password', array('class' => 'control-label mb-10')) !!}
			<input type="password" name="password" class="form-control">
		</div>
		<div class="form-group">
			{!! Form::label('email_user','Email', array('class' => 'control-label mb-10')) !!}
			{!! Form::email('email_user',null, array('class' => 'form-control', 'required')) !!}
		</div>
	</div>
	<div class="col-md-12">
		@include('partials.errors')
		<div class="form-group">
			{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
		</div>
		</div>
	</div>
		
	<!-- Moment JavaScript -->
	<script type="text/javascript" src="/doodle/vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
	<!-- Bootstrap Colorpicker JavaScript -->
	<script src="/doodle/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
	<!-- Bootstrap Datetimepicker JavaScript -->
	<script type="text/javascript" src="/doodle/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

	<script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
            	format: 'YYYY-MM-DD HH:mm:ss'
            });
        });
    </script>