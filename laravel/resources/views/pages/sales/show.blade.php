@extends('layouts.master')

@section('link', '
    <link href="/doodle/vendors/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" type="text/css"/>')

@section('title', 'Profil User')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">User management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>User manager</span></a></li>
                            <li class="active"><span>Detail User</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-list"></i>&nbsp;&nbsp;Data User {{$data['content']->nama}}</h6>
                                </div>
                                @include('partials.panel')
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-wrap">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-body">
                                                        <div class="row">

                                                            <div class="col-md-4">
                                                                @if(File::exists('userphoto/'.$data['content']->foto) and $data['content']->foto != '')
                                                                    <input type="file" id="input-file-now" class="dropify" disabled="" " data-height="300" data-default-file="{{ asset('/userphoto/'.$data['content']->foto) }}"/>
                                                                @else
                                                                    <input type="file" id="input-file-now" class="dropify" disabled="" " data-height="300" data-default-file="{{ asset('/img/user.png') }}"/>
                                                                @endif
                                                            </div>

                                                            <div class="col-md-5">
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-barcode pull-right"></i></label>
                                                                    <div class="col-md-6">
                                                                        <h5 class="form-control-static">NIK : {!! $data['content']->nik !!} </h5>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-user pull-right"></i></label>
                                                                    <div class="col-md-6">
                                                                        <h5 class="form-control-static">Nama User : {!! $data['content']->nama !!} </h5>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-home pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h5 class="form-control-static">Alamat : {!! $data['content']->alamat !!} </h5>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-th pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h5 class="form-control-static">Area : {!! $data['content']->nm_area !!} </h5>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-phone pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h5 class="form-control-static">Telepon : {!! $data['content']->telepon !!} </h5>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-user pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h5 class="form-control-static">Username {!! $data['content']->username !!} </h5>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-envelope pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h5 class="form-control-static">Email : {!! $data['content']->email_user !!} </h5>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-warning card-view panel-refresh">
                                <div class="refresh-container">
                                    <div class="la-anim-1"></div>
                                </div>
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-light"><i class="fa fa-list"></i>&nbsp;&nbsp;Report</h6>
                                    </div>
                                    
                                    @include('partials.panel2')

                                    <div class="clearfix"></div>
                                </div>
                                <div id="collapse_2" class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-3">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-blue">
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <div class="col-md-6 text-center pa-25 pb-0 pr-0 data-wrap-right">
                                                                            <i class="fa fa-truck txt-light data-right-rep-icon pull-right"></i>
                                                                        </div>
                                                                        <div class="col-md-6 text-center pa-25 pb-0 pr-0 pl-20 data-wrap-right">
                                                                            <span class="txt-light block counter">
                                                                                <span class="counter-anim pull-left">{!! $data['outletVisit'] !!}</span>
                                                                            </span>
                                                                        </div>
                                                                        <div class="col-md-12 text-center pa-0 pb-40">
                                                                            <span class="txt-light block font-16">
                                                                                <strong>Outlet Visit</strong>
                                                                            </span>
                                                                        </div>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-3">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-green">
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <div class="col-md-6 text-center pa-25 pb-0 pr-0 data-wrap-right">
                                                                            <i class="fa fa-group txt-light data-right-rep-icon pull-right"></i>
                                                                        </div>
                                                                        <div class="col-md-6 text-center pa-25 pb-0 pr-0 pl-20 data-wrap-right">
                                                                            <span class="txt-light block counter">
                                                                                {{-- <span class="counter-anim pull-left">{!! $data['workingTime'] !!}</span> --}}
                                                                            </span>
                                                                        </div>
                                                                        <div class="col-md-12 text-center pa-0 pb-40">
                                                                            <span class="txt-light block font-16">
                                                                                <strong>Competitor Activity</strong>
                                                                            </span>
                                                                        </div>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-3">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-yellow">
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <div class="col-md-6 text-center pa-25 pb-0 pr-0 data-wrap-right">
                                                                            <i class="fa fa-shopping-cart txt-light data-right-rep-icon pull-right"></i>
                                                                        </div>
                                                                        <div class="col-md-6 text-center pa-25 pb-0 pr-0 pl-20 data-wrap-right">
                                                                            <span class="txt-light block counter">
                                                                                <span class="counter-anim pull-left">{!! $data['takeOrder'] !!}</span>
                                                                            </span>
                                                                        </div>
                                                                        <div class="col-md-12 text-center pa-0 pb-40">
                                                                            <span class="txt-light block font-16">
                                                                                <strong>Take Order</strong>
                                                                            </span>
                                                                        </div>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-3">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-red">
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <div class="col-md-6 text-center pa-25 pb-0 pr-0 data-wrap-right">
                                                                            <i class="fa fa-camera txt-light data-right-rep-icon pull-right"></i>
                                                                        </div>
                                                                        <div class="col-md-6 text-center pa-25 pb-0 pr-0 pl-20 data-wrap-right">
                                                                            <span class="txt-light block counter">
                                                                                <span class="counter-anim pull-left">{!! $data['takePhoto'] !!}</span>
                                                                            </span>
                                                                        </div>
                                                                        <div class="col-md-12 text-center pa-0 pb-40">
                                                                            <span class="txt-light block font-16">
                                                                                <strong>Take Photo</strong>
                                                                            </span>
                                                                        </div>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- /Row -->


                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Log User {{$data['content']->nama}}</h6>
                                </div>
                                @include('partials.panel3')
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_3" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table id="saleslog" class="table datable_1 table-hover display pb-30" >
                                                <thead>
                                                    <tr>
                                                        <th>Time</th>
                                                        <th>Description</th>
                                                        <th>Detail Akses</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data['logs'] as $row)
                                                        <tr>
                                                            <td>{{$row->log_time}}</td>
                                                            <td>{{$row->description}}</td>
                                                            <td>{{$row->detail_akses}}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')
                  @section('custom-js')
           <script src="{{asset ('assets/theme/backend/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset ('assets/theme/backend/vendors/bower_components/jquery.counterup/jquery.counterup.min.js')}}"></script>
    <!-- Bootstrap Daterangepicker JavaScript -->
    <script src="{{asset ('assets/theme/backend/vendors/bower_components/dropify/dist/js/dropify.min.js')}}"></script>
    <!-- Form Flie Upload Data JavaScript -->
    <script src="{{asset ('assets/theme/backend/dist/js/form-file-upload-data.js')}}"></script>
        <script type="text/javascript">
        $(function() {
                $('#saleslog').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    </script>
        @endsection
            </div>
        </div>
        <!-- /Main Content -->

@endsection

@section('script', '
    <!-- Progressbar Animation JavaScript -->
  ')