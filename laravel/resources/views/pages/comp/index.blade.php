@extends('layouts.master')

@section('title', 'Distributor List')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">distributor management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Master Data</span></a></li>
                            <li class="active"><span>Distributor</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-user"></i>&nbsp;&nbsp;Distributor List</h6>
                                </div>
                                @include('partials.panel')
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">

                                        @if(Auth::user()->hasAccess('admin.competitor.create'))    
                                            <a href="{!! route('admin.competitor.create') !!}" class="btn btn-primary"><i class="fa fa-plus">&nbsp;&nbsp;Tambah Photo Competitor </i></a>
                                        @endif

                                        @if(Auth::user()->hasAccess('admin.comp.create'))
                                            <a href="{!! route('admin.comp.create') !!}" class="btn btn-primary"><i class="fa fa-plus">&nbsp;&nbsp;Tambah Daftar Competitor </i></a>
                                        @endif

                                        <div class="table-responsive">
                                            <table class="table datable_1 table-hover table-bordered display mb-30">
                                                <thead>
                                                    <tr>
                                                        <th><center>Detail</center></th>
                                                        <th><center>Nama Competitor</center></th>
                                                        <th><center>Alamat Competitor</center></th>
                                                        <th><center>Sales Name</center></th>
                                                        <th><center>Nama Foto</center></th>
                                                        <th><center>Date Take Foto</center></th>
                                                        <th><center>Date Upload</center></th>
                                                        <th><center>Foto</center></th>
                                                        @if(Auth::user()->kd_role != 3) 
                                                            <th style="text-align:center;">Action</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data as $row)
                                                        <tr>
                                                            <td><a href="#"  data-toggle="tooltip" data-placement="bottom" title="Lihat Detail Photo"><i class="fa fa-search"></a></td>
                                                            <td>{{ $row->nm_competitor }}</td>
                                                            
                                                             @if($row->almt_comp_activity != '')
                                                                <td>{{ $row->almt_comp_activity }}</td>
                                                            @else
                                                                <td><span class="label label-warning">Unknown</span></td>
                                                            @endif

                                                            <td>{{ $row->nama }}</td>
                                                            <td>{{ $row->nm_photo }}</td>
                                                            <td>{{ $row->date_take_photo }}</td>
                                                            <td>{{ $row->date_upload_photo }}</td>
                                                            @if(File::exists('image_upload/competitor/'.$row->path_photo) and $row->path_photo != '')
                                                            <td>
                                                                <img src="{!! url('image_upload/competitor/'.$row->path_photo) !!}" class="img-responsive" />
                                                            </td>
                                                            @endif
                                                            @if(Auth::user()->kd_role != 3)
                                                                <td>
                                                                    <center>
                                                                        @if(Auth::user()->hasAccess('admin.competitor.edit'))
                                                                            <a href="{!! route('admin.competitor.edit',[$row->id]) !!}" data-toggle="tooltip" title="Edit" class="btn btn-success btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-pencil" style="font-size: 14px;"></i></a>
                                                                        @endif
                                                                        @if(Auth::user()->hasAccess('admin.competitor.delete'))      
                                                                            <a href="{!! route('admin.competitor.delete',[$row->id]) !!}" id="delete" data-toggle="tooltip" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-trash" style="font-size: 14px;"></i></a>
                                                                        @endif
                                                                    </center>     
                                                                </td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush
        
@endsection