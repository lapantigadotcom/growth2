<div class="col-xs-6 col-sm-4 col-sm-offset-4">
	<div class="form-group">
		{!! Form::label('nm_competitor','Nama Competitor', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('nm_competitor',null, array('class' => 'form-control', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
	<div class="form-group">
		@include('partials.errors')
	</div>
</div>





