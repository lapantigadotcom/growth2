<div class="col-xs-6 col-sm-4 col-sm-offset-4">
	<div class="form-group">
		{!! Form::label('kd_visitplan','Kode Visit Plan', array('class' => 'control-label mb-10')) !!}
		{!! Form::select('kd_visitplan',$data['visit'], null, array('class' => 'selectpicker', 
		'data-style' => 'data-style="form-control btn-default btn-outline', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('kd_produk','Nama Produk', array('class' => 'control-label mb-10')) !!}
		{!! Form::select('kd_produk', $data['produk'], null, array('class' => 'selectpicker', 
		'data-style' => 'data-style="form-control btn-default btn-outline', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('qty_order','Jumlah Order', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('qty_order', null, ['class' => 'form-control', 'required']) !!}
	</div>
	<div class="form-group">
		{!! Form::label('satuan','Satuan') !!}
		{!! Form::select('satuan', [
					   'Pack' => 'Pack',
					   'Lusin' => 'Lusin',
					   'Kodi' => 'Kodi',
					   'Kardus' => 'Kardus',
					   ], null, array('class' => 'selectpicker', 'data-style' => 'form-control btn-default btn-outline') ) !!}
	</div>
	<div class="form-group">
		{!! Form::label('date_order','Tanggal dan Waktu Order', array('class' => 'control-label mb-10')) !!}
		<div class='input-group date' id='datetimepicker1'>
	        <input type='text' name="date_order" class="form-control" />
	        <span class="input-group-addon">
	            <span class="glyphicon glyphicon-calendar"></span>
	        </span>
	    </div>
	</div>
	<div class="form-group">
		{!! Form::label('status_order','Status Order', array('class' => 'control-label mb-10')) !!}
		{!! Form::select('status_order',[
					   '1' => 'YES',
					   '0' => 'NO',
					   ], null, array('class' => 'selectpicker', 'data-style' => 'form-control btn-default btn-outline')) !!}
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>
	<!-- Moment JavaScript -->
	<script type="text/javascript" src="/doodle/vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
		
	<!-- Bootstrap Colorpicker JavaScript -->
	<script src="/doodle/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
	<!-- Bootstrap Datetimepicker JavaScript -->
	<script type="text/javascript" src="/doodle/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

	<script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
            	format: 'YYYY-MM-DD HH:mm:ss'
            });
        });
    </script>