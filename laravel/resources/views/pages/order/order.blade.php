@extends('layouts.master')


@section('title', 'Sales Order List')

@section('content')

<!-- Main Content -->
<div class="page-wrapper">
    <div class="container-fluid">

        <!-- Title -->
        <div class="row heading-bg">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h5 class="txt-dark">Sales Order management</h5>
            </div>
            <!-- Breadcrumb -->
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                    <li><a href="#"><span>Sales Order</span></a></li>
                    <li class="active"><span>Sales Order List Filter</span></li>
                </ol>
            </div>
            <!-- /Breadcrumb -->
        </div>
        <!-- /Title -->

        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-warning card-view panel-refresh">
                    <div class="refresh-container">
                        <div class="la-anim-1"></div>
                    </div>
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-light"><i class="fa fa-filter"></i>&nbsp;&nbsp;Filter Data Date Order</h6>
                        </div>
                        @include('partials.panel')
                        <div class="clearfix"></div>
                    </div>
                    <div id="collapse_1" class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-wrap">

                                        {!! Form::open(array('route' => 'admin.takeOrder.result', 'method' => 'POST', 'data-toggle' => 'validator', 'role' => 'form', 'class' => 'form-horizontal')) !!}
                                        @include('pages.order.formFilter')
                                        {!! Form::close() !!}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Row -->

        <!-- Row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-warning card-view panel-refresh">
                    <div class="refresh-container">
                        <div class="la-anim-1"></div>
                    </div>
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-light"><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;Sales Order 1 Minggu Terakhir</h6>
                        </div>

                        @include('partials.panel2')

                        <div class="clearfix"></div>
                    </div>

                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div class="table-wrap">
                                <div class="table-responsive">
                                 <table id="tableorder" class="table table-striped table-bordered bootstrap-datatable datatable">
                                     <thead>
                                        <tr>
                                            <th style="text-align:center;">#</th> 
                                            <th style="text-align:center;">Nama Sales</th>              
                                            <th style="text-align:center;">KD Order</th>
                                            <th style="text-align:center;">Outlet</th>
                                            <th style="text-align:center;">Produk</th>
                                            <th style="text-align:center;">Qty Order</th>
                                            <th style="text-align:center;">Total Value</th>
                                            <th style="text-align:center;">Satuan</th>
                                            <th style="text-align:center;">Waktu</th>
                                            <th style="text-align:center;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 1; ?>
                                        @foreach($data['content'] as $row)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{ $row->users->nama}}</td>
                                            <td>{{ $row->kd_to }}</td>
                                            <td><span style="color: green">
                                                <i class="fa fa-circle"></i> {{ $row->kd_outlet }}</span> 
                                                <br>{{ $row->outlets->nm_outlet}}
                                            </td>
                                            <td>{{ $row->other_produk }}</td>
                                            <td>{{ $row->qty_order }}</td>
                                            <td><span class="pull-left">Rp  </span><span class="pull-right"> {{ number_format($row->total_value)}}  </span></td>   
                                            <td>{{ $row->satuans->nama }}</td>
                                            <td>{{ date('d-F-Y H:i:s', strtotime($row->date_order)) }}</td>
                                            <td style="width: 150px">      
                                                @if(Auth::user()->hasAccess('admin.order.edit'))                              
                                               <a href="{!! route('admin.order.edit',[$row->id]) !!}" class="btn btn-xs btn-success text-red"><i class="fa fa-trash"></i></a> 
                                                @endif
                                                &nbsp;
                                                @if(Auth::user()->hasAccess('admin.order.delete'))     
                                                <a href="{!! route('admin.order.delete',[$row->id]) !!}" onclick="return confirm('Are you sure?')"  class="btn btn-xs btn-danger text-red"><i class="fa fa-trash"></i></a> 
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
    <!-- /Row -->

    @include('partials.footer')
    @section('custom-js')
    <link href="{{asset ('assets/theme/backend/vendors/bower_components/summernote/dist/summernote.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset ('assets/theme/backend/vendors/bower_components/switchery/dist/switchery.min.css')}}" rel="stylesheet" type="text/css" />


    <link href="{{asset ('assets/theme/backend/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- Moment JavaScript -->
    <script src="{{asset ('assets/theme/backend/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>   

    <script type="text/javascript">
        $(function () {
            $('.datetimepicker1').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#tableorder').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false
            });
        });
    </script>
    @endsection
</div>
</div>
<!-- /Main Content -->

@include('partials.sweetalert')

@push('message')
@include('partials.toastr')
@endpush


@endsection