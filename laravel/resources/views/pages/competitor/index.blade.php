@extends('layouts.master')

@section('title', 'Competitor Activity')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Competitor Activity management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Photo Activity</span></a></li>
                            <li class="active"><span>List Competitor Activity</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-primary panel-tabs card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-th"></i>&nbsp;&nbsp;Competitor List</h6>
                                </div>
                                <div class="pull-right">
                                    <div class="tab-struct custom-tab-1">
                                        <ul role="tablist" class="nav nav-tabs" id="myTabs_9">
                                            <li class="active panel-title txt-light" role="presentation">
                                                <a aria-expanded="true"  data-toggle="tab" role="tab" id="home_tab_9" href="#list"><p class="panel-title txt-light">Competitor List</p></a>
                                            </li>
                                            <li role="presentation" >
                                                <a data-toggle="tab" id="profile_tab_9" role="tab" href="#photo" aria-expanded="false"><p class="panel-title txt-light">Competitor Photo</p></a>
                                            </li>
                                        </ul>
                                    </div> 
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="tab-content" id="myTabContent_12">
                                <div id="photo" class="tab-pane fade" role="tabpanel">
                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body">
                                            <div class="table-wrap">

                                                @if(Auth::user()->hasAccess('admin.competitor.create'))
                                                    <a href="{!! route('admin.competitor.create') !!}" class="btn btn-primary"><i class="fa fa-plus">&nbsp;&nbsp;Tambah Photo Competitor</i></a>
                                                @endif

                                                <div class="table-responsive">
                                                    <table class="table table-hover datable_1 table-bordered display mb-30">
                                                        <thead>
                                                            <tr>
                                                                <th><center>KD Outlet</center></th>
                                                                <th><center>Nama Outlet</center></th>
                                                                <th><center>Nama Competitor</center></th>
                                                                <th><center>Sales Name</center></th>
                                                                <th><center>Nama Foto</center></th>
                                                                <th><center>Date Take Foto</center></th>
                                                                <th><center>Date Upload</center></th>
                                                                <th><center>Foto</center></th>
                                                                <th><center>Note</center></th>
                                                                @if(Auth::user()->kd_role != 3) 
                                                                  <th style="text-align:center;">Action</th>
                                                                @endif
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($data['competitorAct'] as $row)
                                                            <tr>
                                                                <td>{{ $row->kd_outlet }}</td>
                                                                <td>{{ $row->nm_outlet }}</td>
                                                                <td>{{ $row->nm_competitor }}</td>
                                                                <td>{{ $row->nama }}</td>
                                                                <td>{{ $row->nm_photo }}</td>
                                                                <td>{{ date('d-F-Y H:i:s', strtotime($row->date_take_photo)) }}</td>
                                                                <td>{{ date('d-F-Y H:i:s', strtotime($row->date_upload_photo)) }}</td>
                                                                @if(File::exists('image_upload/competitor/'.$row->path_photo) and $row->path_photo != '')
                                                                <td>
                                                                   <a target="_blank" href="{!! url('image_upload/competitor/'.$row->path_photo) !!}"> <img style="max-height: 60px; max-width: 60px"  src="{!! url('image_upload/competitor/'.$row->path_photo) !!}" class="img-responsive" /></a>
                                                                </td>

                                                                @else
                                                                <td><img style="max-height: 60px; max-width: 60px" src="{!! url('/img/no_image_outlet_upload.png') !!}" class="img-responsive" />
                                                                </td>                                    

                                                                @endif
                                                                <td> {{ $row->keterangan }}</td> 
                                                                @if(Auth::user()->kd_role != 3)
                                                                    <td>
                                                                        <center>
                                                                            <div class="button-list">
                                                                            @if(Auth::user()->hasAccess('admin.competitor.edit'))
                                                                            <a href="{!! route('admin.competitor.edit',[$row->id]) !!}" data-toggle="tooltip" title="Edit" class="btn btn-success btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-pencil" style="font-size: 10px;"></i></a>
                                                                            @endif
                                                                            @if(Auth::user()->hasAccess('admin.competitor.delete'))      
                                                                                <a href="{!! route('admin.competitor.delete',[$row->id]) !!}" id="delete" data-toggle="tooltip" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-trash" style="font-size: 10px;"></i></a>
                                                                            @endif
                                                                            </div>
                                                                        </center>
                                                                    </td>
                                                                @endif
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="list" class="tab-pane fade active in" role="tabpanel">
                                    <div class="panel-wrapper collapse in">
                                        <div class="panel-body">
                                            <div class="table-wrap">

                                                @if(Auth::user()->hasAccess('admin.comp.create'))
                                                    <a href="{!! route('admin.comp.create') !!}" class="btn btn-primary"><i class="fa fa-plus">&nbsp;&nbsp;Tambah Daftar Competitor</i></a>
                                                @endif

                                                <div class="table-responsive">
                                                    <table class="table table-hover datable_1 table-bordered display mb-30" >
                                                        <thead>
                                                            <tr>
                                                                <th>Nama Competitor</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($data['competitor'] as $row)
                                                            <tr>
                                                                <td>{{ $row->nm_competitor }}</td>
                                                                <td>
                                                                <center>
                                                                @if(Auth::user()->hasAccess('admin.comp.edit'))
                                                                    <a href="{!! route('admin.comp.edit',[$row->id]) !!}" class="btn btn-success btn-sm btn-icon-anim btn-square" style="display: unset;"><i class="fa fa-pencil" style="font-size: 14px;"></i></a>
                                                                @endif
                                                                &nbsp;
                                                                @if(Auth::user()->hasAccess('admin.comp.delete'))      
                                                                    <a href="{!! route('admin.comp.delete',[$row->id]) !!}" id="delete" class="btn btn-danger btn-sm btn-icon-anim btn-square" style="display: unset;"><i class="fa fa-trash" style="font-size: 14px;"></i></a>
                                                                @endif
                                                                </center>   
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush

@endsection