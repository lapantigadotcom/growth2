<div class="col-xs-6 col-sm-4 col-sm-offset-4">
	<div class="form-group">
		{!! Form::label('kd_outlet','Nama Outlet', array('class' => 'control-label mb-10')) !!}
		{!! Form::select('kd_outlet',$outlets, null, array('class' => 'selectpicker', 
	'data-style' => 'data-style="form-control btn-default btn-outline', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('kd_competitor','Nama Kompetitor', array('class' => 'control-label mb-10')) !!}
		{!! Form::select('kd_competitor',$competitors, null, array('class' => 'selectpicker', 
	'data-style' => 'data-style="form-control btn-default btn-outline', 'required')) !!}	
	</div>
	<div class="form-group">	
		{!! Form::label('nm_photo','Nama Foto', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('nm_photo',null, array('class' => 'form-control', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('jenis_photo','Jenis Photo', array('class' => 'control-label mb-10')) !!}
		{!! Form::select('jenis_photo',$tipe, null, array('class' => 'selectpicker', 
	'data-style' => 'data-style="form-control btn-default btn-outline', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('almt_comp_acitivity','Alamat Kompetitor', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('almt_comp_activity', null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('date_take_photo','Tanggal Take Photo', array('class' => 'control-label mb-10')) !!}
		<div class='input-group date' id='datetimepicker1'>
	        <input type='text' name="date_take_photo" class="form-control" />
	        <span class="input-group-addon">
	            <span class="glyphicon glyphicon-calendar"></span>
	        </span>
	    </div>
	</div>
	<div class="form-group">
		{!! Form::label('keterangan','Keterangan', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('keterangan',null, array('class' => 'form-control', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('path_photo','Upload Foto', array('class' => 'control-label mb-10')) !!}
		<input type="file" id="input-file-now" name="path_photo" class="dropify" />
		<span><p class="text-info mb-10">(*) Required</p></span>
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>

	@include('partials.errors')
</div>		

	<!-- Moment JavaScript -->
	<script type="text/javascript" src="/doodle/vendors/bower_components/moment/min/moment-with-locales.min.js"></script>
		
	<!-- Bootstrap Colorpicker JavaScript -->
	<script src="/doodle/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
	<!-- Bootstrap Datetimepicker JavaScript -->
	<script type="text/javascript" src="/doodle/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

	<script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
            	format: 'YYYY-MM-DD HH:mm:ss'
            });
        });
    </script>





