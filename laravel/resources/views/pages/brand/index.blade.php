@extends('layouts.master')

@section('title', 'Brand Produk List')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Brand Produk management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Brand Produk</span></a></li>
                            <li class="active"><span>List</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-user"></i>&nbsp;&nbsp;Brand List</h6>
                                </div>

                                @include('partials.panel')

                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        
                                        @if(Auth::user()->hasAccess('admin.brand.create'))  
                                            <a href="{!! route('admin.brand.create') !!}" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Brand</a>
                                            <br><br>
                                        @endif
                                     
                                        <div class="table-responsive">
                                            <table id="brand" class="table datable_1 table-hover table-bordered display mb-30">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align:center;">No</th>
                                                        <th style="text-align:center;">Nama Brand</th>
                                                        <th style="text-align:center;">Kode Brand</th>
                                                        <th style="text-align:center;">Created at</th> 
                                                        @if(Auth::user()->kd_role != 3)
                                                            <th style="text-align:center;">Action</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data as $key => $row)
                                                        <tr>
                                                        	<td><center>{{ $key+1 }}</center></td>
                                                            <td><center>{{ $row->nama }}</center></td>
                                                            <td><center>{{ $row->kode }} - 
                                                                <strong>{{ $row->id }}</strong>
                                                            </center></td>
                                                             
                                                               
							                                <td style="min-width:75px">
                                                                <center>{{ $row->created_at }}</center>
                                                            </td>
							                                
                                                            @if(Auth::user()->kd_role != 3)
                                                                <td style="min-width:58px">
                                                                    <center>
                                                                        <div class="button-list">
                                                                            <a href="{!! route('admin.brand.edit',[$row->id]) !!}" data-toggle="tooltip" title="Edit" class="btn btn-primary btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-pencil" style="font-size: 14px;"></i></a>

                                                                            <a href="{!! route('admin.brand.delete',[$row->id]) !!}" id="delete" data-toggle="tooltip" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-trash" style="font-size: 14px;"></i></a>
                                                                            
                                                                        </div>
                                                                    </center>
                                                                </td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->
         @section('custom-js')
       
        <script type="text/javascript">
        $(function() {
                $('#brand').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    </script>
        @endsection
        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush
        
@endsection