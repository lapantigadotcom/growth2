<div class="row-fluid sortable">       
   <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Laporan Competitor Activity</h2>
        </div>
        <div class="box-content">  
         @foreach($data['sales'] as $val)
                <h3>Sales Name : {!! $val->nama !!}</h3>
             @endforeach
             @foreach($data['area'] as $val)
                <h3>Area : {!! $val->nm_area !!}</h3>
             @endforeach  
                <h3>Periode : {!! $data['periode'] !!}</h3>        
             <table>
              <tbody>
                    <tr>               
                       <th>Kode Outlet</th>
                        <th>Nama Outlet</th>
                        <th>Nama Kompetitor</th>
                        <th>Tanggal Take Photo</th>
                        <th>Tanggal Upload</th>
                        <th>Nama Photo</th>
                        <th>Link Photo</th>
                         <th>Keterangan</th>
                    </tr>
                    @foreach($data['competitorAct'] as $row)
                    <tr>                         
                       <td>{{ $row->kd_outlet }}</td>
                        <td>{{ $row->nm_outlet }}</td>
                        <td>{{ $row->nm_competitor }}</td>
                        <td>{{ date('d-F-Y H:i:s', strtotime($row->date_take_photo))  }}</td>
                        <td>{{ date('d-F-Y H:i:s', strtotime($row->date_upload_photo)) }}</td> 
                        <td>{{ $row->nm_photo }}</td> 
                        <td><a href="{!! url('image_upload/competitor/'.$row->path_photo) !!}">{{ $row->path_photo }}</a></td>   
                         <td>{{ $row->keterangan }}</td>                          
                   </tr>
                   @endforeach  
                </tbody>
            </table>
        </div>
    </div>
</div>