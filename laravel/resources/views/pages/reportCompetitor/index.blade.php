<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>GROWTH | Report Competitor Activity</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
<body>
<div class="container-fluid-full">
    <div class="row-fluid"> 
<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">
@include('partials.sidebar')
</aside>

<!-- Main content -->
<div id="content" class="span10">
  <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="{{url('/admin/dashboard')}}">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li>
            <a href="#">Report</a>
            <i class="icon-angle-right"></i>
            
        </li>
        <li>
            <a href="#">Photo Activity</a>
        </li>
    </ul>
    <div class="row-fluid sortable">        
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Report Jumlah Photo Activity per Tipe</h2>
            </div><br>
                <div class="box-content">                                 
                    <table class="table table-striped table-bordered bootstrap-datatable datatable">
                        <thead>
                            <tr>
                                <th>Jenis Photo</th>
                                <th>Jumlah</th>
                            </tr>
                                
                        </thead>
                        <tbody>
                         
                            @foreach($data['jenis'] as $row)
                            <tr>
                                <td>{{ $row->nama_tipe }}</td>
                                <td>{{ $row->jumlah }}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td style="text-align:center;font-size: 18pt;">Total</td>
                                <td style="font-size: 18pt;">{{$data['total']}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div><!-- /.row (main row) -->
</div>
</div>

@include('partials.footer')
    </body>
</html>