@extends('layouts.master')

@section('title', 'Produk List')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Produk management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Master Data</span></a></li>
                            <li class="active"><span>Produk</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-shopping-bag"></i>&nbsp;&nbsp;Produk List</h6>
                                </div>
                                @include('partials.panel')
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        @if(Auth::user()->hasAccess('admin.produk.create')) 
                                            <a href="{!! route('admin.produk.create') !!}" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;Tambah Produk</a>
                                        @endif

                                        <div class="table-responsive">
                                            <table id="produk" class="table datable_1 table-hover table-bordered display mb-30">
                                              <thead>
                                                <tr>
                                                    <th width="30">#</th>
                                                    <th>Kode Produk</th>
                                                    <th>Nama Produk</th>
                                                    <th>Brand Produk</th>
                                                     <th>Divisi Produk</th>
                                                    <th>Harga Produk</th>
                                                    @if(Auth::user()->kd_role != 3)
                                                    <th style="text-align:center; min-width: 150px">Action</th>
                                                    @else
                                                    <th style="text-align:center;"></th>
                                                    @endif
                                                </tr>
                                            </thead>
                                            <tbody>
                                             @foreach($data['content'] as $key=>$row)
                                             <tr>
                                                <td><center>
                                                {{$key+1}}</center></td>
                                                <td>{{ $row->kd_produk }}</td>
                                                <td>{{ $row->nm_produk }}</td>
                                                <td>{{ $row->brand->nama }}</td>
                                                <td>{{ $row->areas->nm_area }}</td>
                                                <td><span class="pull-left">Rp. {{ $row->harga_produk }},-</span></td>
                                                <td>
                                                    <center>
                                                    @if(Auth::user()->hasAccess('admin.produk.history'))
                                                    <a href="{!! route('admin.produk.history',[$row->id]) !!}" class="icon-book"></a>
                                                    @endif
                                                    @if(Auth::user()->hasAccess('admin.produk.edit'))
                                                    <a href="{!! route('admin.produk.edit',[$row->id]) !!}" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>
                                                    @endif
                                                    &nbsp;
                                                    @if(Auth::user()->hasAccess('admin.produk.delete'))
                                                    <a href="{!! route('admin.produk.delete',[$row->id]) !!}" onclick="return confirm('Are you sure?')"  class="btn btn-xs btn-danger text-red"><i class="fa fa-trash"></i></a>
                                                    @endif
                                                    </center>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                        <tfoot>
                                           <tr>
                                            <th>Kode Produk</th>
                                            <th>Nama Produk</th>
                                             <th>Divisi Produk</th>
                                             <th>Harga Produk</th>
                                            @if(Auth::user()->kd_role != 3)
                                            <th style="text-align:center;">Action</th>
                                            @else
                                            <th style="text-align:center;"></th>
                                            @endif
                                        </tr>
                                    </tfoot>
                                </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

                @section('custom-js')
                  <script type="text/javascript">
                    $(document).ready(function() {
                        $('#produk').DataTable( {
                            "paging":   true,
                            "ordering": true,
                            "info":     true,
                            "autoWidth": true,
                            "search" :true
                        } );
                    } );
                </script>
                @endsection

            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush

@endsection