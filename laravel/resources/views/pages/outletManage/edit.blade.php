@extends('layouts.master')

@section('title', 'Create Toleransi')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Outlet management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/home/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Configuration</span></a></li>
                            <li><a href="{{url('/admin/outletManage')}}"><span>Outlet Management</span></a></li>
                            <li class="active"><span>Toleransi</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-wpforms"></i>&nbsp;&nbsp;Outlet Management</h6>
                                </div>
                                @include('partials.panel')
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-xs-4">
                <h3 class="box-title">
                  Target Visit
                </h3>
                {!! Form::open(array('route' => 'admin.outletManage.updateTvisit', 'method' => 'store', 'onsubmit'=>'return confirm("Apakah anda yakin?")' )) !!}
                
                
                @foreach($data["dist"] as $row)
                <div class="form-group">
                  {!! Form::label('target_visit_'.$row->id, $row->kd_dist.' :  '.$row->target_visit) !!}
                  {!! Form::text('target_visit_'.$row->id, $row->target_visit, ['class' => 'form-control','required'=>'required']) !!}

                </div>
                
                
                @endforeach
                <div class="form-group">
                  {!! Form::submit('Perbarui', array('class' => 'btn btn-primary')) !!}
                </div>
                

                
                {!! Form::close() !!}
              </div>
                                            <div class="form-wrap">
                                                <div class="col-xs-6 col-sm-4 col-sm-offset-4">
                                                 {!! Form::open(array('route' => 'admin.outletManage.updateToleransi', 'method' => 'store', 'onsubmit'=>'return confirm("Apakah anda yakin?")' )) !!}
                                                 <div class="form-group">
                                                    {!! Form::label('toleransi_long','Saat ini: '.$data["konfigurasi"]->toleransi_max) !!}

                                                    {!! Form::text('toleransi_long',null, ['class' => 'form-control','required'=>'required']) !!}


                                                </div>
                                                <div class="form-group">
                                                    {!! Form::submit('Perbarui', array('class' => 'btn btn-warning')) !!}
                                                </div>
                                                {!! Form::close() !!}
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Row -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-building-o"></i>&nbsp;&nbsp;List Tipe Outlet</h6>
                                </div>
                                @include('partials.panel2')
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_2" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        <div class="table-responsive">
                                            <table class="table table-hover datable_1 table-bordered display mb-30">
                                                <thead>
                                                    <tr>
                                                        <th>Tipe Outlet</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                     
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->

                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')


@endsection