@extends('layouts.master')

@section('link', '
    <link href="/doodle/vendors/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" type="text/css"/>
    <link href="/doodle/vendors/bower_components/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css"/>
    <link href="/doodle/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>')

@section('title', 'Edit User')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">User management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>User Manager</span></a></li>
                            <li><a href="{{url('/admin/user')}}"><span>User</span></a></li>
                            <li class="active"><span>Edit User</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-warning card-view">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-light"><i class="fa fa-wpforms"></i>&nbsp;&nbsp;Form Edit User</h6>
                                    </div>
                                    @include('partials.panel')
                                    <div class="clearfix"></div>
                                </div>
                                <div id="collapse_1" class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-wrap">

                                                    {!! Form::model($data['content'],array('route' => ['admin.user.update',$data['content']->id], 'method' => 'PUT', 'files' => true, 'data-toggle' => 'validator', 'role' => 'form')) !!}
												        @include('pages.user.form-edit',array('submit' => 'Perbarui'))
												    {!! Form::close() !!}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- /Row -->

                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

@endsection


@section('script', '
    <!-- Bootstrap Daterangepicker JavaScript -->
    <script src="/doodle/vendors/bower_components/dropify/dist/js/dropify.min.js"></script>
    <!-- Form Flie Upload Data JavaScript -->
    <script src="/doodle/dist/js/form-file-upload-data.js"></script>')