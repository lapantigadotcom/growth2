@extends('layouts.master')

@section('title', 'User List')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">User management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>User manager</span></a></li>
                            <li class="active"><span>Non SF</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-user"></i>&nbsp;&nbsp;User List</h6>
                                </div>
                                @include('partials.panel')
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        @if(Auth::user()->hasAccess('admin.user.create')) 
                                            <a href="{!! route('admin.user.create') !!}" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah User</a>
                                        @endif
                                        <br><br>
                                        <div class="table-responsive">
                                            <table id="tableuser" class="table datable_1 table-hover table-bordered display mb-30">
                                                <thead>
                                                    <tr>
                                                        <th><center>NIK</center></th>
                                                        <th><center>Nama</center></th>
                                                        <th><center>Alamat</center></th>
                                                        <th><center>Area</center></th>
                                                        <th><center>Telepon</center></th>
                                                        <th><center>Role</center></th>
                                                        @if(Auth::user()->kd_role != 3) 
                                                            <th style="min-width: 170px"><center>Action</center></th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data as $row)
                                                        <tr>
                                                            <td>{{ $row->nik }}</td>
                                                            <td>{{ $row->nama }}</td>
                                                            <td>{{ $row->alamat }}</td>
                                                            <td>{{ $row->nm_area }}</td>
                                                            <td>{{ $row->telepon }}</td>
                                                            <td>{{ $row->type_role }}</td>
                                                            @if(Auth::user()->kd_role != 3)
                                                                <td> 
                                                                    <center>
                                                                        <div class="button-list">
                                                                            <a href="{!! route('admin.user.show',[$row->id]) !!}" data-toggle="tooltip" title="Detail User" class="btn btn-primary btn-sm btn-icon-anim btn-square mr-0" style="display: unset;"><i class="fa fa-search-plus" style="font-size: 14px;"></i></a>
                                                                            @if(Auth::user()->hasAccess('admin.user.edit'))
                                                                                <a href="{!! route('admin.user.edit',[$row->id]) !!}" data-toggle="tooltip" title="Edit" class="btn btn-success btn-sm btn-icon-anim btn-square mr-0" style="display: unset;"><i class="fa fa-pencil" style="font-size: 14px;"></i></a>
                                                                            @endif
                                                                            @if(Auth::user()->hasAccess('admin.user.delete'))      
                                                                                <a href="{!! route('admin.user.delete',[$row->id]) !!}" id="delete" data-toggle="tooltip" onclick="return confirm('Are you sure you want to delete this item?');" title="Delete" class="btn btn-danger btn-sm btn-icon-anim btn-square mr-0" style="display: unset;"><i class="fa fa-trash" style="font-size: 14px;"></i></a>
                                                                            @endif
                                                                        </div>
                                                                    </center>    
                                                                </td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')
                 @section('custom-js')
     
        <script type="text/javascript">
        $(function() {
                $('#tableuser').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    </script>
        @endsection
            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush

@endsection