@extends('layouts.master')

@section('title', 'Edit Role')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Role management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Configuration</span></a></li>
                            <li><a href="{{url('/admin/role')}}"><span>Role</span></a></li>
                            <li class="active"><span>Edit Role</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-wpforms"></i>&nbsp;&nbsp;Edit Role</h6>
                                </div>
                                @include('partials.panel')
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">

                                    {!! Form::model($data['content'], array('route' => ['admin.role.update',$data['content']->kd_role], 'method' => 'PUT')) !!}

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-wrap">
                                                <div class="col-md-3">
                                                    <div class="form-group">

                                                        {!! Form::label('type_role','Nama Role', array('class' => 'control-label mb-10')) !!}
                                                        {!! Form::text('type_role',null, array('class' => 'form-control')) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row pa-15">
                                        <h3><center>Admin Menu</center></h3>
                                        @foreach($data['adminmenu'] as $key => $value)
                                            <div class="icon-container col-sm-6 col-md-4 col-lg-3 pa-5 pl-20">
                                                <div class="checkbox checkbox-primary ">
                                                    <input id="checkbox2" type="checkbox" id="menuadmin" name="adminmenu[]" value="{{ $key }}" {{ in_array($key, $data['adminmenu_t'])?'checked':'' }} > 
                                                    <label for="checkbox2">&nbsp;{!! $value !!}</label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="row pa-10">
                                        <h3><center>Permission</center></h3>
                                        @foreach($data['permission'] as $key => $value)
                                            <div class="icon-container col-sm-6 col-md-4 col-lg-3 pa-5 pl-20">
                                                <div class="checkbox checkbox-primary ">
                                                    <input id="checkbox2" type="checkbox" id="routeadmin" name="permission[]" value="{{ $key }}" {{ in_array($key, $data['permission_t'])?'checked':'' }} > 
                                                    <label for="checkbox2">&nbsp;{!! $value !!}</label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="row pa-30 pb-0">
                                        <center>
                                            <input type="button" id="check" class="btn btn-primary" value="Check All" />
                                            <input type="button" id="uncheck" class="btn btn-primary" value="UnCheck All" />
                                        </center>
                                    </div>

                                    <div class="row pa-10">
                                        <center>
                                            {!! Form::submit('Perbarui', array('class' => 'btn btn-success')) !!}
                                        </center>
                                    </div>

                                </div>
                                  
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Row -->

                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

         <script>
            $(document).ready( function() {
                $('#check').on('click', function () {
                    var allInputs = document.getElementsByTagName("input");
                    for (var i = 0, max = allInputs.length; i < max; i++){
                        if (allInputs[i].type === 'checkbox')
                            allInputs[i].checked = true;
                    }
                });
                $('#uncheck').on('click', function () {
                    var allInputs = document.getElementsByTagName("input");
                    for (var i = 0, max = allInputs.length; i < max; i++){
                        if (allInputs[i].type === 'checkbox')
                            allInputs[i].checked = false;
                    }
                });
            });
        </script>

@endsection


       