@extends('layouts.master')

@section('title', 'Role List')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Role management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Configuration</span></a></li>
                            <li class="active"><span>Role</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-lock"></i>&nbsp;&nbsp;Role List</h6>
                                </div>
                                @include('partials.panel')
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        
                                        <a href="{!! route('admin.role.create') !!}" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Role</a>
                                      

                                        <div class="table-responsive">
                                            <table id="role" class="table datable_1 table-hover table-bordered display mb-30">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Role</th>
                                                        @if(Auth::user()->kd_role != 3) 
                                                            <th>Action</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data as $row)
                                                        <tr>
                                                            <td>{{ $row->kd_role }}</td>
                                                            <td>{{ $row->type_role }}</td>  
                                                            @if(Auth::user()->kd_role != 3)   
                                                                <td>
                                                                    <center>
                                                                        <div class="button-list">
                                                                            <a href="{!! route('admin.role.edit',[$row->kd_role]) !!}" data-toggle="tooltip" title="Edit" class="btn btn-success btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-pencil" style="font-size: 14px;"></i></a>    
                                                                            <a href="{!! route('admin.role.delete',[$row->kd_role]) !!}" onclick="return confirm('Are you sure you want to delete this item?');" id="delete" data-toggle="tooltip" title="Delete" class="btn btn-danger btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-trash" style="font-size: 14px;"></i></a>
                                                                        </div>
                                                                    </center>      
                                                                </td>
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')
                @section('custom-js')
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('#role').DataTable( {
                            "paging":   true,
                            "ordering": true,
                            "info":     true,
                            "autoWidth": true,
                            "search" :true
                        } );
                    } );
                </script>
                @endsection
            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush

@endsection