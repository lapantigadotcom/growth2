@extends('layouts.master')

@section('link', '
    <link href="/doodle/vendors/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" type="text/css"/>')

@section('title', 'Export File')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Export  management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/home/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li class="active"><span>Export File</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-warning card-view panel-refresh">
                                <div class="refresh-container">
                                    <div class="la-anim-1"></div>
                                </div>
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-light"><i class="fa fa-download"></i>&nbsp;&nbsp;Download Master Data PMP</h6>
                                    </div>
                                    
                                    @include('partials.panel')

                                    <div class="clearfix"></div>
                                </div>
                                <div id="collapse_1" class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            
                                            <div class="col-md-2">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-green">
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <a href="{{url('admin/download/area')}}">
                                                                            <div class="text-center data-wrap-right">
                                                                                <i class="fa fa-map txt-light data-right-rep-icon"></i>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <span class="txt-light block pa-0 pb-10">
                                                                                    <strong>Area</strong>
                                                                                </span>
                                                                            </div>
                                                                        </a>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-red">
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <a href="{{url('admin/download/outlet')}}">
                                                                            <div class="text-center data-wrap-right">
                                                                                <i class="fa fa-building txt-light data-right-rep-icon"></i>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <span class="txt-light block pa-0 pb-10">
                                                                                    <strong>Outlet</strong>
                                                                                </span>
                                                                            </div>
                                                                        </a>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             
                                            <div class="col-md-2">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-yellow">
                                                                <div class="container-fluid">
                                                                    <div class="row">
                                                                        <a href="{{url('admin/download/produk')}}">
                                                                            <div class="text-center data-wrap-right">
                                                                                <i class="fa fa-product-hunt txt-light data-right-rep-icon"></i>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <span class="txt-light block pa-0 pb-10">
                                                                                    <strong>Produk</strong>
                                                                                </span>
                                                                            </div>
                                                                        </a>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             
                                            <div class="col-md-2">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-green">
                                                                <div class="container-fluid">
                                                                    <div class="row" style="background: darkslategrey">
                                                                        <a href="{{url('admin/download/visitMonitor')}}">
                                                                            <div class="text-center data-wrap-right">
                                                                                <i class="fa fa-map-marker txt-light data-right-rep-icon"></i>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <span class="txt-light block pa-0 pb-10">
                                                                                    <strong>Visit Monitor</strong>
                                                                                </span>
                                                                            </div>
                                                                        </a>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             
                                            <div class="col-md-2">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-yellow">
                                                                <div class="container-fluid">
                                                                    <div class="row" style="background: blueviolet">
                                                                        <a href="{{url('admin/download/salesForce')}}">
                                                                            <div class="text-center data-wrap-right">
                                                                                <i class="fa fa-group txt-light data-right-rep-icon"></i>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <span class="txt-light block pa-0 pb-10">
                                                                                    <strong>Sales</strong>
                                                                                </span>
                                                                            </div>
                                                                        </a>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="panel panel-default card-view pa-0">
                                                    <div  class="panel-wrapper collapse in">
                                                        <div  class="panel-body pa-0">
                                                            <div class="sm-data-box bg-yellow">
                                                                <div class="container-fluid">
                                                                    <div class="row" style="background: #444">
                                                                        <a href="{{url('admin/download/nonSF')}}">
                                                                            <div class="text-center data-wrap-right">
                                                                                <i class="fa fa-keyboard-o txt-light data-right-rep-icon"></i>
                                                                            </div>
                                                                            <div class="text-center">
                                                                                <span class="txt-light block pa-0 pb-10">
                                                                                    <strong>Non Sales</strong>
                                                                                </span>
                                                                            </div>
                                                                        </a>
                                                                    </div>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             

                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                <!-- /Row -->
 
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

        <script type="text/javascript">
        $(document).ready(function() {
            $('#upload').bind("click",function() 
            { 
                var upVal = $('#fileUp').val(); 
                if(upVal=='') 
                { 
                    alert("Empty input file");
                } 
            }); 
        });
        </script> 

@endsection

@section('script', '
    <!-- Bootstrap dropify JavaScript -->
    <script src="/doodle/vendors/bower_components/dropify/dist/js/dropify.min.js"></script>
    <!-- Form Flie Upload Data JavaScript -->
    <script src="/doodle/dist/js/form-file-upload-data.js"></script>')