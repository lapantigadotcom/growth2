 
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v8foenQT317Cf6Q3Lvv-Avcl2dqaaBo&libraries=places"></script> 
</script>
        
      
        <script type="text/javascript">

             var default_lat = {{ $data['content']->latitude }};
            var default_lng = {{ $data['content']->longitude }};
            var default_map_zoom = 15;

            function init() {
               var map = new google.maps.Map(document.getElementById('map-canvas'), {
                 center: {
                   lat: default_lat,
                   lng: default_lng
                 },
                 zoom: default_map_zoom
               });

               var marker = new google.maps.Marker({
                    position: {
                        lat: default_lat,
                        lng: default_lng
                    },
                    map: map,
                    draggable: true
                });

               var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));
               map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('pac-input'));
               google.maps.event.addListener(searchBox, 'places_changed', function() {
                 searchBox.set('map', null);


                 var places = searchBox.getPlaces();

                 var bounds = new google.maps.LatLngBounds();
                 var i, place;
                 for (i = 0; place = places[i]; i++) {
                   (function(place) {
                     var marker = new google.maps.Marker({

                       position: place.geometry.location,
                       draggable: true
                     });
                     marker.bindTo('map', searchBox, 'map');
                     google.maps.event.addListener(marker, 'map_changed', function() {
                       if (!this.getMap()) {
                         this.unbindAll();
                       }
                       var lat = marker.getPosition().lat();
                            var lng = marker.getPosition().lng();

                            $('#lat').val(lat);
                            $('#lng').val(lng);
                     });
                       google.maps.event.addListener(marker, 'position_changed', function(){
                            var lat = marker.getPosition().lat();
                            var lng = marker.getPosition().lng();

                            $('#lat').val(lat);
                            $('#lng').val(lng);
                        });

                     bounds.extend(place.geometry.location);


                   }(place));

                 }
                 map.fitBounds(bounds);
                 searchBox.set('map', map);
                 map.setZoom(Math.min(map.getZoom(),default_map_zoom));

               });

                google.maps.event.addListener(marker, 'position_changed', function(){
                    var lat = marker.getPosition().lat();
                    var lng = marker.getPosition().lng();

                    $('#lat').val(lat);
                    $('#lng').val(lng);
                });
             }
                
            google.maps.event.addDomListener(window, 'load', init);
            
        </script>