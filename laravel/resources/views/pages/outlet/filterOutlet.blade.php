 
<div class="box-body">
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			<div class="box-header with-border">
				<h5 class="box-title">Filter Data</h5>
				
			</div>
			<div class="box-body">
				<div class="form-group row">
					<div class="col-sm-2 col-md-2 col-lg-2"> 
						{!! Form::select('kd_area',$data['areas'], null, array('id'=>'area','class' => 'selects form-control')) !!}
					</div>
					<div class="col-sm-4 col-md-4 col-lg-4"> 
						{!! Form::select('sales',$data['sales'], null, array('id'=>'sales','class' => 'selects form-control')) !!}
					</div> 
					<div class="col-sm- col-md-2 col-lg-2"> 
						{!! Form::submit('Show', array('name'=>'show','class' => 'btn btn-primary' , 'id' => 'show')) !!}
					</div>
					<div class="col-sm-1 col-md-2 col-lg-2">
						@if(Auth::user()->hasAccess('admin.download.outlet'))
						<a href="{{ route('admin.download.outlet') }}" class="btn btn-danger"> 
							<i class="fa fa-download"></i> Download</a>
							@endif
						</div> 
					</div>

				</div>
			</div>
		</div>
	</div>

	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script>
		$(window).load(function() {
		$('form').get(0).reset(); //clear form data on page load
	});
</script>
<script> 
	$('#area').on('change', function(e) {
		console.log(e);
		var id = e.target.value;

		$.getJSON('{{url('/ajax-sales?id=')}}'+id, function (data) {
			$('#sales').empty();
			$('#sales').append('<option value="">All Sales</option>');
			$.each(data, function(index, salesObj){
				$('#sales').append('<option value="'+salesObj.id+'">'+salesObj.nama+'</option>');
			});
		});
	});
</script>