@extends('layouts.master')

 
@section('title', 'Detail Outlet')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Outlet management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/home/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Master Data</span></a></li>
                            <li><a href="{{url('/admin/outlet')}}"><span>Outlet</span></a></li>
                            <li class="active"><span>Detail Outlet</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-list"></i>&nbsp;&nbsp;Detail Outlet : 
                                   {!! $data['content']->nm_outlet!!}</h6>
                                </div>
                                @include('partials.panel')
                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-wrap">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-body">
                                                        <div class="row">

                                                            <div class="col-md-4">
                                                               @if(File::exists('image_upload/outlet/'.$data['content']->foto_outlet) and $data['content']->foto_outlet != '')
                                                                 <center>
                                                                     <img src="{{  asset('/image_upload/outlet/'.$data['content']->foto_outlet) }}" width="208" /></center>
                                                                @else
                                                                <center>
                                                                  <img src="{{ asset('/img/no_image_outlet_upload.png') }}" width="150" /></center>
                                                                @endif
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-user pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h6 class="form-control-static">Sales Name : {!! $data['content']->nama !!} </h6>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-barcode pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h6 class="form-control-static">Kode Outlet : {!! $data['content']->kd_outlet !!} </h6>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-th pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h6 class="form-control-static">Area : {!! $data['content']->kd_area !!} </h6>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-home pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h6 class="form-control-static">Alamat : {!! $data['content']->almt_outlet !!} </h6>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-briefcase pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h6 class="form-control-static">Kota : {!! $data['content']->nm_kota !!} </h6>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-envelope pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h6 class="form-control-static">Kode Pos : {!! $data['content']->kodepos !!} </h6>
                                                                    </div>
                                                                </div>
                                                               
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-user pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h6 class="form-control-static">Nama PIC : {!! $data['content']->nm_pic !!} </h6>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-phone pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h6 class="form-control-static">Telepon PIC : {!! $data['content']->tlp_pic !!} </h6>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-list-alt pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h6 class="form-control-static">Tipe Outlet : {!! $data['content']->nm_tipe !!} </h6>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-star pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h6 class="form-control-static">Rank : {!! $data['content']->rank_outlet !!} </h6>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-map-marker pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        @if($data['content']->longitude != '')
                                                                            <h6 class="form-control-static">Longitude : {!! $data['content']->longitude !!} </h6>
                                                                        @else
                                                                            <h6>Undefined</h6>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-map-marker pull-right"></i></label>
                                                                    <div class="col-md-9">

                                                                        @if($data['content']->latitude != '')
                                                                            <h6 class="form-control-static">Latitude : {!! $data['content']->latitude !!} </h6>
                                                                        @else
                                                                            <h6 class="form-control-static">Latitude : Undefined</h6>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-plus pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        @if($data['content']->toleransi_long != '')
                                                                            <h6 class="form-control-static">Toleransi Max : {!! $data['content']->toleransi_long !!} </h6>
                                                                        @else
                                                                            <h6 class="form-control-static">Toleransi Max : Undefined</h6>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-signal pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        @if($data['content']->status_area = 0)
                                                                            <h6 class="form-control-static">Status Outlet : Bad Coverage </h6>
                                                                        @else
                                                                            <h6 class="form-control-static">Status Outlet : Good Coverage</h6>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

@endsection

@section('script', '
    <!-- Bootstrap Daterangepicker JavaScript -->
    <script src="/doodle/vendors/bower_components/dropify/dist/js/dropify.min.js"></script>
    <!-- Form Flie Upload Data JavaScript -->
    <script src="/doodle/dist/js/form-file-upload-data.js"></script>')