{
	"data": [
	<?php $i = 0;
$len = count($data);?>
	@foreach($data as $row)
		["{{ route('admin.outlet.show',[$row->kd_outlet]) }}", "{{$row->nm_outlet}}", "{{$row->almt_outlet}}", "{{$row->kodepos}}", "{{$row->nm_pic}}", "{{$row->nama}}", "{{$row->tlp_pic}}", "{{$row->nm_tipe}}", "{{$row->kd_area}}", "{{$row->nm_kota}}", 

		@if(Auth::user()->kd_role != 3)
			@if(Auth::user()->hasAccess('admin.outlet.edit'))
				"{{ route('admin.outlet.edit',[$row->kd_outlet]) }}", 
			@endif
			@if(Auth::user()->hasAccess('admin.outlet.delete')) 
				"{{ route('admin.outlet.delete',[$row->kd_outlet]) }}"
			@endif
		@endif

		]
		@if($i < $len - 1)
        	,
    	@endif
    	<?php $i++;?>
	@endforeach
	]
}