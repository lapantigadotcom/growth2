<div class="row-fluid" style="background-color: #fff">
    <div class="span3">
        <div class="box-body">
            <div class="form-group">
              {!! Form::label('kd_dist', 'Outlet Distributor', array('class' => 'control-label mb-10')) !!}
              {!! Form::select('kd_dist', $dist, null, array('class' => 'form-control', 
              'data-style' => 'form-control mb-10 btn-default btn-outline', 'required')) !!}
               {!! Form::label('kd_area', 'Outlet Area', array('class' => 'control-label mt-10')) !!}
                    {!! Form::select('kd_area',$areas, null, array('id' => 'id_area', 'class' => 'form-control', 
                    'data-style' => 'form-control btn-default btn-outline', 'required')) !!}
                {!! Form::label('kd_kota','Nama Kota',array('class' => 'control-label mt-10')) !!}
                {!! Form::select('kd_kota',$city, null, array('id'=>'kota', 'class' => 'form-control mb-10')) !!}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="box-body">
            <div class="form-group">
                    {!! Form::label('kd_user', 'Registered Sales Name', array('class' => 'control-label mb-10')) !!}
                    {!! Form::select('kd_user', $users, null, array('class' => 'form-control', 
                    'data-style' => 'form-control btn-default btn-outline')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('nm_outlet','Nama Outlet', array('class' => 'control-label mb-10')) !!}
                    {!! Form::text('nm_outlet',null, array('class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('kode','Kode Outlet', array('class' => 'control-label mb-10')) !!}
                    {!! Form::text('kode',null, array('class' => 'form-control')) !!}
                </div>
                 <div class="form-group">
                    {!! Form::label('nm_pic','Nama PIC', array('class' => 'control-label mb-10')) !!}
                    {!! Form::text('nm_pic',null, array('class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('tlp_pic','Telepon PIC', array('class' => 'control-label mb-10')) !!}
                    {!! Form::text('tlp_pic',null, array('class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('kodepos','Kode Pos', array('class' => 'control-label mb-10')) !!}
                    {!! Form::text('kodepos',null, array('class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('almt_outlet','Alamat', array('class' => 'control-label mb-10')) !!}
                    {!! Form::textarea('almt_outlet',null, array('rows'=>'3', 'class' => 'form-control')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('kd_tipe','Tipe Outlet', array('class' => 'control-label mb-10')) !!}
                    {!! Form::select('kd_tipe',$tipe, null, array('class' => 'form-control', 
                    'data-style' => 'form-control btn-default btn-outline', 'required')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('rank_outlet','Rank Outlet', array('class' => 'control-label mb-10')) !!}
                    {!! Form::select('rank_outlet', [
                               'A' => 'A',
                               'B' => 'B',
                               'C' => 'C',
                               'D' => 'D',
                               'E' => 'E',
                               'F' => 'F',
                            ], null, array('class' => 'form-control', 'data-style' => 'form-control btn-default btn-outline')) !!} 
                </div>
                <div class="form-group">
                    {!! Form::label('reg_status','Register Status', array('class' => 'control-label mb-10')) !!}
                    {!! Form::select('reg_status', [
                                   'YES' => 'YES',
                                   'NO' => 'NO',
                                   ], null, array('class' => 'form-control', 'data-style' => 'form-control btn-default btn-outline')) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('status_area','Status Area', array('class' => 'control-label mb-10')) !!}
                    {!! Form::select('status_area',[
                           '1' => 'Good Coverage',
                           '0' => 'Bad Coverage',
                           ], null, array('class' => 'form-control', 'data-style' => 'form-control btn-default btn-outline')) !!}   
                </div>
        </div>
    </div>
     <fieldset>
    <!--CREDIT CART PAYMENT-->
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label class="control-label">Lokasi Outlet</label>
                <input type="text" id="pac-input" placeholder="Masukkan alamat outlet" style="width: 300px; height: 35px; text-align: center;">
            </div>
            <div class="form-group">
                <div id="map-canvas" style="width:100%;height:500px;"></div>
            </div>
            <div class="form-group">
                {!! Form::label('latitude','Latitude', array('class' => 'control-label mb-10')) !!}
                {!! Form::text('latitude',null, array('id' => 'lat', 'class' => 'form-control', 'readonly')) !!}
            </div>
            <div class="form-group">
                {!! Form::label('longitude','Longitude', array('class' => 'control-label mb-10')) !!}
                {!! Form::text('longitude',null, array('id' => 'lng', 'class' => 'form-control', 'readonly')) !!}
            </div>
        </div>
    </div>
    <!--CREDIT CART PAYMENT END-->
</fieldset>
    <div class="span3">
        <div class="box-body">
            <div class="form-group">
               
                     <div class="form-group">
                       {!! Form::label('foto','Upload Foto', array('class' => 'control-label mb-10')) !!}
                        <input type="file" id="input-file-now-custom-2" name="path_photo" class="dropify" data-height="300" data-default-file="{{ asset('/image_upload/outlet/'.$data['content']->foto_outlet) }}" style="height: inherit;"/>
                    </div>
                  <div class="form-group">
                    @if (count( $errors ) > 0)
                    <div class="alert alert-error">
                        @foreach ($errors->all() as $error)
                        {!! $error !!}<br />        
                        @endforeach
                    </div>
                    @endif

                 </div>
                 <div class="form-group">
       <button class="btn btn-success" onclick="return confirm('Are you sure you want to edit this outlet?');" type="submit">Submit</button>
    </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#area').on('change', function(e) {
        console.log(e);
        var id = e.target.value;
        //ajax
        $.getJSON('{{url('/ajax-city?id=')}}'+id, function (data) {
            // console.log(data);
            $('#city').empty();
            $.each(data, function(index, cityObj){
                $('#city').append('<option value="'+cityObj.id+'">'+cityObj.nm_kota+'</option>');
            });
        });
        $.getJSON('{{url('/ajax-sales?id=')}}'+id, function (data) {
            //console.log(data);
            $('#sales').empty();
            $.each(data, function(index, salesObj){
                console.log(salesObj.nama);
                $('#sales').append('<option value="'+salesObj.id+'">'+salesObj.nama+'</option>');
            });
        });
    });

    
</script>
