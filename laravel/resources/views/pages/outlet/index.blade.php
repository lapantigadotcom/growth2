@extends('layouts.master')

@section('title', 'Outlet List')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">outlet management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Master Data</span></a></li>
                            <li class="active"><span>Outlet</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-building-o"></i>&nbsp;&nbsp;Outlet List</h6>
                                </div>
                                <div class="pull-right">
                                    
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="panel-wrapper collapse in">

                                <div class="panel-body">
                                    @if(Auth::user()->hasAccess('admin.outlet.create'))
                                    <a href="{!! route('admin.outlet.create') !!}" class="btn btn-success  "><i class="fa fa-plus"></i> Add Outlet</a>
                                    @endif
                                    <br><br>
                                    <div class="table-wrap">
                                    

                                        <form id="filter">
                                        @include('pages.outlet.filterOutlet')
                                     </form>
                                        <div class="table-responsive">
                                            
                                                <table id="dataoutlet" class="table datable_1 table-hover table-bordered display mb-30">
                                                    <thead>
                                                        <tr>
                                                            <th>View</th>
                                                            <th>ID Outlet</th>
                                                            <th>Nama Outlet</th>
                                                            <th>Alamat </th>
                                                            <th>Kota</th>
                                                            <th>Area</th>
                                                            <th>Jenis Outlet</th>
                                                            <th>CP Outlet</th>
                                                            <th>Sales Name</th> 
                                                            <th>Action</th> 
                                                        </tr>
                                                    </thead>
                                                 
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->

            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')
        @section('custom-css')
        <!-- customcss -->
      
<link rel="stylesheet" href="{{asset ('assets/theme/backend/vendors/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

 

        @push('scripts')

 <script src="{{url('assets/theme/backend/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/theme/backend/vendors/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
 

 
@if(Auth::user()->hasAccess('admin.outlet.edit'))
    <script>
        var edit = "{!! route('admin.outlet.edit',0) !!}"
    </script>
@else
    <script>
        var edit = "";
    </script>
@endif

@if(Auth::user()->hasAccess('admin.outlet.delete'))
    <script>
        var hapus =  "{!! route('admin.outlet.delete',0) !!}"
    </script>
@else
    <script>
        var hapus =  "";
    </script>
@endif

<script>
    show = '{!! route('admin.outlet.show',0) !!}'
    bshow = '" data-toggle="tooltip" data-placement="bottom" title="Lihat Detail Outlet"><i class="fa fa-search-plus text-hijau"></a>'
    adel = '<a href="'
    bdel =  '" onclick="return confirm('+"'Are You Sure'"+')" class="fa fa-trash text-merah"></a>';
    aed = '<a href="'
    bed = '" class="fa fa-pencil text-orange"></a> &nbsp;'

   // $(function() {
        ashow='<a href="'
        var table = $('#dataoutlet').DataTable({
            serverSide: true,
            processing: true,
            searching: true,
            ajax: {
                url : '{!! route('admin.outlet.ajax') !!}',
                dataType : 'json',
                type : 'POST',
                data  : function( d ) {
                  d._token      = "{{ csrf_token() }}";
                  d.area        = $('#area').val();
                  d.sales       = $('#sales').val();
                  d.tipe        = $('#tipe').val();
                  d.distributor = $('#distributor').val();
                }
            },
            columns: [
                 { 
                    "data": "kd_outlet",
                    "render": function ( data, type, full ) 
                    {
                        send =  ashow+show.slice(0, -1)+data+bshow;
                        return send;
                    }
                },
                { data: 'kd_outlet', name: 'outlet.kd_outlet' },
                 { data: 'nm_outlet', name: 'outlet.nm_outlet' },
                { data: 'almt_outlet', name: 'outlet.almt_outlet' },
                { data: 'nm_kota', name: 'kota.nm_kota'},
                { data: 'kd_area', name: 'area.kd_area'},
                { data: 'nm_tipe', name: 'tipe.nm_tipe'},
                { data: 'nm_pic', name: 'outlet.nm_pic'},
                { data: 'nama', name: 'user.nama'},
                { 
                    "data": "kd_outlet",
                    "render": function ( data, type, full ) 
                    {
                            send =''
                            if(edit!=''){
                                send =  aed+edit.slice(0, -6)+data+"/edit"+bed;
                            }
                            if(hapus!=''){
                                send = send+adel+hapus.slice(0, -8)+data+"/delete"+bdel;
                            }
                            return send;
                    }
                }
            ]
            ,
            sDom: "<'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-sm-12 col-md-6 col-lg-6'i><'col-sm-12 col-md-6 col-lg-6 center'p>>",
            oLanguage: 
            {
                sLengthMenu: "_MENU_ records per page"
            }
        });
   // });

    var el = document.getElementById('filter');
    el.addEventListener("submit", function(e){
        table.ajax.reload()
        e.preventDefault();
    });

</script>
 @endpush
 @endsection