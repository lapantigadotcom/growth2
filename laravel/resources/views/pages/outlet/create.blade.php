@extends('layouts.master')

 

@section('title', 'Create Outlet')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">outlet management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/home/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Master Data</span></a></li>
                            <li><a href="{{url('/admin/outlet')}}"><span>Outlet</span></a></li>
                            <li class="active"><span>Create Outlet</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->
                
                <!-- Row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-warning card-view">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-light"><i class="fa fa-wpforms"></i>&nbsp;&nbsp;Form Create Outlet</h6>
                                    </div>
                                    @include('partials.panel')
                                    <div class="clearfix"></div>
                                </div>
                                <div id="collapse_1" class="panel-wrapper collapse in">
                                	<div class="panel-body">
                                		{!! Form::open(array('route' => 'admin.outlet.store', 'method' => 'POST','files' => true)) !!}
                                		@include('pages.outlet.form',array('submit' => 'Simpan'))
                                		{!! Form::close() !!}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->
       
@endsection 

@section('custom-css')
<link href="{{asset ('assets/theme/backend/vendors/bower_components/jquery.steps/demo/css/jquery.steps.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset ('assets/theme/backend/vendors/bower_components/dropify/dist/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />


@endsection
 @section('custom-js')

 
 <!-- Form Wizard JavaScript -->
 <script src="{{asset ('assets/theme/backend/vendors/bower_components/jquery.steps/build/jquery.steps.min.js')}}"></script>
 <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>
 <!-- Form Wizard Data JavaScript -->
 <script src="{{asset ('assets/theme/backend/dist/js/form-wizard-data.js')}}"></script>

 <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

 <script>
    $(document).ready(function(){
        $("#id_area").change(function(e){
                    //alert("Hello! I am an alert box!");
                    //console.log(e);
                    var id_area = e.target.value;
                    //ajax
                    $.getJSON('{{url('/ajax-city?id=')}}'+id_area, function (data) {
                    //$.get('/ajax-city?id=' + id_area, function(data){
                        // console.log(data);
                        $('#city').empty();
                        $.each(data, function(index, cityObj){
                            $('#city').append('<option value="'+cityObj.id+'">'+cityObj.nm_kota+'</option>');
                        });
                    });
                });
    });
</script>
 
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6v8foenQT317Cf6Q3Lvv-Avcl2dqaaBo&libraries=places"></script> 
</script>
        
      
        <script type="text/javascript">
      function init() {
          var map = new google.maps.Map(document.getElementById('map-canvas'), {
           center: {
             lat: -7.257472,
             lng: 112.752088
           },
           zoom: 15
         });
         var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));
         map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('pac-input'));
         google.maps.event.addListener(searchBox, 'places_changed', function() {
           searchBox.set('map', null);

           var places = searchBox.getPlaces();
           var bounds = new google.maps.LatLngBounds();
           var i, place;
           for (i = 0; place = places[i]; i++) {
             (function(place) {
               var marker = new google.maps.Marker({
                 position: place.geometry.location,
                 draggable: true
               });
               marker.bindTo('map', searchBox, 'map');
               google.maps.event.addListener(marker, 'map_changed', function() {
                 if (!this.getMap()) {
                   this.unbindAll();
                 }
                 var lat = marker.getPosition().lat();
              var lng = marker.getPosition().lng();

              $('#lat').val(lat);
              $('#lng').val(lng);
               });
                 google.maps.event.addListener(marker, 'position_changed', function(){
                var lat = marker.getPosition().lat();
              var lng = marker.getPosition().lng();
              $('#lat').val(lat);
              $('#lng').val(lng);
            });
               bounds.extend(place.geometry.location);
             }(place));

           }
           map.fitBounds(bounds);
           searchBox.set('map', map);
           map.setZoom(Math.min(map.getZoom(),15));

         });
       }
       google.maps.event.addDomListener(window, 'load', init);
    </script>
<!-- Bootstrap Daterangepicker JavaScript -->
<script src="{{asset ('assets/theme/backend/vendors/bower_components/dropify/dist/js/dropify.min.js')}}"></script>
<script src="{{asset ('assets/theme/backend/dist/js/form-file-upload-data.js')}}"></script>

     
@endsection 
