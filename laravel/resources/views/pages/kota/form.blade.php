<div class="col-xs-6 col-sm-4 col-sm-offset-4">
	<div class="form-group">
		{!! Form::label('kd_area', 'Kode Area', array('class' => 'control-label mb-10')) !!}
	     {!! Form::select('kd_area', $data['area'], null, ['class'=> 'form-control', 'required'=>'required', 'disabled'=>'true'])!!}
	</div>
	<div class="form-group">
	    {!! Form::label('nm_area','Nama Area : ', array('class' => 'control-label mb-10')) !!}
	    <div id="namaArea" name="namaArea" class="control-label mb-10"></div>
	</div>
	<div class="form-group">
		{!! Form::label('nm_kota','Nama Kota', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('nm_kota',null, array('class' => 'form-control', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script>
		$('#area').on('change', function(e) {
	        console.log(e);
	        var id = e.target.value;
	        //ajax
	        $.getJSON('{{url('/ajax-area?id=')}}'+id, function (data) {
				//console.log(data);
	            $('#namaArea').empty();
	          	$.each(data, function(index, areaObj){
	            	 $('#namaArea').append('<div><mark>'+areaObj.nm_area+'</mark></div>');
				});
	        });
	    });
	</script>