<!-- bootstrap 3.0.2 -->
{!! Html::style('css/bootstrap.min.css') !!}
<!-- font Awesome -->
{!! Html::style('css/font-awesome.min.css') !!} 
<!-- Ionicons -->
{!! Html::style('css/ionicons.min.css') !!}
<!-- bootstrap wysihtml5 - text editor -->
{!! Html::style('css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}
<!-- Theme style -->
{!! Html::style('css/AdminLTE.css') !!}

{!! Html::style('css/skins/_all-skins.min.css') !!}
@section('content')
<div style="margin-left:30px;margin-right:30px;">
<section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! route('admin.dashboard') !!}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Area Lists &nbsp;&nbsp;&nbsp;
                            <br>
                            <a href="{!! route('admin.area.create') !!}" class="btn btn-primary">Tambah Area </a>
                            
                            <a href="{!! route('admin.kota.create') !!}" class="btn btn-primary">Tambah Kota </a>
                            </br>
                        </h3>                                    
                    </div>
                    <div class="box-body table-responsive">                                 
                        <table id="dataTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>Kode Area</th>
                                    <th>Nama Area</th>
                                    <th>Kode Kota</th>
                                    <th>Nama Kota</th>
                                    @if(Auth::user()->kd_role != 3)
                                        <th>Action</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $row)
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>{{ $row->kd_area }}</td>
                                    <td>{{ $row->nm_area }}</td>
                                    <td>{{ $row->id }}</td>
                                    <td>{{ $row->nm_kota }}</td>
                                    @if(Auth::user()->kd_role != 3)
                                        <td>                                   
                                        <a href="{!! route('admin.area.edit',[$row->id]) !!}" class="fa fa-pencil-square-o">Edit</a>
                                        <!--@if(Auth::user()->hasAccess('admin.user.delete'))-->
                                        <a href="javascript:void(0);" onclick="return confirm('Are you sure you want to delete this item?');" data-href="{!! route('admin.area.delete',[$row->id]) !!}" class="fa fa-trash-o"> 
                                        Delete
                                        </a>
                                        <!--@endif-->
                                        </td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div><!-- /.row (main row) -->
</section><!-- /.content -->
</div>
@include('scripts.delete-modal')
@endsection

@section('custom-head')
    {!! HTML::style('css/datatables/dataTables.bootstrap.css') !!}
@endsection

@section('custom-footer')
    {!! HTML::script('plugins/datatables/jquery.dataTables.js') !!}
    {!! HTML::script('plugins/datatables/dataTables.bootstrap.js') !!}
    <script type="text/javascript">
        $(function() {
                $('#dataTable').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    </script>
@endsection