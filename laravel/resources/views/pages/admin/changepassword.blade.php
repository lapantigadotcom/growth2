@extends('layouts.master')

@section('title', 'Create Admin Menu')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Admin Menu management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/home/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>Configuration</span></a></li>
                            <li><a href="{{url('/admin/adminmenu')}}"><span>Admin Menu</span></a></li>
                            <li class="active"><span>Add Admin Menu</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-warning card-view">
                                <div class="panel-heading">
                                    <div class="pull-left">
                                        <h6 class="panel-title txt-light"><i class="fa fa-wpforms"></i>&nbsp;&nbsp;Form Add Admin Menu</h6>
                                    </div>
                                    @include('partials.panel')
                                    <div class="clearfix"></div>
                                </div>
                                <div id="collapse_1" class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-wrap">


                                                    {!! Form::open(array('route' => 'admin.postChangePassword', 'method' => 'POST', 'data-toggle' => 'validator', 'role' => 'form')) !!}
                                                    
                                                        <div class="form-group">
                                                            {!! Form::label('old_password ','Old Password', array('class' => 'control-label mb-10')) !!}
                                                            {!! Form::password('old_password',array('class' => 'form-control', 'placeholder' => 'Old Password', 'required')) !!}
                                                        </div>
                                                        <div class="form-group">
                                                            {!! Form::label('new_password','New Password', array('class' => 'control-label mb-10')) !!}
                                                            {!! Form::password('new_password',array('class' => 'form-control', 'placeholder' => 'New Password', 'required')) !!}
                                                        </div>
                                                        <div class="form-group">
                                                            {!! Form::label('retype_password','Re-Type Password', array('class' => 'control-label mb-10')) !!}
                                                            {!! Form::password('retype_password',array('class' => 'form-control', 'placeholder' => 'Re-Type Password', 'required')) !!}
                                                        </div>
                                                        <div class="form-group mb-0">
                                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                                        </div>

                                                    {!! Form::close() !!}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- /Row -->

                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

@endsection