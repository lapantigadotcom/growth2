<div class="col-xs-6 col-sm-4 col-sm-offset-4">
	<div class="form-group">
		{!! Form::label('kd_dist','Kode Distributor', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('kd_dist',null, array('class' => 'form-control', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('nm_dist','Nama Distributor', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('nm_dist',null, array('class' => 'form-control', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('kd_tipe','Tipe', array('class' => 'control-label mb-10')) !!}
		{!! Form::select('kd_tipe', ['Distributor' => 'Distributor',
						   'Sub Distributor' => 'Sub Distributor'], null, array('class' => 'selectpicker', 'data-style' => 'form-control btn-default btn-outline')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('almt_dist','Alamat', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('almt_dist',null, array('class' => 'form-control', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('kd_kota','Kota', array('class' => 'control-label mb-10')) !!}
		{!! Form::select('kd_kota', $data['city'], null, array('class' => 'selectpicker', 
		'data-style' => 'data-style="form-control btn-default btn-outline', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('telp_dist','Telepon', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('telp_dist',null, array('class' => 'form-control', 'required')) !!}
	</div>
	@include('partials.errors')
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary btn-anim')) !!}
	</div>
</div>