<div class="col-xs-6 col-sm-4 col-sm-offset-4">
	<div class="form-group">
		{!! Form::label('kd_outlet','Nama Outlet', array('class' => 'control-label mb-10')) !!}
		{!! Form::select('kd_outlet', $outlet, null, array('class' => 'form-control', 
		'data-style' => 'data-style="form-control btn-default btn-outline', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('status_visit','Status Visiting', array('class' => 'control-label mb-10')) !!}
		{!! Form::select('status_visit', [
				   '' => 'Select Status Visiting',
				   '1' => 'YES',
				   '0' => 'NO'], null, array('class' => 'form-control', 'data-style' => 'form-control btn-default btn-outline')
				) !!}
	</div>
	<div class="form-group">
        {!! Form::label('date_visit','Tanggal Visit Plan', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('date_visit', date('d-F-Y H:i:s', strtotime($data['content']->date_visit)), array('class' => 'form-control', 'disabled' => 'disabled')) !!}
	</div>
	<div class="form-group">
        <div class="control-group success">
			<div class="controls">
			    {!! Form::label('date_visiting','Tanggal Kunjungan', array('class' => 'control-label mb-10')) !!}
			     
	            <span><p class="text-success mb-10">Di isi jika Status Visit "YES"</p></span>
            </div>
            <div class="input-group form-group date datetimepicker1" >
                    <input type='text' name="date_visiting" class="form-control" id="datepicker1" placeholder="TGL awal" required/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
               </div>  
		</div>
	
		<div class="control-group error">
		 
	</div>

	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>

	 @section('custom-js')
        <link href="{{asset ('assets/theme/backend/vendors/bower_components/summernote/dist/summernote.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset ('assets/theme/backend/vendors/bower_components/switchery/dist/switchery.min.css')}}" rel="stylesheet" type="text/css" />

        
        <link href="{{asset ('assets/theme/backend/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
         <!-- Moment JavaScript -->
          <script src="{{asset ('assets/theme/backend/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}"></script>   
  
        <script type="text/javascript">
            $(function () {
                $('.datetimepicker1').datetimepicker({
                    format: 'YYYY-MM-DD'
                });
            });
        </script>
         
        @endsection




