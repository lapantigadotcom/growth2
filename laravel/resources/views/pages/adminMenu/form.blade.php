<div class="col-xs-6 col-sm-4 col-sm-offset-4">
	<div class="form-group">
		{!! Form::label('nm_menu','Nama Menu', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('nm_menu',null, array('class' => 'form-control', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('route','Route', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('route',null, array('class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('icon','Icon', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('icon',null, array('class' => 'form-control', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('parent_menu','Parent Menu', array('class' => 'control-label mb-10')) !!}
		{!! Form::select('parent_menu',$data['adminmenu'],null,array('class' => 'form-control', 
		'data-style' => 'data-style="form-control btn-default btn-outline', 'required')) !!}	
		</div>
	<div class="form-group">
		{!! Form::label('enable','Availability', array('class' => 'control-label mb-10')) !!}
		{!! Form::select('enable',[
					   '1' => 'Enable',
					   '0' => 'Disable',
					   ], null, array('class' => 'form-control', 'data-style' => 'form-control btn-default btn-outline')) !!}		
	</div>
	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary')) !!}
	</div>
</div>