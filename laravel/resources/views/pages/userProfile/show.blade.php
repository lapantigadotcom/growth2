@extends('layouts.master')

@section('link', '
    <link href="/doodle/vendors/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" type="text/css"/>')

@section('title', 'Profil User')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">Profile</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/home/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li class="active"><span>My Profile</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-list"></i>&nbsp;&nbsp;My Profile : 
                                    <span class="label label-info capitalize-font inline-block" style="font-size: 17px;">{!! $data['content']->nama!!}</span></h6>
                                </div>
                                
                                    <div class="pull-right txt-light">
                                        <a class="pull-left inline-block mr-15" data-toggle="collapse" href="#collapse_1" aria-expanded="true">
                                            <i class="zmdi zmdi-chevron-down" style="color: white;"></i>
                                            <i class="zmdi zmdi-chevron-up" style="color: white;"></i>
                                        </a>
                                        <div class="pull-left inline-block dropdown mr-15">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false" role="button"><i class="fa fa-pencil" style="color: white;"></i></a>
                                            <ul class="dropdown-menu bullet dropdown-menu-right"  role="menu">
                                                <li role="presentation"><a href="{{ route('admin.edit.profile',[Auth::user()->username]) }}" role="menuitem"><i class="icon wb-reply" aria-hidden="true"></i>Edit Profile</a></li>
                                            </ul>
                                        </div>
                                        <a href="#" class="pull-left inline-block refresh mr-15">
                                            <i class="zmdi zmdi-replay" style="color: white;"></i>
                                        </a>
                                        <a href="#" class="pull-left inline-block full-screen mr-15">
                                            <i class="zmdi zmdi-fullscreen" style="color: white;"></i>
                                        </a>
                                    </div>


                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-wrap">
                                                <form class="form-horizontal" role="form">
                                                    <div class="form-body">
                                                        <div class="row">

                                                            <div class="col-md-4">
                                                                @if(File::exists('userphoto/'.$data['content']->foto) and $data['content']->foto != '')
                                                                    <input type="file" id="input-file-now" class="dropify" disabled="" " data-height="300" data-default-file="{{ asset('/userphoto/'.$data['content']->foto) }}"/>
                                                                @else
                                                                    <input type="file" id="input-file-now" class="dropify" disabled="" " data-height="300" data-default-file="{{ asset('/img/user.png') }}"/>
                                                                @endif
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-user pull-right"></i></label>
                                                                    <div class="col-md-6">
                                                                        <h5 class="form-control-static">Nama : {!! $data['content']->nama !!} </h5>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-barcode pull-right"></i></label>
                                                                    <div class="col-md-6">
                                                                        <h5 class="form-control-static">NIK : {!! $data['content']->nik !!} </h5>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-th pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h5 class="form-control-static">Area : {!! $data['content']->nm_area !!} </h5>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-home pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h5 class="form-control-static">Alamat : {!! $data['content']->alamat !!} </h5>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-calendar pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h5 class="form-control-static">Join Date : {!! $data['content']->join_date !!} </h5>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-phone pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h5 class="form-control-static">Telepon : {!! $data['content']->telepon !!} </h5>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-user pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h5 class="form-control-static">Username {!! $data['content']->username !!} </h5>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <label class="control-label col-md-1" style="padding-top: 12px;"><i class="glyphicon glyphicon-envelope pull-right"></i></label>
                                                                    <div class="col-md-9">
                                                                        <h5 class="form-control-static">Email : {!! $data['content']->email_user !!} </h5>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

@endsection

@section('script', '
    <!-- Bootstrap Daterangepicker JavaScript -->
    <script src="/doodle/vendors/bower_components/dropify/dist/js/dropify.min.js"></script>
    <!-- Form Flie Upload Data JavaScript -->
    <script src="/doodle/dist/js/form-file-upload-data.js"></script>')