<div class="row-fluid sortable">       
   <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Laporan Photo Activity Per Sales Name</h2>
        </div>
        <div class="box-content">    
            @foreach($data['sales'] as $val)
                <h3>Sales Name : {!! $val->nama !!}</h3>
             @endforeach
             @foreach($data['area'] as $val)
                <h3>Area : {!! $val->nm_area !!}</h3>
             @endforeach  
                <h3>Periode : {!! $data['periode'] !!}</h3>     
            <table>
              <tbody>
                    <tr style="text-style:font-style: bold; background:#2D89EF; color:#fff;text-align:center;height:30px;padding-bottom:3px;border:1px solid #444">               
                        <th style="max-width:80px"> Kode Outlet</th>
                        <th>Nama Outlet</th>
                        <th>Alamat Outlet</th>
                        <th>Tanggal Take Photo</th>
                        <th>Tanggal Upload</th>
                        <th>Tipe Photo</th>
                        <th>Link Photo</th>
                       <!-- <th>Thumbnail</th> -->
                        <th>Keterangan Photo</th>
                    </tr>
                    @foreach($data['photoAct'] as $row)
                    <tr style="text-style:font-style: bold; background:#fefefe; color:#444;text-align:center;height:25px;padding-bottom:3px;border:1px solid #444">               
                       <td style="max-width:80px">{{ $row->kode }}</td>
                        <td>{{ $row->nm_outlet }}</td>
                        <td>{{ $row->almt_outlet }}</td>     
                        <td>{{ date('d-F-Y H:i:s', strtotime($row->date_take_photo))  }}</td>
                         <td>{{ date('d-F-Y H:i:s', strtotime($row->date_upload_photo)) }}</td> 
                        <td>{{ $row->nama_tipe }}</td>      
                        <td><a href="{!! url('image_upload/outlet_photoact/'.$row->path_photo) !!}">{{ $row->path_photo }}</a></td>   
                       <!-- <td><img src="{{'image_upload/outlet_photoact/'.$row->path_photo}}"/></td> -->
                        <td>{{ $row->keterangan }}</td>                            
                   </tr>
                   @endforeach  
                </tbody>
            </table>
        </div>
    </div>
</div>