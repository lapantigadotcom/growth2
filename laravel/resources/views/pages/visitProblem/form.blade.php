<div class="box-body">
	<div class="form-group">

		{!! Form::label('kd_visit','Kode Visit Plan') !!}
		{!! Form::text('kd_visit', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}

		<div class="control-group">
			<div class="controls">
				{!! Form::label('date_upload_photo','Tanggal Upload') !!}
				{!! Form::text('date_upload_photo', null, array('id' => 'datepicker','disabled'=>'disabled')) !!}
				<span class="help-inline"></span>
			</div>
		</div>

		<div class="control-group">
			<div class="controls">
				{!! Form::label('detail_problem','Detail Problem') !!}
				{!! Form::text('detail_problem', null, array('class' => 'form-control')) !!}

			</div>
		</div>
		<!--div class="control-group">
			<div class="controls">
				{!! Form::label('status','Status Approval') !!}
				{!! Form::select('status', [
				   
				   '1' => 'Approved',
				   '0' => 'UnApproved'], null, ['class' => 'form-control']
				) !!} 
			</div>
		</div-->
		<div class="control-group">
			<div class="controls">
				{!! Form::label('nm_photo','Upload Foto') !!}
				<span class="help-inline">*) Required</span>
				{!! Form::file('nm_photo',null, array('class' => 'nm_photo form-control')) !!}
			</div>
		</div>
	</div>

	<div class="form-group">
		{!! Form::submit($submit, array('class' => 'btn btn-primary submit')) !!}
	</div>
</div>

<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
$(function() {
	$("form").submit(function() {
      if($("#nm_photo").val()=="") {
                    $("#nm_photo").remove();
            }
    });
	$('#datepicker').datepicker({ dateFormat: 'yy-mm-dd' }).val();
});
</script>
