@extends('layouts.app')
@section('title', 'Edit Bad Coverage')
@section('content')
<section class="content">
    <div class="row">     
        <div class="box col-sm-12 col-md-6 col-lg-6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                        <span class="fa fa-edit">  </span> Edit Bad Coverage
                    </div>
                </div>
                <div class="widget-body">
                    {!! Form::model($data['content'],array('route' => ['admin.visitProblem.update',$data['content']->kd_problem], 'method' => 'PUT','files' => true)) !!}
                        @include('pages.visitProblem.form',array('submit' => 'Perbarui'))

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('custom-head')

@endsection

@section('custom-footer')

@endsection
