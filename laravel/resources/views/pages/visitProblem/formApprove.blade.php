@extends('layouts.app')

@section('content')

@section('title', 'Approval Bad Coverage')
<section class="content">
    <div class="row">     
        <div class="box col-sm-12 col-md-6 col-lg-6">
            <div class="widget">
                <div class="widget-header">
                    <div class="title">
                    <span class="fa fa-check">  </span> Approval Bad Coverage
                    </div>
                </div>
                <div class="widget-body">
                {!! Form::model($data,array('route' =>'admin.visitProblem.approved', 'method' => 'POST')) !!}
                    <table class="table table-striped table-bordered bootstrap-datatable">
                        <thead>
                            <tr>         
                                <th style="text-align:center;">#</th>

                                <th style="text-align:center;">Sales Force</th>
                                <th style="text-align:center;">Kode Problem</th>
                                <th style="text-align:center;">Kode Visit</th>
                                <th style="text-align:center;">Kode Outlet</th>
                                <th style="text-align:center;">Outlet</th>
                                <th style="text-align:center;">Detail</th>
                                <th style="text-align:center;">Waktu Take Photo</th>
                                <th style="text-align:center;">Waktu Upload Photo</th>
                                <th style="text-align:center;">Thumbnail</th>
                                <th style="text-align:center;">Denial</th>
                                <th style="text-align:center;">Approval</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1;?>
                            @foreach($data['data'] as $row)
                            <tr> 
                                <td>{{$i++}}</td>
                                <td>{{ $row->nama }}</td>
                                <td>{{ $row->kd_problem }}</td>
                                <td>{{ $row->kd_visit }}</td>
                                <td>{{ $row->kd_outlet }}</td>
                                <td>{{ $row->nm_outlet }}</td>
                                <td>{{ $row->detail_problem }}</td>
                                <td>{{ $row->date_take_photo }}</td>
                                <td>{{ $row->date_upload_photo }}</td>
                                @if(File::exists('image_upload/visit_problem/'.$row->nm_photo) and $row->nm_photo != '')
                                <td> <a href="{!! url('image_upload/visit_problem/'.$row->nm_photo) !!}" target="_blank" title="{{ $row->nm_photo }}">
                                    <img style="max-height: 60px; max-width: 60px;" src="{!! url('image_upload/visit_problem/'.$row->nm_photo) !!}" class="img-responsive" /></a>
                                </td>
                                @else
                                <td><img style="max-height: 60px; max-width: 60px;" src="{!! url('/img/no_image_outlet_upload.png') !!}" class="img-responsive" /></td>
                                @endif


                                <td id="denial"><input type="checkbox" name="denied_problem[]" id="denied" value="{{$row->kd_problem}}" class="denied flat-red">
                                </td>
                                <td id="approval"><input type="checkbox" name="approve_problem[]" id="approved" value="{{$row->kd_problem}}" class="approve flat-red">
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <p>
                        <input type="button" id="checka" value="Approve All" />
                        <input type="button" id="checkd" value="Denied All" />
                        <input type="button" id="uncheck" value="Uncheck All" />
                    </p> 
                    <div class="form-group">
                        {!! Form::submit('Perbarui', array('class' => 'btn btn-primary')) !!}
                    </div>
                                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script> 
<script>
    $(document).ready( function() {
        $('#checka').on('click', function () {
            $(':checkbox').attr("checked", false);
            $('.approve:checkbox').attr("checked", true);
        });
        $('#checkd').on('click', function () {
            $(':checkbox').attr("checked", false);
            $('.denied:checkbox').attr("checked", true);
        });
        $('#uncheck').on('click', function () {
            $(':checkbox').attr("checked", false);
        });
        $('.approve').on('click', function(){
             $(this).parents('#approval').siblings('#denial').find('.denied').removeAttr('checked');
        });
        $('.denied').on('click', function(){
             $(this).parents('#denial').siblings('#approval').find('.approve').removeAttr('checked');
        });
    });
</script>

@endsection


@section('custom-head')

@endsection

@section('custom-footer')

@endsection



