<div class="col-xs-6 col-sm-4 col-sm-offset-4">
	<div class="form-group">
		{!! Form::label('judul','Judul', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('judul',null, array('class' => 'form-control', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('headline','Headline', array('class' => 'control-label mb-10')) !!}
		{!! Form::text('headline',null, array('class' => 'form-control', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('content','Isi Berita', array('class' => 'control-label mb-10')) !!}
		{!! Form::textarea('content',null, array('rows'=>'5', 'class' => 'form-control', 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('path_image','Upload Picture', array('class' => 'control-label mb-10')) !!}
		<input type="file" id="input-file-now" name="path_image" class="dropify" data-height="206"/>
	</div>
	<div class="form-group">
		{!! Form::label('status','Status', array('class' => 'control-label mb-10')) !!}
		{!! Form::select('status', [
		   '1' => 'Enable',
		   '0' => 'Disable',
		], null, array('class' => 'selectpicker', 'data-style' => 'form-control btn-default btn-outline')) !!} 
	</div>
	@include('partials.errors')
	<div class="form-group mb-0">
		{!! Form::submit($submit, array('class' => 'btn btn-primary btn-anim'))!!}
	</div>
</div>

<!-- Moment JavaScript -->
<script type="text/javascript" src="/doodle/vendors/bower_components/moment/min/moment-with-locales.min.js"></script>		