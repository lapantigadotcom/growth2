@extends('layouts.master')

@section('title', 'News List')

@section('content')

        <!-- Main Content -->
        <div class="page-wrapper">
            <div class="container-fluid">
                
                <!-- Title -->
                <div class="row heading-bg">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h5 class="txt-dark">News management</h5>
                    </div>
                    <!-- Breadcrumb -->
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{url('/admin/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
                            <li><a href="#"><span>News</span></a></li>
                            <li class="active"><span>List</span></li>
                        </ol>
                    </div>
                    <!-- /Breadcrumb -->
                </div>
                <!-- /Title -->

                <!-- Row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-warning card-view">
                            <div class="panel-heading">
                                <div class="pull-left">
                                    <h6 class="panel-title txt-light"><i class="fa fa-user"></i>&nbsp;&nbsp;News List</h6>
                                </div>

                                @include('partials.panel')

                                <div class="clearfix"></div>
                            </div>
                            <div id="collapse_1" class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <div class="table-wrap">
                                        
                                        @if(Auth::user()->hasAccess('admin.news.create'))  
                                            <a href="{!! route('admin.news.create') !!}" class="btn btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;Tambah Article</a>
                                        @endif
                                     
                                        <div class="table-responsive">
                                            <table class="table datable_1 table-hover table-bordered display mb-30">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align:center;">No</th>
                                                        <th style="text-align:center;">Judul</th>
                                                        <th style="text-align:center;">Headline</th>
                                                        <th style="text-align:center;">Content</th>
                                                        <th style="text-align:center;">Thumbnail</th>
                                                        <th style="text-align:center;">Date Upload</th>
                                                        <th style="text-align:center;">Status</th>
                                                        @if(Auth::user()->kd_role != 3)
                                                            <th style="min-width:108px">Action</th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($data as $key => $row)
                                                        <tr>
                                                        	<td><center>{{ $key+1 }}</center></td>
                                                            <td><center>{{ $row->judul }}</center></td>
                                                            <td><center>{{ $row->headline }}</center></td>
                                                            <td><center>{{  substr($row->content, 0, 200) }}</center></td>
                                                        	@if(File::exists('image_upload/news/'.$row->path_image) and $row->path_image != '')
							                                    <td><center><a href="{!! url('image_upload/news/'.$row->path_image) !!}" target="_blank" >
							                                        <img style="max-height: 120px; max-width: 120px;" src="{!! url('image_upload/news/'.$row->path_image) !!}" class="img-responsive" /></a>
							                                    </center></td>
							                                @else
							                                    <td><center><img style="max-height: 120px; max-width: 120px;" src="{!! url('/img/no_image_outlet_upload.png') !!}" class="img-responsive" /></center></td>
							                                @endif 
                                                               
							                                <td style="min-width:75px">
                                                                <center>{{ $row->date_upload }}</center>
                                                            </td>
							                               
							                                @if($row->status == 1)
                                                                <td>
                                                                    <center><span class="label label-success">Enabled</span></center>
                                                                </td>
                                                            @elseif($row->status == 0)
                                                                <td>
                                                                    <center><span class="label label-warning">Disabled</span></center>
                                                                </td>
                                                            @endif
                                                            
                                                            
                                                                <td >
                                                                        @if(Auth::user()->kd_role != 3)
                                                                       <div class="button-list">
                                                                            <a href="{!! route('admin.news.edit',[$row->id]) !!}" data-toggle="tooltip" title="Edit" class="btn btn-primary btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-pencil" style="font-size: 14px;"></i></a>

                                                                            <a href="{!! route('admin.news.delete',[$row->id]) !!}" id="delete" data-toggle="tooltip" title="Delete" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn-sm btn-icon-anim btn-square mr-5" style="display: unset;"><i class="fa fa-trash" style="font-size: 14px;"></i></a>
                                                                            
                                                                        </div>
                                                                         @endif
                                                                </td>
                                                           
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- /Row -->
            
                @include('partials.footer')

            </div>
        </div>
        <!-- /Main Content -->

        @include('partials.sweetalert')

        @push('message')
            @include('partials.toastr')
        @endpush
        
@endsection