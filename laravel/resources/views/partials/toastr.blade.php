

<script type="text/javascript">
  @if(Session::has('create'))
     $.toast({
      heading: 'Create Success',
      text: '{{ Session::get('create') }}',
      position: 'bottom-right',
      loaderBg:'#e69a2a',
      icon: 'success',
      hideAfter: 3500, 
      stack: 6
    });
  @elseif(Session::has('update'))
    $.toast({
      heading: 'Update Success',
      text: '{{ Session::get('update') }}',
      position: 'bottom-right',
      loaderBg:'#e69a2a',
      icon: 'success',
      hideAfter: 3500, 
      stack: 6
    });
  @elseif(Session::has('delete'))
    $.toast({
      heading: 'Delete Success',
      text: '{{ Session::get('delete') }}',
      position: 'bottom-right',
      loaderBg:'#e69a2a',
      icon: 'success',
      hideAfter: 3500, 
      stack: 6
    });
  @endif
</script>