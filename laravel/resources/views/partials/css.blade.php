 
<link href="{{asset ('assets/theme/backend/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
<!-- Data table CSS -->
<link href="{{asset ('assets/theme/backend/vendors/bower_components/datatables/media/css/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
 <!--Toast CSS -->
 <link href="{{asset ('assets/theme/backend/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet" type="text/css" />
 <!--Sweetalert CSS -->

 
@yield('link')

<link href="{{asset ('assets/theme/backend/dist/css/style.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset ('assets/theme/backend/dist/css/style.css')}}" rel="stylesheet" type="text/css" />
<script src="{{asset ('assets/theme/backend/vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>
@yield('custom-css')
 