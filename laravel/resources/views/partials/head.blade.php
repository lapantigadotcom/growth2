		<!-- Top Menu Items -->
		<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="mobile-only-brand pull-left">
				<div class="nav-header pull-left">
					<div class="logo-wrap">
						<a href="{{url('admin/dashboard')}}">
						 
 							<span class="brand-text"><img src="{{ asset('/img/pmp_header.png')}}" width="130"></span>
						</a>
					</div>
				</div>	
				<a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
				<a id="toggle_mobile_search" data-toggle="collapse" data-target="#search_form" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-search"></i></a>
				<a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>
				
			</div>
			
			<div id="mobile_only_nav" class="mobile-only-nav pull-right">
				<ul class="nav navbar-right top-nav pull-right">
					<li>
						<a href="#">
							<i>
							@if(Auth::check())
                                Welcome, {!! Auth::user()->nama !!}
                            @else
                                Guest
                            @endif
							</i>
						</a>
						
					</li>
					<li class="dropdown auth-drp">
						<a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown">
							 <img style="max-height: 60px; max-width: 60px;" src="{{url ('/userphoto')}}/{!! Auth::user()->foto !!}" alt="user_auth" class="user-auth-img img-circle" />
							<!-- <img src="/doodle/dist/img/user2.png" alt="user_auth" class="user-auth-img img-circle"/><span class="user-online-status"> -->								
							</span></a>
						<ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
							<center>{{ date('Y-m-d H:i:s') }}</center>
							<li class="divider"></li>
							<li>
								<a href="{{ route('admin.profile',[Auth::user()->username]) }}"><i class="zmdi zmdi-account"></i><span>Profile</span></a>
							</li>
							<li>
								<a href="{!! route('admin.getChangePassword') !!}"><i class="zmdi zmdi-settings"></i><span>Change Password</span></a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="{!! route('admin.logout') !!}"><i class="zmdi zmdi-power"></i><span>Log Out</span></a>
							</li>
						</ul>
					</li>
				</ul>
			</div>	
		</nav>
		<!-- /Top Menu Items -->