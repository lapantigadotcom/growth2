
 <!-- jQuery -->
	
	<!-- Bootstrap Core JavaScript -->
 	<script src="{{asset ('assets/theme/backend/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
	<!-- Data table JavaScript -->
 	<script src="{{asset ('assets/theme/backend/vendors/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
 	 
	<!-- Slimscroll JavaScript -->
 	<script src="{{asset ('assets/theme/backend/dist/js/jquery.slimscroll.js')}}"></script>

	<!-- simpleWeather JavaScript -->
	<script src="{{asset ('assets/theme/backend/vendors/bower_components/moment/min/moment.min.js')}}"></script>
	<script src="{{asset ('assets/theme/backend/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js')}}"></script>
	<script src="{{asset ('assets/theme/backend/dist/js/simpleweather-data.js')}}"></script>


	<!-- Progressbar Animation JavaScript -->
	<script src="{{asset ('assets/theme/backend/vendors/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
	<script src="{{asset ('assets/theme/backend/vendors/bower_components/jquery.counterup/jquery.counterup.min.js')}}"></script>


	<!-- Fancy Dropdown JS -->
	<script src="{{asset ('assets/theme/backend/dist/js/dropdown-bootstrap-extended.js')}}"></script>

	<!-- Sparkline JavaScript -->
	<!-- <script src="{{asset ('assets/theme/backend/vendors/jquery.sparkline/dist/jquery.sparkline.min.js')}}"></script> -->

	<!-- Owl JavaScript -->
	<script src="{{asset ('assets/theme/backend/vendors/bower_components/owl.carousel/dist/owl.carousel.min.js')}}"></script>

	<!-- ChartJS JavaScript -->
	<script src="{{asset ('assets/theme/backend/vendors/chart.js/Chart.min.js')}}"></script>

	<!-- Morris Charts JavaScript -->
	<script src="{{asset ('assets/theme/backend/vendors/bower_components/raphael/raphael.min.js')}}"></script>
	<script src="{{asset ('assets/theme/backend/vendors/bower_components/morris.js/morris.min.js')}}"></script>
	<script src="{{asset ('assets/theme/backend/vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>

	<!-- Switchery JavaScript -->
	<script src="{{asset ('assets/theme/backend/vendors/bower_components/switchery/dist/switchery.min.js')}}"></script>

	<!-- Init JavaScript -->
	<script src="{{asset ('assets/theme/backend/dist/js/init.js')}}"></script>
	@yield('custom-js')
	