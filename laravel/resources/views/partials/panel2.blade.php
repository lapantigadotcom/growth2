                                <div class="pull-right">
                                    <a class="pull-left inline-block mr-15" data-toggle="collapse" href="#collapse_2" aria-expanded="true">
                                        <i class="zmdi zmdi-chevron-down" style="color: white;"></i>
                                        <i class="zmdi zmdi-chevron-up" style="color: white;"></i>
                                    </a>
                                    <a href="#" class="pull-left inline-block refresh mr-15">
                                        <i class="zmdi zmdi-replay" style="color: white;"></i>
                                    </a>
                                    <a href="#" class="pull-left inline-block full-screen mr-15">
                                        <i class="zmdi zmdi-fullscreen" style="color: white;"></i>
                                    </a>
                                </div>