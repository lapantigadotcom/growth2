	  
		<!-- Left Sidebar Menu -->
		<div class="fixed-sidebar-left">
			<ul class="nav navbar-nav side-nav nicescroll-bar">
				<li class="navigation-header">
					<span>Main</span> 
					<i class="zmdi zmdi-more"></i>
				</li>

				@foreach(Auth::user()->getEffectiveMenu() as $menu)
			@if(count($menu->_child) > 0) 
			<li>
				<a href="javascript:void(0);" data-toggle="collapse" data-target="#menu{{$menu->icon}}"><div class="pull-left"><i class="fa {!!$menu->icon!!} mr-10"></i><span class="right-nav-text">{{ $menu->nm_menu }}</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div> 
				</a>
				<ul id="menu{{$menu->icon}}" class="collapse collapse-level-1">
					@foreach($menu->_child as $child)
					<li>
						<a href="{!!$child->route==''?'':route($child->route)!!}">
							
							{{ $child->nm_menu }}
						</a>
					</li>
					@endforeach
				</ul>
			</li>
			@else
			<li>
				<a href="{{ $menu->route==''?'':route($menu->route) }}">
					<i class="fa {!!$menu->icon!!} mr-10"></i>
					<span>{{ $menu->nm_menu }}</span>
				</a>
			</li>
			@endif
			@endforeach
				<div class="col-lg-12 ">
						<div class="panel panel-default card-view pa-0">
							<div class="panel-wrapper collapse in">
								<div class="panel-body pa-0">
									<div class="sm-data-box bg-yellow">
										<div class="container-fluid">
											<div class="row">
												<div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
													<span class="txt-light block counter"><span class="counter-anim">{{ date('Y') }}</span></span>
													<span class="weight-500 uppercase-font txt-light block">{{ date('m-d H:i:s') }}</span>
												</div>
												<div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
													<i class="fa fa-calendar txt-light data-right-rep-icon"></i>
												</div>
											</div>	
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
			</ul>

				
			
		</div>

		<!-- /Left Sidebar Menu -->
