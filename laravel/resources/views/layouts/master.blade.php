<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>PMP Backend Panel | @yield('title')</title>
	
	<meta name="author" content="trikarya teknologi indonesia"/>
	@include('partials.css')
	
</head>

<body>
	<!--Preloader-->
	<div class="preloader-it">
		<div class="la-anim-1"></div>
	</div>
	<!--/Preloader-->
	<div class="wrapper theme-2-active box-layout pimary-color-blue">

		@include('partials.head')

		@include('partials.sidebar_role')

		@yield('content')

	</div>
	<!-- /#wrapper -->
	
	@include('partials.javascript')
	@stack('message')
	 @stack('scripts')

</body>

</html>
