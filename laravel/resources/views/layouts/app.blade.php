<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>PMP Backend Panel | @yield('title')</title> 
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
        <script src="//cdn.ckeditor.com/4.5.2/standard/ckeditor.js"></script>

    </head>
    <body >
        <div class="container-fluid-full">
            <div class="row-fluid">    
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="left-side sidebar-offcanvas">
                 @include('partials.sidebar')
                </aside>
                <!-- Right side column. Contains the navbar and content of the page -->
                <aside class="right-side">
                    <!-- Content Header (Page header) -->
                    @yield('content')
                </aside><!-- /.right-side -->
            </div>
        </div>

        <!-- add new calendar event modal -->
        @include('partials.footer')

    </body>
</html>