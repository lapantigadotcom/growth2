<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>PMP Backend Panel | @yield('title')</title>
	<meta name="description" content="Growth adalah aplikasi berbasis android yang focus dalam bidang sales dan marketing. Terlahir dari pengalaman bertahun-tahun dalam bidang penjualan dimana kesulitan terbesar dalam dunia penjualan adalah lead management." />
	<meta name="keywords" content="sales, aplikasi sales, sales android, android sales app" />
	<meta name="author" content="PT. Trikarya Teknologi Indonesia"/>
	
	<!-- Favicon -->
	<link rel="shortcut icon" href="favicon.ico">
 	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<link href="{{ asset('/css/login.css')}}" rel="stylesheet" type="text/css"/>
 </head>

<body style="background: #D38006">
 	<section class="login-block" style="background: #D38006">
				    <div class="container">
   
   	@yield('content')
	
	 </div>
	</section>
	
</body>

</html>
