@extends('layouts.auth')

@section('title', 'Login')

@section('content')

  		<div class="wrapper pa-0">
			<header class="sp-header">
				<div class="sp-logo-wrap pull-left">
					<a href="{{url('/admin/dashboard')}}">
						<img class="brand-img text-center mr-10" src="{{ asset('/img/logo_pmp.png')}}" width="290" alt="brand"/>
 					</a>
				</div>
				<div class="form-group mb-0 pull-right">
					<a class="inline-block btn btn-info btn-rounded btn-outline nonecase-font" href="{{url('/admin/dashboard')}}">Back to Home</a>
				</div>

				<div class="clearfix"></div>
			</header>

			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0 error-bg-img">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											
											<h4 class="text-center">Error 404 | Page Not Found</h4>
											<p class="text-center">The URL may be misplaced or the page you are looking is no longer available.</p>
 										</div>	
									</div>	
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->	
				</div>
				
			</div>
			<!-- /Main Content -->


		</div>
    	<!-- /#wrapper -->


@endsection
