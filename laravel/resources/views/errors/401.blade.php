@extends('layouts.auth')

@section('title', 'Error 401')

@section('content')

  		<div class="wrapper pa-0">
			<header class="sp-header">
				 
				<div class="clearfix"></div>
			</header>

			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<center>
											<img class="brand-img text-center mr-10" src="{{ asset('/img/barrier.png')}}" style="margin-top: 30px" width="350" alt="no access"/>
											<h4 class="text-center" >Error 401 | Akses Ditolak</h4>
											<p class="text-center" >Anda Harus mendapatkan persetujuan admin untuk mengakses halaman ini, Silahkan Hubungi Admin PMP</p>
											<a class="btn btn-default" style="color: red" href="{{url('/admin/dashboard')}}" style="margin: 40px">Kembali ke Home</a></center>
 										</div>	
									</div>	
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->	
				</div>
				
			</div>
			<!-- /Main Content -->


		</div>
    	<!-- /#wrapper -->


@endsection
