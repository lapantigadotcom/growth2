@extends('layouts.auth')

@section('title', 'Login')

@section('content')

  		<div class="wrapper pa-0">
			<header class="sp-header">
				<div class="sp-logo-wrap pull-left">
					<a href="index.html">
						<img class="brand-img mr-10" src="/doodle/dist/img/logo.png" alt="brand"/>
						<span class="brand-text">growth</span>
					</a>
				</div>
				<div class="form-group mb-0 pull-right">
					<a class="inline-block btn btn-info btn-rounded btn-outline nonecase-font" href="{{url('/admin/dashboard')}}">Back to Home</a>
				</div>

				<div class="clearfix"></div>
			</header>

			<!-- Main Content -->
			<div class="page-wrapper pa-0 ma-0 error-bg-img">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="mb-30">
											<span class="block error-head text-center txt-info mb-10">503</span>
											<span class="text-center nonecase-font mb-20 block error-comment">The service is unavailable</span>
											<p class="text-center">Please try again later....</p>
										</div>	
									</div>	
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->	
				</div>
				
			</div>
			<!-- /Main Content -->


		</div>
    	<!-- /#wrapper -->


@endsection
