@extends('layouts.auth')

@section('title', 'Forgot Password')

@section('content')
		<!-- Main Content -->
		<div class="wrapper pa-0">

			<div class="page-wrapper pa-0 ma-0 auth-page">
				<div class="container-fluid">
					<!-- Row -->
					<div class="table-struct full-width full-height">
						<div class="table-cell vertical-align-middle auth-form-wrap">
							<div class="auth-form  ml-auto mr-auto no-float">
								<div class="row">
									<div class="col-sm-12 col-xs-12">
										<div class="sp-logo-wrap text-center pa-0 mb-30">
											<a href="{{url('/')}}">
 												 <img src="./img/growth_color.png" width="200px"><br>
											</a>
										</div>

										 
	
										<div class="form-wrap">

											{!! Form::open(['route'=>'admin.reset.password', 'method' => 'store', 'class'=>'form-horizontal']) !!}
												<div class="form-group">
													<label class="control-label mb-10" for="Email">Email</label>
													{!! Form::text('email_user',null, array('class' => 'form-control', 'placeholder' => 'Email')) !!}
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="NIK">NIK</label>
													{!! Form::text('nik',null, array('class' => 'form-control', 'placeholder' => 'NIK')) !!}
												</div>
												<div class="form-group">
													<label class="control-label mb-10" for="Telepon">Telepon</label>
													{!! Form::text('telepon',null, array('class' => 'form-control', 'placeholder' => 'Telepon')) !!}
												</div>
												
												<div class="form-group text-center">
													<button type="submit" class="btn btn-info btn-rounded">reset</button>
												</div>
											{!! Form::close() !!}

										</div>
									</div>	
								</div>
							</div>
						</div>
					</div>
					<!-- /Row -->	
				</div>
				
			</div>
			
		</div>
		<!-- /Main Content -->


@endsection
