@extends('layouts.auth')

@section('title', 'Login')

@section('content')


			
					<div class="row">
						<div class="col-md-4 login-sec">
						    <!-- <h2 class="text-center">Login Now</h2> -->
						    <img src="{{ asset('/img/logo_pmp.png')}}" width="200px"><br>
						    <d style="font-size: 12px"><i class="fa fa-clock-o"></i>&nbsp; : &nbsp;{{ date('Y-m-d H:i:s') }}</d>
						    <br>
						    <h5 style="font-size: 10px">@include('partials.errors')</h5>
						    
						    		{!! Form::open(['route'=>'admin.auth.login', 'method' => 'store', 'class'=>' ']) !!}
				  <div class="form-group">
				    <label for="username" class="text-uppercase">Username</label>
				   {!! Form::text('username',null, array('class' => 'form-control', 'placeholder' => 'Enter Username')) !!}
				    
				  </div>
				  <div class="form-group">
				    <label for="password" class="text-uppercase">Password</label>
				    {!! Form::password('password', array('class' => 'form-control', 'placeholder' => 'Enter Password')) !!}
				  </div>
				  
				  
				    <div class="form-check">
				    <label class="form-check-label">
				      <input type="checkbox" class="form-check-input">
				      <small>Remember Me</small>
				    </label>

				    <button type="submit" class="btn btn-login float-right">Submit</button>

				  </div>
				  
				{!! Form::close() !!}
				 
				  
				<div class="copy-text"><a href="{{ route('admin.autentikasi') }}"><b>Forgot Password ?</b> </a><br>Copyright 2019. CV. Putra Mas Pratama<a href="{{url ('/')}}"></a></div>
						</div>
						<div class="col-md-8 banner-sec">
				            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
				                 <ol class="carousel-indicators">
				                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
				                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
				                  </ol>
				            <div class="carousel-inner" role="listbox">
				    <div class="carousel-item active">
				      <img class="d-block img-fluid" src="{{ asset('/img/slide01.jpg')}}" alt="First slide">
				      <div class="carousel-caption d-none d-md-block">
				        <div class="banner-text">
				            <h2>Sales App</h2>
				            <p>berperan setelah proses branding dan marketing dilakukan dimana akan datang konsumen – konsumen baru yang membutuhkan produk perusahaan. Calon – calon konsumen ini membutuhkan untuk dimanage dengan efektif sehingga perusahaan tidak kehilangan peluang.</p>
				        </div>	
				  </div>
				    </div>
				    <div class="carousel-item">
				      <img class="d-block img-fluid" src="{{ asset('/img/slide2.jpg')}}" alt="First slide">
				      <div class="carousel-caption d-none d-md-block">
				        <div class="banner-text">
				            <h2>Efficiency</h2>
				            <p>memanage lead dengan efisien dan tepat sasaran dimana ini akan banyak membantu perusahaan dalam meningkatkan penjualan. Seperti kita tahu bahwa sales dan branding serta marketing adalah sebuah kesatuan system dimana untuk bisa mencapai kesempurnaan, efektifitas ketiganya harus berjalan sempurna.</p>
				        </div>	
				    </div>
				    </div>
				    <div class="carousel-item">
				      <img class="d-block img-fluid" src="{{ asset('/img/slide3.jpg')}}" alt="First slide">
				      <div class="carousel-caption d-none d-md-block">
				        <div class="banner-text">
				            <h2>Focus on Marketing</h2>
				            <p>aplikasi berbasis android yang focus dalam bidang sales dan marketing. Terlahir dari pengalaman bertahun-tahun dalam bidang penjualan dimana kesulitan terbesar dalam dunia penjualan adalah lead management.</p>
				        </div>	
				    </div>
				  </div>
				            </div>	   
						    
						</div>
					</div>
				 

@endsection
