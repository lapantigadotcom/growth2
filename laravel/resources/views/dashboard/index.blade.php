<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>PMP Sales App | Main Dashboard</title> 
    @include('partials.css')
	@yield('link')
</head>

<body>
	<!--Preloader-->
	<div class="preloader-it">
		<div class="la-anim-1"></div>
	</div>
	<!--/Preloader-->
    <div class="wrapper theme-2-active box-layout pimary-color-blue">

		@include('partials.head')

		@include('partials.sidebar_role')
       
       	<!-- Main Content -->
		<div class="page-wrapper">
            <div class="container-fluid">
				
				<!-- Title -->
				<div class="row heading-bg">
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<h5 class="txt-dark">dashboard page</h5>
					</div>
					<!-- Breadcrumb -->
					<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
						<ol class="breadcrumb">
							<li><a href="{{url('/home/dashboard')}}"><i class="fa fa-home"></i>&nbsp;Home</a></li>
							<li class="active"><span>Dashboard</span></li>
						</ol>
					</div>
					<!-- /Breadcrumb -->
				</div>
				<!-- /Title -->

				@if(AUth::user()->kd_role != 3)

					<div class="row">
					<!-- Basic Table -->
					<div class="col-sm-12">
						<div class="panel panel-default card-view">
							<div class="panel-heading">
								<div class="pull-left">
									<h6 class="panel-title txt-dark"><i class="fa fa-user"></i>&nbsp;&nbsp;Top 5 Sales This Month <b>( <?php echo date('F Y', strtotime('now'));?> )</b></h6>
								</div>
								<div class="clearfix"></div>
								@include('partials.panel')
							</div>
							<div class="panel-wrapper collapse in">
								<div class="panel-body">  
									<div class="table-wrap mt-40">
										<div class="table-responsive">
											<table class="table table-striped table-bordered mb-0">
												<thead>
													<tr>
														<th>#</th>
 														<th>Nama Sales</th>
														<th>Divisi</th> 
														<th>Jumlah Visit</th>
														<th>Jumlah Order</th>
													</tr>
												</thead>
												<tbody>
													<?php $i = 1; ?>
		          									@foreach($data['topsalesorder'] as $val)
													<tr>
														<td>{{$i++}}</td>
 										               	<td>{{$val->nama}}</td>
										               	<td>{{$val->nm_area}}</td> 
										               	<td>{{$val->TotalVisit}}</td>
										               	<td>{{$val->totalorderku}}</td>

												  	</tr>
												  	@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Basic Table -->
				</div>

				<br>

				<!-- Row -->
                <div class="row">
                    
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div  class="panel-wrapper collapse in">
                                <div  class="panel-body pa-0">
                                    <div class="sm-data-box bg-yellow">
                                        <div class="container-fluid">
                                            <div class="row">
                                            	<div class="col-md-6 text-center pa-25 pb-0 pr-0 data-wrap-right">
                                                    <i class="fa fa-shopping-cart txt-light data-right-rep-icon pull-right"></i>
                                                </div>
                                                <div class="col-md-6 text-center pa-25 pb-0 pr-0 pl-20 data-wrap-right">
                                                    <span class="txt-light block counter">
                                                    	<span class="counter-anim pull-left">{{ $data['countTO'] }} </span>
                                                    </span>
                                                </div>
                                                <div class="col-md-12 text-center pa-0 pb-40">
                                                    <span class="txt-light block font-16">
                                                     	<strong>Order Month ( <?php echo date('F Y', strtotime('now'));?> )</strong>
                                                    </span>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div  class="panel-wrapper collapse in">
                                <div  class="panel-body pa-0">
                                    <div class="sm-data-box bg-green">
                                        <div class="container-fluid">
                                            <div class="row">
                                            	<div class="col-md-6 text-center pa-25 pb-0 pr-0 data-wrap-right">
                                                    <i class="fa fa-exchange txt-light data-right-rep-icon pull-right"></i>
                                                </div>
                                                <div class="col-md-6 text-center pa-25 pb-0 pr-0 pl-20 data-wrap-right">
                                                    <span class="txt-light block counter">
                                                    	<span class="counter-anim pull-left">{{ $data['countVisitbukti'] }} </span>
                                                    </span>
                                                </div>
                                                <div class="col-md-12 text-center pa-0 pb-40">
                                                    <span class="txt-light block font-16">
                                                    	<strong>Visit Rep. Month ( <?php echo date('F Y', strtotime('now'));?> )</strong>
                                                    </span>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div  class="panel-wrapper collapse in">
                                <div  class="panel-body pa-0">
                                    <div class="sm-data-box bg-pink">
                                        <div class="container-fluid">
                                            <div class="row">
                                            	<div class="col-md-6 text-center pa-25 pb-0 pr-0 data-wrap-right">
                                                    <i class="fa fa fa-wpforms txt-light data-right-rep-icon pull-right"></i>
                                                </div>
                                                <div class="col-md-6 text-center pa-25 pb-0 pr-0 pl-20 data-wrap-right">
                                                    <span class="txt-light block counter">
                                                    	<span class="counter-anim pull-left">{{ $data['countBilling'] }} </span>
                                                    </span>
                                                </div>
                                                <div class="col-md-12 text-center pa-0 pb-40">
                                                    <span class="txt-light block font-16">
                                                    	<strong>Billing Month ( <?php echo date('F Y', strtotime('now'));?> )</strong>
                                                    </span>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <div class="panel panel-default card-view pa-0">
                            <div  class="panel-wrapper collapse in">
                                <div  class="panel-body pa-0">
                                    <div class="sm-data-box bg-dark">
                                        <div class="container-fluid">
                                            <div class="row" style="background: teal">
                                            	<div class="col-md-6 text-center pa-25 pb-0 pr-0 data-wrap-right">
                                                    <i class="fa fa-home txt-light data-right-rep-icon pull-right"></i>
                                                </div>
                                                <div class="col-md-6 text-center pa-25 pb-0 pr-0 pl-20 data-wrap-right">
                                                    <span class="txt-light block counter">
                                                    	<span class="counter-anim pull-left">{!! $data['regOutlet'] !!}</span>
                                                    </span>
                                                </div>
                                                <div class="col-md-12 text-center pa-0 pb-40">
                                                    <span class="txt-light block font-16">
                                                    	<strong>Total Outlet</strong>
                                                    </span>
                                                </div>
                                            </div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Row -->

				@endif

				<br>
				
				<div class="row">
					<!-- Basic Table -->
					<div class="col-sm-12">
						<div class="panel card-view">
							<div class="panel-heading">
 									<h6 class="panel-title" style="color: #444"><i class="fa fa-signal"></i>&nbsp;&nbsp;Visit Activity <?php echo "This Month " . date("m-Y"); ?></h6>
 
 								
 
								<div id="collapse_2" class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                    	<div style="display: block;" class="box-content alerts">
											<div class="content">
											    <div id="pop_div" style="height:300px"></div>
												<?= Lava::render('AreaChart', 'VisitPlan', 'pop_div'); ?>
												@areachart('VisitPlan', 'pop_div')
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Basic Table -->
				</div>

				<br>

				<div class="row">
					<!-- Basic Table -->
					<div class="col-sm-12">
						<div class="panel card-view">
							<div class="panel-heading">
 									<h6 class="panel-title "><i class="fa fa-tags"></i>&nbsp;&nbsp;Sales Order <?php echo "This Month " . date("m-Y"); ?></h6>
 
 								
								<div class="clearfix"></div>

								<div id="collapse_3" class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                    	<div style="display: block;" class="box-content alerts">
											<div class="content">
											    <div id="order_div" style="height:300px"></div>
												<?= Lava::render('AreaChart', 'TakeOrder', 'pop_div'); ?>
												@areachart('TakeOrder', 'order_div')
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Basic Table -->
				</div>
				
			
				@include('partials.footer')

			</div>
		</div>
        <!-- /Main Content -->
        @section('custom-js')
         <script type="text/javascript">
        $(function() {
                $('#tabletop').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
    </script>
        @endsection
    </div>
 @include('partials.javascript')


</body>

</html>
