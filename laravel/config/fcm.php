<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => true,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAA_xS6-L0:APA91bEYaR-O3xM8FvcFqo6BOuOWNHaYId6Th0KExWi9dHfWlGWLAfnsUaQpLaVrZKqKIjWZD2ExvfxlOf2gZlPPA0msaGcilVik5mYEOA95TDriQ7plaZwZkes7-3tm4fWHJNINdr4W'),
        'sender_id' => env('FCM_SENDER_ID', 'Your sender id'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
