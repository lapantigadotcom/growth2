<?php

return [


    'driver' => 'smtp',
    'host' => 'smtp.hisamitsu.co.id',
    'port' => 587,
    'from' => ['address' => null, 'name' => null],
    'encryption' => null,
    'username' => env('MAIL_USERNAME'),
    'password' => env('MAIL_PASSWORD'),
    'sendmail' => '/usr/sbin/sendmail -bs',
];
