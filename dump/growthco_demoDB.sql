-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 07, 2017 at 01:04 PM
-- Server version: 10.0.31-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `growthco_demoDB`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

CREATE TABLE `admin_menu` (
  `id` int(11) NOT NULL,
  `parent_menu` int(11) NOT NULL,
  `nm_menu` varchar(100) NOT NULL,
  `route` varchar(100) NOT NULL,
  `urutan` int(11) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `enable` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `parent_menu`, `nm_menu`, `route`, `urutan`, `icon`, `enable`) VALUES
(1, 0, 'Dashboard', 'admin.dashboard', 1, 'icon-leaf', 1),
(2, 0, 'Master Data', '', 2, 'icon-bar-chart', 1),
(3, 0, 'Visit Plan', '', 3, 'icon-calendar', 1),
(4, 0, 'Photo Activity', '', 4, 'icon-camera', 1),
(5, 0, 'User Manager', '', 5, 'icon-user', 1),
(6, 0, 'Report', 'admin.report', 6, 'icon-book', 1),
(7, 0, 'Configuration', '', 7, 'icon-cogs', 1),
(8, 0, 'Export/Import File', 'admin.download', 8, 'icon-download-alt', 1),
(9, 2, 'Area', 'admin.area.index', 1, 'icon-caret-right', 1),
(10, 2, 'Outlet', 'admin.outlet.index', 2, 'icon-caret-right', 1),
(11, 2, 'Distributor', 'admin.distributor.index', 3, 'icon-caret-right', 1),
(12, 2, 'Produk', 'admin.produk.index', 4, 'icon-caret-right', 1),
(13, 3, 'Create Visit Plan', 'admin.visit.create', 1, 'icon-caret-right', 1),
(14, 3, 'Visit Plan List', 'admin.visitPlan.form', 2, 'icon-caret-right', 1),
(15, 3, 'Visiting Monitor', 'admin.visitingMonitor.form', 3, 'icon-caret-right', 1),
(16, 3, 'Take Order List', 'admin.takeOrder.form', 4, 'icon-caret-right', 1),
(17, 4, 'Photo Display', 'admin.photoAct.form', 1, 'icon-caret-right', 1),
(18, 4, 'Competitor Activity', 'admin.competitor.index', 2, 'icon-caret-right', 1),
(19, 5, 'Sales Force', 'admin.sales.index', 1, 'icon-caret-right', 1),
(20, 5, 'Non SF', 'admin.user.index', 2, 'icon-caret-right', 1),
(21, 5, 'Add User', 'admin.user.create', 3, 'icon-caret-right', 1),
(22, 7, 'Role Management', 'admin.role.index', 1, 'icon-caret-right', 1),
(23, 7, 'Admin Menu', 'admin.adminmenu.index', 2, 'icon-caret-right', 1),
(24, 7, 'Permission', 'admin.permission.index', 3, 'icon-caret-right', 1),
(25, 7, 'Outlet Management', 'admin.outletManage.index', 4, 'icon-caret-right', 1),
(26, 7, 'Tipe Photo', 'admin.tipephoto.index', 6, 'icon-caret-right', 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu_role`
--

CREATE TABLE `admin_menu_role` (
  `id` int(11) NOT NULL,
  `kd_role` int(11) NOT NULL,
  `kd_admin_menu` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_menu_role`
--

INSERT INTO `admin_menu_role` (`id`, `kd_role`, `kd_admin_menu`) VALUES
(1684, 3, 18),
(1612, 6, 26),
(1611, 6, 25),
(1437, 1, 26),
(1673, 2, 19),
(1672, 2, 18),
(1671, 2, 17),
(1670, 2, 16),
(1669, 2, 15),
(1668, 2, 14),
(1667, 2, 12),
(1666, 2, 11),
(1665, 2, 10),
(1664, 2, 9),
(1663, 2, 6),
(1662, 2, 5),
(1661, 2, 4),
(1660, 2, 3),
(1659, 2, 2),
(1658, 2, 1),
(1657, 7, 26),
(1656, 7, 25),
(1655, 7, 24),
(1654, 7, 23),
(1653, 7, 22),
(1652, 7, 21),
(1651, 7, 20),
(1436, 1, 19),
(1683, 3, 17),
(1682, 3, 16),
(1435, 1, 18),
(1434, 1, 17),
(1433, 1, 16),
(1432, 1, 15),
(1431, 1, 14),
(1430, 1, 12),
(1429, 1, 11),
(1428, 1, 10),
(1427, 1, 9),
(1426, 1, 6),
(1425, 1, 5),
(1424, 1, 4),
(1423, 1, 3),
(1681, 3, 15),
(1680, 3, 14),
(1679, 3, 13),
(1678, 3, 10),
(1677, 3, 4),
(1676, 3, 3),
(1675, 3, 2),
(1674, 3, 1),
(1487, 4, 19),
(1486, 4, 18),
(1485, 4, 17),
(1484, 4, 16),
(1483, 4, 15),
(1482, 4, 14),
(1481, 4, 12),
(1480, 4, 11),
(1479, 4, 10),
(1478, 4, 9),
(1477, 4, 6),
(1476, 4, 5),
(1475, 4, 4),
(1474, 4, 3),
(1473, 4, 2),
(1472, 4, 1),
(1610, 6, 24),
(1609, 6, 23),
(1608, 6, 22),
(1607, 6, 21),
(1606, 6, 20),
(1605, 6, 19),
(1604, 6, 18),
(1603, 6, 17),
(1602, 6, 16),
(1601, 6, 15),
(1600, 6, 14),
(1599, 6, 13),
(1598, 6, 12),
(1597, 6, 11),
(1596, 6, 10),
(1595, 6, 9),
(1594, 6, 8),
(1593, 6, 7),
(1592, 6, 6),
(1591, 6, 5),
(1590, 6, 4),
(1589, 6, 3),
(1588, 6, 2),
(1587, 6, 1),
(1422, 1, 2),
(1650, 7, 19),
(1649, 7, 18),
(1648, 7, 17),
(1647, 7, 16),
(1646, 7, 15),
(1645, 7, 14),
(1644, 7, 12),
(1643, 7, 11),
(1642, 7, 10),
(1641, 7, 9),
(1640, 7, 7),
(1639, 7, 6),
(1638, 7, 5),
(1637, 7, 4),
(1636, 7, 3),
(1635, 7, 2),
(1634, 7, 1),
(1421, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `kd_area` varchar(10) NOT NULL,
  `nm_area` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `kd_area`, `nm_area`) VALUES
(1, 'EJ', 'EAST JAVA '),
(2, 'CJ', 'CENTRAL JAVA'),
(676, 'WS', 'Sumatera Barat'),
(3, 'WJ', 'WEST JAVA'),
(4, 'MKS', 'MAKASSAR'),
(5, 'PLB', 'PALEMBANG'),
(6, 'JKT', 'JAKARTA'),
(674, 'WJ4', 'West Java 4'),
(7, 'KLM', 'KALIMANTAN'),
(8, 'MDN', 'MEDAN'),
(100, 'AA', 'ALL AREA');

-- --------------------------------------------------------

--
-- Table structure for table `competitor`
--

CREATE TABLE `competitor` (
  `id` int(11) NOT NULL,
  `nm_competitor` varchar(100) NOT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `kd_kota` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `competitor`
--

INSERT INTO `competitor` (`id`, `nm_competitor`, `alamat`, `kd_kota`) VALUES
(1, 'COMPETITOR JAYA', NULL, 0),
(5, 'COMPETITOR 2', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `distributor`
--

CREATE TABLE `distributor` (
  `id` int(11) NOT NULL,
  `kd_dist` varchar(10) NOT NULL,
  `kd_tipe` varchar(30) DEFAULT NULL,
  `kd_kota` int(11) DEFAULT NULL,
  `nm_dist` varchar(100) DEFAULT NULL,
  `almt_dist` varchar(255) DEFAULT NULL,
  `telp_dist` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `distributor`
--

INSERT INTO `distributor` (`id`, `kd_dist`, `kd_tipe`, `kd_kota`, `nm_dist`, `almt_dist`, `telp_dist`) VALUES
(1, 'DIST001', 'Distributor', 290, 'DISTRIBUTOR PT. ABC', 'Jl. A. YANI SURABAYA 212', '+62-21-3141906'),
(2, 'DIST002', 'Distributor', 153, 'CV. X DISTRIBUTOR', 'Jl. KELAPA GANG 2', '(021) 7902781');

-- --------------------------------------------------------

--
-- Table structure for table `konfigurasi`
--

CREATE TABLE `konfigurasi` (
  `id` int(11) NOT NULL,
  `toleransi_max` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konfigurasi`
--

INSERT INTO `konfigurasi` (`id`, `toleransi_max`) VALUES
(1, 500);

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE `kota` (
  `id` int(11) NOT NULL,
  `kd_area` int(11) NOT NULL,
  `nm_kota` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`id`, `kd_area`, `nm_kota`) VALUES
(1, 0, 'Aceh Barat'),
(3, 0, 'Aceh Besar'),
(4, 0, 'Aceh Jaya'),
(5, 0, 'Aceh Selatan'),
(6, 0, 'Aceh Singkil'),
(7, 0, 'Aceh Tamiang'),
(8, 0, 'Aceh Tengah'),
(9, 0, 'Aceh Tenggara'),
(10, 0, 'Aceh Timur'),
(11, 0, 'Aceh Utara'),
(12, 0, 'Agam'),
(13, 0, 'Alor'),
(14, 0, 'Ambon'),
(15, 0, 'Asahan'),
(16, 0, 'Asmat'),
(17, 2, 'Badung'),
(18, 0, 'Balangan'),
(19, 0, 'Balikpapan'),
(20, 0, 'Banda Aceh'),
(21, 0, 'Bandar Lampung'),
(22, 5, 'Bandung'),
(24, 0, 'Bandung Barat'),
(25, 0, 'Banggai'),
(26, 0, 'Banggai Kepulauan'),
(27, 0, 'Bangka'),
(28, 0, 'Bangka Barat'),
(29, 0, 'Bangka Selatan'),
(30, 0, 'Bangka Tengah'),
(31, 1, 'Bangkalan'),
(32, 2, 'Bangli'),
(33, 0, 'Banjar'),
(35, 0, 'Banjarbaru'),
(36, 10, 'Banjarmasin'),
(37, 0, 'Banjarnegara'),
(38, 0, 'Bantaeng'),
(39, 0, 'Bantul'),
(40, 0, 'Banyuasin'),
(41, 0, 'Banyumas'),
(42, 2, 'Banyuwangi'),
(43, 0, 'Barito Kuala'),
(44, 0, 'Barito Selatan'),
(45, 0, 'Barito Timur'),
(46, 0, 'Barito Utara'),
(47, 0, 'Barru'),
(48, 0, 'Batam'),
(49, 0, 'Batang'),
(50, 0, 'Batang Hari'),
(51, 0, 'Batu'),
(52, 0, 'Batu Bara'),
(53, 0, 'Bau-Bau'),
(54, 0, 'Bekasi'),
(56, 0, 'Belitung'),
(57, 0, 'Belitung Timur'),
(58, 0, 'Belu'),
(59, 0, 'Bener Meriah'),
(60, 0, 'Bengkalis'),
(61, 0, 'Bengkayang'),
(62, 0, 'Bengkulu'),
(63, 0, 'Bengkulu Selatan'),
(64, 0, 'Bengkulu Tengah'),
(65, 0, 'Bengkulu Utara'),
(66, 0, 'Berau'),
(67, 0, 'Biak Numfor'),
(68, 0, 'Bima'),
(70, 0, 'Binjai'),
(71, 0, 'Bintan'),
(72, 0, 'Bireuen'),
(73, 0, 'Bitung'),
(74, 2, 'Blitar'),
(76, 0, 'Blora'),
(77, 0, 'Boalemo'),
(78, 0, 'Bogor'),
(80, 1, 'Bojonegoro'),
(81, 0, 'Bolaang Mongondow (B'),
(82, 0, 'Bolaang Mongondow Se'),
(83, 0, 'Bolaang Mongondow Ti'),
(84, 0, 'Bolaang Mongondow Ut'),
(85, 0, 'Bombana'),
(86, 2, 'Bondowoso'),
(87, 0, 'Bone'),
(88, 0, 'Bone Bolango'),
(89, 0, 'Bontang'),
(90, 0, 'Boven Digoel'),
(91, 0, 'Boyolali'),
(92, 0, 'Brebes'),
(93, 0, 'Bukittinggi'),
(94, 2, 'Buleleng'),
(95, 0, 'Bulukumba'),
(96, 0, 'Bulungan (Bulongan)'),
(97, 0, 'Bungo'),
(98, 0, 'Buol'),
(99, 0, 'Buru'),
(100, 0, 'Buru Selatan'),
(101, 0, 'Buton'),
(102, 0, 'Buton Utara'),
(103, 0, 'Ciamis'),
(104, 0, 'Cianjur'),
(105, 0, 'Cilacap'),
(106, 0, 'Cilegon'),
(107, 0, 'Cimahi'),
(108, 0, 'Cirebon'),
(110, 0, 'Dairi'),
(111, 0, 'Deiyai (Deliyai)'),
(112, 0, 'Deli Serdang'),
(113, 0, 'Demak'),
(114, 2, 'Denpasar'),
(115, 0, 'Depok'),
(116, 0, 'Dharmasraya'),
(117, 0, 'Dogiyai'),
(118, 0, 'Dompu'),
(119, 0, 'Donggala'),
(120, 0, 'Dumai'),
(121, 0, 'Empat Lawang'),
(122, 0, 'Ende'),
(123, 0, 'Enrekang'),
(124, 0, 'Fakfak'),
(125, 0, 'Flores Timur'),
(126, 0, 'Garut'),
(127, 0, 'Gayo Lues'),
(128, 2, 'Gianyar'),
(129, 0, 'Gorontalo'),
(131, 0, 'Gorontalo Utara'),
(132, 0, 'Gowa'),
(133, 1, 'Gresik'),
(134, 0, 'Grobogan'),
(135, 0, 'Gunung Kidul'),
(136, 0, 'Gunung Mas'),
(137, 0, 'Gunungsitoli'),
(138, 0, 'Halmahera Barat'),
(139, 0, 'Halmahera Selatan'),
(140, 0, 'Halmahera Tengah'),
(141, 0, 'Halmahera Timur'),
(142, 0, 'Halmahera Utara'),
(143, 0, 'Hulu Sungai Selatan'),
(144, 0, 'Hulu Sungai Tengah'),
(145, 0, 'Hulu Sungai Utara'),
(146, 0, 'Humbang Hasundutan'),
(147, 0, 'Indragiri Hilir'),
(148, 0, 'Indragiri Hulu'),
(149, 0, 'Indramayu'),
(150, 0, 'Intan Jaya'),
(151, 0, 'Jakarta Barat'),
(152, 8, 'Jakarta Pusat'),
(153, 0, 'Jakarta Selatan'),
(154, 0, 'Jakarta Timur'),
(155, 0, 'Jakarta Utara'),
(156, 0, 'Jambi'),
(157, 0, 'Jayapura'),
(158, 1, 'Jayapura'),
(159, 0, 'Jayawijaya'),
(160, 2, 'Jember'),
(161, 2, 'Jembrana'),
(162, 0, 'Jeneponto'),
(163, 0, 'Jepara'),
(164, 2, 'Jombang'),
(165, 0, 'Kaimana'),
(166, 0, 'Kampar'),
(167, 0, 'Kapuas'),
(168, 0, 'Kapuas Hulu'),
(169, 0, 'Karanganyar'),
(170, 2, 'Karangasem'),
(171, 0, 'Karawang'),
(172, 0, 'Karimun'),
(173, 0, 'Karo'),
(174, 0, 'Katingan'),
(175, 0, 'Kaur'),
(176, 0, 'Kayong Utara'),
(177, 0, 'Kebumen'),
(178, 1, 'Kediri'),
(180, 0, 'Keerom'),
(181, 0, 'Kendal'),
(182, 0, 'Kendari'),
(183, 0, 'Kepahiang'),
(184, 0, 'Kepulauan Anambas'),
(185, 0, 'Kepulauan Aru'),
(186, 0, 'Kepulauan Mentawai'),
(187, 0, 'Kepulauan Meranti'),
(188, 0, 'Kepulauan Sangihe'),
(189, 0, 'Kepulauan Seribu'),
(190, 0, 'Kepulauan Siau Tagul'),
(191, 0, 'Kepulauan Sula'),
(192, 0, 'Kepulauan Talaud'),
(193, 0, 'Kepulauan Yapen (Yap'),
(194, 0, 'Kerinci'),
(195, 0, 'Ketapang'),
(196, 0, 'Klaten'),
(197, 2, 'Klungkung'),
(198, 0, 'Kolaka'),
(199, 0, 'Kolaka Utara'),
(200, 0, 'Konawe'),
(201, 0, 'Konawe Selatan'),
(202, 0, 'Konawe Utara'),
(203, 0, 'Kotabaru'),
(204, 0, 'Kotamobagu'),
(205, 0, 'Kotawaringin Barat'),
(206, 0, 'Kotawaringin Timur'),
(207, 0, 'Kuantan Singingi'),
(208, 0, 'Kubu Raya'),
(209, 0, 'Kudus'),
(210, 0, 'Kulon Progo'),
(211, 0, 'Kuningan'),
(212, 2, 'Kupang'),
(214, 0, 'Kutai Barat'),
(215, 0, 'Kutai Kartanegara'),
(216, 0, 'Kutai Timur'),
(217, 0, 'Labuhan Batu'),
(218, 0, 'Labuhan Batu Selatan'),
(219, 0, 'Labuhan Batu Utara'),
(220, 0, 'Lahat'),
(221, 0, 'Lamandau'),
(222, 1, 'Lamongan'),
(223, 0, 'Lampung Barat'),
(224, 0, 'Lampung Selatan'),
(225, 0, 'Lampung Tengah'),
(226, 0, 'Lampung Timur'),
(227, 0, 'Lampung Utara'),
(228, 0, 'Landak'),
(229, 0, 'Langkat'),
(230, 0, 'Langsa'),
(231, 0, 'Lanny Jaya'),
(232, 0, 'Lebak'),
(233, 0, 'Lebong'),
(234, 0, 'Lembata'),
(235, 0, 'Lhokseumawe'),
(236, 0, 'Lima Puluh Koto/Kota'),
(237, 0, 'Lingga'),
(238, 0, 'Lombok Barat'),
(239, 0, 'Lombok Tengah'),
(240, 0, 'Lombok Timur'),
(241, 0, 'Lombok Utara'),
(242, 0, 'Lubuk Linggau'),
(243, 2, 'Lumajang'),
(244, 0, 'Luwu'),
(245, 0, 'Luwu Timur'),
(246, 0, 'Luwu Utara'),
(247, 1, 'Madiun'),
(249, 0, 'Magelang'),
(250, 0, 'Magelang'),
(251, 1, 'Magetan'),
(252, 0, 'Majalengka'),
(253, 0, 'Majene'),
(254, 6, 'Makassar'),
(255, 2, 'Malang'),
(257, 0, 'Malinau'),
(258, 0, 'Maluku Barat Daya'),
(259, 0, 'Maluku Tengah'),
(260, 0, 'Maluku Tenggara'),
(261, 0, 'Maluku Tenggara Bara'),
(262, 0, 'Mamasa'),
(263, 0, 'Mamberamo Raya'),
(264, 0, 'Mamberamo Tengah'),
(265, 0, 'Mamuju'),
(266, 0, 'Mamuju Utara'),
(267, 0, 'Manado'),
(268, 0, 'Mandailing Natal'),
(269, 0, 'Manggarai'),
(270, 0, 'Manggarai Barat'),
(271, 0, 'Manggarai Timur'),
(272, 1, 'Manokwari'),
(273, 1, 'Timika'),
(274, 0, 'Mappi'),
(275, 0, 'Maros'),
(276, 2, 'Mataram'),
(277, 0, 'Maybrat'),
(278, 11, 'Medan'),
(279, 0, 'Melawi'),
(280, 0, 'Merangin'),
(281, 0, 'Merauke'),
(282, 0, 'Mesuji'),
(283, 0, 'Metro'),
(284, 0, 'Mimika'),
(285, 0, 'Minahasa'),
(286, 0, 'Minahasa Selatan'),
(287, 0, 'Minahasa Tenggara'),
(288, 0, 'Minahasa Utara'),
(289, 2, 'Mojokerto'),
(290, 2, 'Surabaya2'),
(291, 0, 'Morowali'),
(292, 0, 'Muara Enim'),
(293, 0, 'Muaro Jambi'),
(294, 0, 'Muko Muko'),
(295, 0, 'Muna'),
(296, 0, 'Murung Raya'),
(297, 0, 'Musi Banyuasin'),
(298, 0, 'Musi Rawas'),
(299, 0, 'Nabire'),
(300, 0, 'Nagan Raya'),
(301, 0, 'Nagekeo'),
(302, 0, 'Natuna'),
(303, 0, 'Nduga'),
(304, 0, 'Ngada'),
(305, 1, 'Nganjuk'),
(306, 1, 'Ngawi'),
(307, 0, 'Nias'),
(308, 0, 'Nias Barat'),
(309, 0, 'Nias Selatan'),
(310, 0, 'Nias Utara'),
(311, 0, 'Nunukan'),
(312, 0, 'Ogan Ilir'),
(313, 0, 'Ogan Komering Ilir'),
(314, 0, 'Ogan Komering Ulu'),
(315, 0, 'Ogan Komering Ulu Se'),
(316, 0, 'Ogan Komering Ulu Ti'),
(317, 1, 'Pacitan'),
(318, 0, 'Padang'),
(319, 0, 'Padang Lawas'),
(320, 0, 'Padang Lawas Utara'),
(321, 0, 'Padang Panjang'),
(322, 0, 'Padang Pariaman'),
(323, 0, 'Padang Sidempuan'),
(324, 0, 'Pagar Alam'),
(325, 0, 'Pakpak Bharat'),
(326, 10, 'Palangka Raya'),
(327, 7, 'Palembang'),
(328, 0, 'Palopo'),
(329, 0, 'Palu'),
(330, 1, 'Pamekasan'),
(331, 0, 'Pandeglang'),
(332, 0, 'Pangandaran'),
(333, 0, 'Pangkajene Kepulauan'),
(334, 0, 'Pangkal Pinang'),
(335, 0, 'Paniai'),
(336, 0, 'Parepare'),
(337, 0, 'Pariaman'),
(338, 0, 'Parigi Moutong'),
(339, 0, 'Pasaman'),
(340, 0, 'Pasaman Barat'),
(341, 0, 'Paser'),
(342, 2, 'Pasuruan'),
(344, 0, 'Pati'),
(345, 0, 'Payakumbuh'),
(346, 0, 'Pegunungan Arfak'),
(347, 0, 'Pegunungan Bintang'),
(348, 0, 'Pekalongan'),
(350, 0, 'Pekanbaru'),
(351, 0, 'Pelalawan'),
(352, 0, 'Pemalang'),
(353, 0, 'Pematang Siantar'),
(354, 0, 'Penajam Paser Utara'),
(355, 0, 'Pesawaran'),
(356, 0, 'Pesisir Barat'),
(357, 0, 'Pesisir Selatan'),
(358, 0, 'Pidie'),
(359, 0, 'Pidie Jaya'),
(360, 0, 'Pinrang'),
(361, 0, 'Pohuwato'),
(362, 0, 'Polewali Mandar'),
(363, 1, 'Ponorogo'),
(364, 0, 'Pontianak'),
(366, 0, 'Poso'),
(367, 0, 'Prabumulih'),
(368, 0, 'Pringsewu'),
(369, 2, 'Probolinggo'),
(371, 0, 'Pulang Pisau'),
(372, 0, 'Pulau Morotai'),
(373, 0, 'Puncak'),
(374, 0, 'Puncak Jaya'),
(375, 0, 'Purbalingga'),
(376, 0, 'Purwakarta'),
(377, 0, 'Purworejo'),
(378, 0, 'Raja Ampat'),
(379, 0, 'Rejang Lebong'),
(380, 0, 'Rembang'),
(381, 0, 'Rokan Hilir'),
(382, 0, 'Rokan Hulu'),
(383, 0, 'Rote Ndao'),
(384, 0, 'Sabang'),
(385, 0, 'Sabu Raijua'),
(386, 0, 'Salatiga'),
(387, 0, 'Samarinda'),
(388, 0, 'Sambas'),
(389, 0, 'Samosir'),
(390, 1, 'Sampang'),
(391, 0, 'Sanggau'),
(392, 0, 'Sarmi'),
(393, 0, 'Sarolangun'),
(394, 0, 'Sawah Lunto'),
(395, 0, 'Sekadau'),
(396, 0, 'Selayar (Kepulauan S'),
(397, 0, 'Seluma'),
(398, 3, 'Semarang'),
(399, 0, 'Semarang'),
(400, 0, 'Seram Bagian Barat'),
(401, 0, 'Seram Bagian Timur'),
(402, 0, 'Serang'),
(404, 0, 'Serdang Bedagai'),
(405, 0, 'Seruyan'),
(406, 0, 'Siak'),
(407, 0, 'Sibolga'),
(408, 0, 'Sidenreng Rappang/Ra'),
(409, 2, 'Sidoarjo'),
(410, 0, 'Sigi'),
(411, 0, 'Sijunjung (Sawah Lun'),
(412, 0, 'Sikka'),
(413, 0, 'Simalungun'),
(414, 0, 'Simeulue'),
(415, 0, 'Singkawang'),
(416, 0, 'Sinjai'),
(417, 0, 'Sintang'),
(418, 2, 'Situbondo'),
(419, 0, 'Sleman'),
(420, 0, 'Solok'),
(422, 0, 'Solok Selatan'),
(423, 0, 'Soppeng'),
(424, 0, 'Sorong'),
(426, 0, 'Sorong Selatan'),
(427, 0, 'Sragen'),
(428, 0, 'Subang'),
(429, 0, 'Subulussalam'),
(430, 0, 'Sukabumi'),
(432, 0, 'Sukamara'),
(433, 0, 'Sukoharjo'),
(434, 0, 'Sumba Barat'),
(435, 0, 'Sumba Barat Daya'),
(436, 0, 'Sumba Tengah'),
(437, 0, 'Sumba Timur'),
(438, 0, 'Sumbawa'),
(439, 0, 'Sumbawa Barat'),
(440, 0, 'Sumedang'),
(441, 1, 'Sumenep'),
(442, 0, 'Sungaipenuh'),
(443, 0, 'Supiori'),
(444, 1, 'Surabaya1'),
(445, 4, 'Surakarta (Solo)'),
(446, 0, 'Tabalong'),
(447, 2, 'Tabanan'),
(448, 0, 'Takalar'),
(449, 0, 'Tambrauw'),
(450, 0, 'Tana Tidung'),
(451, 0, 'Tana Toraja'),
(452, 0, 'Tanah Bumbu'),
(453, 0, 'Tanah Datar'),
(454, 0, 'Tanah Laut'),
(455, 0, 'Tangerang'),
(457, 0, 'Tangerang Selatan'),
(458, 0, 'Tanggamus'),
(459, 0, 'Tanjung Balai'),
(460, 0, 'Tanjung Jabung Barat'),
(461, 0, 'Tanjung Jabung Timur'),
(462, 0, 'Tanjung Pinang'),
(463, 0, 'Tapanuli Selatan'),
(464, 0, 'Tapanuli Tengah'),
(465, 0, 'Tapanuli Utara'),
(466, 0, 'Tapin'),
(467, 0, 'Tarakan'),
(468, 0, 'Tasikmalaya'),
(470, 0, 'Tebing Tinggi'),
(471, 0, 'Tebo'),
(472, 3, 'Tegal1'),
(473, 0, 'Tegal2'),
(474, 0, 'Teluk Bintuni'),
(475, 0, 'Teluk Wondama'),
(476, 0, 'Temanggung'),
(477, 0, 'Ternate'),
(478, 0, 'Tidore Kepulauan'),
(479, 0, 'Timor Tengah Selatan'),
(480, 0, 'Timor Tengah Utara'),
(481, 0, 'Toba Samosir'),
(482, 0, 'Tojo Una-Una'),
(483, 0, 'Toli-Toli'),
(484, 0, 'Tolikara'),
(485, 0, 'Tomohon'),
(486, 0, 'Toraja Utara'),
(487, 1, 'Trenggalek'),
(488, 0, 'Tual'),
(489, 1, 'Tuban'),
(490, 0, 'Tulang Bawang'),
(491, 0, 'Tulang Bawang Barat'),
(492, 1, 'Tulungagung'),
(493, 0, 'Wajo'),
(494, 0, 'Wakatobi'),
(495, 0, 'Waropen'),
(496, 0, 'Way Kanan'),
(497, 0, 'Wonogiri'),
(498, 0, 'Wonosobo'),
(499, 0, 'Yahukimo'),
(500, 0, 'Yalimo'),
(501, 4, 'Yogyakarta');

-- --------------------------------------------------------

--
-- Table structure for table `logging`
--

CREATE TABLE `logging` (
  `id` int(11) NOT NULL,
  `kd_user` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `log_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `detail_akses` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logging`
--

INSERT INTO `logging` (`id`, `kd_user`, `description`, `log_time`, `detail_akses`) VALUES
(4402, 1, 'LOGIN', '2016-10-23 22:04:22', 'Backend'),
(4401, 1, 'LOGIN', '2016-10-23 19:09:27', 'Backend'),
(4400, 1, 'LOGIN', '2016-10-23 13:30:45', 'Backend'),
(4398, 1, 'LOGIN', '2016-10-23 09:27:29', 'Backend'),
(4399, 1, 'LOGIN', '2016-10-23 13:04:29', 'Backend'),
(4397, 1, 'LOGIN', '2016-10-20 10:14:20', 'Backend'),
(4396, 1, 'LOGIN', '2016-10-20 09:05:39', 'Backend'),
(4395, 1, 'LOGIN', '2016-10-20 08:53:10', 'Backend'),
(4394, 0, 'Register visit plan to outlet Toko Saudagar', '2016-10-20 08:17:03', 'desktop'),
(4393, 0, 'Register visit plan to outlet Toko Saudagar', '2016-10-20 08:16:38', 'desktop'),
(4392, 0, 'Register visit plan to outlet Toko Saudagar', '2016-10-20 07:55:47', 'desktop'),
(4391, 0, 'Register visit plan to outlet Toko Saudagar', '2016-10-20 07:55:34', 'desktop'),
(4390, 1, 'LOGIN', '2016-10-20 06:18:48', 'Backend'),
(4389, 1, 'LOGIN', '2016-10-20 05:57:20', 'Backend'),
(4388, 1, 'LOGIN', '2016-10-14 06:55:11', 'Backend'),
(4387, 1, 'LOGIN', '2016-10-13 14:29:58', 'Backend'),
(4386, 1, 'LOGIN', '2016-10-07 07:22:15', 'Backend'),
(4385, 1, 'LOGIN', '2016-10-07 06:50:01', 'Backend'),
(4384, 1, 'LOGIN', '2016-10-07 06:31:36', 'Backend'),
(4383, 1, 'LOGOUT', '2016-09-29 08:33:34', 'Backend'),
(4382, 1, 'LOGIN', '2016-09-29 08:31:23', 'Backend'),
(4381, 1, 'LOGOUT', '2016-09-29 03:31:47', 'Backend'),
(4380, 1, 'LOGIN', '2016-09-29 03:31:03', 'Backend'),
(4379, 1, 'LOGIN', '2016-09-28 08:25:52', 'Backend'),
(4378, 1, 'LOGIN', '2016-09-23 07:19:05', 'Backend'),
(4377, 1, 'LOGIN', '2016-09-22 08:18:03', 'Backend'),
(4376, 1, 'LOGIN', '2016-09-22 06:50:04', 'Backend'),
(4375, 1, 'LOGIN', '2016-09-22 05:44:18', 'Backend'),
(4374, 1, 'LOGIN', '2016-09-22 05:20:53', 'Backend'),
(4373, 332, 'Update outlet Toko Saudagar', '0000-00-00 00:00:00', 'desktop'),
(4372, 1, 'Delete Outlet with ID 975', '2016-09-22 04:08:57', 'Backend'),
(4371, 1, 'LOGIN', '2016-09-22 04:08:43', 'Backend'),
(4370, 328, 'Register outlet coba', '0000-00-00 00:00:00', 'desktop'),
(4369, 328, 'Register outlet coba', '0000-00-00 00:00:00', 'desktop'),
(4368, 1, 'Delete Outlet with ID 974', '2016-09-16 06:31:53', 'Backend'),
(4367, 1, 'LOGIN', '2016-09-16 06:26:15', 'Backend'),
(4366, 334, 'Register outlet coba', '2016-09-16 06:25:09', 'mobile'),
(4365, 1, 'LOGIN', '2016-09-15 08:36:34', 'Backend'),
(4364, 1, 'LOGIN', '2016-09-15 07:11:53', 'Backend'),
(4363, 1, 'LOGIN', '2016-09-15 06:33:40', 'Backend'),
(4362, 0, 'Register visit plan to outlet CV. lorem ipsum 23', '2016-09-08 09:56:47', 'desktop'),
(4361, 1, 'LOGIN', '2016-09-08 07:44:55', 'Backend'),
(4360, 1, 'LOGIN', '2016-09-08 06:50:41', 'Backend'),
(4359, 337, 'Update Sales Force with ID 336', '2016-09-08 05:15:55', 'Backend'),
(4358, 337, 'LOGIN', '2016-09-08 05:13:50', 'Backend'),
(4357, 336, 'LOGIN', '2016-09-07 11:48:57', 'Backend'),
(4355, 337, 'LOGIN', '2016-09-07 08:56:13', 'Backend'),
(4356, 1, 'LOGIN', '2016-09-07 09:49:57', 'Backend'),
(4354, 337, 'Update User with ID 335', '2016-09-07 08:29:22', 'Backend'),
(4353, 337, 'Update Sales Force with ID 336', '2016-09-07 08:28:07', 'Backend'),
(4352, 337, 'Update User with ID 337', '2016-09-07 08:26:24', 'Backend'),
(4351, 337, 'LOGIN', '2016-09-07 08:24:43', 'Backend'),
(4350, 0, 'Register visit plan to outlet Toko Saudagar', '2016-09-07 08:22:13', 'desktop'),
(4349, 0, 'Register visit plan to outlet Toko Saudagar', '2016-09-07 08:10:57', 'desktop'),
(4348, 0, 'Register visit plan to outlet Toko Saudagar', '2016-09-07 08:01:19', 'desktop'),
(4347, 1, 'Make New User', '2016-09-07 07:54:48', 'Backend'),
(4346, 1, 'Make New Sales Force', '2016-09-07 07:52:16', 'Backend'),
(4345, 1, 'Update User with ID 335', '2016-09-07 07:49:15', 'Backend'),
(4344, 1, 'Update User with ID 333', '2016-09-07 07:48:59', 'Backend'),
(4343, 1, 'Update User with ID 335', '2016-09-07 07:48:34', 'Backend'),
(4342, 1, 'LOGIN', '2016-09-07 07:45:52', 'Backend'),
(4341, 1, 'LOGIN', '2016-09-07 00:29:29', 'Backend'),
(4340, 1, 'LOGOUT', '2016-09-06 11:15:07', 'Backend'),
(4339, 1, 'LOGIN', '2016-09-06 11:13:03', 'Backend'),
(4338, 1, 'LOGIN', '2016-09-06 10:48:32', 'Backend'),
(4337, 333, 'LOGIN', '2016-09-06 10:47:54', 'Backend'),
(4336, 1, 'LOGIN', '2016-09-06 02:44:02', 'Backend'),
(4335, 1, 'LOGIN', '2016-09-05 04:31:48', 'Backend'),
(4334, 1, 'LOGIN', '2016-09-02 09:43:54', 'Backend'),
(4333, 1, 'LOGIN', '2016-09-02 09:25:57', 'Backend'),
(4332, 1, 'LOGIN', '2016-09-02 09:14:04', 'Backend'),
(4331, 1, 'LOGIN', '2016-09-02 07:55:42', 'Backend'),
(4330, 1, 'LOGIN', '2016-09-02 07:24:36', 'Backend'),
(4329, 1, 'LOGIN', '2016-09-02 06:54:01', 'Backend'),
(4328, 1, 'LOGIN', '2016-09-02 06:31:47', 'Backend'),
(4327, 1, 'LOGIN', '2016-09-01 09:51:13', 'Backend'),
(4326, 1, 'Download Master Data Outlet', '2016-09-01 09:29:32', 'Backend'),
(4325, 1, 'LOGIN', '2016-09-01 09:16:43', 'Backend'),
(4324, 1, 'LOGIN', '2016-09-01 08:40:18', 'Backend'),
(4323, 1, 'LOGIN', '2016-09-01 05:12:50', 'Backend'),
(4322, 1, 'LOGIN', '2016-09-01 03:53:10', 'Backend'),
(4321, 1, 'LOGIN', '2016-08-31 13:41:57', 'Backend'),
(4320, 1, 'LOGIN', '2016-08-31 07:23:12', 'Backend'),
(4319, 1, 'LOGIN', '2016-08-31 07:10:10', 'Backend'),
(4318, 1, 'LOGIN', '2016-08-29 07:38:02', 'Backend'),
(4317, 1, 'LOGIN', '2016-08-29 06:37:07', 'Backend'),
(4316, 1, 'LOGIN', '2016-08-29 05:11:51', 'Backend'),
(4315, 1, 'LOGIN', '2016-08-29 03:11:05', 'Backend'),
(4314, 1, 'LOGOUT', '2016-08-29 03:00:27', 'Backend'),
(4313, 1, 'LOGIN', '2016-08-29 02:59:18', 'Backend'),
(4312, 1, 'LOGIN', '2016-08-23 08:01:09', 'Backend'),
(4311, 1, 'LOGIN', '2016-08-23 07:48:33', 'Backend'),
(4310, 1, 'LOGIN', '2016-08-23 06:30:51', 'Backend'),
(4309, 1, 'LOGIN', '2016-08-23 03:53:31', 'Backend'),
(4308, 1, 'LOGIN', '2016-08-22 09:52:53', 'Backend'),
(4307, 1, 'LOGIN', '2016-08-22 09:37:07', 'Backend'),
(4306, 1, 'LOGIN', '2016-08-22 09:18:27', 'Backend'),
(4305, 1, 'LOGIN', '2016-08-13 02:55:29', 'Backend'),
(4304, 1, 'Update Outlet with ID 971', '2016-08-11 13:34:59', 'Backend'),
(4303, 1, 'Update Outlet with ID 970', '2016-08-11 13:34:33', 'Backend'),
(4302, 1, 'LOGIN', '2016-08-11 13:33:53', 'Backend'),
(4301, 1, 'LOGIN', '2016-08-11 12:40:15', 'Backend'),
(4300, 1, 'LOGIN', '2016-08-11 12:27:24', 'Backend'),
(4299, 1, 'Update Outlet with ID 969', '2016-08-11 12:10:58', 'Backend'),
(4298, 1, 'Update Outlet with ID 969', '2016-08-11 12:10:18', 'Backend'),
(4297, 1, 'Update Outlet with ID 969', '2016-08-11 12:09:47', 'Backend'),
(4296, 1, 'Update Outlet with ID 971', '2016-08-11 12:09:28', 'Backend'),
(4295, 1, 'LOGIN', '2016-08-11 12:07:02', 'Backend'),
(4294, 1, 'LOGIN', '2016-08-11 11:51:36', 'Backend'),
(4293, 1, 'Update Outlet with ID 969', '2016-08-11 11:29:25', 'Backend'),
(4292, 1, 'Update Outlet with ID 968', '2016-08-11 11:29:07', 'Backend'),
(4291, 1, 'LOGIN', '2016-08-11 11:22:34', 'Backend'),
(4290, 1, 'LOGIN', '2016-08-11 10:17:12', 'Backend'),
(4289, 1, 'LOGIN', '2016-08-11 09:48:55', 'Backend'),
(4288, 335, 'LOGIN', '2016-08-11 08:41:38', 'Backend'),
(4287, 335, 'LOGIN', '2016-08-11 08:39:05', 'Backend'),
(4286, 1, 'Update Outlet with ID 968', '2016-08-11 03:13:36', 'Backend'),
(4285, 1, 'Update Outlet with ID 973', '2016-08-11 03:01:44', 'Backend'),
(4284, 1, 'LOGIN', '2016-08-11 03:00:47', 'Backend'),
(4283, 1, 'LOGIN', '2016-08-11 01:11:38', 'Backend'),
(4282, 335, 'LOGOUT', '2016-08-11 01:11:25', 'Backend'),
(4281, 335, 'LOGIN', '2016-08-11 01:10:24', 'Backend'),
(4280, 334, 'Register visit plan to outlet To Tamyis', '2016-08-11 00:55:26', 'mobile'),
(4279, 333, 'LOGOUT', '2016-08-11 00:15:50', 'Backend'),
(4278, 333, 'LOGIN', '2016-08-11 00:15:34', 'Backend'),
(4277, 1, 'LOGOUT', '2016-08-11 00:15:27', 'Backend'),
(4276, 1, 'Update User with ID 333', '2016-08-11 00:15:22', 'Backend'),
(4275, 1, 'Update User with ID 1', '2016-08-11 00:14:38', 'Backend'),
(4274, 1, 'Update User with ID 333', '2016-08-11 00:13:20', 'Backend'),
(4273, 1, 'Make New User', '2016-08-11 00:12:25', 'Backend'),
(4272, 1, 'LOGIN', '2016-08-11 00:10:23', 'Backend'),
(4271, 334, 'LOGOUT', '2016-08-11 00:10:15', 'Backend'),
(4270, 334, 'LOGIN', '2016-08-11 00:09:55', 'Backend'),
(4269, 1, 'LOGOUT', '2016-08-11 00:09:47', 'Backend'),
(4268, 1, 'Make New Product', '2016-08-11 00:06:14', 'Backend'),
(4267, 1, 'Make New Product', '2016-08-11 00:06:00', 'Backend'),
(4266, 1, 'Delete Product with ID 23', '2016-08-11 00:05:50', 'Backend'),
(4265, 1, 'Make New Product', '2016-08-11 00:05:46', 'Backend'),
(4264, 1, 'LOGIN', '2016-08-11 00:05:19', 'Backend'),
(4263, 1, 'LOGIN', '2016-08-10 09:23:40', 'Backend'),
(4262, 1, 'LOGOUT', '2016-08-10 05:09:13', 'Backend'),
(4261, 1, 'LOGIN', '2016-08-10 05:08:10', 'Backend'),
(4260, 334, 'Upload foto _outlet', '2016-08-10 04:19:21', 'mobile'),
(4259, 334, 'Submit visit plan to outlet To Tamyis', '2016-08-10 04:17:02', 'mobile'),
(4258, 334, 'Register visit plan to outlet To Tamyis', '2016-08-10 04:16:33', 'mobile'),
(4257, 334, 'Submit visit plan to outlet To Tamyis', '2016-08-10 04:15:28', 'mobile'),
(4256, 1, 'Delete Visit Plan', '2016-08-10 04:14:37', 'Backend'),
(4255, 1, 'LOGIN', '2016-08-10 04:14:02', 'Backend'),
(4254, 334, 'Register visit plan to outlet To Tamyis', '2016-08-10 04:13:09', 'mobile'),
(4253, 334, 'Register visit plan to outlet To Tamyis', '2016-08-10 04:12:48', 'mobile'),
(4252, 334, 'Submit visit plan to outlet To Tamyis', '2016-08-10 04:12:21', 'mobile'),
(4251, 334, 'Register visit plan to outlet To Tamyis', '2016-08-10 04:11:50', 'mobile'),
(4250, 334, 'Register outlet To Tamyis', '2016-08-10 04:11:50', 'mobile'),
(4249, 1, 'Update Sales Force with ID 331', '2016-08-10 04:04:48', 'Backend'),
(4248, 1, 'Update Sales Force with ID 331', '2016-08-10 04:04:06', 'Backend'),
(4247, 1, 'Update Sales Force with ID 331', '2016-08-10 04:03:36', 'Backend'),
(4246, 1, 'Make New User', '2016-08-10 04:02:32', 'Backend'),
(4245, 1, 'LOGIN', '2016-08-10 03:51:49', 'Backend'),
(4244, 1, 'LOGIN', '2016-08-10 03:49:47', 'Backend'),
(4243, 1, 'Update Outlet with ID 969', '2016-08-10 03:38:30', 'Backend'),
(4242, 1, 'Make Visit Plan', '2016-08-10 03:38:14', 'Backend'),
(4241, 1, 'LOGIN', '2016-08-10 03:37:59', 'Backend'),
(4240, 1, 'Update Product with ID 23', '2016-08-10 03:37:14', 'Backend'),
(4239, 1, 'Update Distributor with ID 1', '2016-08-10 03:37:04', 'Backend'),
(4238, 1, 'Update Kota with ID 1', '2016-08-10 03:36:55', 'Backend'),
(4237, 1, 'LOGIN', '2016-08-10 03:25:24', 'Backend'),
(4236, 332, 'Submit visit plan to outlet PT Trikarya Teknologi Indonesia', '2016-08-09 05:26:19', 'mobile'),
(4235, 332, 'Register visit plan to outlet PT Trikarya Teknologi Indonesia', '2016-08-09 05:26:06', 'mobile'),
(4234, 332, 'Register outlet PT Trikarya Teknologi Indonesia', '2016-08-09 05:26:06', 'mobile'),
(4233, 332, 'Register visit plan to outlet CV. lorem ipsum', '2016-08-08 12:24:17', 'mobile'),
(4232, 1, 'Delete Outlet with ID 967', '2016-08-08 12:22:22', 'Backend'),
(4231, 332, 'Submit visit plan to outlet PT. Lorem ipsum', '2016-08-08 12:21:22', 'mobile'),
(4230, 332, 'Register visit plan to outlet PT. Lorem ipsum', '2016-08-08 12:21:00', 'mobile'),
(4229, 332, 'Register outlet PT. Lorem ipsum', '2016-08-08 12:21:00', 'mobile'),
(4228, 1, 'LOGIN', '2016-08-08 12:19:23', 'Backend'),
(4227, 1, 'LOGIN', '2016-08-06 11:34:59', 'Backend'),
(4226, 1, 'LOGOUT', '2016-08-05 05:33:35', 'Backend'),
(4225, 1, 'LOGIN', '2016-08-05 05:32:12', 'Backend'),
(4224, 332, 'Register visit plan to outlet dependencies 1', '2016-08-01 11:56:11', 'mobile'),
(4223, 332, 'Register outlet dependencies 1', '2016-08-01 11:56:11', 'mobile'),
(4222, 332, 'LOGIN', '2016-08-01 04:13:45', 'Backend'),
(4221, 1, 'LOGOUT', '2016-08-01 04:13:40', 'Backend'),
(4220, 1, 'LOGIN', '2016-08-01 03:38:36', 'Backend'),
(4219, 332, 'LOGOUT', '2016-08-01 03:38:32', 'Backend'),
(4218, 332, 'LOGIN', '2016-08-01 03:25:13', 'Backend'),
(4217, 1, 'LOGOUT', '2016-08-01 03:25:07', 'Backend'),
(4216, 1, 'LOGIN', '2016-08-01 03:06:43', 'Backend'),
(4215, 332, 'Register visit plan to outlet CV. lorem ipsum', '2016-07-27 14:50:33', 'mobile'),
(4214, 332, 'Register outlet CV. lorem ipsum', '2016-07-27 14:50:33', 'mobile'),
(4213, 332, 'Upload foto _outlet', '2016-07-26 13:34:45', 'mobile'),
(4212, 1, 'LOGIN', '2016-07-26 10:01:31', 'Backend'),
(4211, 333, 'Download Data Take Order', '2016-07-26 09:38:00', 'Backend'),
(4210, 333, 'Download Data Take Order', '2016-07-26 09:37:56', 'Backend'),
(4209, 333, 'LOGIN', '2016-07-26 09:36:32', 'Backend'),
(4208, 1, 'LOGOUT', '2016-07-26 09:36:24', 'Backend'),
(4207, 1, 'Update User with ID 333', '2016-07-26 09:36:18', 'Backend'),
(4206, 1, 'LOGIN', '2016-07-26 09:36:00', 'Backend'),
(4205, 333, 'LOGOUT', '2016-07-26 09:35:51', 'Backend'),
(4203, 333, 'LOGIN', '2016-07-26 07:56:38', 'Backend'),
(4204, 333, 'LOGIN', '2016-07-26 09:35:07', 'Backend'),
(4202, 1, 'LOGOUT', '2016-07-26 07:56:32', 'Backend'),
(4201, 1, 'Update User with ID 333', '2016-07-26 07:56:22', 'Backend'),
(4200, 1, 'Make New User', '2016-07-26 07:56:07', 'Backend'),
(4199, 1, 'Download Data Take Order', '2016-07-26 07:48:17', 'Backend'),
(4198, 1, 'Download Data Take Order', '2016-07-26 07:47:29', 'Backend'),
(4197, 1, 'Download Data Take Order', '2016-07-26 07:46:39', 'Backend'),
(4196, 1, 'LOGIN', '2016-07-26 07:44:31', 'Backend'),
(4195, 1, 'LOGIN', '2016-07-26 05:23:44', 'Backend'),
(4194, 1, 'LOGIN', '2016-07-26 05:09:16', 'Backend'),
(4193, 1, 'Make New Area', '2016-07-26 04:38:56', 'Backend'),
(4192, 1, 'Update Sales Force with ID 328', '2016-07-26 04:31:04', 'Backend'),
(4191, 1, 'Download Master Data Sales Force', '2016-07-26 04:30:27', 'Backend'),
(4190, 1, 'Download Master Data Outlet', '2016-07-26 04:30:00', 'Backend'),
(4189, 1, 'Download Data Take Order', '2016-07-26 04:17:41', 'Backend'),
(4188, 1, 'Download Data Take Order', '2016-07-26 04:16:40', 'Backend'),
(4187, 1, 'Download Data Take Order', '2016-07-26 04:13:34', 'Backend'),
(4186, 1, 'Download Data Take Order', '2016-07-26 04:12:58', 'Backend'),
(4185, 1, 'Download Data Take Order', '2016-07-26 04:12:24', 'Backend'),
(4184, 1, 'Download Data Take Order', '2016-07-26 03:40:06', 'Backend'),
(4183, 1, 'Download Data Take Order', '2016-07-26 03:38:58', 'Backend'),
(4182, 1, 'Download Data Take Order', '2016-07-26 03:14:49', 'Backend'),
(4181, 1, 'Download Data Take Order', '2016-07-26 03:14:07', 'Backend'),
(4180, 1, 'Download Data Take Order', '2016-07-26 03:13:00', 'Backend'),
(4179, 1, 'Download Data Take Order', '2016-07-26 03:01:40', 'Backend'),
(4178, 1, 'Download Data Take Order', '2016-07-26 02:52:09', 'Backend'),
(4177, 1, 'Download Data Take Order', '2016-07-26 02:50:27', 'Backend'),
(4176, 1, 'Download Data Take Order', '2016-07-26 02:49:58', 'Backend'),
(4175, 1, 'Download Data Take Order', '2016-07-26 02:47:38', 'Backend'),
(4174, 1, 'Download Data Take Order', '2016-07-26 02:43:22', 'Backend'),
(4173, 1, 'Download Data Take Order', '2016-07-26 02:39:19', 'Backend'),
(4172, 1, 'Download Data Take Order', '2016-07-26 02:38:47', 'Backend'),
(4171, 1, 'Download Data Take Order', '2016-07-26 02:35:36', 'Backend'),
(4170, 332, 'Submit visit plan to outlet storr', '2016-07-26 02:34:37', 'mobile'),
(4169, 1, 'Make New Product', '2016-07-26 02:33:43', 'Backend'),
(4166, 1, 'LOGIN', '2016-07-26 02:28:59', 'Backend'),
(4167, 332, 'Register outlet storr', '2016-07-26 02:32:45', 'mobile'),
(4168, 332, 'Register visit plan to outlet storr', '2016-07-26 02:32:45', 'mobile'),
(4403, 1, 'LOGIN', '2016-11-08 06:29:54', 'Backend'),
(4404, 336, 'LOGIN', '2016-11-08 06:40:21', 'Backend'),
(4405, 333, 'LOGIN', '2016-11-08 06:44:17', 'Backend'),
(4406, 337, 'LOGIN', '2016-11-08 06:45:02', 'Backend'),
(4407, 337, 'LOGOUT', '2016-11-08 06:53:21', 'Backend'),
(4408, 336, 'LOGIN', '2016-11-08 06:54:03', 'Backend'),
(4409, 337, 'LOGIN', '2016-11-08 06:56:04', 'Backend'),
(4410, 337, 'Update Sales Force with ID 336', '2016-11-08 06:56:35', 'Backend'),
(4411, 337, 'Update Product with ID 24', '2016-11-08 07:00:30', 'Backend'),
(4412, 337, 'Update Product with ID 25', '2016-11-08 07:00:40', 'Backend'),
(4413, 337, 'Update Product with ID 26', '2016-11-08 07:00:49', 'Backend'),
(4414, 1, 'LOGIN', '2016-11-08 07:02:27', 'Backend'),
(4415, 1, 'Update Sales Force with ID 332', '2016-11-08 07:10:44', 'Backend'),
(4416, 332, 'Register outlet makwk', '2016-11-08 07:21:58', 'mobile'),
(4417, 332, 'Register visit plan to outlet PT Trikarya Teknologi Indonesia', '2016-11-08 07:22:36', 'mobile'),
(4418, 1, 'LOGIN', '2016-11-08 07:27:23', 'Backend'),
(4419, 1, 'LOGIN', '2016-11-14 04:24:41', 'Backend'),
(4420, 1, 'Make New Sales Force', '2016-11-14 04:27:05', 'Backend'),
(4421, 1, 'Update Sales Force with ID 328', '2016-11-14 04:30:59', 'Backend'),
(4422, 1, 'Update Sales Force with ID 334', '2016-11-14 04:31:23', 'Backend'),
(4423, 1, 'Update Sales Force with ID 331', '2016-11-14 04:31:34', 'Backend'),
(4424, 1, 'Update User with ID 337', '2016-11-14 04:32:54', 'Backend'),
(4425, 1, 'Update User with ID 335', '2016-11-14 04:34:02', 'Backend'),
(4426, 1, 'Update User with ID 335', '2016-11-14 04:34:30', 'Backend'),
(4427, 1, 'Update User with ID 333', '2016-11-14 04:34:58', 'Backend'),
(4428, 1, 'Update User with ID 333', '2016-11-14 04:35:08', 'Backend'),
(4429, 1, 'LOGOUT', '2016-11-14 04:35:24', 'Backend'),
(4430, 335, 'LOGIN', '2016-11-14 04:35:39', 'Backend'),
(4431, 335, 'LOGOUT', '2016-11-14 04:37:36', 'Backend'),
(4432, 338, 'LOGIN', '2016-11-14 04:38:07', 'Backend'),
(4433, 338, 'LOGOUT', '2016-11-14 04:41:02', 'Backend'),
(4434, 1, 'LOGIN', '2016-11-14 04:41:07', 'Backend'),
(4435, 335, 'LOGIN', '2016-11-14 04:57:03', 'Backend'),
(4436, 335, 'LOGIN', '2016-11-14 05:18:24', 'Backend'),
(4437, 338, 'Register visit plan to outlet Toko Matahari Keputih', '2016-11-14 05:28:29', 'mobile'),
(4438, 338, 'Register visit plan to outlet Toko Matahari Keputih', '2016-11-14 05:28:49', 'mobile'),
(4439, 338, 'Register visit plan to outlet Fix Auto Care', '2016-11-14 05:44:21', 'mobile'),
(4440, 338, 'Register visit plan to outlet Fix Auto Care', '2016-11-14 05:45:30', 'mobile'),
(4441, 335, 'LOGIN', '2016-11-14 05:46:11', 'Backend'),
(4442, 335, 'LOGOUT', '2016-11-14 05:59:27', 'Backend'),
(4443, 1, 'LOGIN', '2016-11-14 05:59:38', 'Backend'),
(4444, 1, 'LOGIN', '2016-11-14 06:16:06', 'Backend'),
(4445, 1, 'Make New Order', '2016-11-14 06:19:47', 'Backend'),
(4446, 1, 'Make New Order', '2016-11-14 06:25:31', 'Backend'),
(4447, 1, 'LOGIN', '2016-11-14 06:26:25', 'Backend'),
(4448, 1, 'Make New Order', '2016-11-14 06:27:09', 'Backend'),
(4449, 338, 'Submit visit plan to outlet Fix Auto Care', '2016-11-14 06:30:01', 'mobile'),
(4450, 338, 'Submit visit plan to outlet Fix Auto Care', '2016-11-14 06:30:01', 'mobile'),
(4451, 1, 'LOGIN', '2016-11-14 08:30:18', 'Backend'),
(4452, 1, 'Make Competitor Activity', '2016-11-14 08:37:42', 'Backend'),
(4453, 1, 'Make Competitor Activity', '2016-11-14 08:40:51', 'Backend'),
(4454, 1, 'LOGIN', '2016-11-16 07:04:54', 'Backend'),
(4455, 1, 'LOGIN', '2016-11-16 08:10:02', 'Backend'),
(4456, 1, 'LOGIN', '2016-11-16 09:04:00', 'Backend'),
(4457, 1, 'LOGIN', '2016-11-23 05:23:49', 'Backend'),
(4458, 1, 'Update Sales Force with ID 336', '2016-11-23 05:24:17', 'Backend'),
(4459, 1, 'Update User with ID 335', '2016-11-23 05:25:54', 'Backend'),
(4460, 336, 'LOGIN', '2016-11-23 05:53:01', 'Backend'),
(4461, 336, 'LOGOUT', '2016-11-23 05:53:08', 'Backend'),
(4462, 1, 'LOGIN', '2016-11-23 05:53:17', 'Backend'),
(4463, 1, 'Update Sales Force with ID 336', '2016-11-23 05:53:37', 'Backend'),
(4464, 1, 'Update Sales Force with ID 338', '2016-11-23 05:53:48', 'Backend'),
(4465, 1, 'LOGOUT', '2016-11-23 05:54:28', 'Backend'),
(4466, 338, 'LOGIN', '2016-11-23 05:54:41', 'Backend'),
(4467, 338, 'LOGOUT', '2016-11-23 05:54:48', 'Backend'),
(4468, 335, 'LOGIN', '2016-11-23 05:54:59', 'Backend'),
(4469, 335, 'LOGOUT', '2016-11-23 05:55:11', 'Backend'),
(4470, 335, 'LOGIN', '2016-11-24 07:53:45', 'Backend'),
(4471, 338, 'LOGIN', '2016-11-24 12:49:34', 'Backend'),
(4472, 338, 'Register visit plan to outlet Fix Auto Care', '2016-11-24 12:54:40', 'mobile'),
(4473, 338, 'Register visit plan to outlet Fix Auto Care', '2016-11-24 12:55:19', 'mobile'),
(4474, 338, 'LOGIN', '2016-11-24 12:59:52', 'Backend'),
(4475, 338, 'LOGIN', '2016-11-24 12:59:54', 'Backend'),
(4476, 338, 'LOGOUT', '2016-11-24 13:00:56', 'Backend'),
(4477, 335, 'LOGIN', '2016-11-24 13:01:59', 'Backend'),
(4478, 1, 'LOGIN', '2016-11-24 15:06:07', 'Backend'),
(4479, 338, 'LOGIN', '2017-01-06 09:09:33', 'Backend'),
(4480, 338, 'LOGOUT', '2017-01-06 09:14:06', 'Backend'),
(4481, 1, 'LOGIN', '2017-04-05 08:13:49', 'Backend'),
(4482, 1, 'Make New Sales Force', '2017-04-05 08:15:07', 'Backend'),
(4483, 1, 'LOGIN', '2017-04-05 08:15:41', 'Backend'),
(4484, 1, 'LOGOUT', '2017-04-05 08:16:03', 'Backend'),
(4485, 339, 'LOGIN', '2017-04-05 08:16:22', 'Backend'),
(4486, 339, 'LOGIN', '2017-04-05 08:16:28', 'Backend'),
(4487, 339, 'LOGOUT', '2017-04-05 08:16:42', 'Backend'),
(4488, 1, 'LOGIN', '2017-04-05 08:16:48', 'Backend'),
(4489, 339, 'LOGOUT', '2017-04-05 08:16:52', 'Backend'),
(4490, 1, 'LOGOUT', '2017-04-05 08:18:31', 'Backend'),
(4491, 1, 'LOGOUT', '2017-04-05 08:22:07', 'Backend'),
(4492, 1, 'LOGIN', '2017-04-05 08:23:36', 'Backend'),
(4493, 1, 'LOGOUT', '2017-04-05 08:23:41', 'Backend'),
(4494, 335, 'LOGIN', '2017-04-13 09:11:07', 'Backend'),
(4495, 335, 'Make New Sales Force', '2017-04-13 09:17:13', 'Backend'),
(4496, 340, 'Register outlet PODOMORO', '2017-04-13 09:20:10', 'mobile'),
(4497, 340, 'Register visit plan to outlet PODOMORO', '2017-04-13 09:21:50', 'mobile'),
(4498, 340, 'Register visit plan to outlet PODOMORO', '2017-04-13 09:22:33', 'mobile'),
(4499, 340, 'Register visit plan to outlet PODOMORO', '2017-04-13 09:22:56', 'mobile'),
(4500, 340, 'Register visit plan to outlet PODOMORO', '2017-04-13 09:23:18', 'mobile'),
(4501, 340, 'Register outlet podomoro', '2017-04-13 09:24:49', 'mobile'),
(4502, 340, 'Register visit plan to outlet podomoro', '2017-04-13 09:24:49', 'mobile'),
(4503, 340, 'Register visit plan to outlet podomoro', '2017-04-13 09:25:07', 'mobile'),
(4504, 340, 'Submit visit plan to outlet podomoro', '2017-04-13 09:25:27', 'mobile'),
(4505, 338, 'LOGIN', '2017-06-21 11:18:10', 'Backend'),
(4506, 335, 'LOGIN', '2017-06-21 11:25:24', 'Backend'),
(4507, 1, 'LOGIN', '2017-07-19 08:18:39', 'Backend'),
(4508, 1, 'LOGIN', '2017-07-19 08:23:20', 'Backend'),
(4509, 338, 'LOGIN', '2017-07-25 06:30:02', 'Backend'),
(4510, 338, 'LOGOUT', '2017-07-25 06:31:04', 'Backend'),
(4511, 335, 'LOGIN', '2017-07-25 06:31:21', 'Backend');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `outlet`
--

CREATE TABLE `outlet` (
  `kd_outlet` int(11) NOT NULL,
  `kode` varchar(20) DEFAULT NULL,
  `kd_dist` int(11) DEFAULT NULL,
  `kd_kota` int(11) DEFAULT NULL,
  `kd_tipe` int(11) DEFAULT NULL,
  `nm_pic` varchar(140) DEFAULT NULL,
  `tlp_pic` varchar(20) DEFAULT NULL,
  `kd_area` int(11) DEFAULT NULL,
  `kd_user` int(11) DEFAULT NULL,
  `nm_outlet` varchar(100) NOT NULL,
  `almt_outlet` varchar(255) DEFAULT NULL,
  `rank_outlet` char(1) DEFAULT NULL,
  `kodepos` varchar(50) DEFAULT NULL,
  `reg_status` varchar(5) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `toleransi_long` varchar(50) DEFAULT NULL,
  `toleransi_lat` varchar(50) DEFAULT '0',
  `foto_outlet` varchar(255) DEFAULT NULL,
  `status_area` smallint(6) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `outlet`
--

INSERT INTO `outlet` (`kd_outlet`, `kode`, `kd_dist`, `kd_kota`, `kd_tipe`, `nm_pic`, `tlp_pic`, `kd_area`, `kd_user`, `nm_outlet`, `almt_outlet`, `rank_outlet`, `kodepos`, `reg_status`, `latitude`, `longitude`, `toleransi_long`, `toleransi_lat`, `foto_outlet`, `status_area`) VALUES
(968, NULL, 1, 444, 1, 'Bu Marika', '087855063917', 1, 332, 'Toko Saudagar', 'Jalan Kembang 57', 'A', '85226', 'YES', '-7.2767701', '112.7585337', '500', '0', '968_20160922041224.jpeg', 1),
(969, NULL, 1, 444, 7, 'Pak Aji', '6593629', 1, 334, 'CV. Mandiri Jaya', 'Jemur sari no 234', 'A', '65362', 'YES', '-7.2767993', '112.7585107', '500', '0', '20160810033830_969_35.JPG', 1),
(970, NULL, 1, 444, 1, 'Pak Mahmud', '856345', 1, 334, 'Toko Sumber Rejeki', 'Tenggilis mejoyo 23', 'A', '80965', 'YES', '-7.276799', '112.7585119', '500', '0', '20160811133433_970_tuksar.jpg', 1),
(973, NULL, 1, 444, 1, 'Pak Darmaji', '085632565', 1, 334, 'Tak Edit Outlet Lang', 'Tenggilis jaya\r\n', 'A', '123232', 'YES', '-7.3185387', '112.76136', '500', '0', '20160811030144_973_walikota-b.jpg', 1),
(972, NULL, 1, 444, 12, 'Yonsen Amir', '653269299', 1, 338, 'Toko Matahari Keputih', 'Jl. Keputih Tegal Timur 12 A', 'A', '68966', 'YES', '-7.3200099', '112.7585458', '500', '0', '972_20160809052609.jpeg', 1),
(978, NULL, 1, 444, 1, 'Pak Joko Murdianto', '3562626', 1, 332, 'Toko Amanah', 'Jl. Rungkut alang-alang 49', 'A', '64346', 'YES', '-7.3197926', '112.7588795', '500', '0', '978_20161108072201.jpeg', 1),
(979, NULL, 1, 444, 12, 'Tech in Asia', '102909', 1, 338, 'Toko Tech in Asia', 'Jl. Jend. Gatot Subroto', 'A', '68966', 'YES', '-6.233959', '106.825563', '500', '0', '972_20160809052609.jpeg', 1),
(980, NULL, 1, 444, 12, 'Fixautocare', '102909', 1, 338, 'Fix Auto Care', 'Jl. Raya Kali Rungkut 75-B', 'A', '68966', 'YES', '-7.321935', '112.769659', '500', '0', '972_20160809052609.jpeg', 1),
(981, NULL, 1, 17, 7, 'MIKE', '082243777854', 2, 340, 'PODOMORO', 'Jalan Manggis 2 no 7 RT 2 RW 21, Ngtingo, Jaten', 'A', '57213', 'YES', '-7.5344061', '110.8348951', '500', '0', '981_20170413092011.jpeg', 1),
(982, NULL, 1, 17, 1, 'mikr', '082243777854', 2, 340, 'podomoro', 'jalan manggis', 'A', '88', 'YES', '-7.5344061', '110.8348951', '500', '0', '982_20170413092451.jpeg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `id` int(11) NOT NULL,
  `nm_permission` varchar(100) NOT NULL,
  `enable` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission`
--

INSERT INTO `permission` (`id`, `nm_permission`, `enable`) VALUES
(1, 'admin.dashboard', 1),
(2, 'admin.logout', 1),
(3, 'admin.sales.index', 1),
(4, 'admin.sales.create', 1),
(5, 'admin.sales.store', 1),
(6, 'admin.sales.edit', 1),
(7, 'admin.sales.update', 1),
(8, 'admin.sales.show', 1),
(9, 'admin.sales.delete', 1),
(10, 'admin.sales.destroy', 1),
(11, 'admin.outlet.create', 1),
(12, 'admin.outlet.index', 1),
(13, 'admin.outlet.store', 1),
(14, 'admin.outlet.edit', 1),
(15, 'admin.outlet.update', 1),
(16, 'admin.outlet.show', 1),
(17, 'admin.outlet.delete', 1),
(18, 'admin.outlet.destroy', 1),
(19, 'admin.area.index', 1),
(20, 'admin.area.create', 1),
(21, 'admin.area.store', 1),
(22, 'admin.area.update', 1),
(23, 'admin.area.edit', 1),
(24, 'admin.area.delete', 1),
(25, 'admin.area.destroy', 1),
(26, 'admin.distributor.index', 1),
(27, 'admin.distributor.create', 1),
(28, 'admin.distributor.store', 1),
(29, 'admin.distributor.update', 1),
(30, 'admin.distributor.edit', 1),
(31, 'admin.distributor.delete', 1),
(32, 'admin.distributor.destroy', 1),
(33, 'admin.produk.index', 1),
(34, 'admin.produk.create', 1),
(35, 'admin.produk.store', 1),
(36, 'admin.produk.update', 1),
(37, 'admin.produk.edit', 1),
(38, 'admin.produk.delete', 1),
(39, 'admin.produk.destroy', 1),
(40, 'admin.user.index', 1),
(41, 'admin.user.create', 1),
(42, 'admin.user.store', 1),
(43, 'admin.user.update', 1),
(44, 'admin.user.edit', 1),
(45, 'admin.user.delete', 1),
(46, 'admin.user.destroy', 1),
(47, 'admin.user.show', 1),
(48, 'admin.visit.index', 1),
(49, 'admin.visit.create', 1),
(50, 'admin.visit.store', 1),
(51, 'admin.visit.update', 1),
(52, 'admin.visit.edit', 1),
(53, 'admin.visit.delete', 1),
(54, 'admin.visit.destroy', 1),
(55, 'admin.photo.index', 1),
(56, 'admin.photo.create', 1),
(57, 'admin.photo.store', 1),
(58, 'admin.photo.update', 1),
(59, 'admin.photo.edit', 1),
(60, 'admin.photo.delete', 1),
(61, 'admin.photo.destroy', 1),
(62, 'admin.tipephoto.index', 1),
(63, 'admin.tipephoto.create', 1),
(64, 'admin.tipephoto.store', 1),
(65, 'admin.tipephoto.update', 1),
(66, 'admin.tipephoto.edit', 1),
(67, 'admin.tipephoto.delete', 1),
(68, 'admin.tipephoto.destroy', 1),
(69, 'admin.competitor.index', 1),
(70, 'admin.competitor.create', 1),
(71, 'admin.competitor.store', 1),
(72, 'admin.competitor.update', 1),
(73, 'admin.competitor.edit', 1),
(74, 'admin.competitor.delete', 1),
(75, 'admin.competitor.destroy', 1),
(76, 'admin.order.index', 1),
(77, 'admin.order.create', 1),
(78, 'admin.order.store', 1),
(79, 'admin.order.update', 1),
(80, 'admin.order.edit', 1),
(81, 'admin.order.delete', 1),
(82, 'admin.order.destroy', 1),
(83, 'admin.kota.index', 1),
(84, 'admin.kota.create', 1),
(85, 'admin.kota.store', 1),
(86, 'admin.kota.update', 1),
(87, 'admin.kota.edit', 1),
(88, 'admin.kota.delete', 1),
(89, 'admin.kota.destroy', 1),
(90, 'admin.comp.index', 1),
(91, 'admin.comp.create', 1),
(92, 'admin.comp.store', 1),
(93, 'admin.comp.update', 1),
(94, 'admin.comp.edit', 1),
(95, 'admin.comp.delete', 1),
(96, 'admin.comp.destroy', 1),
(97, 'admin.role.index', 1),
(98, 'admin.role.create', 1),
(99, 'admin.role.store', 1),
(100, 'admin.role.update', 1),
(101, 'admin.role.edit', 1),
(102, 'admin.role.delete', 1),
(103, 'admin.role.destroy', 1),
(104, 'admin.visitMonitor.index', 1),
(105, 'admin.visitMonitor.create', 1),
(106, 'admin.visitMonitor.store', 1),
(107, 'admin.visitMonitor.update', 1),
(108, 'admin.visitMonitor.edit', 1),
(109, 'admin.visitMonitor.delete', 1),
(110, 'admin.visitMonitor.destroy', 1),
(111, 'admin.permission.index', 1),
(112, 'admin.permission.create', 1),
(113, 'admin.permission.store', 1),
(114, 'admin.permission.update', 1),
(115, 'admin.permission.edit', 1),
(116, 'admin.permission.delete', 1),
(117, 'admin.permission.destroy', 1),
(118, 'admin.download', 1),
(119, 'admin.download.area', 1),
(120, 'admin.download.produk', 1),
(121, 'admin.download.distributor', 1),
(122, 'admin.download.salesForce', 1),
(123, 'admin.download.nonSF', 1),
(124, 'admin.download.takeOrder', 1),
(125, 'admin.download.visitPlan', 1),
(126, 'admin.download.outlet', 1),
(127, 'admin.download.salesDetail', 1),
(128, 'admin.upload.outlet', 1),
(129, 'admin.upload.area', 1),
(130, 'admin.upload.distributor', 1),
(131, 'admin.upload.produk', 1),
(132, 'admin.upload.user', 1),
(133, 'admin.profile', 1),
(134, 'admin.edit.profile', 1),
(135, 'admin.edited.profile', 1),
(136, 'admin.report', 1),
(137, 'admin.report.outletVisit', 1),
(138, 'admin.report.resultReport', 1),
(139, 'admin.report.workingTime', 1),
(140, 'admin.report.resultWorkTime', 1),
(141, 'admin.report.effectiveCall', 1),
(142, 'admin.report.resultEffectiveCall', 1),
(143, 'admin.getChangePassword', 1),
(144, 'admin.postChangePassword', 1),
(145, 'admin.outletManage.index', 1),
(146, 'admin.outletManage.updateToleransi', 1),
(147, 'admin.visitPlan.form', 1),
(148, 'admin.visitPlan.result', 1),
(149, 'admin.visitingMonitor.form', 1),
(150, 'admin.visitingMonitor.result', 1),
(151, 'admin.adminmenu.index', 1),
(152, 'admin.adminmenu.create', 1),
(153, 'admin.adminmenu.store', 1),
(154, 'admin.adminmenu.edit', 1),
(155, 'admin.adminmenu.update', 1),
(156, 'admin.adminmenu.delete', 1),
(157, 'admin.adminmenu.destroy', 1),
(158, 'admin.report.photoActivity', 1),
(159, 'admin.report.resultPhotoActivity', 1),
(160, 'admin.visit.editApprove', 1),
(161, 'admin.visit.approved', 1),
(162, 'admin.download.visitMonitor', 1),
(163, 'admin.tipe.index', 1),
(164, 'admin.tipe.create', 1),
(165, 'admin.tipe.store', 1),
(166, 'admin.tipe.edit', 1),
(167, 'admin.tipe.update', 1),
(168, 'admin.tipe.delete', 1),
(169, 'admin.tipe.destroy', 1),
(170, 'admin.takeOrder.form', 1),
(171, 'admin.takeOrder.result', 1),
(172, 'admin.photoAct.form', 1),
(173, 'admin.photoAct.result', 1),
(174, 'admin.report.competitorActivity', 1),
(175, 'admin.report.resultCompetitorActivity', 1);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(11) NOT NULL,
  `kd_role` int(11) NOT NULL,
  `kd_permission` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `kd_role`, `kd_permission`) VALUES
(10456, 3, 150),
(10455, 3, 149),
(10328, 7, 171),
(10327, 7, 170),
(10326, 7, 169),
(10325, 7, 168),
(10324, 7, 167),
(10323, 7, 166),
(10322, 7, 165),
(10020, 6, 175),
(10019, 6, 174),
(10321, 7, 164),
(10454, 3, 148),
(10453, 3, 147),
(10452, 3, 146),
(10451, 3, 145),
(10450, 3, 144),
(10449, 3, 143),
(10448, 3, 137),
(10447, 3, 134),
(10446, 3, 133),
(8874, 1, 175),
(8873, 1, 174),
(8872, 1, 173),
(8871, 1, 172),
(8870, 1, 171),
(8869, 1, 170),
(8868, 1, 169),
(8867, 1, 167),
(8866, 1, 166),
(8865, 1, 165),
(8864, 1, 164),
(8863, 1, 163),
(8862, 1, 162),
(8861, 1, 161),
(8860, 1, 160),
(8859, 1, 159),
(8858, 1, 158),
(8857, 1, 157),
(8856, 1, 155),
(8855, 1, 154),
(8854, 1, 153),
(8853, 1, 152),
(8852, 1, 151),
(8851, 1, 150),
(8850, 1, 149),
(8849, 1, 148),
(8848, 1, 147),
(8847, 1, 145),
(8846, 1, 144),
(8845, 1, 143),
(8844, 1, 142),
(8843, 1, 141),
(8842, 1, 140),
(8841, 1, 139),
(8840, 1, 138),
(8839, 1, 137),
(8838, 1, 136),
(8837, 1, 135),
(8836, 1, 134),
(8835, 1, 133),
(8834, 1, 127),
(8833, 1, 126),
(8832, 1, 125),
(8831, 1, 124),
(8830, 1, 123),
(8829, 1, 122),
(8828, 1, 120),
(8827, 1, 119),
(8826, 1, 118),
(8825, 1, 117),
(8824, 1, 115),
(8823, 1, 114),
(8822, 1, 113),
(8821, 1, 112),
(8820, 1, 111),
(8819, 1, 110),
(8818, 1, 108),
(8817, 1, 107),
(8816, 1, 106),
(8815, 1, 105),
(8814, 1, 104),
(8813, 1, 103),
(8812, 1, 101),
(8811, 1, 100),
(8810, 1, 99),
(8809, 1, 98),
(8808, 1, 97),
(8807, 1, 96),
(8806, 1, 94),
(8805, 1, 93),
(8804, 1, 92),
(8803, 1, 91),
(8802, 1, 90),
(8801, 1, 89),
(8800, 1, 87),
(8799, 1, 86),
(8798, 1, 85),
(8797, 1, 84),
(8796, 1, 83),
(8795, 1, 82),
(8794, 1, 80),
(8793, 1, 79),
(8792, 1, 78),
(8791, 1, 77),
(8790, 1, 76),
(8789, 1, 75),
(8788, 1, 73),
(8787, 1, 72),
(8786, 1, 71),
(8785, 1, 70),
(8784, 1, 69),
(8783, 1, 68),
(8782, 1, 66),
(8781, 1, 65),
(8780, 1, 64),
(8779, 1, 63),
(8778, 1, 62),
(8777, 1, 61),
(8776, 1, 59),
(8775, 1, 58),
(8774, 1, 57),
(8773, 1, 56),
(8772, 1, 55),
(8771, 1, 54),
(8770, 1, 52),
(8769, 1, 51),
(10445, 3, 127),
(10444, 3, 126),
(10443, 3, 125),
(10442, 3, 124),
(10441, 3, 123),
(10440, 3, 122),
(10407, 2, 175),
(10406, 2, 174),
(10332, 7, 175),
(10331, 7, 174),
(10330, 7, 173),
(10329, 7, 172),
(8768, 1, 50),
(8767, 1, 48),
(8766, 1, 47),
(8765, 1, 46),
(8764, 1, 45),
(8763, 1, 44),
(8762, 1, 43),
(10439, 3, 121),
(10438, 3, 120),
(10437, 3, 119),
(10436, 3, 118),
(10435, 3, 104),
(8761, 1, 42),
(10434, 3, 78),
(10433, 3, 77),
(10432, 3, 76),
(10431, 3, 71),
(10430, 3, 70),
(10429, 3, 69),
(10428, 3, 64),
(10427, 3, 63),
(10426, 3, 62),
(10425, 3, 59),
(10424, 3, 58),
(10423, 3, 57),
(10422, 3, 56),
(10421, 3, 55),
(10420, 3, 50),
(10419, 3, 49),
(10418, 3, 48),
(10417, 3, 18),
(8760, 1, 41),
(8759, 1, 40),
(8758, 1, 33),
(8757, 1, 26),
(10405, 2, 173),
(10404, 2, 172),
(10403, 2, 171),
(10402, 2, 170),
(10401, 2, 162),
(10400, 2, 161),
(10399, 2, 160),
(10398, 2, 159),
(10397, 2, 158),
(10396, 2, 150),
(10395, 2, 149),
(10394, 2, 148),
(10393, 2, 147),
(10392, 2, 144),
(10391, 2, 143),
(10390, 2, 142),
(10389, 2, 141),
(10388, 2, 140),
(10387, 2, 139),
(10386, 2, 138),
(10385, 2, 137),
(10384, 2, 136),
(10383, 2, 135),
(10382, 2, 134),
(10381, 2, 133),
(10380, 2, 125),
(10379, 2, 124),
(10378, 2, 118),
(10377, 2, 104),
(10376, 2, 92),
(10375, 2, 91),
(10374, 2, 90),
(10373, 2, 87),
(10372, 2, 86),
(10371, 2, 85),
(10370, 2, 83),
(10369, 2, 79),
(10368, 2, 78),
(10367, 2, 76),
(10366, 2, 69),
(10365, 2, 66),
(10364, 2, 65),
(10363, 2, 64),
(10362, 2, 63),
(10361, 2, 62),
(10360, 2, 61),
(10359, 2, 60),
(10358, 2, 59),
(10357, 2, 58),
(10356, 2, 57),
(10355, 2, 56),
(10354, 2, 55),
(10353, 2, 51),
(10352, 2, 50),
(10351, 2, 48),
(10350, 2, 33),
(10349, 2, 26),
(10348, 2, 21),
(10347, 2, 19),
(10346, 2, 16),
(10345, 2, 15),
(8756, 1, 25),
(8755, 1, 23),
(8754, 1, 22),
(8753, 1, 21),
(8752, 1, 20),
(8751, 1, 19),
(9281, 4, 175),
(9280, 4, 174),
(9279, 4, 173),
(9278, 4, 172),
(9277, 4, 171),
(9276, 4, 170),
(9275, 4, 167),
(9274, 4, 166),
(9273, 4, 165),
(9272, 4, 164),
(9271, 4, 163),
(9270, 4, 162),
(9269, 4, 161),
(9268, 4, 160),
(9267, 4, 159),
(9266, 4, 158),
(9265, 4, 155),
(9264, 4, 154),
(9263, 4, 153),
(9262, 4, 152),
(9261, 4, 151),
(9260, 4, 150),
(9259, 4, 149),
(9258, 4, 148),
(9257, 4, 147),
(9256, 4, 146),
(9255, 4, 145),
(9254, 4, 144),
(9253, 4, 143),
(9252, 4, 142),
(9251, 4, 141),
(9250, 4, 140),
(9249, 4, 139),
(9248, 4, 138),
(9247, 4, 137),
(9246, 4, 136),
(9245, 4, 135),
(9244, 4, 134),
(9243, 4, 133),
(9242, 4, 132),
(9241, 4, 131),
(9240, 4, 130),
(9239, 4, 129),
(9238, 4, 128),
(9237, 4, 127),
(9236, 4, 126),
(9235, 4, 125),
(9234, 4, 124),
(9233, 4, 123),
(9232, 4, 122),
(9231, 4, 121),
(9230, 4, 120),
(9229, 4, 119),
(9228, 4, 118),
(9227, 4, 115),
(9226, 4, 114),
(9225, 4, 113),
(9224, 4, 112),
(9223, 4, 111),
(9222, 4, 108),
(9221, 4, 107),
(9220, 4, 106),
(9219, 4, 105),
(9218, 4, 104),
(9217, 4, 101),
(9216, 4, 100),
(9215, 4, 99),
(9214, 4, 98),
(9213, 4, 97),
(9212, 4, 94),
(9211, 4, 93),
(9210, 4, 92),
(9209, 4, 91),
(9208, 4, 90),
(9207, 4, 87),
(9206, 4, 86),
(9205, 4, 85),
(9204, 4, 84),
(9203, 4, 83),
(9202, 4, 80),
(9201, 4, 79),
(9200, 4, 78),
(9199, 4, 77),
(9198, 4, 76),
(9197, 4, 73),
(9196, 4, 72),
(9195, 4, 71),
(9194, 4, 70),
(9193, 4, 69),
(9192, 4, 66),
(9191, 4, 65),
(9190, 4, 64),
(9189, 4, 63),
(9188, 4, 62),
(9187, 4, 59),
(9186, 4, 58),
(9185, 4, 57),
(9184, 4, 56),
(9183, 4, 55),
(9182, 4, 52),
(9181, 4, 51),
(9180, 4, 50),
(9179, 4, 49),
(9178, 4, 48),
(9177, 4, 47),
(9176, 4, 44),
(9175, 4, 43),
(9174, 4, 42),
(9173, 4, 41),
(9172, 4, 40),
(9171, 4, 37),
(9170, 4, 36),
(9169, 4, 35),
(9168, 4, 34),
(9167, 4, 33),
(9166, 4, 32),
(9165, 4, 30),
(9164, 4, 29),
(9163, 4, 28),
(9162, 4, 27),
(9161, 4, 26),
(9160, 4, 24),
(9159, 4, 23),
(9158, 4, 22),
(9157, 4, 21),
(9156, 4, 20),
(9155, 4, 19),
(9154, 4, 16),
(9153, 4, 15),
(9152, 4, 14),
(9151, 4, 13),
(9150, 4, 12),
(9149, 4, 11),
(9148, 4, 8),
(9147, 4, 7),
(9146, 4, 6),
(9145, 4, 5),
(9144, 4, 4),
(9143, 4, 3),
(9142, 4, 2),
(9141, 4, 1),
(8750, 1, 18),
(8749, 1, 16),
(8748, 1, 15),
(8747, 1, 14),
(8746, 1, 13),
(8745, 1, 12),
(8744, 1, 11),
(8743, 1, 10),
(8742, 1, 8),
(8741, 1, 7),
(8740, 1, 6),
(8739, 1, 5),
(8738, 1, 4),
(8737, 1, 3),
(8736, 1, 2),
(8735, 1, 1),
(10344, 2, 14),
(10343, 2, 13),
(10342, 2, 12),
(10341, 2, 11),
(10340, 2, 8),
(10339, 2, 7),
(10338, 2, 6),
(10337, 2, 5),
(10336, 2, 4),
(10335, 2, 3),
(10334, 2, 2),
(10333, 2, 1),
(10018, 6, 173),
(10017, 6, 172),
(10016, 6, 171),
(10015, 6, 170),
(10014, 6, 169),
(10013, 6, 168),
(10012, 6, 167),
(10011, 6, 166),
(10010, 6, 165),
(10009, 6, 164),
(10008, 6, 163),
(10007, 6, 162),
(10006, 6, 161),
(10005, 6, 160),
(10004, 6, 159),
(10003, 6, 158),
(10002, 6, 157),
(10001, 6, 156),
(10000, 6, 155),
(9999, 6, 154),
(9998, 6, 153),
(9997, 6, 152),
(9996, 6, 151),
(9995, 6, 150),
(9994, 6, 149),
(9993, 6, 148),
(9992, 6, 147),
(9991, 6, 146),
(9990, 6, 145),
(9989, 6, 144),
(9988, 6, 143),
(9987, 6, 142),
(9986, 6, 141),
(9985, 6, 140),
(9984, 6, 139),
(9983, 6, 138),
(9982, 6, 137),
(9981, 6, 136),
(9980, 6, 135),
(9979, 6, 134),
(9978, 6, 133),
(9977, 6, 132),
(9976, 6, 131),
(9975, 6, 130),
(9974, 6, 129),
(9973, 6, 128),
(9972, 6, 127),
(9971, 6, 126),
(9970, 6, 125),
(9969, 6, 124),
(9968, 6, 123),
(9967, 6, 122),
(9966, 6, 121),
(9965, 6, 120),
(9964, 6, 119),
(9963, 6, 118),
(9962, 6, 117),
(9961, 6, 116),
(9960, 6, 115),
(9959, 6, 114),
(9958, 6, 113),
(9957, 6, 112),
(9956, 6, 111),
(9955, 6, 110),
(9954, 6, 109),
(9953, 6, 108),
(9952, 6, 107),
(9951, 6, 106),
(9950, 6, 105),
(9949, 6, 104),
(9948, 6, 103),
(9947, 6, 102),
(9946, 6, 101),
(9945, 6, 100),
(9944, 6, 99),
(9943, 6, 98),
(9942, 6, 97),
(9941, 6, 96),
(9940, 6, 95),
(9939, 6, 94),
(9938, 6, 93),
(9937, 6, 92),
(9936, 6, 91),
(9935, 6, 90),
(9934, 6, 89),
(9933, 6, 88),
(9932, 6, 87),
(9931, 6, 86),
(9930, 6, 85),
(9929, 6, 84),
(9928, 6, 83),
(9927, 6, 82),
(9926, 6, 81),
(9925, 6, 80),
(9924, 6, 79),
(9923, 6, 78),
(9922, 6, 77),
(9921, 6, 76),
(9920, 6, 75),
(9919, 6, 74),
(9918, 6, 73),
(9917, 6, 72),
(9916, 6, 71),
(9915, 6, 70),
(9914, 6, 69),
(9913, 6, 68),
(9912, 6, 67),
(9911, 6, 66),
(9910, 6, 65),
(9909, 6, 64),
(9908, 6, 63),
(9907, 6, 62),
(9906, 6, 61),
(9905, 6, 60),
(9904, 6, 59),
(9903, 6, 58),
(9902, 6, 57),
(9901, 6, 56),
(9900, 6, 55),
(9899, 6, 54),
(9898, 6, 53),
(9897, 6, 52),
(9896, 6, 51),
(9895, 6, 50),
(9894, 6, 49),
(9893, 6, 48),
(9892, 6, 47),
(9891, 6, 46),
(9890, 6, 45),
(9889, 6, 44),
(9888, 6, 43),
(9887, 6, 42),
(9886, 6, 41),
(9885, 6, 40),
(9884, 6, 39),
(9883, 6, 38),
(9882, 6, 37),
(9881, 6, 36),
(9880, 6, 35),
(9879, 6, 34),
(9878, 6, 33),
(9877, 6, 32),
(9876, 6, 31),
(9875, 6, 30),
(9874, 6, 29),
(9873, 6, 28),
(9872, 6, 27),
(9871, 6, 26),
(9870, 6, 25),
(9869, 6, 24),
(9868, 6, 23),
(9867, 6, 22),
(9866, 6, 21),
(9865, 6, 20),
(9864, 6, 19),
(9863, 6, 18),
(9862, 6, 17),
(9861, 6, 16),
(9860, 6, 15),
(9859, 6, 14),
(9858, 6, 13),
(9857, 6, 12),
(9856, 6, 11),
(9855, 6, 10),
(9854, 6, 9),
(9853, 6, 8),
(9852, 6, 7),
(9851, 6, 6),
(9850, 6, 5),
(9849, 6, 4),
(9848, 6, 3),
(9847, 6, 2),
(9846, 6, 1),
(10416, 3, 17),
(10415, 3, 16),
(10414, 3, 15),
(10413, 3, 14),
(10412, 3, 13),
(10411, 3, 12),
(10410, 3, 11),
(10409, 3, 2),
(10408, 3, 1),
(10320, 7, 163),
(10319, 7, 162),
(10318, 7, 161),
(10317, 7, 160),
(10316, 7, 159),
(10315, 7, 158),
(10314, 7, 157),
(10313, 7, 156),
(10312, 7, 155),
(10311, 7, 154),
(10310, 7, 153),
(10309, 7, 152),
(10308, 7, 151),
(10307, 7, 150),
(10306, 7, 149),
(10305, 7, 148),
(10304, 7, 147),
(10303, 7, 146),
(10302, 7, 145),
(10301, 7, 144),
(10300, 7, 143),
(10299, 7, 142),
(10298, 7, 141),
(10297, 7, 140),
(10296, 7, 139),
(10295, 7, 138),
(10294, 7, 137),
(10293, 7, 136),
(10292, 7, 135),
(10291, 7, 134),
(10290, 7, 133),
(10289, 7, 132),
(10288, 7, 131),
(10287, 7, 130),
(10286, 7, 129),
(10285, 7, 128),
(10284, 7, 127),
(10283, 7, 126),
(10282, 7, 125),
(10281, 7, 124),
(10280, 7, 123),
(10279, 7, 122),
(10278, 7, 121),
(10277, 7, 120),
(10276, 7, 119),
(10275, 7, 118),
(10274, 7, 117),
(10273, 7, 116),
(10272, 7, 115),
(10271, 7, 114),
(10270, 7, 113),
(10269, 7, 112),
(10268, 7, 111),
(10267, 7, 110),
(10266, 7, 109),
(10265, 7, 108),
(10264, 7, 107),
(10263, 7, 106),
(10262, 7, 105),
(10261, 7, 104),
(10260, 7, 103),
(10259, 7, 102),
(10258, 7, 101),
(10257, 7, 100),
(10256, 7, 99),
(10255, 7, 98),
(10254, 7, 97),
(10253, 7, 96),
(10252, 7, 95),
(10251, 7, 94),
(10250, 7, 93),
(10249, 7, 92),
(10248, 7, 91),
(10247, 7, 90),
(10246, 7, 89),
(10245, 7, 88),
(10244, 7, 87),
(10243, 7, 86),
(10242, 7, 85),
(10241, 7, 84),
(10240, 7, 83),
(10239, 7, 82),
(10238, 7, 81),
(10237, 7, 80),
(10236, 7, 79),
(10235, 7, 78),
(10234, 7, 77),
(10233, 7, 76),
(10232, 7, 75),
(10231, 7, 74),
(10230, 7, 73),
(10229, 7, 72),
(10228, 7, 71),
(10227, 7, 70),
(10226, 7, 69),
(10225, 7, 68),
(10224, 7, 67),
(10223, 7, 66),
(10222, 7, 65),
(10221, 7, 64),
(10220, 7, 63),
(10219, 7, 62),
(10218, 7, 61),
(10217, 7, 60),
(10216, 7, 59),
(10215, 7, 58),
(10214, 7, 57),
(10213, 7, 56),
(10212, 7, 55),
(10211, 7, 54),
(10210, 7, 53),
(10209, 7, 52),
(10208, 7, 51),
(10207, 7, 50),
(10206, 7, 49),
(10205, 7, 48),
(10204, 7, 47),
(10203, 7, 46),
(10202, 7, 45),
(10201, 7, 44),
(10200, 7, 43),
(10199, 7, 42),
(10198, 7, 41),
(10197, 7, 40),
(10196, 7, 39),
(10195, 7, 38),
(10194, 7, 37),
(10193, 7, 36),
(10192, 7, 35),
(10191, 7, 34),
(10190, 7, 33),
(10189, 7, 32),
(10188, 7, 31),
(10187, 7, 30),
(10186, 7, 29),
(10185, 7, 28),
(10184, 7, 27),
(10183, 7, 26),
(10182, 7, 25),
(10181, 7, 24),
(10180, 7, 23),
(10179, 7, 22),
(10178, 7, 21),
(10177, 7, 20),
(10176, 7, 19),
(10175, 7, 18),
(10174, 7, 17),
(10173, 7, 16),
(10172, 7, 15),
(10171, 7, 14),
(10170, 7, 13),
(10169, 7, 12),
(10168, 7, 11),
(10167, 7, 10),
(10166, 7, 9),
(10165, 7, 8),
(10164, 7, 7),
(10163, 7, 6),
(10162, 7, 5),
(10161, 7, 4),
(10160, 7, 3),
(10159, 7, 2),
(10158, 7, 1),
(10457, 3, 162);

-- --------------------------------------------------------

--
-- Table structure for table `photo_activity`
--

CREATE TABLE `photo_activity` (
  `id` int(11) NOT NULL,
  `kd_outlet` int(11) NOT NULL,
  `kd_competitor` int(11) NOT NULL,
  `kd_user` int(11) NOT NULL DEFAULT '0',
  `nm_photo` varchar(100) NOT NULL,
  `jenis_photo` int(11) NOT NULL,
  `date_take_photo` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `almt_comp_activity` varchar(255) DEFAULT NULL,
  `date_upload_photo` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `keterangan` varchar(255) DEFAULT NULL,
  `path_photo` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `photo_activity`
--

INSERT INTO `photo_activity` (`id`, `kd_outlet`, `kd_competitor`, `kd_user`, `nm_photo`, `jenis_photo`, `date_take_photo`, `almt_comp_activity`, `date_upload_photo`, `keterangan`, `path_photo`) VALUES
(201, 968, 0, 332, 'nonr', 2, '2016-07-26 13:34:16', 'adreess', '2016-07-26 13:34:45', 'none', '201_outlet_20160726133447.jpeg'),
(202, 973, 0, 334, 'new POP', 3, '2016-08-10 04:17:52', 'Tenggilis enjoying\n', '2016-08-10 04:19:21', 'pop toko Tamiya ', '202_outlet_20160810041924.jpeg'),
(203, 0, 1, 332, 'Aktifitas competitor jaya', 0, '2016-11-13 17:00:00', 'Mall Grand city', '2016-11-14 08:37:42', '', 'Ug1w2c_images.jpeg'),
(204, 0, 1, 334, 'Aktifitas competitor jaya ', 0, '2016-11-04 17:00:00', 'Jl. Biliton', '2016-11-14 08:40:51', '', 'CgVht7_umbul-umbul-honda-suprax.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `kd_produk` varchar(10) NOT NULL,
  `nm_produk` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id`, `kd_produk`, `nm_produk`) VALUES
(26, '003', 'Item 3'),
(24, '001', 'Item 1'),
(25, '002', 'Item 2');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `kd_role` smallint(6) NOT NULL,
  `type_role` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`kd_role`, `type_role`) VALUES
(1, 'Admin Sales'),
(2, 'Area Manager'),
(3, 'Sales Force'),
(4, 'Kantor Pusat'),
(6, 'Super Admin'),
(7, 'Web Admin');

-- --------------------------------------------------------

--
-- Table structure for table `take_order`
--

CREATE TABLE `take_order` (
  `id` int(11) NOT NULL,
  `kd_to` varchar(255) NOT NULL,
  `kd_visitplan` int(11) DEFAULT NULL,
  `kd_produk` int(11) DEFAULT NULL,
  `qty_order` int(11) DEFAULT NULL,
  `satuan` varchar(50) NOT NULL,
  `date_order` timestamp NULL DEFAULT NULL,
  `status_order` smallint(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `take_order`
--

INSERT INTO `take_order` (`id`, `kd_to`, `kd_visitplan`, `kd_produk`, `qty_order`, `satuan`, `date_order`, `status_order`) VALUES
(158, '', 725, 26, 12, 'pack', '2016-11-04 11:18:00', 1),
(159, '', 711, 26, 2, 'Lusin', '2016-11-11 17:00:00', 1),
(160, '', 728, 24, 14, 'Pack', '2016-11-04 17:00:00', 1),
(161, '', 729, 25, 14, 'Pack', '2016-11-04 17:00:00', 1),
(162, 'to_86', 731, 24, 12, 'sachet', '2016-11-14 06:30:01', 1),
(163, 'to_86', 731, 26, 55, 'sachet', '2016-11-14 06:30:01', 1),
(164, 'to_87', 739, 25, 2, 'sachet', '2017-04-13 09:25:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tipe`
--

CREATE TABLE `tipe` (
  `id` int(11) NOT NULL,
  `nm_tipe` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipe`
--

INSERT INTO `tipe` (`id`, `nm_tipe`) VALUES
(1, 'Agent'),
(2, 'Apotek'),
(3, 'Toko Obat'),
(4, 'Distributor'),
(6, 'Wholesaler'),
(7, 'Retail'),
(8, 'Hospital'),
(9, 'Koperasi'),
(10, 'School'),
(11, 'Sport Center'),
(12, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `to_counter`
--

CREATE TABLE `to_counter` (
  `id` int(11) NOT NULL,
  `counter` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `to_counter`
--

INSERT INTO `to_counter` (`id`, `counter`) VALUES
(1, 88);

-- --------------------------------------------------------

--
-- Table structure for table `tp_photo`
--

CREATE TABLE `tp_photo` (
  `id` int(11) NOT NULL,
  `nama_tipe` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tp_photo`
--

INSERT INTO `tp_photo` (`id`, `nama_tipe`) VALUES
(2, 'POP'),
(3, 'Block Selving');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `kd_role` smallint(10) DEFAULT NULL,
  `kd_kota` int(11) NOT NULL,
  `kd_area` int(11) NOT NULL,
  `nik` varchar(20) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email_user` varchar(100) NOT NULL,
  `join_date` date DEFAULT NULL,
  `remember_token` varchar(255) NOT NULL,
  `id_gcm` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `kd_role`, `kd_kota`, `kd_area`, `nik`, `nama`, `alamat`, `telepon`, `foto`, `username`, `password`, `email_user`, `join_date`, `remember_token`, `id_gcm`) VALUES
(1, 6, 444, 100, 'GR000007788', 'Keira', 'JL. HR. Muhammad 432', '0318911454', 'PYxWxD_salonpas-logo.png', 'keira', '$2y$10$nOgB7ZaHhWEb/f4/lCYA8.//RT0QxW5PSu7MZs3Km5nG0VNAwWoZ.', 'SPA@gmail.com', '2015-01-01', '6tLGS443a7z5IdKv71Rjd0YPewHf76wFXJ0CDU75gfj4LrrdjsGkyKn6a03U', 'testing 123'),
(337, 6, 444, 100, 'GR0000099', 'Admin-demo', 'Administrator', '11223344', '', 'admin', '$2y$10$CI9q1.7E3jKpz4El9WpL.eVi40LUZ8XApvLT4r/4gkKLHPH6V2uNO', 'adminadmin@growth.co.id', '2015-08-05', '6xHyfUUNuJ4fzNxUctY3MRvbsXTyNKxQM3oE5xT3sIIGfTTV0JGpSqCorhIk', ''),
(336, 3, 444, 1, 'GR000003', 'Sales Demo', 'Kemangisan 123', '08128182820', '', 'salesdemo', '$2y$10$T5moVEdJDJk85DK2YFB6Wuq6tJ7zUvg3BwPz1UuolV/zoZNkDnciC', 'salesdemo@growth.co.id', '2015-09-01', 'mQNip2jzrbgpiOi4kKmeP5CqywuCBbqwCGeLkwjoa6QAipi4fnZdXxayA84m', 'eRgfqzaU1-A:APA91bH_Fb8JJghcgmvzS8kc4a5Igc21-rppgHJ0Uz1bmuQgny2nd_QIGSwvjdFxkgVGphnS2CJdAWplEw9WXRQp2Zc9h03lQcORF_E67erbzcuLhlIdwWvRZXWkUbwMba9tk8fcV0CG'),
(335, 2, 444, 1, 'GR000002', 'Area manager', 'Metropolis mkb 307, Surabaya', '087676556576', '', 'areamanager', '$2y$10$JPTMXuoxR/PvbB7VlI8oc.pYuTxI0buFGhsdXincT/FFBPBw0l.bm', 'AMEJ@growth.co.id', '2016-08-01', 'p5DV5Gti6u8cnyiA8lNMnYE2lvQWudKDpwMRdH7irI6Lf1kwFT3azQIAgPpv', ''),
(328, 3, 444, 1, 'NIK5112100100', 'Ahmad Mustofa', 'Keputih gg 2b no 19', '087855063917', '', 'mustofa27', '$2y$10$tn4aY6l8kNEsLhHktiiCvOSl58NVvVCjsB4UT27lBzFw3OHJf.4s2', 'buffonmustofa@gmail.com', '2016-05-20', '', 'eec4j4qta6U:APA91bEhd13z2Zxmy0NDJPPvv6sAa6hIbxOoVmo7TT5fk36Hsp6CVYCUKRswtPXVNsh_iS3fJkZzmnX3fFjJQXLc36tB8a-yEhmfAc2_NGfJB443XdmeH9xy2HMFq4TlWSNxtQNI27ms'),
(333, 1, 444, 1, '20091212121', 'Admin SF', 'Medokan semampir 28', '08877877787', '', 'adminsales', '$2y$10$sE7gzngH5URFXy8a6KKS8.koPX5a/SuvhTc2GZK2cB6G5Ky.BCCu.', 'adminsf@growth.co.id', '2016-07-18', 'XElJ3kVclme71DjJPDdlDTmaT4CMBOBcBqhbEYkF9FGhz7sPGpoFu5Wl3mIc', ''),
(334, 3, 444, 1, 'GR0000098', 'Jhon Doe', 'Ketintang madya 567', '0812818989098', '', 'Jdoe', '$2y$10$awx9Mvj3zMMPre63QOFLcuM0yF6gHhLFAjTbt6PPX.d/OvYt/XOP6', 'Jdoe@growth.co.id', '2016-05-11', 'GJwXc4QxbHWLuC1y5bc6EPZpKVl5oqEi7TfCx623MhumDh9L0MyecOsXAXzl', 'fHloV4fWeFs:APA91bHM8Z3OBRArg0QFL4ThXodsiZtr3GIxMcZoMTJP_PRXDxYsn6z9m--TDMtcp0CsLmdxeFhfLECpvlrr4KWp8iu_chOE2Z3OiIXTJHlhu3yLv3iGVZq_m38rX_HhHFhFkkf9KuGx'),
(331, 3, 444, 1, 'GR000004', 'Moris Broock', 'Jl. Krakatau gg 10', '08738747878374', '', 'Moris', '$2y$10$tYzw2FCV5Kck8pu/Wk9yE.ew3aZkXhg7j8FfgqP/vvT./oDmDwAEm', 'it@hisamitsu.co.id', '2016-06-01', 'O9zYNNnTrFgirsP29U9akAWIFncDlZwNePciRJB54CrbvsObo1zVOInJvq52', 'e-WkHoAwPOs:APA91bGfFJLCpG_pqxnv8J8jASFrqlNaonO_1pCcP9-w77I-Bzh4hTaJsjaKDfoIy6C3iDgHiAPGP-IPz_77Yj0xyPDPhCJBh6aVpQpZJv6_mZnmZVT1_mjCcKCgXmAtg9_1l3oCJohR'),
(332, 3, 444, 1, '20060325212', 'Debian Greg', 'somewhere', '02334948', '', 'debian', '$2y$10$w80bpp33cn3HfAoCO4FMw.gF7.8BFh/ZvJ6J0x2bFnN0f6pMASyeW', 'debian@growth.co.id', '2016-06-07', 'BA1utPyITNW30zd63X3qbmjebN7ewGoUykV80K3eBr9ZuyK5EiDonVxF2mac', 'cvpDgXpDhH0:APA91bFpJB2JZU9eFPDba6Rgrts3JuKAVqrM6V9951R-naRb_6BHicxJsz4PKpXJZ4eY0II5tkbiXtgjlGm822ldxIifIwHObfjt6ylLW9pu6lik427kbeZ_3reijaW_c3lOytb_fXAl'),
(338, 3, 0, 1, 'GR000001', 'Andi Mulyono', 'Jl. Medokan sawah Blok 6 C12', '08111111234', '', 'sales', '$2y$10$iTmZ4wfVO/MzyczfwN1jhOf666zAeF1Xb5DfIU6YDZj8l8nt9loR.', 'andi@growth.co.id', '2015-03-31', 'pIL8sbFcweuER5m5CZuzziuT3uOtocz9029ZJBKlmOyt9h49laKOipR1L1dk', 'ckJIoH_hpfE:APA91bGV0iJHk2fJY9JRKIrL0Lfi4NWIwrASNpM_cs_MnTK0K-D11gFFv1ltQcvqPlD5iXRlY0NZ_PXsV-PG80Z3WkoQlrmWzfqJIbZo-VwJ-qtYLlSr01XCk33te34Tt7-bwpaSOBhZ'),
(339, 3, 0, 1, '200077788', 'saitama', 'City Z', '0303939393', '', 'saitama', '$2y$10$AHh4iJgOGq/gx3u9ertoqutzcgVVOEuDBZHlElyMeeN7oQ/u0efNe', 'saitama@saitama.dev', '2017-04-03', 'PLp7fUKRWf6yYMdqZZbM2jJe8tUUwLQuP8pWqUPvKcVNe2bpji2MQUA9a14x', ''),
(340, 3, 0, 2, '001', 'mike', 'kahuripan barat 23', '082243777854', '', 'mike', '$2y$10$3yAETcMZ1rur0D/YpjDMp.S8GWsgYoc8PYwkPJYDAgbzEIVE0MlTC', 'mikemagic690@gmail.com', '2017-04-01', '', 'eAttw25aZqM:APA91bFrFzmITlT4YfQsIg10DTgNWgN-reRtTV0SUnCvUniLMxEJq3wupaB_B8L_7bp1286Af3aZbtfEVzCX7CqzR8qA-18xm0cpYNzj-gqeDzWXPje5Z79GG0925qNVmclZRMrVCcxa');

-- --------------------------------------------------------

--
-- Table structure for table `visit_plan`
--

CREATE TABLE `visit_plan` (
  `id` int(11) NOT NULL,
  `kd_outlet` int(11) DEFAULT NULL,
  `date_visit` timestamp NULL DEFAULT NULL,
  `date_create_visit` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `approve_visit` smallint(6) DEFAULT NULL,
  `status_visit` smallint(11) DEFAULT NULL,
  `date_visiting` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `skip_order_reason` varchar(100) DEFAULT NULL,
  `skip_reason` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `visit_plan`
--

INSERT INTO `visit_plan` (`id`, `kd_outlet`, `date_visit`, `date_create_visit`, `approve_visit`, `status_visit`, `date_visiting`, `skip_order_reason`, `skip_reason`) VALUES
(725, 968, '2016-11-05 14:00:00', '2016-11-11 08:17:03', 1, 1, '2016-11-04 11:18:00', NULL, ''),
(724, 968, '2016-11-07 14:00:00', '2016-11-01 01:00:00', 1, 1, '2016-11-07 04:12:21', NULL, NULL),
(723, 968, '2016-11-07 14:00:00', '2016-11-01 01:00:00', 1, 1, '2016-11-06 04:52:21', NULL, NULL),
(721, 969, '2016-11-05 14:00:00', '2016-11-01 01:00:00', 1, 1, '2016-11-04 01:32:19', NULL, NULL),
(717, 973, '2016-11-05 14:00:00', '2016-11-01 00:55:26', 1, 0, '0000-00-00 00:00:00', NULL, 'no reason'),
(715, 973, '2016-11-08 14:00:00', '2016-11-01 01:00:00', 1, 1, '2016-11-07 04:12:21', '', NULL),
(716, 973, '2016-11-05 14:00:00', '2016-11-01 01:00:00', 1, 1, '2016-11-04 04:17:02', '', NULL),
(713, 973, '2016-11-09 14:00:00', '2016-11-01 01:00:00', 1, 1, '2016-11-07 04:12:21', '', NULL),
(711, 972, '2016-11-05 14:00:00', '2016-11-01 01:00:00', 1, 1, '2016-11-03 05:26:19', '', NULL),
(712, 968, '2016-11-05 14:00:00', '2016-11-01 01:00:00', 1, 1, '2016-11-04 03:00:00', NULL, 'outletnya pindah'),
(710, 969, '2016-11-06 14:00:00', '2016-11-01 01:00:00', 1, 0, '0000-00-00 00:00:00', NULL, 'lagi males'),
(709, 971, '2016-11-05 14:00:00', '2016-11-01 01:00:00', 1, 1, '2016-11-03 04:50:33', '', NULL),
(722, 968, '2016-11-06 14:00:00', '2016-11-01 01:00:00', 1, 1, '2016-11-07 05:08:15', NULL, NULL),
(707, 969, '2016-11-05 14:00:00', '2016-11-01 01:00:00', 1, 1, '2016-11-04 02:00:00', NULL, ''),
(708, 970, '2016-11-05 14:00:00', '2016-11-01 01:00:00', 1, 0, '0000-00-00 00:00:00', NULL, 'lagi sakit'),
(726, 972, '2016-11-07 14:00:00', '2016-11-01 01:00:00', 2, 1, '2016-11-06 04:12:21', NULL, NULL),
(727, 978, '2016-11-05 14:00:00', '2016-11-11 08:17:03', 1, 1, '2016-11-04 11:18:00', NULL, ''),
(728, 972, '2016-11-15 05:28:24', '2016-11-14 05:28:29', 2, 0, '0000-00-00 00:00:00', NULL, NULL),
(729, 972, '2016-11-15 05:28:46', '2016-11-14 05:28:49', 1, 0, '0000-00-00 00:00:00', NULL, NULL),
(730, 980, '2016-11-18 05:44:17', '2016-11-14 05:44:21', 2, 0, '0000-00-00 00:00:00', NULL, NULL),
(731, 980, '2016-11-19 05:45:28', '2016-11-14 05:45:30', 1, 1, '2016-11-14 06:30:01', '', NULL),
(732, 980, '2016-11-18 05:45:28', '2016-11-15 05:45:30', 1, 0, '0000-00-00 00:00:00', NULL, NULL),
(733, 980, '2016-11-25 12:54:07', '2016-11-24 12:54:40', 2, 0, '0000-00-00 00:00:00', NULL, NULL),
(734, 980, '2016-11-25 12:55:14', '2016-11-24 12:55:19', 2, 0, '0000-00-00 00:00:00', NULL, NULL),
(735, 981, '2017-04-14 09:21:47', '2017-04-13 09:21:50', 2, 0, '0000-00-00 00:00:00', NULL, NULL),
(736, 981, '2017-04-14 09:22:31', '2017-04-13 09:22:33', 2, 0, '0000-00-00 00:00:00', NULL, NULL),
(737, 981, '2017-04-14 09:22:54', '2017-04-13 09:22:56', 2, 0, '0000-00-00 00:00:00', NULL, NULL),
(738, 981, '2017-04-14 09:23:15', '2017-04-13 09:23:18', 2, 0, '0000-00-00 00:00:00', NULL, NULL),
(739, 982, '2017-04-13 09:24:49', '2017-04-13 09:24:51', 1, 1, '2017-04-13 09:25:27', '', NULL),
(740, 982, '2017-04-14 09:25:05', '2017-04-13 09:25:07', 2, 0, '0000-00-00 00:00:00', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_menu_role`
--
ALTER TABLE `admin_menu_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `competitor`
--
ALTER TABLE `competitor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `distributor`
--
ALTER TABLE `distributor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `konfigurasi`
--
ALTER TABLE `konfigurasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logging`
--
ALTER TABLE `logging`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outlet`
--
ALTER TABLE `outlet`
  ADD PRIMARY KEY (`kd_outlet`),
  ADD UNIQUE KEY `kd_outlet` (`kd_outlet`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photo_activity`
--
ALTER TABLE `photo_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`kd_role`);

--
-- Indexes for table `take_order`
--
ALTER TABLE `take_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipe`
--
ALTER TABLE `tipe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `to_counter`
--
ALTER TABLE `to_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tp_photo`
--
ALTER TABLE `tp_photo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `visit_plan`
--
ALTER TABLE `visit_plan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `admin_menu_role`
--
ALTER TABLE `admin_menu_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1685;
--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=677;
--
-- AUTO_INCREMENT for table `competitor`
--
ALTER TABLE `competitor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `distributor`
--
ALTER TABLE `distributor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `konfigurasi`
--
ALTER TABLE `konfigurasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kota`
--
ALTER TABLE `kota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=515;
--
-- AUTO_INCREMENT for table `logging`
--
ALTER TABLE `logging`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4512;
--
-- AUTO_INCREMENT for table `outlet`
--
ALTER TABLE `outlet`
  MODIFY `kd_outlet` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=983;
--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10458;
--
-- AUTO_INCREMENT for table `photo_activity`
--
ALTER TABLE `photo_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `kd_role` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `take_order`
--
ALTER TABLE `take_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;
--
-- AUTO_INCREMENT for table `tipe`
--
ALTER TABLE `tipe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `to_counter`
--
ALTER TABLE `to_counter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tp_photo`
--
ALTER TABLE `tp_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=341;
--
-- AUTO_INCREMENT for table `visit_plan`
--
ALTER TABLE `visit_plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=741;
DELIMITER $$
--
-- Events
--
CREATE DEFINER=`growthco`@`localhost` EVENT `UPDATE_STATUS_VISIT` ON SCHEDULE EVERY 1 DAY STARTS '2016-07-03 00:00:00' ENDS '2021-01-06 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE sales.visit_plan 
SET status_visit = 0, skip_reason='Pass the date' 
WHERE DATE(date_visit) < DATE(NOW()) 
AND DATE(date_visiting) = '0000-00-00'$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
